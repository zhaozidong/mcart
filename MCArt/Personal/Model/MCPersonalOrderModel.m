//
//  MCPersonalOrderModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/5.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCPersonalOrderModel.h"

@implementation MCPersonalOrderGoodsModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"gid":@"gid",
             @"goid":@"goid",
             @"name":@"name",
             @"img":@"img",
             @"num":@"num",
             @"price":@"price",
             @"is_comment":@"is_comment"};
}
@end


@implementation MCPersonalOrderDetailModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"goid":@"goid",
             @"code":@"code",
             @"userid":@"userid",
             @"ordertime":@"ordertime",
             @"price":@"price",
             @"status":@"status",
             @"shipMethod":@"shipMethod",
             @"receiverName":@"receiverName",
             @"receiverAddress":@"receiverAddress",
             @"receiverCell":@"receiverCell",
             @"goodlist":@"goodlist"};
}

+ (NSValueTransformer *)shipMethodJSONTransformer{
    return [NSValueTransformer mtl_valueMappingTransformerWithDictionary:@{@"0":@(MCOrderExpressTypeSelfFetch),
                                                                           @"1":@(MCOrderExpressTypePost)}];
    
}

+ (NSValueTransformer *)statusJSONTransformer{
    return [NSValueTransformer mtl_valueMappingTransformerWithDictionary:@{@"0":@(MCOrderStatusNotPay),
                                                                           @"1":@(MCOrderStatusPaid),
                                                                           @"2":@(MCOrderStatusOverTime),
                                                                           @"3":@(MCOrderStatusFinished)}];
}

+ (NSValueTransformer *)goodlistJSONTransformer{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        NSArray *jsonArray = value;
        NSMutableArray *array = [NSMutableArray array];
        [jsonArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            MCPersonalOrderGoodsModel *goods = [MTLJSONAdapter modelOfClass:[MCPersonalOrderGoodsModel class] fromJSONDictionary:obj error:error];
            [array addObject:goods];
        }];
        return array;
    }];
}

@end


@implementation MCPersonalOrderModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"orderlist":@"orderlist"};
}

+ (NSValueTransformer *)orderlistJSONTransformer{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        NSArray *jsonArray = value;
        NSMutableArray *array = [NSMutableArray array];
        [jsonArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            MCPersonalOrderDetailModel *detail = [MTLJSONAdapter modelOfClass:[MCPersonalOrderDetailModel class] fromJSONDictionary:obj error:error];
            if (!error) {
                [array addObject:detail];
            }else{
                NSLog(@"返回数据不正确2！");
            }
        }];
        return array;
    }];
}



@end
