//
//  MCPersonalModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/4.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCPersonalModel.h"

@implementation MCPersonalModel

- (instancetype)initWithUserID:(NSString *)userID{
    self = [super init];
    if (self) {
        self.userid = userID;
    }
    return self;
}


+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"headimg" : @"headimg",
             @"username" : @"username",
             @"realname" : @"realname",
             @"gender" : @"gender",
             @"birthday" : @"birthday",
             @"cellphone" : @"cellphone",
             @"hobby" : @"hobby",
             @"receiverName":@"receiverName",
             @"receiverAddress":@"receiverAddress",
             @"receiverCell":@"receiverCell",
             @"userid" : @"userid"
             };
}


//- (void)setNilValueForKey:(NSString *)key {
//    if ([key isEqualToString:@"gender"]) {
//        [self setValue:@"男" forKey:key];
//    }else if ([key isEqualToString:@"birthday"]) {
//        [self setValue:@"1980-01-01" forKey:key];
//    }else if ([key isEqualToString:@"cellphone"]) {
//        [self setValue:@"" forKey:key];
//    }else if ([key isEqualToString:@"hobby"]) {
//        [self setValue:@"1111" forKey:key];
//    }else if ([key isEqualToString:@"realname"]) {
//        [self setValue:@"2222" forKey:key];
//    }
//}


@end




@implementation MCPersonalExpressModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"receiver":@"receiver"};
}

+ (NSValueTransformer *)receiverJSONTransformer{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        NSArray *jsonArray = value;
        NSMutableArray *array = [NSMutableArray arrayWithCapacity:jsonArray.count];
        [jsonArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            MCPersonalExpressDetailModel *detail = [MTLJSONAdapter modelOfClass:[MCPersonalExpressDetailModel class] fromJSONDictionary:obj error:error];
            [array addObject:detail];
        }];
        return array;
    }];
}
@end



@implementation MCPersonalExpressDetailModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"name":@"name",
             @"address":@"address",
             @"cell":@"cell"};
}
@end
