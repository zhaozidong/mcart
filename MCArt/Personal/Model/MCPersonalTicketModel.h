//
//  MCPersonalTicketModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/5.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Mantle/Mantle.h>
typedef NS_ENUM(NSInteger,MCTicketStatus) {
    MCTicketStatusAll = 1,
    MCTicketStatusNotPay,
    MCTicketStatusPaid
};


@interface MCPersonalTicketSeatModel : MTLModel<MTLJSONSerializing>

///观看区
@property (nonatomic, copy) NSString *vname;

///座位位置
@property (nonatomic, copy) NSString *sename;

///二维码
@property (nonatomic, copy) NSString *qrcode;

@end


@interface MCPersonalTicketShowModel : MTLModel<MTLJSONSerializing>

///演出名称
@property (nonatomic, copy) NSString *showname;

///演出时间
@property (nonatomic, copy) NSString *showdate;

///封面图片
@property (nonatomic, copy) NSString *img2;

///座位列表
@property (nonatomic, copy) NSArray *seatlist;

@end


@interface MCPersonalTicketModel : MTLModel<MTLJSONSerializing>

///订单主键
@property (nonatomic, copy) NSString *toid;

///订单号
@property (nonatomic, copy) NSString *code;

///用户主键
@property (nonatomic, copy) NSString *userid;

///下单时间
@property (nonatomic, copy) NSString *ordertime;

///状态 0:未支付 1:已支付
@property (nonatomic, assign) MCTicketStatus status;

///总价
@property (nonatomic, copy) NSString *price;

///票号
@property (nonatomic, copy) NSString *tonumber;

///密码
@property (nonatomic, copy) NSString *topassword;

///演出列表
@property (nonatomic, copy) NSArray *showlist;

@end
