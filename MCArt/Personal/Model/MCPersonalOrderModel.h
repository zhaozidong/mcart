//
//  MCPersonalOrderModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/5.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Mantle/Mantle.h>

typedef NS_ENUM(NSInteger,MCOrderStatus) {
    MCOrderStatusAll = 1,
    MCOrderStatusNotPay,
    MCOrderStatusPaid,
    MCOrderStatusOverTime,
    MCOrderStatusFinished
};

typedef NS_ENUM(NSInteger,MCOrderExpressType) {
    MCOrderExpressTypeSelfFetch,
    MCOrderExpressTypePost
};


@interface MCPersonalOrderGoodsModel : MTLModel<MTLJSONSerializing>

///商品主键
@property (nonatomic, copy) NSString *gid;

///订单主键
@property (nonatomic, copy) NSString *goid;

///商品名称
@property (nonatomic, copy) NSString *name;

///商品封面图片
@property (nonatomic, copy) NSString *img;

///购买数量
@property (nonatomic, copy) NSString *num;

///单价
@property (nonatomic, copy) NSString *price;

///是否可以评论  0：可以  1:不可以
@property (nonatomic, copy) NSString *is_comment;

@end



@interface MCPersonalOrderDetailModel : MTLModel<MTLJSONSerializing>

///订单主键
@property (nonatomic, copy) NSString *goid;

///订单编号
@property (nonatomic, copy) NSString *code;

///用户主键
@property (nonatomic, copy) NSString *userid;

///下单时间
@property (nonatomic, copy) NSString *ordertime;

///订单总价
@property (nonatomic, copy) NSString *price;

///订单状态 0：未支付  1：已支付  2：已过期  3:已完成
@property (nonatomic, assign) MCOrderStatus status;

///物流方式 0：自取 1：送货上门
@property (nonatomic, assign) MCOrderExpressType shipMethod;

///收货人姓名
@property (nonatomic, copy) NSString *receiverName;

///收货地址
@property (nonatomic, copy) NSString *receiverAddress;

///收货人手机号码
@property (nonatomic, copy) NSString *receiverCell;

///订单下商品
@property (nonatomic, copy) NSArray *goodlist;

@end



@interface MCPersonalOrderModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, copy) NSArray *orderlist;

@end
