//
//  MCPersonalModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/4.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "MCBaseModel.h"


@interface MCPersonalModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, copy) NSString *headimg;

@property (nonatomic, copy) NSString *username;

@property (nonatomic, copy) NSString *realname;

@property (nonatomic, copy) NSString *gender;

@property (nonatomic, copy) NSString *birthday;

@property (nonatomic, copy) NSString *cellphone;

@property (nonatomic, copy) NSString *hobby;

@property (nonatomic, copy) NSString *receiverName;

@property (nonatomic, copy) NSString *receiverAddress;

@property (nonatomic, copy) NSString *receiverCell;

@property (nonatomic, copy) NSString *userid;

@end



@interface  MCPersonalExpressModel: MCBaseModel

@property (nonatomic, copy) NSArray *receiver;

@end



@interface MCPersonalExpressDetailModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, copy) NSString *name;

@property (nonatomic, copy) NSString *address;

@property (nonatomic, copy) NSString *cell;

@end