//
//  MCPersonalTicketModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/5.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCPersonalTicketModel.h"

@implementation MCPersonalTicketSeatModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"vname":@"vname",
             @"sename":@"sename",
             @"qrcode":@"qrcode"};
}

@end


@implementation MCPersonalTicketShowModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"showname":@"showname",
             @"showdate":@"showdate",
             @"img2":@"img2",
             @"seatlist":@"seatlist"};
}

+ (NSValueTransformer *)seatlistJSONTransformer{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        NSArray *jsonArray = value;
        NSMutableArray *array = [NSMutableArray array];
        [jsonArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            MCPersonalTicketSeatModel *seat = [MTLJSONAdapter modelOfClass:[MCPersonalTicketSeatModel class] fromJSONDictionary:obj error:error];
            [array addObject:seat];
        }];
        return array;
    }];
}




@end


@implementation MCPersonalTicketModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"toid":@"toid",
             @"code":@"code",
             @"userid":@"userid",
             @"ordertime":@"ordertime",
             @"status":@"status",
             @"price":@"price",
             @"tonumber":@"tonumber",
             @"topassword":@"topassword",
             @"showlist":@"showlist"};
}

+ (NSValueTransformer *)statusJSONTransformer{
    return [MTLValueTransformer mtl_valueMappingTransformerWithDictionary:@{@"0":@(MCTicketStatusNotPay),
                                                                            @"1":@(MCTicketStatusPaid)}];
}


+ (NSValueTransformer *)showlistJSONTransformer{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        NSArray *jsonArray = value;
        NSMutableArray *array = [NSMutableArray array];
        [jsonArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            MCPersonalTicketShowModel *show = [MTLJSONAdapter modelOfClass:[MCPersonalTicketShowModel class] fromJSONDictionary:obj error:error];
            [array addObject:show];
        }];
        return array;
    }];
}

@end
