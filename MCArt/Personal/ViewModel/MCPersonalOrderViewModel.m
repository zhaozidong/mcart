//
//  MCPersonalOrderViewModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/5.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCPersonalOrderViewModel.h"
#import "MCNetworkManager.h"
#import "MCLoginViewModel.h"

@implementation MCPersonalOrderViewModel

SINGLETON_FOR_CLASS(MCPersonalOrderViewModel);

- (void)getPersonalGoodsList:(MCOrderStatus)status page:(NSInteger)page success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    NSDictionary *dict;
    switch (status) {
        case MCOrderStatusAll: {
            dict = @{@"userid":[MCLoginViewModel sharedInstance].userInfo.id,@"type":@(0),@"page":@(page),@"pageSize":MC_PAGE_SIZE};
            break;
        }
        case MCOrderStatusNotPay: {
            dict = @{@"userid":[MCLoginViewModel sharedInstance].userInfo.id,@"type":@(1),@"page":@(page),@"pageSize":MC_PAGE_SIZE};
            break;
        }
        case MCOrderStatusPaid: {
            dict = @{@"userid":[MCLoginViewModel sharedInstance].userInfo.id,@"type":@(2),@"page":@(page),@"pageSize":MC_PAGE_SIZE};
            break;
        }
        case MCOrderStatusOverTime: {
            dict = @{@"userid":[MCLoginViewModel sharedInstance].userInfo.id,@"type":@(3),@"page":@(page),@"pageSize":MC_PAGE_SIZE};
            break;
        }
        case MCOrderStatusFinished: {
            dict = @{@"userid":[MCLoginViewModel sharedInstance].userInfo.id,@"type":@(4),@"page":@(page),@"pageSize":MC_PAGE_SIZE};
            break;
        }
    }
    
    [[MCNetworkManager sharedInstance] postWithURL:@"appUser/getOrder" parameter:dict success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            NSError *err;
            NSArray *orderList = [MTLJSONAdapter modelsOfClass:[MCPersonalOrderDetailModel class] fromJSONArray:[resquestData objectForKey:@"orderlist"] error:&err];
            if (!err) {
                if (successBlock) {
                    successBlock(orderList);
                }
            }else{
                if (failureBlock) {
                    failureBlock(err);
                }
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}



///确认收货
- (void)confirmReceiptById:(NSString *)index success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    NSDictionary *dict = @{@"userid":[MCLoginViewModel sharedInstance].userInfo.id,@"goid":index};
    [[MCNetworkManager sharedInstance] postWithURL:@"appUser/confirmReceipt" parameter:dict success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            if (successBlock) {
                successBlock([resquestData objectForKey:@"info"]);
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}

///评价晒单
- (void)addCommit:(NSString *)orderId goodsId:(NSString *)goodsId comment:(NSString *)comment success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    NSDictionary *dict = @{@"userid":[MCLoginViewModel sharedInstance].userInfo.id,@"goid":orderId,@"gid":goodsId,@"content":comment};
    [[MCNetworkManager sharedInstance] postWithURL:@"appUser/addGoodComment" parameter:dict success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            if (successBlock) {
                successBlock([resquestData objectForKey:@"info"]);
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}



@end
