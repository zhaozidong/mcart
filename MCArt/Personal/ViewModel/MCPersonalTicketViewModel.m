//
//  MCPersonalTicketViewModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/5.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCPersonalTicketViewModel.h"
#import "MCNetworkManager.h"
#import "MCLoginViewModel.h"

@implementation MCPersonalTicketViewModel

SINGLETON_FOR_CLASS(MCPersonalTicketViewModel);

- (void)getTicketList:(MCTicketStatus)type page:(NSInteger)page success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    NSDictionary *dict;
    switch (type) {
        case MCTicketStatusAll: {
            dict = @{@"userid":[MCLoginViewModel sharedInstance].userInfo.id,@"type":@0,@"page":@(page),@"pageSize":MC_PAGE_SIZE};
            break;
        }
        case MCTicketStatusNotPay: {
            dict = @{@"userid":[MCLoginViewModel sharedInstance].userInfo.id,@"type":@1,@"page":@(page),@"pageSize":MC_PAGE_SIZE};
            break;
        }
        case MCTicketStatusPaid: {
            dict = @{@"userid":[MCLoginViewModel sharedInstance].userInfo.id,@"type":@2,@"page":@(page),@"pageSize":MC_PAGE_SIZE};
            break;
        }
    }
    
    [[MCNetworkManager sharedInstance] postWithURL:@"appUser/getTicket" parameter:dict success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            NSError *err;
            NSArray *array = [MTLJSONAdapter modelsOfClass:[MCPersonalTicketModel class] fromJSONArray:[resquestData objectForKey:@"ticketlist"] error:&err];
            if (!err) {
                if (successBlock) {
                    successBlock(array);
                }
            }else{
                if (failureBlock) {
                    failureBlock(err);
                }
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}




@end
