//
//  MCPersonalActivityViewModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/4/23.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCPersonalActivityViewModel.h"
#import "MCNetworkManager.h"
#import "MCActivityModel.h"
#import "MCLoginViewModel.h"

@implementation MCPersonalActivityViewModel

SINGLETON_FOR_CLASS(MCPersonalActivityViewModel);

- (void)getMyActivityList:(NSInteger)pageIndex success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    NSString *page = [NSString stringWithFormat:@"%ld",(long)pageIndex];
    NSDictionary *dict = @{@"page": page , @"pageSize" : MC_PAGE_SIZE , @"userId": [MCLoginViewModel sharedInstance].userInfo.id};
    [[MCNetworkManager sharedInstance] postWithURL:@"appActivity/getActivityList" parameter:dict success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            NSError *err=nil;
            NSArray *array=[MTLJSONAdapter modelsOfClass:[MCActivityModel class] fromJSONArray:[resquestData objectForKey:@"list"] error:&err];
            if (!err) {
                successBlock(array);
            }else{
                if (failureBlock) {
                    failureBlock(err);
                }
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}


@end
