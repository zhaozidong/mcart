//
//  MCPersonalOrderViewModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/5.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MCPersonalOrderModel.h"


@interface MCPersonalOrderViewModel : NSObject

+ (instancetype)sharedInstance;

///我的商品列表
- (void)getPersonalGoodsList:(MCOrderStatus)status page:(NSInteger)page success:(successBlock)successBlock failure:(failureBlock)failureBlock;

///确认收货
- (void)confirmReceiptById:(NSString *)index success:(successBlock)successBlock failure:(failureBlock)failureBlock;


///评价晒单
- (void)addCommit:(NSString *)orderId goodsId:(NSString *)goodsId comment:(NSString *)comment success:(successBlock)successBlock failure:(failureBlock)failureBlock;





@end
