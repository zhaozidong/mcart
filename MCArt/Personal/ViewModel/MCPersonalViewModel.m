//
//  MCPersonalViewModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/4.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCPersonalViewModel.h"
#import "MCNetworkManager.h"
#import "MCLoginViewModel.h"


@implementation MCPersonalViewModel

SINGLETON_FOR_CLASS(MCPersonalViewModel);

- (void)getUserInfobyID:(NSString *)userID success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    [[MCNetworkManager sharedInstance] postWithURL:@"appUser/getUser" parameter:@{@"userid" : userID} success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            NSError *err;
            MCPersonalModel *person = [MTLJSONAdapter modelOfClass:[MCPersonalModel class] fromJSONDictionary:resquestData error:&err];
            if (!err) {
                if (successBlock) {
                    successBlock(person);
                }
            }else{
                if (failureBlock) {
                    failureBlock(err);
                }
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}

- (void)updateUserInfo:(MCPersonalModel *)userInfo success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    NSDictionary *dict = @{@"headimg":userInfo.headimg ? userInfo.headimg : @"",
                           @"realname":userInfo.realname ? userInfo.realname : @"",
                           @"gender":[userInfo.gender isEqualToString:@"男"] ? @"1" : @"2",
                           @"birthday":userInfo.birthday ? userInfo.birthday : @"",
                           @"hobby":userInfo.hobby ? userInfo.hobby : @"",
                           @"receiverName":userInfo.receiverName ? userInfo.receiverName : @"",
                           @"receiverAddress":userInfo.receiverAddress ? userInfo.receiverAddress : @"",
                           @"receiverCell" :userInfo.receiverCell ? userInfo.receiverCell : @"",
                           @"userid":userInfo.userid
                           };
    
    [[MCNetworkManager sharedInstance] postWithURL:@"appUser/updateUser" parameter:dict success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            if (successBlock) {
                successBlock(resquestData);
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}

- (void)getExpressInfo:(successBlock)successBlock failure:(failureBlock)failureBlock{
    NSDictionary *dict = @{@"userid":[MCLoginViewModel sharedInstance].userInfo.id};
    [[MCNetworkManager sharedInstance] postWithURL:@"appGoods/getUsershipBySelf" parameter:dict success:^(id resquestData) {
        NSError *err;
        MCPersonalExpressModel *express = [MTLJSONAdapter modelOfClass:[MCPersonalExpressModel class] fromJSONDictionary:resquestData error:&err];
        if (!err) {
            if (successBlock) {
                successBlock(express);
            }
        }else{
            if (failureBlock) {
                failureBlock(err);
            }
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}



@end
