//
//  MCPersonalViewModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/4.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MCPersonalModel.h"

@interface MCPersonalViewModel : NSObject

+ (instancetype)sharedInstance;

- (void)getUserInfobyID:(NSString *)userID success:(successBlock)successBlock failure:(failureBlock)failureBlock;

- (void)updateUserInfo:(MCPersonalModel *)userInfo success:(successBlock)successBlock failure:(failureBlock)failureBlock;

//获取用户物流信息
- (void)getExpressInfo:(successBlock)successBlock failure:(failureBlock)failureBlock;



@end
