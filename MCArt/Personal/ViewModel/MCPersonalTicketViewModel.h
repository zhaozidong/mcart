//
//  MCPersonalTicketViewModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/5.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MCPersonalTicketModel.h"


@interface MCPersonalTicketViewModel : NSObject

+ (instancetype)sharedInstance;


- (void)getTicketList:(MCTicketStatus)type page:(NSInteger)page success:(successBlock)successBlock failure:(failureBlock)failureBlock;


@end
