//
//  MCPersonalActivityViewModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/4/23.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MCPersonalActivityViewModel : NSObject

+ (instancetype)sharedInstance;

///我的活动列表
- (void)getMyActivityList:(NSInteger)pageIndex success:(successBlock)successBlock failure:(failureBlock)failureBlock;




@end
