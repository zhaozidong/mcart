//
//  MCPersonalOrderExpressCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/4/16.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseTableViewCell.h"

@interface MCPersonalOrderExpressCell : MCBaseTableViewCell


- (void)updateWithTitle:(NSString *)title subTitle:(NSString *)subTitle;

@end
