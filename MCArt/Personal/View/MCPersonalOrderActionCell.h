//
//  MCPersonalOrderActionCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/16.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseTableViewCell.h"
#import "MCPersonalOrderModel.h"

@protocol MCPersonalOrderActionDelegate <NSObject>

- (void)clickedReceive:(MCPersonalOrderDetailModel *)info;

- (void)clickedPay:(MCPersonalOrderDetailModel *)info;

@end




@interface MCPersonalOrderActionCell : MCBaseTableViewCell

@property (nonatomic, weak) id<MCPersonalOrderActionDelegate> delegate;


- (void)updateWithDetail:(MCPersonalOrderDetailModel *)info;

+ (CGFloat)cellHeight;


@end
