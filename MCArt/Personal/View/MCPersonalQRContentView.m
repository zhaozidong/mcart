//
//  MCPersonalQRContentView.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/22.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCPersonalQRContentView.h"
#import "QRCodeGenerator.h"


@implementation MCPersonalQRContentView{
    UILabel *_lblName;
    UILabel *_lblAddress;
    UILabel *_lblTime;
    UIImageView *_imgQR;
    UIImageView *container;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}

- (void)setUpSubViews{
    
    container = [[UIImageView alloc] initWithFrame:self.bounds];
    container.image = [UIImage imageNamed:@"qrBackground"];
    [self addSubview:container];
    
    if (!_lblName) {
        _lblName = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, CGRectGetWidth(container.frame), 30)];
        _lblName.textAlignment = NSTextAlignmentCenter;
    }
    [container addSubview:_lblName];
    
    
    if (!_lblAddress) {
        _lblAddress = [[UILabel alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_lblName.frame)+4, CGRectGetWidth(_lblName.frame), 30)];
        _lblAddress.textAlignment = NSTextAlignmentCenter;
    }
    [container addSubview:_lblAddress];
    
    if (!_lblTime) {
        _lblTime = [[UILabel alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_lblAddress.frame)+4, CGRectGetWidth(_lblAddress.frame), 20)];
        _lblTime.textColor = [UIColor redColor];
        _lblTime.textAlignment = NSTextAlignmentCenter;
    }
    [container addSubview:_lblTime];
    
    
    if (!_imgQR) {
        _imgQR = [[UIImageView alloc] initWithFrame:CGRectMake((CGRectGetWidth(container.frame)-200)/2, CGRectGetMaxY(_lblTime.frame)+8, 200, 200)];
    }
    [container addSubview:_imgQR];
    
}


- (void)updateWithSeat:(MCPersonalTicketSeatModel *)seat showName:(NSString *)showName showTime:(NSString *)showTime{
    if (!seat) {
        return;
    }
    
    if (showName) {
        _lblName.text = showName;
        [_lblName sizeToFit];
    }
    
    if (seat.vname) {
        _lblAddress.text = [NSString stringWithFormat:@"%@ %@",seat.vname,seat.sename];
        [_lblAddress sizeToFit];
    }
    
    if (showTime) {
        _lblTime.text = showTime;
        [_lblTime sizeToFit];
    }
    
    if (seat.qrcode) {
        _imgQR.image = [QRCodeGenerator qrImageForString:seat.qrcode imageSize:200];
    }else{
        _imgQR.image = [QRCodeGenerator qrImageForString:@"http://www.mcysw.net/" imageSize:200];
    }
    
}


- (void)layoutSubviews{
    [super layoutSubviews];
    
    CGRect frame = _lblName.frame;
    _lblName.frame = CGRectMake(15, 10, frame.size.width, frame.size.height);
    
    frame = _lblAddress.frame;
    _lblAddress.frame = CGRectMake(15, CGRectGetMaxY(_lblName.frame)+4, frame.size.width, frame.size.height);
    
    frame = _lblTime.frame;
    _lblTime.frame = CGRectMake(15, CGRectGetMaxY(_lblAddress.frame)+4, frame.size.width, frame.size.height);
    
}

@end
