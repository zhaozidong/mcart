//
//  MCPersonalQRContentView.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/22.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCPersonalTicketModel.h"



@interface MCPersonalQRContentView : UIView


- (void)updateWithSeat:(MCPersonalTicketSeatModel *)seat showName:(NSString *)showName showTime:(NSString *)showTime;


@end
