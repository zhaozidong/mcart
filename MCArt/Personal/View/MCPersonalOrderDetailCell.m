//
//  MCPersonalOrderDetailCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/16.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCPersonalOrderDetailCell.h"
#import <UIImageView+WebCache.h>


@implementation MCPersonalOrderDetailCell{
    UIImageView *_imageView;
    UILabel *_lblTitle;
    
    UILabel *_lblPrice;
    UILabel *_lblNumber;
    
    UIButton *_btnBuy;
    UIButton *_btnCommit;
    
    MCPersonalOrderGoodsModel *_info;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setUpSubViews{
    if (!_imageView) {
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(8, 8, 94, 94)];
    }
    [self.contentView addSubview:_imageView];
    
    if (!_lblTitle) {
        _lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_imageView.frame)+8, 9, CGRectGetWidth(self.bounds)-CGRectGetMaxX(_imageView.frame)-2*8, 80)];
        _lblTitle.preferredMaxLayoutWidth = CGRectGetWidth(self.bounds)-CGRectGetMaxX(_imageView.frame)-2*8;
        _lblTitle.numberOfLines = 3;
    }
    [self.contentView addSubview:_lblTitle];
    
    if (!_lblPrice) {
        _lblPrice = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_imageView.frame)+8, CGRectGetMaxY(_lblTitle.frame), 50, 20)];
        _lblPrice.font = kAppFont(12.f);
    }
    [self.contentView addSubview:_lblPrice];
    
    if (!_lblNumber) {
        _lblNumber = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_lblPrice.frame)+8, CGRectGetMinY(_lblPrice.frame), 50, 20)];
        _lblNumber.font = kAppFont(12.f);
    }
    [self.contentView addSubview:_lblNumber];
    
    
    if (!_btnCommit) {
        _btnCommit = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 80, 30)];
        _btnCommit.layer.cornerRadius = 3.f;
        _btnCommit.layer.borderColor = [UIColor redColor].CGColor;
        _btnCommit.layer.borderWidth = 1.f;
        [_btnCommit.titleLabel setFont:kAppFont(14.f)];
        [_btnCommit setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [_btnCommit setTitle:@"晒单评价" forState:UIControlStateNormal];
        [_btnCommit addTarget:self action:@selector(btnDidClickCommit:) forControlEvents:UIControlEventTouchUpInside];
    }
    [self.contentView addSubview:_btnCommit];
    
    
    if (!_btnBuy) {
        _btnBuy = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 80, 30)];
        _btnBuy.layer.cornerRadius = 3.f;
        _btnBuy.layer.borderColor = [UIColor redColor].CGColor;
        _btnBuy.layer.borderWidth = 1.f;
        [_btnBuy.titleLabel setFont:kAppFont(14.f)];
        [_btnBuy setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [_btnBuy setTitle:@"再次购买" forState:UIControlStateNormal];
        [_btnBuy addTarget:self action:@selector(btnDidClickBuy:) forControlEvents:UIControlEventTouchUpInside];
    }
    [self.contentView addSubview:_btnBuy];
    
}


- (void)updateWithGoods:(MCPersonalOrderGoodsModel *)info{
    if (!info) {
        return;
    }
    
    _info = info;
    
    if (info.img) {
        [_imageView sd_setImageWithURL:[NSURL URLWithString:kImagePath(info.img)]];
        _lblTitle.text = info.name;
        [_lblTitle sizeToFit];
    }
    
    if (info.price) {
        _lblPrice.text = [NSString stringWithFormat:@"单价：￥%@",info.price];
        [_lblPrice sizeToFit];
    }
    
    if (info.num) {
        _lblNumber.text = [NSString stringWithFormat:@"数量：%@",info.num];
        [_lblNumber sizeToFit];
    }
    
    if (![info.is_comment integerValue]) {
        _btnCommit.hidden = NO;
    }else{
        _btnCommit.hidden = YES;
    }
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    _imageView.frame = CGRectMake(8, 8, 94, 94);
    
    CGRect frame = _lblTitle.frame;
    _lblTitle.frame = CGRectMake(CGRectGetMaxX(_imageView.frame)+8, 9, CGRectGetWidth(self.bounds)-CGRectGetMaxX(_imageView.frame)-2*8, frame.size.height);
    
    frame = _lblPrice.frame;
    _lblPrice.frame = CGRectMake(CGRectGetMaxX(_imageView.frame)+8, CGRectGetMaxY(_lblTitle.frame)+4, 100, frame.size.height);
    
    frame = _lblNumber.frame;
    _lblNumber.frame = CGRectMake(CGRectGetMaxX(_lblPrice.frame)+4, CGRectGetMinY(_lblPrice.frame), frame.size.width, frame.size.height);
    
    
    if (_btnCommit.hidden) {
        _btnBuy.frame = CGRectMake(CGRectGetWidth(self.bounds) - 80 -8, CGRectGetHeight(self.bounds)-8-30, 80, 30);
    }else{
        _btnCommit.frame = CGRectMake(CGRectGetWidth(self.bounds) - 80 -8, CGRectGetHeight(self.bounds)-8-30, 80, 30);
        _btnBuy.frame = CGRectMake(CGRectGetMinX(_btnCommit.frame) - 80 -8, CGRectGetMinY(_btnCommit.frame), 80, 30);
    }
}

+ (CGFloat)cellHeightWithGoods:(MCPersonalOrderGoodsModel *)goods{
    CGRect rect = [goods.name boundingRectWithSize:CGSizeMake(kMainFrameWidth-94-3*8, 200)
                                           options:NSStringDrawingUsesLineFragmentOrigin
                                        attributes:@{NSFontAttributeName:kAppFont(14.f)}
                                           context:nil];
    
    if (rect.size.height > 25) {
        return 110+25;
    }
    return 110.f;
}

- (void)btnDidClickCommit:(id)sender{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedCommit:)]) {
        [self.delegate clickedCommit:_info];
    }
}

- (void)btnDidClickBuy:(id)sender{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedBuy:)]) {
        [self.delegate clickedBuy:_info];
    }
}

@end
