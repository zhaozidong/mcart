//
//  MCPersonalTitleCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/1/25.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseTableViewCell.h"
#import "MCPersonalModel.h"


@interface MCPersonalTitleCell : MCBaseTableViewCell

- (void)updateWithInfo:(MCPersonalModel *)info;


+ (CGFloat)cellHeight;


@end
