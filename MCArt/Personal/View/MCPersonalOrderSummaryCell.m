//
//  MCPersonalOrderSummaryCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/16.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCPersonalOrderSummaryCell.h"

@implementation MCPersonalOrderSummaryCell{
    UILabel *_lblTitle;
    UILabel *_lblOrderNum;
    UILabel *_lblStatus;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setUpSubViews{
    if (!_lblTitle) {
        _lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(8, (CGRectGetHeight(self.bounds)-30)/2, 100, 30)];
        _lblTitle.textColor = [UIColor lightGrayColor];
        _lblTitle.text = @"订单编号:";
        [_lblTitle sizeToFit];
    }
    [self.contentView addSubview:_lblTitle];
    
    if (!_lblOrderNum) {
        _lblOrderNum = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_lblTitle.frame)+4, CGRectGetMinY(_lblTitle.frame), 100, 30)];
        _lblOrderNum.textColor = [UIColor lightGrayColor];
    }
    [self.contentView addSubview:_lblOrderNum];
    
    
    if (!_lblStatus) {
        _lblStatus = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.bounds)-8-60, CGRectGetMinY(_lblOrderNum.frame), 60, 30)];
        _lblStatus.textColor = [UIColor redColor];
    }
    [self.contentView addSubview:_lblStatus];
    
    
}

- (void)updateWithDetail:(MCPersonalOrderDetailModel *)info{
    if (!info) {
        return;
    }
    
    if (info.code) {
        _lblOrderNum.text = info.code;
        [_lblOrderNum sizeToFit];
    }
    
    if (info.status == MCOrderStatusNotPay) {
        _lblStatus.text = @"未支付";
    }else if(info.status == MCOrderStatusPaid) {
        _lblStatus.text =@"待收货";
    }else if (info.status == MCOrderStatusOverTime) {
        _lblStatus.text = @"已过期";
    }else if (info.status == MCOrderStatusFinished) {
        _lblStatus.text = @"已完成";
    }
    [_lblStatus sizeToFit];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    CGRect frame = _lblTitle.frame;
    _lblTitle.frame = CGRectMake(8, (CGRectGetHeight(self.bounds)-frame.size.height)/2, frame.size.width, frame.size.height);
    
    frame = _lblOrderNum.frame;
    _lblOrderNum.frame = CGRectMake(CGRectGetMaxX(_lblTitle.frame)+4, CGRectGetMinY(_lblTitle.frame), frame.size.width, frame.size.height);
    
    frame = _lblStatus.frame;
    _lblStatus.frame = CGRectMake(CGRectGetWidth(self.bounds)-8-frame.size.width, CGRectGetMinY(_lblOrderNum.frame), frame.size.width, frame.size.height);
    
}

@end
