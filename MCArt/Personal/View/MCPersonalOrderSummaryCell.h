//
//  MCPersonalOrderSummaryCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/16.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseTableViewCell.h"
#import "MCPersonalOrderModel.h"


@interface MCPersonalOrderSummaryCell : MCBaseTableViewCell

- (void)updateWithDetail:(MCPersonalOrderDetailModel *)info;


@end
