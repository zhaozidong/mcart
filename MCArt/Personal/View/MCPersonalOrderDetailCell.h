//
//  MCPersonalOrderDetailCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/16.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseTableViewCell.h"
#import "MCPersonalOrderModel.h"

@protocol MCPersonalOrderDetailDelegate <NSObject>

- (void)clickedBuy:(MCPersonalOrderGoodsModel *)info;

- (void)clickedCommit:(MCPersonalOrderGoodsModel *)info;

@end





@interface MCPersonalOrderDetailCell : MCBaseTableViewCell

@property (nonatomic, weak) id<MCPersonalOrderDetailDelegate> delegate;


- (void)updateWithGoods:(MCPersonalOrderGoodsModel *)info;

+ (CGFloat)cellHeightWithGoods:(MCPersonalOrderGoodsModel *)goods;

@end
