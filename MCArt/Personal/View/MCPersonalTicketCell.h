//
//  MCPersonalTicketCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/10.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseTableViewCell.h"
#import "MCPersonalTicketModel.h"



@protocol MCPersonalTicketCellDelegate <NSObject>

- (void)clickedTicket:(MCPersonalTicketModel *)ticket;

- (void)clickedPay:(MCPersonalTicketModel *)ticket;

@end




@interface MCPersonalTicketCell : MCBaseTableViewCell


@property (nonatomic, strong) id<MCPersonalTicketCellDelegate> delegate;


- (void)updateWithShow:(MCPersonalTicketModel *)show;


+ (CGFloat) cellHeightWithShow:(MCPersonalTicketModel *)show;

@end
