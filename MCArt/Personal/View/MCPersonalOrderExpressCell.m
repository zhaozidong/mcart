//
//  MCPersonalOrderExpressCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/4/16.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCPersonalOrderExpressCell.h"

@implementation MCPersonalOrderExpressCell{
    UILabel *_title;
    UILabel *_subTitle;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setUpSubViews{
    if (!_title) {
        _title = [[UILabel alloc] initWithFrame:CGRectMake(8, 10, 80, 30)];
    }
    
    [self.contentView addSubview:_title];
    
    if (!_subTitle) {
        _subTitle = [[UILabel alloc] initWithFrame:CGRectMake(100, 10, 150, 30)];
    }
    [self.contentView addSubview:_subTitle];
}

- (void)updateWithTitle:(NSString *)title subTitle:(NSString *)subTitle{
    
    if (title) {
        _title.text = title;
        [_title sizeToFit];
    }
    
    if (subTitle) {
        _subTitle.text = subTitle;
        [_subTitle sizeToFit];
    }
}


- (void)layoutSubviews{
    [super layoutSubviews];
    
    CGRect frame = _title.frame;
    _title.frame = CGRectMake(8, (CGRectGetHeight(self.bounds)-frame.size.height)/2, 90, frame.size.height);
    
    frame = _subTitle.frame;
    _subTitle.frame = CGRectMake(CGRectGetMaxX(_title.frame)+8, CGRectGetMinY(_title.frame), frame.size.width, frame.size.height);
}



@end
