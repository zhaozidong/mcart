//
//  MCPersonalOrderActionCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/16.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCPersonalOrderActionCell.h"

@implementation MCPersonalOrderActionCell{
    UILabel *_lblMoney;
    UIButton *_btnAction;
    MCPersonalOrderDetailModel *_info;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setUpSubViews{
    if (!_lblMoney) {
        _lblMoney = [[UILabel alloc] initWithFrame:CGRectMake(8, (CGRectGetHeight(self.bounds)-20)/2, 100, 20)];
    }
    [self.contentView addSubview:_lblMoney];
    
    if (!_btnAction) {
        _btnAction = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.bounds)-80-8, (CGRectGetHeight(self.bounds)- 30)/2, 80, 30)];
        _btnAction.layer.cornerRadius = 3.f;
        _btnAction.layer.borderColor = [UIColor redColor].CGColor;
        _btnAction.layer.borderWidth = 1.f;
        [_btnAction.titleLabel setFont:kAppFont(14.f)];
        [_btnAction setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [_btnAction addTarget:self action:@selector(btnDidClickAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    [self.contentView addSubview:_btnAction];
}


- (void)updateWithDetail:(MCPersonalOrderDetailModel *)info{
    if (!info) {
        return;
    }
    
    _info = info;
    
    if (info.price) {
        _lblMoney.text = [NSString stringWithFormat:@"实付款：￥%@",info.price];
        [_lblMoney sizeToFit];
    }
    
    _btnAction.hidden = NO;
    if (info.status == MCOrderStatusNotPay) {
        [_btnAction setTitle:@"去支付" forState:UIControlStateNormal];
    }else if (info.status == MCOrderStatusPaid) {
        [_btnAction setTitle:@"确认收货" forState:UIControlStateNormal];
    }else {
        _btnAction.hidden = YES;
    }
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    CGRect frame = _lblMoney.frame;
    _lblMoney.frame = CGRectMake(8, (CGRectGetHeight(self.bounds)-frame.size.height)/2, frame.size.width, frame.size.height);
    
    _btnAction.frame = CGRectMake(CGRectGetWidth(self.bounds)-80-8, (CGRectGetHeight(self.bounds)- 30)/2, 80, 30);
}

+ (CGFloat)cellHeight{
    return 50.f;
}

- (void)btnDidClickAction:(id)sender{
    if (_info.status == MCOrderStatusNotPay) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(clickedPay:)]) {
            [self.delegate clickedPay:_info];
        }
    }else if (_info.status == MCOrderStatusPaid) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(clickedReceive:)]) {
            [self.delegate clickedReceive:_info];
        }
    }
}

@end
