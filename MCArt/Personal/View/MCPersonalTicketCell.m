//
//  MCPersonalTicketCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/10.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCPersonalTicketCell.h"

static NSInteger row = 0;

@implementation MCPersonalTicketCell{
    UILabel *_lblTitle;
    UILabel *_lblPrice;
    UILabel *_lblArea;
    UILabel *_lblSeat;
    UILabel *_lblTime;
    UIButton *_btnAction;
    MCPersonalTicketModel *_info;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setUpSubViews{
    if (!_lblTitle) {
        _lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(8, 0, kMainFrameWidth, 40)];
        _lblTitle.font = kAppFont(18.f);
    }
    [self.contentView addSubview:_lblTitle];
    
    if (!_lblPrice) {
        _lblPrice = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 60, 30)];
        _lblPrice.textAlignment = NSTextAlignmentRight;
    }
    [self.contentView addSubview:_lblPrice];
    
    if (!_lblArea) {
        _lblArea = [[UILabel alloc] initWithFrame:CGRectMake(8, 30, 100, 30)];
        _lblArea.font = kAppFont(20.f);
        _lblArea.textColor = [UIColor orangeColor];
        
    }
    [self.contentView addSubview:_lblArea];
    
    
    if (!_lblSeat) {
        _lblSeat = [[UILabel alloc] initWithFrame:CGRectMake(16, 30, 200, 80)];
        _lblSeat.numberOfLines = 0;
        _lblSeat.preferredMaxLayoutWidth = CGRectGetWidth(self.bounds)-2*16;
        _lblSeat.font = kAppFont(14.f);
        _lblSeat.textColor = [UIColor orangeColor];
    }
    [self.contentView addSubview:_lblSeat];
    
    if (!_lblTime) {
        _lblTime = [[UILabel alloc] initWithFrame:CGRectMake(16, 100, 100, 20)];
        _lblTime.font = kAppFont(14.f);
    }
    [self.contentView addSubview:_lblTime];
    
    if (!_btnAction) {
        _btnAction = [[UIButton alloc] initWithFrame:CGRectMake(100, 50, 70, 30)];
        _btnAction.layer.cornerRadius = 3.f;
        _btnAction.layer.borderColor = [UIColor redColor].CGColor;
        _btnAction.layer.borderWidth = 1.f;
        [_btnAction setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [_btnAction addTarget:self action:@selector(btnDidClickAction:) forControlEvents:UIControlEventTouchUpInside];
        [_btnAction setTitle:@"去看看" forState:UIControlStateNormal];
    }
    [self.contentView addSubview:_btnAction];
}

- (void)updateWithShow:(MCPersonalTicketModel *)show{
    if (!show) {
        return;
    }
    _info = show;
    
    MCPersonalTicketShowModel *showDetail = [show.showlist objectAtIndex:0];
    
    if (showDetail.showname) {
        _lblTitle.text = showDetail.showname;
        [_lblTitle sizeToFit];
    }
    
    if (show.price) {
        _lblPrice.text = [NSString stringWithFormat:@"￥%@",show.price];
        [_lblPrice sizeToFit];
    }
    
    if (showDetail.seatlist.count > 0 ) {
        MCPersonalTicketSeatModel *seat = [showDetail.seatlist objectAtIndex:0];
        _lblArea.text =seat.vname;
        [_lblArea sizeToFit];
    }
    
    
    NSMutableString *string = [[NSMutableString alloc] init];
    for (MCPersonalTicketSeatModel *seat in showDetail.seatlist) {
        [string appendString:seat.sename];
        [string appendString:@"\n"];
    }
    [string deleteCharactersInRange:NSMakeRange(string.length-1, 1)];
    _lblSeat.text = string;
    [_lblSeat sizeToFit];
    
    if (show.ordertime) {
        _lblTime.text = show.ordertime;
        [_lblTime sizeToFit];
    }
    
    if (show.status == MCTicketStatusNotPay) {
        _btnAction.tag =2;
        [_btnAction setTitle:@"去支付" forState:UIControlStateNormal];
    }else if (show.status == MCTicketStatusPaid) {
        _btnAction.tag =3;
        [_btnAction setTitle:@"详情" forState:UIControlStateNormal];
    }
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    CGRect frame = _lblTitle.frame;
    _lblTitle.frame = CGRectMake(15, 8, kMainFrameWidth - 80 - 15 - 2*8, frame.size.height);
    
    frame = _lblPrice.frame;
    _lblPrice.frame = CGRectMake(CGRectGetWidth(self.bounds)-80 - 8, 8, 80, frame.size.height);
    
    frame = _lblArea.frame;
    _lblArea.frame = CGRectMake(16, CGRectGetMaxY(_lblTitle.frame)+8, frame.size.width, frame.size.height);
    
    frame = _lblSeat.frame;
    _lblSeat.frame = CGRectMake(16, CGRectGetMaxY(_lblArea.frame)+4, CGRectGetWidth(self.bounds)-2*16, frame.size.height);
    
    frame = _lblTime.frame;
    _lblTime.frame = CGRectMake(16, CGRectGetMaxY(_lblSeat.frame)+8, frame.size.width, frame.size.height);
    
    frame = _btnAction.frame;
    _btnAction.frame = CGRectMake(CGRectGetWidth(self.bounds)-frame.size.width - 8, CGRectGetHeight(self.bounds)-frame.size.height - 8, frame.size.width, frame.size.height);
    
}

+ (CGFloat) cellHeightWithShow:(MCPersonalTicketModel *)show{
    MCPersonalTicketShowModel *showDetail = [show.showlist objectAtIndex:0];
    row = showDetail.seatlist.count;
    return row*18 + 100;
}

- (void)btnDidClickAction:(UIButton *)sender{
    if (sender.tag == 2) {//去支付
        if (self.delegate && [self.delegate respondsToSelector:@selector(clickedPay:)]) {
            [self.delegate clickedPay:_info];
        }  
    }else if (sender.tag == 3) {//去看看
        if (self.delegate && [self.delegate respondsToSelector:@selector(clickedTicket:)]) {
            [self.delegate clickedTicket:_info];
        }
    }
}

@end
