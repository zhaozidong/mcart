//
//  MCPersonalTitleCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/25.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCPersonalTitleCell.h"

@implementation MCPersonalTitleCell{
    UIImageView *_bgImageView;
    UIImageView *_imgViewHead;
    
    UILabel *_lblName;
    UIImageView *_imgViewGender;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setUpSubViews{
    if (!_bgImageView) {
        _bgImageView =[[UIImageView alloc] initWithFrame:self.bounds];
        _bgImageView.image = [UIImage imageNamed:@"personalTitle"];
    }
    [self.contentView addSubview:_bgImageView];
    
    
    if (!_imgViewHead) {
        _imgViewHead = [[UIImageView alloc] initWithFrame:CGRectMake((CGRectGetWidth(self.bounds)-80)/2, (CGRectGetHeight(self.bounds)-80)/2, 80, 80)];
        _imgViewHead.layer.cornerRadius = 40.f;
        _imgViewHead.clipsToBounds = YES;
    }
    [self.contentView addSubview:_imgViewHead];
    
    if (!_lblName) {
        _lblName = [[UILabel alloc] init];
        _lblName.font = kAppFont(14.f);
    }
    [self.contentView addSubview:_lblName];
    
    if (!_imgViewGender) {
        _imgViewGender = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_lblName.frame), CGRectGetMaxY(_imgViewHead.frame), 20, 20)];
    }
    [self.contentView addSubview:_imgViewGender];
    
}

- (void)updateWithInfo:(MCPersonalModel *)info{
    if (!info) {
        return;
    }
    
    if (info.headimg) {
        [_imgViewHead sd_setImageWithURL:[NSURL URLWithString:kImagePath(info.headimg)]];
    }
    
    if (info.realname) {
        _lblName.text = info.realname;
        [_lblName sizeToFit];
    }
    
    if ([info.gender isEqualToString:@"男"]) {
        _imgViewGender.image = [UIImage imageNamed:@"personal_male"];
    }else{
        _imgViewGender.image = [UIImage imageNamed:@"personal_female"];
    }
    
}


- (void)layoutSubviews{
    [super layoutSubviews];
    _bgImageView.frame = self.bounds;
    _imgViewHead.frame = CGRectMake((CGRectGetWidth(self.bounds)-80)/2, (CGRectGetHeight(self.bounds)-80)/2, 80, 80);
    
    CGRect frame = _lblName.frame;
    _lblName.frame = CGRectMake((CGRectGetWidth(self.bounds)-frame.size.width)/2, CGRectGetMaxY(_imgViewHead.frame)+8, frame.size.width, frame.size.height);
    
    _imgViewGender.frame = CGRectMake(CGRectGetMaxX(_lblName.frame), CGRectGetMinY(_lblName.frame), 20, 20);
}

+ (CGFloat)cellHeight{
    return kMainFrameWidth * 0.66;
}



@end
