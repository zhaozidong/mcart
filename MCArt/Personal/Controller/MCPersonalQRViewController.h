//
//  MCPersonalQRViewController.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/11.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseViewController.h"
#import "MCPersonalTicketModel.h"


@interface MCPersonalQRViewController : MCBaseViewController

- (instancetype)initWithTicket:(MCPersonalTicketModel *)ticket;


@end
