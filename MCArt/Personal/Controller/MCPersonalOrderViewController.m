//
//  MCPersonalOrderViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/4.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCPersonalOrderViewController.h"
#import "MCPersonalOrderViewModel.h"
#import "MCPersonalOrderSummaryCell.h"
#import "MCPersonalOrderDetailCell.h"
#import "MCPersonalOrderActionCell.h"
#import "MCMallTitleArrowView.h"
#import "MLKMenuPopover.h"
#import "MCMallDetailViewController.h"
#import "MCPersonalCommitViewController.h"
#import "MCPersonalCommitViewController.h"
#import "MCMallOrderViewModel.h"
#import "MCMallOrderModel.h"
#import "MCMallPayViewController.h"
#import "MCPersonalOrderExpressCell.h"

static NSString *const summaryIdentifier = @"summaryCell";
static NSString *const detailIdentifier = @"detailCell";
static NSString *const actionIdentifier = @"actionCell";
static NSString *const expressIdentifier = @"expressCell";


@interface MCPersonalOrderViewController ()<MCPersonalOrderActionDelegate,MCPersonalOrderDetailDelegate,MCMallTitleArrowViewDelegate,MLKMenuPopoverDelegate>{
    __block NSInteger _pageIndex;
    __block NSMutableArray *_arrList;
    
    NSArray *_titleArray;
    MCMallTitleArrowView *_titleView;
    MLKMenuPopover *_popOver;
    
    MCOrderStatus _status;
}


@end

@implementation MCPersonalOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTitle:@"我的订单" withStyle:MCNavStyleBlack];
    
    _titleView = [[MCMallTitleArrowView alloc] initWithTitle:@"我的订单"];
    _titleView.delegate = self;
    self.navigationItem.titleView = _titleView;
    
    _pageIndex = 1;
    _arrList = [NSMutableArray array];
    _status = MCOrderStatusAll;
    _titleArray = @[@"我的订单",@"未支付",@"待收货",@"已过期",@"已完成"];
    
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth, CGFLOAT_MIN)];
    
    [self.tableView registerClass:[MCPersonalOrderSummaryCell class] forCellReuseIdentifier:summaryIdentifier];
    [self.tableView registerClass:[MCPersonalOrderDetailCell class] forCellReuseIdentifier:detailIdentifier];
    [self.tableView registerClass:[MCPersonalOrderActionCell class] forCellReuseIdentifier:actionIdentifier];
    [self.tableView registerClass:[MCPersonalOrderExpressCell class] forCellReuseIdentifier:expressIdentifier];
    
    [self getData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [_popOver removeFromSuperview];
}

- (void)back:(id)sender{
    if (self.presentingViewController) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark IWWebTitleArrowDelegate
- (void)clickTitle{
    if (!_popOver) {
        _popOver=[[MLKMenuPopover alloc] initWithFrame:CGRectMake((kMainFrameWidth-100)/2, 64, 100, _titleArray.count*44) menuItems:_titleArray imgItems:nil];
        _popOver.menuPopoverDelegate=self;
    }
    UIWindow *window = self.view.window;
    [_popOver showInView:window];
}

#pragma mark MLKMenuPopoverDelegate
- (void)menuPopover:(MLKMenuPopover *)menuPopover didSelectMenuItemAtIndex:(NSInteger)selectedIndex{
    _pageIndex =1;
    _titleView.title = [_titleArray objectAtIndex:selectedIndex];
    if (selectedIndex == 0){
        _status = MCOrderStatusAll;
    }else if (selectedIndex == 1) {
        _status = MCOrderStatusNotPay;
    }else if (selectedIndex == 2) {
        _status = MCOrderStatusPaid;
    }else if (selectedIndex == 3) {
        _status = MCOrderStatusOverTime;
    }else if (selectedIndex == 4) {
        _status = MCOrderStatusFinished;
    }
    [self getData];
}

- (void)getData{
    __weak typeof(self) weakSelf = self;
    [[MCPersonalOrderViewModel sharedInstance] getPersonalGoodsList:_status page:_pageIndex success:^(id resquestData) {
        if (_pageIndex == 1) {
            _arrList = [resquestData mutableCopy];
        }else{
            [_arrList addObjectsFromArray:resquestData];
        }
        _pageIndex++;
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}


#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _arrList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    MCPersonalOrderDetailModel *detail = [_arrList objectAtIndex:section];
    return detail.goodlist.count+6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MCPersonalOrderDetailModel *detail = [_arrList objectAtIndex:indexPath.section];
    if (indexPath.row == 0) {
        MCPersonalOrderSummaryCell *summaryCell = [tableView dequeueReusableCellWithIdentifier:summaryIdentifier];
        [summaryCell updateWithDetail:detail];
        return summaryCell;
    }else if (indexPath.row > 0 && indexPath.row <= detail.goodlist.count) {
        MCPersonalOrderGoodsModel *goods = [detail.goodlist objectAtIndex:indexPath.row - 1];
        MCPersonalOrderDetailCell *detailCell = [tableView dequeueReusableCellWithIdentifier:detailIdentifier];
        detailCell.delegate = self;
        [detailCell updateWithGoods:goods];
        return detailCell;
    }else if (indexPath.row == detail.goodlist.count+1) {
        MCPersonalOrderActionCell *actionCell = [tableView dequeueReusableCellWithIdentifier:actionIdentifier];
        actionCell.delegate = self;
        [actionCell updateWithDetail:detail];
        return actionCell;
    }else if (indexPath.row == detail.goodlist.count+2) {
        MCPersonalOrderExpressCell *expressCell = [tableView dequeueReusableCellWithIdentifier:expressIdentifier];
        if (detail.shipMethod == MCOrderExpressTypeSelfFetch) {
            [expressCell updateWithTitle:@"物流方式：" subTitle:@"自取"];
        }else{
            [expressCell updateWithTitle:@"物流方式：" subTitle:@""];
        }
        return expressCell;
    }else if (indexPath.row == detail.goodlist.count+3) {
        MCPersonalOrderExpressCell *expressCell = [tableView dequeueReusableCellWithIdentifier:expressIdentifier];
        [expressCell updateWithTitle:@"收货人：" subTitle:detail.receiverName];
        return expressCell;
    }else if (indexPath.row == detail.goodlist.count+4) {
        MCPersonalOrderExpressCell *expressCell = [tableView dequeueReusableCellWithIdentifier:expressIdentifier];
        [expressCell updateWithTitle:@"收货地址：" subTitle:detail.receiverAddress];
        return expressCell;
    }else if (indexPath.row == detail.goodlist.count+5) {
        MCPersonalOrderExpressCell *expressCell = [tableView dequeueReusableCellWithIdentifier:expressIdentifier];
        [expressCell updateWithTitle:@"收货电话：" subTitle:detail.receiverCell];
        return expressCell;
    }
    return nil;
}

- (CGFloat )tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    MCPersonalOrderDetailModel *detail = [_arrList objectAtIndex:indexPath.section];
    if (indexPath.row > 0 && indexPath.row <= detail.goodlist.count) {
        MCPersonalOrderGoodsModel *goods = [detail.goodlist objectAtIndex:indexPath.row - 1];
        return [MCPersonalOrderDetailCell cellHeightWithGoods:goods];
    }else if (indexPath.row > detail.goodlist.count) {
        return [MCPersonalOrderActionCell cellHeight];
    }
    return 44.f;
}

#pragma mark MCPersonalOrderActionDelegate
//TODO:确认收货
- (void)clickedReceive:(MCPersonalOrderDetailModel *)info{
    __weak typeof(self) weakSelf = self;
    [[MCPersonalOrderViewModel sharedInstance] confirmReceiptById:info.goid success:^(id resquestData) {
        [MCMessageUtilities showSuccessMessage:resquestData];
        _pageIndex = 1;
        [weakSelf getData];
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}

//TODO:去支付
- (void)clickedPay:(MCPersonalOrderDetailModel *)info{
    __weak typeof(self) weakSelf = self;
    [[MCMallOrderViewModel sharedInstance] getOrderResult:info.goid success:^(id resquestData) {
        MCMallOrderModel *_order = resquestData;
        [weakSelf payOrder:_order];
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}

- (void)payOrder:(MCMallOrderModel *)order{
    MCMallPayViewController *pay = [[MCMallPayViewController alloc] initWithOrderInfo:order];
    [self.navigationController pushViewController:pay animated:YES];
}

#pragma mrak MCPersonalOrderDetailDelegate
//TODO:再次购买
- (void)clickedBuy:(MCPersonalOrderGoodsModel *)info{
    MCMallDetailViewController *detail = [[MCMallDetailViewController alloc] initWithId:info.gid seckiling:NO];
    [self.navigationController pushViewController:detail animated:YES];
}

//TODO:评价晒单
- (void)clickedCommit:(MCPersonalOrderGoodsModel *)info{
    MCPersonalCommitViewController *commit = [[MCPersonalCommitViewController alloc] initWIthGoods:info];
    [self.navigationController pushViewController:commit animated:YES];
}


- (void)beginRefreshHeader:(MJRefreshHeader *)header{
    [header endRefreshing];
    _pageIndex = 1;
    [self getData];
    
}

- (void)beginRefreshFooter:(MJRefreshFooter *)footer{
    [footer endRefreshing];
    [self getData];
}


@end
