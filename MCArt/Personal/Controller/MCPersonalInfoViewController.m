//
//  MCPersonalInfoViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/4.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCPersonalInfoViewController.h"
#import "MCStyleButton.h"
#import "MCPersonalViewModel.h"
#import "MCLoginViewModel.h"
#import "NSString+Utilities.h"
#import "MCPersonalModel.h"
#import "SimplePickerInputTableViewCell.h"
#import "DateInputTableViewCell.h"
#import "StringInputTableViewCell.h"
#import <DateTools.h>
#import "MCPhotoSelectCell.h"


static NSString *const basicIdentifier = @"basicCell";
static NSString *const stringIdentifier = @"stringCell";
static NSString *const simpleIdentifier = @"simpleCell";
static NSString *const dateIdentifier = @"dateCell";
static NSString *const photoIdentifier = @"photoCell";

@interface MCPersonalInfoViewController ()<StringInputTableViewCellDelegate,DateInputTableViewCellDelegate,SimplePickerInputTableViewCellDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,MCPhotoSelectCellDelegate>{
    __block MCPersonalModel *_personalInfo;
    __block MCPersonalExpressModel *_express;
    
}


@end

@implementation MCPersonalInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"个人资料" withStyle:MCNavStyleBlack];
    
    MCStyleButton *button = [[MCStyleButton alloc] initWithFrame:CGRectMake(0, 0, 45, 25)];
    [button setTitle:@"保存" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(btnDidClickSave) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:basicIdentifier];
    
    [self getUserInfo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (void)getUserInfo{
    NSString *userID = [MCLoginViewModel sharedInstance].userInfo.id;
    __weak typeof(self) weakSelf = self;
    [[MCPersonalViewModel sharedInstance] getUserInfobyID:userID success:^(id resquestData) {
        _personalInfo = resquestData;
//        [weakSelf.tableView reloadData];
        [weakSelf getExpressInfo];
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}

- (void)getExpressInfo{
    __weak typeof(self) weakSelf = self;
    [[MCPersonalViewModel sharedInstance] getExpressInfo:^(id resquestData) {
        _express = resquestData;
        if (!_express.response) {
            MCPersonalExpressDetailModel *detail = [_express.receiver objectAtIndex:0];
            _personalInfo.receiverName = detail.name;
            _personalInfo.receiverAddress = detail.address;
            _personalInfo.receiverCell = detail.cell;
            [weakSelf.tableView reloadData];
        }else{
            [MCMessageUtilities showErrorMessage:_express.info];
        }
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}

- (void)btnDidClickSave{
    if (!_personalInfo.birthday) {
        [MCMessageUtilities showTotast:@"生日不正确。"];
        return;
    }
    
    if (!_personalInfo.realname) {
        [MCMessageUtilities showTotast:@"姓名不正确。"];
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    [[MCPersonalViewModel sharedInstance] updateUserInfo:_personalInfo success:^(id resquestData) {
        [MCMessageUtilities showSuccessMessage:@"已更新"];
        [weakSelf updateUserInfo];
        [weakSelf.navigationController popViewControllerAnimated:YES];
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}

- (void)updateUserInfo{
    [MCLoginViewModel sharedInstance].userInfo.realname = _personalInfo.username;
    [MCLoginViewModel sharedInstance].userInfo.gender = _personalInfo.gender;
    [MCLoginViewModel sharedInstance].userInfo.headimg = _personalInfo.headimg;
    [MCLoginViewModel sharedInstance].userInfo.cellphone = _personalInfo.cellphone;

}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return _personalInfo ? 6 : 0;
    }
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            MCPhotoSelectCell *photoCell = [[MCPhotoSelectCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:photoIdentifier];
            photoCell.delegate = self;
            photoCell.parentViewContrller = self;
            photoCell.textLabel.text = @"头像";
            if (_personalInfo.headimg) {
                photoCell.imgPath = _personalInfo.headimg;
            }
            return photoCell;
        }else if (indexPath.row == 1) {
            StringInputTableViewCell *stringCell = [[StringInputTableViewCell alloc] init];
            stringCell.delegate = self;
            stringCell.cellType = StringInputCellTypeName;
            stringCell.textLabel.text = @"真实姓名";
            stringCell.stringValue = _personalInfo.realname;
            stringCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            return stringCell;
        }else if (indexPath.row == 2) {
            SimplePickerInputTableViewCell *simpleCell = [[SimplePickerInputTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:simpleIdentifier];
            simpleCell.delegate = self;
            simpleCell.textLabel.text = @"性别";
            simpleCell.values = @[@"男",@"女"];
            simpleCell.value = _personalInfo.gender;
            simpleCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            return simpleCell;
        }else if (indexPath.row == 3) {
            DateInputTableViewCell *dateCell = [[DateInputTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:dateIdentifier];
            dateCell.delegate = self;
            dateCell.textLabel.text = @"出生年月";
            dateCell.datePickerMode = UIDatePickerModeDate;
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy-MM-dd"];
            dateCell.dateValue =[formatter dateFromString:_personalInfo.birthday ? _personalInfo.birthday : @"1970-01-01"];
            dateCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            return dateCell;
        }else if (indexPath.row == 4) {
            UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:baseCell];
            cell.textLabel.text = @"手机号码";
            cell.detailTextLabel.text = _personalInfo.cellphone;
            NSLog(@"cellphone = %@",_personalInfo.cellphone);
            
            return cell;
        }else if (indexPath.row == 5) {
            StringInputTableViewCell *stringCell = [[StringInputTableViewCell alloc] init];
            stringCell.delegate = self;
            stringCell.cellType = StringInputCellTypeInterest;
            stringCell.textLabel.text = @"个人爱好";
            stringCell.stringValue = _personalInfo.hobby;
            stringCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            return stringCell;
        }
    }else{
        MCPersonalExpressDetailModel *detail = [_express.receiver objectAtIndex:0];
        if (indexPath.row == 0) {
            StringInputTableViewCell *stringCell = [[StringInputTableViewCell alloc] init];
            stringCell.delegate = self;
            stringCell.cellType = StringInputCellTypeReceiverName;
            stringCell.textLabel.text = @"收货人";
            stringCell.stringValue = detail.name;
            stringCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            return stringCell;
        }else if (indexPath.row == 1) {
            StringInputTableViewCell *stringCell = [[StringInputTableViewCell alloc] init];
            stringCell.delegate = self;
            stringCell.cellType = StringInputCellTypeReceiverAddress;
            stringCell.textLabel.text = @"收货地址";
            stringCell.stringValue = detail.address;
            stringCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            return stringCell;
        }else if (indexPath.row == 2) {
            StringInputTableViewCell *stringCell = [[StringInputTableViewCell alloc] init];
            stringCell.delegate = self;
            stringCell.cellType = StringInputCellTypeReceiverPhone;
            stringCell.textLabel.text = @"收货电话";
            stringCell.stringValue = detail.cell;
            stringCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            return stringCell;
        }
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 && indexPath.row == 0) {
        return 100.f;
    }
    return 50.f;
}

#pragma mark UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark MCPhotoSelectCellDelegate
- (void)finishedImageWithPath:(NSString *)imagePath{
    _personalInfo.headimg = imagePath;
}

#pragma mark StringInputTableViewCellDelegate
- (void)tableViewCell:(StringInputTableViewCell *)cell didEndEditingWithString:(NSString *)value{
    switch (cell.cellType) {
        case StringInputCellTypeName: {
            _personalInfo.realname = value;
            break;
        }
        case StringInputCellTypeInterest: {
            _personalInfo.hobby = value;
            break;
        }
        case StringInputCellTypeSkill: {

            break;
        }
        case StringInputCellTypePhone: {
//            _personalInfo.cellphone = value;
            break;
        }
        case StringInputCellTypeReceiverName: {
            _personalInfo.receiverName = value;
            break;
        }
        case StringInputCellTypeReceiverAddress: {
            _personalInfo.receiverAddress = value;
            break;
        }
        case StringInputCellTypeReceiverPhone: {
            _personalInfo.receiverCell = value;
            break;
        }
    }
}

#pragma mark SimplePickerInputTableViewCellDelegate
- (void)tableViewCell:(SimplePickerInputTableViewCell *)cell didEndEditingWithValue:(NSString *)value{
    _personalInfo.gender = value;
}

#pragma mark DateInputTableViewCellDelegate
- (void)tableViewCell:(DateInputTableViewCell *)cell didEndEditingWithDate:(NSDate *)value{
    _personalInfo.birthday = [value formattedDateWithFormat:@"yyyy-MM-dd"];
}


@end
