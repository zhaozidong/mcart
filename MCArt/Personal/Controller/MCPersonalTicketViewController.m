//
//  MCPersonalTicketViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/4.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCPersonalTicketViewController.h"
#import "MCPersonalTicketViewModel.h"
#import "MCPersonalTicketCell.h"
#import "MCPersonalQRViewController.h"
#import "MCTicketOrderViewModel.h"
#import "MCTicketOrderModel.h"
#import "MCTicketPayViewController.h"
#import "MCMallTitleArrowView.h"
#import "MLKMenuPopover.h"



static NSString *const ticketIdentifier = @"ticketCell";

@interface MCPersonalTicketViewController ()<MCPersonalTicketCellDelegate,MCMallTitleArrowViewDelegate,MLKMenuPopoverDelegate>{
    __block NSInteger _pageIndex;
    __block NSMutableArray *_arrList;
    
    NSArray *_titleArray;
    MCMallTitleArrowView *_titleView;
    MLKMenuPopover *_popOver;
    
    MCTicketStatus _status;
}

@end

@implementation MCPersonalTicketViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTitle:@"我的购票" withStyle:MCNavStyleBlack];
    
    _titleView = [[MCMallTitleArrowView alloc] initWithTitle:@"我的购票"];
    _titleView.delegate = self;
    self.navigationItem.titleView = _titleView;
    
    
    _pageIndex = 1;
    _arrList = [NSMutableArray array];
    _titleArray = @[@"全部",@"未支付",@"已支付"];
    _status = MCTicketStatusAll;
    
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth, CGFLOAT_MIN)];
    
    [self.tableView registerClass:[MCPersonalTicketCell class] forCellReuseIdentifier:ticketIdentifier];
    
    [self getData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [_popOver removeFromSuperview];
}

- (void)back:(id)sender{
    if (self.presentingViewController) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark IWWebTitleArrowDelegate
- (void)clickTitle{
    if (!_popOver) {
        _popOver=[[MLKMenuPopover alloc] initWithFrame:CGRectMake((kMainFrameWidth-100)/2, 64, 100, _titleArray.count*44) menuItems:_titleArray imgItems:nil];
        _popOver.menuPopoverDelegate=self;
    }
    UIWindow *window = self.view.window;
    [_popOver showInView:window];
}

#pragma mark MLKMenuPopoverDelegate
- (void)menuPopover:(MLKMenuPopover *)menuPopover didSelectMenuItemAtIndex:(NSInteger)selectedIndex{
    _pageIndex =1;
    _titleView.title = [_titleArray objectAtIndex:selectedIndex];
    if (selectedIndex == 0){
        _status = MCTicketStatusAll;
    }else if (selectedIndex == 1) {
        _status = MCTicketStatusNotPay;
    }else if (selectedIndex == 2) {
        _status = MCTicketStatusPaid;
    }
    [self getData];
}


- (void)getData{
    __weak typeof(self) weakSelf = self;
    [[MCPersonalTicketViewModel sharedInstance] getTicketList:_status page:_pageIndex success:^(id resquestData) {
        if (_pageIndex == 1) {
            _arrList = [resquestData mutableCopy];
        }else{
            [_arrList addObjectsFromArray:resquestData];
        }
        _pageIndex++;
        
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}


#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _arrList.count;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MCPersonalTicketModel *ticket = [_arrList objectAtIndex:indexPath.section];
    MCPersonalTicketCell *ticketCell = [tableView dequeueReusableCellWithIdentifier:ticketIdentifier];
    ticketCell.delegate = self;
    [ticketCell updateWithShow:ticket];
    return ticketCell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    MCPersonalTicketModel *ticket = [_arrList objectAtIndex:indexPath.section];
    return [MCPersonalTicketCell cellHeightWithShow:ticket];
}



- (void)beginRefreshHeader:(MJRefreshHeader *)header{
    [header endRefreshing];
    _pageIndex =1;
    [self getData];
}

- (void)beginRefreshFooter:(MJRefreshFooter *)footer{
    [footer endRefreshing];
    [self getData];
}

#pragma mark MCPersonalTicketCellDelegate
- (void)clickedTicket:(MCPersonalTicketModel *)ticket{
    MCPersonalQRViewController *qr = [[MCPersonalQRViewController alloc] initWithTicket:ticket];
    [self.navigationController pushViewController:qr animated:YES];
}

- (void)clickedPay:(MCPersonalTicketModel *)ticket{
    MCTicketPayViewController *pay = [[MCTicketPayViewController alloc] initWithOrderId:ticket.toid];
    pay.showTicketList = NO;
    [self.navigationController pushViewController:pay animated:YES];
    
//    __weak typeof(self) weakSelf = self;
//    [[MCTicketOrderViewModel sharedInstance] getTicktPayResult:ticket.toid success:^(id resquestData) {
//        MCTicketOrderModel *order = resquestData;
//        [weakSelf payTicket:order];
//    } failure:^(NSError *error) {
//        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
//    }];
}

//- (void)payTicket:(MCTicketOrderModel *)order{
//    MCTicketPayViewController *pay = [[MCTicketPayViewController alloc] initWithPayInfo:order];
//    [self.navigationController pushViewController:pay animated:YES];
//}

@end
