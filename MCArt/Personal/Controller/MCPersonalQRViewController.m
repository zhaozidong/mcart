//
//  MCPersonalQRViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/11.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCPersonalQRViewController.h"
#import "UIImage+MDQRCode.h"
#import "MCPersonalQRContentView.h"



@interface MCPersonalQRViewController ()<UIScrollViewDelegate>{
    MCPersonalTicketModel *_ticket;
}
@property (nonatomic, retain) UIButton *btnTicketNo;

@property (nonatomic, retain) UIButton *btnPassword;

@property (nonatomic, strong) UIImageView *arrowRight;

@property (nonatomic, retain) UIScrollView *scrollView;

@property (nonatomic, retain) UIPageControl *pageControl;

@end

@implementation MCPersonalQRViewController

- (instancetype)initWithTicket:(MCPersonalTicketModel *)ticket{
    self = [super init];
    if (self) {
        _ticket = ticket;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTitle:@"我的票号" withStyle:MCNavStyleBlack];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    
    [self.view addSubview:self.scrollView];
    
    
    [self setUpViews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(20, 40, kMainFrameWidth-40, 300)];
        _scrollView.pagingEnabled = YES;
        _scrollView.delegate = self;
    }
    return _scrollView;
}

- (UIPageControl *)pageControl{
    if (!_pageControl) {
        _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(50, CGRectGetMaxY(self.scrollView.frame)+4, kMainFrameWidth-100, 15)];
        _pageControl.pageIndicatorTintColor = kRGBA(0, 0, 0, 0.54);
    }
    return _pageControl;
}


- (UIButton *)btnTicketNo{
    if (!_btnTicketNo) {
        _btnTicketNo = [[UIButton alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(self.scrollView.frame)+30, kMainFrameWidth - 40 , 40)];
        [_btnTicketNo setBackgroundColor:[UIColor redColor]];
        _btnTicketNo.layer.cornerRadius = 3.f;
        _btnTicketNo.titleLabel.font = kAppFont(13.f);
    }
    return _btnTicketNo;
}

- (UIButton *)btnPassword{
    if (!_btnPassword) {
        _btnPassword = [[UIButton alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(self.btnTicketNo.frame)+8, kMainFrameWidth-40, 40)];
        [_btnPassword setBackgroundColor:[UIColor redColor]];
        _btnPassword.layer.cornerRadius = 3.f;
        _btnPassword.titleLabel.font = kAppFont(13.f);
    }
    return _btnPassword;
}


- (UIImageView *)arrowRight{
    if (!_arrowRight) {
        _arrowRight = [[UIImageView alloc] initWithFrame:CGRectMake(kMainFrameWidth-8-25, 120, 25, 25)];
        _arrowRight.image = [UIImage imageNamed:@"arrowRight"];
    }
    return _arrowRight;
}


- (void)setUpViews{
    MCPersonalTicketShowModel *show = [_ticket.showlist objectAtIndex:0];
    
    CGFloat width = CGRectGetWidth(self.scrollView.frame);
    CGFloat height = CGRectGetHeight(self.scrollView.frame);
    self.scrollView.contentSize = CGSizeMake(show.seatlist.count*width, height);
    
    for (int i = 0; i < show.seatlist.count; i++) {
        MCPersonalTicketSeatModel *seat = [show.seatlist objectAtIndex:i];
        MCPersonalQRContentView *qrView = [[MCPersonalQRContentView alloc] initWithFrame:CGRectMake(i*width, 0, width, height)];
        [qrView updateWithSeat:seat showName:show.showname showTime:show.showdate];
        [self.scrollView addSubview:qrView];
    }
    
    if (show.seatlist.count > 0) {
        self.pageControl.numberOfPages = show.seatlist.count;
        [self.view addSubview:self.pageControl];
    }
    
    
    [self.btnTicketNo setTitle:[NSString stringWithFormat:@"取票号：%@",_ticket.tonumber] forState:UIControlStateNormal];
    [self.view addSubview:self.btnTicketNo];
    
    [self.btnPassword setTitle:[NSString stringWithFormat:@"取票密码：%@",_ticket.topassword] forState:UIControlStateNormal];
    [self.view addSubview:self.btnPassword];

}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGPoint point = scrollView.contentOffset;
    CGFloat width = self.scrollView.frame.size.width;
    NSInteger page = point.x/width;
    self.pageControl.currentPage = page;
}

@end
