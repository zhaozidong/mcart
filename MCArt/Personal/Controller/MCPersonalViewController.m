//
//  MCPersonalViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/25.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCPersonalViewController.h"
#import "MCPersonalTitleCell.h"
#import "MCPersonalInfoViewController.h"
#import "MCPersonalOrderViewController.h"
#import "MCPersonalTicketViewController.h"
#import "MCLoginViewModel.h"
#import "NSString+Utilities.h"
#import "MCPersonalActivityViewController.h"
#import "MCPersonalViewModel.h"
#import "MCPersonalModel.h"
#import "MCBaseWebViewController.h"

NSString *const titleIdentifier = @"titleCell";

@interface MCPersonalViewController (){
    MCPersonalModel *_personalInfo;
}

@property (nonatomic, retain) UIView *bottomView;

@end

@implementation MCPersonalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.edgesForExtendedLayout=UIRectEdgeAll;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    
    [self setTitle:@"个人中心" withStyle:MCNavStyleBlack];
    
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth, CGFLOAT_MIN)];
    
    if ([MCLoginViewModel sharedInstance].isLogin) {
        self.tableView.tableFooterView = self.bottomView;
    }
    
    [self.tableView registerClass:[MCPersonalTitleCell class] forCellReuseIdentifier:titleIdentifier];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    
    [self getData];
}



- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
    if (IOS7) {
        self.navigationController.navigationBar.backIndicatorImage = nil;
    }
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (UIView *)bottomView{
    if (!_bottomView) {
        _bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth, 80)];
        UIButton *btnLogout = [[UIButton alloc] initWithFrame:CGRectMake(24, 20, kMainFrameWidth-48, 40)];
        [btnLogout setBackgroundColor:[UIColor redColor]];
        btnLogout.layer.cornerRadius = 3.f;
        [btnLogout setTitle:@"退出登录" forState:UIControlStateNormal];
        [btnLogout addTarget:self action:@selector(btnDidClickLogout:) forControlEvents:UIControlEventTouchUpInside];
        [_bottomView addSubview:btnLogout];
    }
    return _bottomView;
}

- (void)btnDidClickLogout:(UIButton *)sender{
    [[MCLoginViewModel sharedInstance] logout];
    self.tableView.tableFooterView = nil;
//    [self getData];
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (void)getData{
    if ([MCLoginViewModel sharedInstance].isLogin) {
        NSString *userID = [MCLoginViewModel sharedInstance].userInfo.id;
        __weak typeof(self) weakSelf = self;
        [[MCPersonalViewModel sharedInstance] getUserInfobyID:userID success:^(id resquestData) {
            _personalInfo = resquestData;
            [weakSelf.tableView reloadData];
        } failure:^(NSError *error) {
            [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
        }];
    }
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 6;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        MCPersonalTitleCell *cell = [tableView dequeueReusableCellWithIdentifier:titleIdentifier forIndexPath:indexPath];
        if ([MCLoginViewModel sharedInstance].isLogin) {
            [cell updateWithInfo:_personalInfo];
        }
        return cell;
    }else if (indexPath.section == 1){
        UITableViewCell *basicCell = [tableView dequeueReusableCellWithIdentifier:baseCell];
        basicCell.textLabel.text = @"个人资料";
        basicCell.imageView.image = [UIImage imageNamed:@"personalCenter_Info"];
        basicCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return basicCell;
    }else if (indexPath.section == 2){
        UITableViewCell *basicCell = [tableView dequeueReusableCellWithIdentifier:baseCell];
        basicCell.textLabel.text = @"我的订单";
        basicCell.imageView.image = [UIImage imageNamed:@"personalCenter_Order"];
        basicCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return basicCell;
    }else if (indexPath.section == 3){
        UITableViewCell *basicCell = [tableView dequeueReusableCellWithIdentifier:baseCell];
        basicCell.textLabel.text = @"我的购票";
        basicCell.imageView.image = [UIImage imageNamed:@"personalCenter_Ticket"];
        basicCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return basicCell;
    }else if (indexPath.section == 4){
        UITableViewCell *basicCell = [tableView dequeueReusableCellWithIdentifier:baseCell];
        basicCell.textLabel.text = @"我的活动";
        basicCell.imageView.image = [UIImage imageNamed:@"personalCenter_Activity"];
        basicCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return basicCell;
    }else if (indexPath.section == 5){
        UITableViewCell *basicCell = [tableView dequeueReusableCellWithIdentifier:baseCell];
        basicCell.textLabel.text = @"关于我们";
        basicCell.imageView.image = [UIImage imageNamed:@"aboutUS"];
        basicCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return basicCell;
    }
    return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return [MCPersonalTitleCell cellHeight];
    }
    return 44.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return CGFLOAT_MIN;
    }
    return 10.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (![MCLoginViewModel sharedInstance].isLogin) {
        [self showLoginViewController];
        return;
    }
    
    if (indexPath.section == 1){
        MCPersonalInfoViewController *info = [[MCPersonalInfoViewController alloc] initWithStyle:UITableViewStyleGrouped];
        [self.navigationController pushViewController:info animated:YES];
    }else if (indexPath.section == 2){
        MCPersonalOrderViewController *order = [[MCPersonalOrderViewController alloc] initWithStyle:UITableViewStyleGrouped refreshType:MCTableViewRefreshTypeTop | MCTableViewRefreshTypeBottom];
        [self.navigationController pushViewController:order animated:YES];
    }else if (indexPath.section == 3){
        MCPersonalTicketViewController *ticket = [[MCPersonalTicketViewController alloc] initWithStyle:UITableViewStyleGrouped refreshType:MCTableViewRefreshTypeTop | MCTableViewRefreshTypeBottom];
        [self.navigationController pushViewController:ticket animated:YES];
    }else if (indexPath.section == 4) {
        MCPersonalActivityViewController *activity = [[MCPersonalActivityViewController alloc] init];
        [self.navigationController pushViewController:activity animated:YES];
    }else if (indexPath.section == 5) {
        MCBaseWebViewController *web = [[MCBaseWebViewController alloc] init];
        web.url = [NSString stringWithFormat:@"%@/%@",kAppHost,@"appNews/aboutUs"];
        [self.navigationController pushViewController:web animated:YES];
    }
}


@end
