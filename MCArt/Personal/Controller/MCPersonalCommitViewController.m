//
//  MCPersonalCommitViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/12.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCPersonalCommitViewController.h"
#import "MCStyleButton.h"
#import "MCPersonalOrderViewModel.h"


@interface MCPersonalCommitViewController (){
    MCPersonalOrderGoodsModel *_goods;
}


@property (nonatomic, retain) UITextView *txtCommit;

@property (nonatomic, retain) MCStyleButton *postButton;


@end

@implementation MCPersonalCommitViewController

- (instancetype)initWIthGoods:(MCPersonalOrderGoodsModel *)goods{
    self = [super init];
    if (self) {
        _goods = goods;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTitle:@"晒单评价" withStyle:MCNavStyleBlack];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.view.backgroundColor= [UIColor groupTableViewBackgroundColor];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.postButton];
    
    [self.view addSubview:self.txtCommit];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (UITextView *)txtCommit{
    if (!_txtCommit) {
        _txtCommit = [[UITextView alloc] initWithFrame:CGRectMake(8, 16, kMainFrameWidth - 16, 250)];
    }
    return _txtCommit;
}


- (MCStyleButton *)postButton{
    if (!_postButton) {
        _postButton = [[MCStyleButton alloc] initWithFrame:CGRectMake(0, 0, 40, 25)];
        [_postButton setTitle:@"提交" forState:UIControlStateNormal];
        [_postButton addTarget:self action:@selector(btnDidClickPost:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _postButton;
}

- (void)btnDidClickPost:(UIButton *)sender{
    __weak typeof(self) weakSelf = self;
    [[MCPersonalOrderViewModel sharedInstance] addCommit:_goods.goid goodsId:_goods.gid comment:self.txtCommit.text success:^(id resquestData) {
        [MCMessageUtilities showSuccessMessage:resquestData];
        [weakSelf.navigationController popViewControllerAnimated:YES];
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}


@end
