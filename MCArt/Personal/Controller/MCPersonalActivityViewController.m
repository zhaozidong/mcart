//
//  MCPersonalActivityViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/10.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCPersonalActivityViewController.h"
#import "MCActivityCell.h"
#import "MCActivityDetailViewController.h"
#import "MCPersonalActivityViewModel.h"

static NSString *const activityIdentifier = @"activityCell";


@interface MCPersonalActivityViewController (){
    __block NSInteger _pageIndex;
    __block NSMutableArray *_arrData;
}

@end

//活动首页
@implementation MCPersonalActivityViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTitle:@"艺术活动" withStyle:MCNavStyleBlack];
    
    _arrData = [NSMutableArray array];
    
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth, CGFLOAT_MIN)];
    
    [self.tableView registerClass:[MCActivityCell class] forCellReuseIdentifier:activityIdentifier];
    
    [self getData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (void)getData{
    _pageIndex = 1;
    __weak typeof(self) weakSelf = self;
    [[MCPersonalActivityViewModel sharedInstance] getMyActivityList:_pageIndex success:^(id resquestData) {
        _pageIndex ++;
        _arrData = [resquestData mutableCopy];
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _arrData.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MCActivityModel *acti = [_arrData objectAtIndex:indexPath.section];
    MCActivityCell *activityCell = [tableView dequeueReusableCellWithIdentifier:activityIdentifier];
    [activityCell updateWithInfo:acti];
    return activityCell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [MCActivityCell cellHeight];
}


#pragma mark UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    MCActivityModel *acti = [_arrData objectAtIndex:indexPath.section];
    MCActivityDetailViewController *detail = [[MCActivityDetailViewController alloc] initWithId:acti.id];
    [self.navigationController pushViewController:detail animated:YES];
}


#pragma mark MJRefresh

- (void)beginRefreshHeader:(MJRefreshHeader *)header{
    [header endRefreshing];
    [self getData];
}

- (void)beginRefreshFooter:(MJRefreshFooter *)footer{
    [footer endRefreshing];
    __weak typeof(self) weakSelf = self;
    [[MCPersonalActivityViewModel sharedInstance] getMyActivityList:_pageIndex success:^(id resquestData) {
        _pageIndex ++;
        [_arrData addObjectsFromArray:resquestData];
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error) {
        
    }];
}

@end
