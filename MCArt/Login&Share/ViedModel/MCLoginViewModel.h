//
//  MCLoginViewModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/3.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MCLoginModel.h"


@interface MCLoginViewModel : NSObject

@property (nonatomic, strong, readonly) MCLoginModel *userInfo;

@property (nonatomic, assign, readonly) BOOL isLogin;


+ (instancetype)sharedInstance;

- (void)logout;

- (void)registerWithPhone:(NSString *)phoneNum password:(NSString *)passwd verify:(NSString *)verifyCode success:(successBlock)successBlock failure:(failureBlock)failureBlock;

- (void)getVerifyCodeByPhone:(NSString *)phoneNum success:(successBlock)successBlock failure:(failureBlock)failureBlock;

- (void)loginWithPhone:(NSString *)phoneNum password:(NSString *)passwd success:(successBlock)successBlock failure:(failureBlock)failureBlock;

@end
