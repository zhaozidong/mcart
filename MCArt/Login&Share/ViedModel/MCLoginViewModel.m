//
//  MCLoginViewModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/3.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCLoginViewModel.h"
#import "MCNetworkManager.h"
#import "MCLoginModel.h"
#import "MCUserDefaultUtils.h"


@implementation MCLoginViewModel


SINGLETON_FOR_CLASS(MCLoginViewModel);

- (MCLoginModel *)userInfo{
    if ([MCUserDefaultUtils valueWithKey:@"login"]) {
        NSData *data = [MCUserDefaultUtils valueWithKey:@"login"];
        MCLoginModel *login = [NSKeyedUnarchiver unarchiveObjectWithData:data];
        return login;
    }
    return nil;
}

- (BOOL)isLogin{
    if ([MCUserDefaultUtils valueWithKey:@"login"]) {
        return YES;
    }
    return NO;
}

- (void)logout{
    [MCUserDefaultUtils removeValueForKey:@"login"];
}


- (void)registerWithPhone:(NSString *)phoneNum password:(NSString *)passwd verify:(NSString *)verifyCode success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    NSDictionary *dict = @{@"password" : passwd , @"cellphone" : phoneNum, @"checkcode" : verifyCode};
    [[MCNetworkManager sharedInstance] postWithURL:@"appUser/regist" parameter:dict success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            if (successBlock) {
                successBlock([resquestData objectForKey:@"userid"]);
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}

- (void)getVerifyCodeByPhone:(NSString *)phoneNum success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    NSDictionary *dict = @{@"cellphone" : phoneNum};
    [[MCNetworkManager sharedInstance] postWithURL:@"appUser/getPhoneCode" parameter:dict success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            if (successBlock) {
                successBlock([resquestData objectForKey:@"content"]);
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}

- (void)loginWithPhone:(NSString *)phoneNum password:(NSString *)passwd success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    NSDictionary *dict = @{@"account":phoneNum , @"password": passwd};
    [[MCNetworkManager sharedInstance] postWithURL:@"appUser/login" parameter:dict success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            NSError *err;
            MCLoginModel *loginUserInfo = [MTLJSONAdapter modelOfClass:[MCLoginModel class] fromJSONDictionary:resquestData error:&err];
            if (!err) {
                //保存登录信息
                [MCUserDefaultUtils saveValue:[NSKeyedArchiver archivedDataWithRootObject:loginUserInfo] forKey:@"login"];
                
                if (successBlock) {
                    successBlock(loginUserInfo);
                }
            }else{
                if (failureBlock) {
                    failureBlock(err);
                }
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}



@end
