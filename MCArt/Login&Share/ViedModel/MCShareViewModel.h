//
//  MCShareViewModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/11.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ShareSDK/ShareSDK.h>

//SSDKPlatformSubTypeWechatSession
//SSDKPlatformSubTypeWechatTimeline
//SSDKPlatformSubTypeQQFriend
//SSDKPlatformSubTypeQZone
//SSDKPlatformTypeSinaWeibo


@interface MCShareViewModel : NSObject

+ (void)registerShareSDK;

+ (void)shareWithType:(SSDKPlatformType)type
              content:(NSString *)content
                image:(NSString *)image
                  url:(NSString *)url
                title:(NSString *)title;
@end
