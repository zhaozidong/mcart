//
//  MCShareViewModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/11.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCShareViewModel.h"

#import <ShareSDKConnector/ShareSDKConnector.h>


//腾讯开放平台（对应QQ和QQ空间）SDK头文件
#import <TencentOpenAPI/TencentOAuth.h>
#import <TencentOpenAPI/QQApiInterface.h>

//微信SDK头文件
#import "WXApi.h"

//新浪微博SDK头文件
#import "WeiboSDK.h"

@implementation MCShareViewModel

+ (void)registerShareSDK {
    [ShareSDK registerApp:@"104acd31e4b44"
          activePlatforms:@[
                            @(SSDKPlatformTypeSinaWeibo),
                            @(SSDKPlatformTypeWechat),
                            @(SSDKPlatformTypeQQ)]
                 onImport:^(SSDKPlatformType platformType)
     {
         switch (platformType)
         {
             case SSDKPlatformTypeWechat:
                 [ShareSDKConnector connectWeChat:[WXApi class]];
                 break;
             case SSDKPlatformTypeQQ:
                 [ShareSDKConnector connectQQ:[QQApiInterface class] tencentOAuthClass:[TencentOAuth class]];
                 break;
             case SSDKPlatformTypeSinaWeibo:
                 [ShareSDKConnector connectWeibo:[WeiboSDK class]];
                 break;
             default:
                 break;
         }
     }
          onConfiguration:^(SSDKPlatformType platformType, NSMutableDictionary *appInfo)
     {
         switch (platformType)
         {
             case SSDKPlatformTypeSinaWeibo:
                 //设置新浪微博应用信息,其中authType设置为使用SSO＋Web形式授权
                 [appInfo SSDKSetupSinaWeiboByAppKey:@"1435762360"
                                           appSecret:@"8e1869ca6a11fbfa0810fe8c71fd69e5"
                                         redirectUri:@"http://www.mcysw.net/"
                                            authType:SSDKAuthTypeBoth];
                 break;
             case SSDKPlatformTypeWechat:
                 [appInfo SSDKSetupWeChatByAppId:@"wx26e99fd129ddd5da"
                                       appSecret:@"0b09d69f1f5f28c61fa56aafdb32de52"];
                 break;
             case SSDKPlatformTypeQQ:
                 [appInfo SSDKSetupQQByAppId:@"1105216446"
                                      appKey:@"ftcqnQM1vagIP09l"
                                    authType:SSDKAuthTypeBoth];
                 break;
             default:
                 break;
         }
     }];
}


+ (void)shareWithType:(SSDKPlatformType)type content:(NSString *)content image:(NSString *)image url:(NSString *)url title:(NSString *)title{
    NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
    [shareParams SSDKEnableUseClientShare];
    [shareParams SSDKSetupShareParamsByText:content images:image url:[NSURL URLWithString:url] title:title type:SSDKContentTypeWebPage];
    [ShareSDK share:type parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
        
        switch (state) {
            case SSDKResponseStateBegin: {
                NSLog(@"开始分享");
                break;
            }
            case SSDKResponseStateSuccess: {
                NSLog(@"分享成功");
                break;
            }
            case SSDKResponseStateFail: {
                NSLog(@"分享失败 %@",error);
                break;
            }
            case SSDKResponseStateCancel: {
                NSLog(@"取消分享");
                break;
            }
        }
    }];
}



@end
