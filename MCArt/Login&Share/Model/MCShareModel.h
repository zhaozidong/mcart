//
//  MCShareModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/6/2.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface MCShareModel : MTLModel

@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *image;

@property (nonatomic, copy) NSString *url;

@property (nonatomic, copy) NSString *content;


@end
