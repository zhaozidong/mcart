//
//  MCLoginModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/4.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface MCLoginModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, strong) NSString *id;

@property (nonatomic, strong) NSString *username;

@property (nonatomic, strong) NSString *headimg;

@property (nonatomic, strong) NSString *cellphone;

@property (nonatomic, strong) NSString *realname;

@property (nonatomic, strong) NSString *gender;

@property (nonatomic, strong) NSString *address;


@end
