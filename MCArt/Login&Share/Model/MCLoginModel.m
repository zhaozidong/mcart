//
//  MCLoginModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/4.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCLoginModel.h"

@implementation MCLoginModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"id" : @"id",
             @"username" : @"username",
             @"headimg" : @"headimg",
             @"cellphone" : @"cellphone",
             @"realname" : @"realname",
             @"gender" : @"gender",
             @"address" : @"address"
             };
}




@end
