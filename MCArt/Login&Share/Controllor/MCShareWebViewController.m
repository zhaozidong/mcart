//
//  MCShareWebViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/6/2.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCShareWebViewController.h"
#import "MCShareView.h"


@interface MCShareWebViewController () {
    UIButton *_shareButton;
    NSString *_strURL;
    MCShareModel *_shareInfo;
}

@end



@implementation MCShareWebViewController

- (instancetype)initWithUrl:(NSString *)url shareInfo:(MCShareModel *)info {
    self = [super init];
    if (self) {
        _strURL = url;
        _shareInfo = info;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithCustomView:self.shareButton];
    self.navigationItem.rightBarButtonItem = right;
    
    [self updateWithURL:_strURL];
}


- (UIButton *)shareButton{
    if (!_shareButton) {
        _shareButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 45, 45)];
        [_shareButton setImage:[UIImage imageNamed:@"titleShare_white"] forState:UIControlStateNormal];
        [_shareButton addTarget:self action:@selector(btnDidClickedShare:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _shareButton;
}

- (void)btnDidClickedShare:(id)sender{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    MCShareView *share = [[MCShareView alloc] initWithFrame:self.view.bounds];
    //    share.delegate = self;
    [share updateWithTitle:_shareInfo.title image:_shareInfo.image url:_shareInfo.url content:_shareInfo.content];
    [window addSubview:share];
}


@end
