//
//  RegisterViewController.h
//  MCArt
//
//  Created by Derek on 15/12/21.
//  Copyright © 2015年 zzd. All rights reserved.
//

#import "MCBaseViewController.h"
typedef NS_ENUM(NSInteger,MCRegisterType) {
    MCRegisterTypeRegister,
    MCRegisterTypeForgot
};


@interface RegisterViewController : MCBaseViewController

@property (nonatomic, assign) MCRegisterType type;


@end
