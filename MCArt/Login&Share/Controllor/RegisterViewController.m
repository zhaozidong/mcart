//
//  RegisterViewController.m
//  MCArt
//
//  Created by Derek on 15/12/21.
//  Copyright © 2015年 zzd. All rights reserved.
//

#import "RegisterViewController.h"
#import "MCLoginViewModel.h"
#import "NSString+Utilities.h"

@interface RegisterViewController ()<UIAlertViewDelegate>{
    NSTimer *_timer;
    NSInteger _timeCount;
}
@property (weak, nonatomic) IBOutlet UITextField *txtPhone;
@property (weak, nonatomic) IBOutlet UITextField *txtVerify;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirm;
@property (weak, nonatomic) IBOutlet UIButton *btnVerifyCode;
@property (weak, nonatomic) IBOutlet UIButton *btnRegister;
@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if (self.type == MCRegisterTypeRegister) {
        [self setTitle:@"注册" withStyle:MCNavStyleWhite];
        [self.btnRegister setTitle:@"注册" forState:UIControlStateNormal];
    }else if (self.type == MCRegisterTypeForgot) {
        [self setTitle:@"忘记密码" withStyle:MCNavStyleWhite];
        [self.btnRegister setTitle:@"找回密码" forState:UIControlStateNormal];
    }
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    if (_timer) {
        [_timer invalidate];
        _timer = nil;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self resignFirstResponder];
    [self.txtPhone resignFirstResponder];
    [self.txtVerify resignFirstResponder];
    [self.txtPassword resignFirstResponder];
    [self.txtConfirm resignFirstResponder];
}


- (IBAction)clickedVerify:(id)sender {
    if ([self.txtPhone.text isEqualToString:@""] || !self.txtPhone.text) {
        [MCMessageUtilities showErrorMessage:@"手机号码不能为空"];
        return;
    }
    [self.txtVerify becomeFirstResponder];
    
    if (self.txtPhone.text.length != 11) {
        [MCMessageUtilities showErrorMessage:@"手机号码格式不正确"];
        return;
    }
    
    [self.btnVerifyCode setBackgroundColor:kRGB(34, 97, 00)];
    self.btnVerifyCode.enabled = YES;
    _timer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(timeFire) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
    _timeCount = 60;
    [[MCLoginViewModel sharedInstance] getVerifyCodeByPhone:self.txtPhone.text success:^(id resquestData) {
        [MCMessageUtilities showSuccessMessage:@"验证码发送成功"];
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}

- (IBAction)clickedRegister:(id)sender {
    if ([NSString isNullOrEmpty:self.txtPassword.text]) {
        [MCMessageUtilities showErrorMessage:@"密码不能为空"];
        return;
    }
    if ([NSString isNullOrEmpty:self.txtVerify.text]) {
        [MCMessageUtilities showErrorMessage:@"验证码不能为空"];
        return;
    }
    
    if ( ![self.txtPassword.text isEqualToString:self.txtConfirm.text]) {
//        NSString *password = self.txtPassword.text;
//        NSString *confirm = self.txtConfirm.text;
//        NSString *res = [NSString stringWithFormat:@"密码1：%@，密码2：%@",password,confirm];
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:res delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
//        [alert show];
        [MCMessageUtilities showErrorMessage:@"两次输入的密码不一致"];
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    [[MCLoginViewModel sharedInstance] registerWithPhone:self.txtPhone.text password:self.txtPassword.text verify:self.txtVerify.text success:^(id resquestData) {//TODO:注册成功以后跳转到哪里还没定
        [MCMessageUtilities showSuccessMessage:@"注册成功"];
        [weakSelf.navigationController popViewControllerAnimated:YES];
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}


- (void)timeFire {
    _timeCount --;
    if (_timeCount < 1) {
        _timeCount = 0;
        [_timer invalidate];
        [self.btnVerifyCode setBackgroundColor:kRGB(34, 97, 00)];
        self.btnVerifyCode.enabled = YES;
        return;
    }else{
        NSString *title = [NSString stringWithFormat:@"%ld秒",_timeCount];
        [self.btnVerifyCode setTitle:title forState:UIControlStateDisabled];
        [self.btnVerifyCode setBackgroundColor:[UIColor lightGrayColor]];
        self.btnVerifyCode.enabled = NO;
    }
}





@end
