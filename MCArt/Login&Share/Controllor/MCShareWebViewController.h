//
//  MCShareWebViewController.h
//  MCArt
//
//  Created by Derek.zhao on 16/6/2.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseWebViewController.h"
#import "MCShareModel.h"


@interface MCShareWebViewController : MCBaseWebViewController

- (instancetype)initWithUrl:(NSString *)url shareInfo:(MCShareModel *)info;

@end
