//
//  MCLoginViewController.m
//  MCArt
//
//  Created by Derek on 15/12/21.
//  Copyright © 2015年 zzd. All rights reserved.
//

#import "MCLoginViewController.h"
#import "MCStyleButton.h"
#import "RegisterViewController.h"
#import "MCLoginViewModel.h"
#import "NSString+Utilities.h"

@interface MCLoginViewController ()

@property (weak, nonatomic) IBOutlet UITextField *txtPhoneNum;

@property (weak, nonatomic) IBOutlet UITextField *txtPassword;


@end

@implementation MCLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setTitle:@"登录" withStyle:MCNavStyleBlack];
    
    MCStyleButton *btnRegister = [[MCStyleButton alloc] initWithFrame:CGRectMake(0, 0, 50, 25)];
    [btnRegister setTitle:@"注册" forState:UIControlStateNormal];
    [btnRegister addTarget:self action:@selector(btnDidClickRegister:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnRegister];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)back{
    if (self.presentingViewController) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)btnDidClickRegister:(id)sender{
    RegisterViewController *registerController = [[RegisterViewController alloc] initWithNibName:NSStringFromClass([RegisterViewController class]) bundle:nil];
    registerController.type = MCRegisterTypeRegister;
    [self.navigationController pushViewController:registerController animated:YES];
}

- (IBAction)btnDidClickLogin:(id)sender {
    if ([NSString isNullOrEmpty:self.txtPhoneNum.text]) {
        [MCMessageUtilities showErrorMessage:@"手机号码不能为空"];
        return;
    }
    
    if ([NSString isNullOrEmpty:self.txtPassword.text]) {
        [MCMessageUtilities showErrorMessage:@"密码不能为空"];
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    [MCMessageUtilities showProgressMessage:@""];
    [[MCLoginViewModel sharedInstance] loginWithPhone:self.txtPhoneNum.text password:self.txtPassword.text success:^(id resquestData) {
        [MCMessageUtilities dismissHUD];
        [MCMessageUtilities showSuccessMessage:@"登录成功"];
        [weakSelf back];
    } failure:^(NSError *error) {
        [MCMessageUtilities dismissHUD];
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}

- (IBAction)btnDidClickForgotPassword:(id)sender {
    RegisterViewController *reg = [[RegisterViewController alloc] initWithNibName:NSStringFromClass([RegisterViewController class]) bundle:nil];
    reg.type = MCRegisterTypeForgot;
    [self.navigationController pushViewController:reg animated:YES];
}


@end
