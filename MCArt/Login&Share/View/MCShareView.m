//
//  MCShareView.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/11.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCShareView.h"
#import "MCShareViewModel.h"


@implementation MCShareView{
    NSString *_strTitle;
    NSString *_strImage;
    NSString *_strURL;
    NSString *_strContent;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}

- (void)setUpSubViews{
    self.backgroundColor = kRGBA(0, 0, 0, 0.3);
    self.userInteractionEnabled = YES;
    
    
    CGFloat width = kMainFrameWidth/4;
    CGFloat height = width;
    
    UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, kMainFrameHeight - 250, CGRectGetWidth(self.bounds), 250)];
    container.backgroundColor = UIColorFromRGB(0xe5e5e5);
    [self addSubview:container];
    
    NSArray *arrTitle = @[@"微信",@"朋友圈",@"QQ",@"QQ空间",@"微博"];
    
    for (int i = 0; i <= 3; i++) {
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(i*width, 0, width, height)];
        NSString *strImage = [NSString stringWithFormat:@"share_0%d",i+1];
        button.tag = i;
        [button setImage:[UIImage imageNamed:strImage] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(btnDidClickShare:) forControlEvents:UIControlEventTouchUpInside];
        [container addSubview:button];
        
        UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(i*width, CGRectGetMaxY(button.frame)-20, width, 20)];
        lblTitle.textAlignment = NSTextAlignmentCenter;
        lblTitle.font = kAppFont(12.f);
        lblTitle.text = [arrTitle objectAtIndex:i];
        [container addSubview:lblTitle];
        
    }
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, height, width, height)];
    NSString *strImage = [NSString stringWithFormat:@"share_0%d",5];
    button.tag = 4;
    [button setImage:[UIImage imageNamed:strImage] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(btnDidClickShare:) forControlEvents:UIControlEventTouchUpInside];
    [container addSubview:button];
    
    UILabel *lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(button.frame)-20, width, 20)];
    lblTitle.textAlignment = NSTextAlignmentCenter;
    lblTitle.font = kAppFont(12.f);
    lblTitle.text = [arrTitle objectAtIndex:4];
    [container addSubview:lblTitle];
    
    UIButton *btnCancel = [[UIButton alloc] initWithFrame:CGRectMake(8, CGRectGetHeight(container.frame)-8-40, CGRectGetWidth(container.frame) - 2* 8, 40)];
    [btnCancel setBackgroundColor:kWhiteColor];
    [btnCancel setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [btnCancel setTitle:@"取消" forState:UIControlStateNormal];
    [btnCancel addTarget:self action:@selector(btnDidClickCancel:) forControlEvents:UIControlEventTouchUpInside];
    [container addSubview:btnCancel];
    
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tap)];
    tap.numberOfTapsRequired = 1;
    [self addGestureRecognizer:tap];
}

- (void)tap{
    [self removeFromSuperview];
}

- (void)btnDidClickShare:(UIButton *)sender{
    NSInteger tag = sender.tag;
    if (tag == 0) {
        [MCShareViewModel shareWithType:SSDKPlatformSubTypeWechatSession content:_strContent image:_strImage url:_strURL title:_strTitle];
    }else if (tag == 1) {
        [MCShareViewModel shareWithType:SSDKPlatformSubTypeWechatTimeline content:_strContent image:_strImage url:_strURL title:_strTitle];
    }else if (tag == 2) {
        [MCShareViewModel shareWithType:SSDKPlatformSubTypeQQFriend content:_strContent image:_strImage url:_strURL title:_strTitle];
    }else if (tag == 3) {
        [MCShareViewModel shareWithType:SSDKPlatformSubTypeQZone content:_strContent image:_strImage url:_strURL title:_strTitle];
    }else if (tag == 4) {
        [MCShareViewModel shareWithType:SSDKPlatformTypeSinaWeibo content:_strContent image:_strImage url:_strURL title:_strTitle];
    }
    [self removeFromSuperview];
}

- (void)btnDidClickCancel:(UIButton *)sender{
    [self removeFromSuperview];
}

- (void)updateWithTitle:(NSString *)title image:(NSString *)image url:(NSString *)url content:(NSString *)content{
    
//    NSLog(@"url=%@",url);
//    NSLog(@"picURL = %@",kImagePath(image));
    
    _strTitle = title;
    _strImage = kImagePath(image);
    _strURL = url;
    _strContent = content;
}


@end
