//
//  MCShareView.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/11.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, MCShareType) {
    MCShareTypeWeiXin,
    MCShareTypeWieXinFriends,
    MCShareTypeQQ,
    MCShareTypeQZone,
    MCShareTypeWeibo
};


@protocol MCShareViewDelegate <NSObject>

- (void)clickedShare:(MCShareType)type;

@end



@interface MCShareView : UIView

@property (nonatomic, weak) id<MCShareViewDelegate> delegate;

- (void)updateWithTitle:(NSString *)title image:(NSString *)image url:(NSString *)url content:(NSString *)content;


@end
