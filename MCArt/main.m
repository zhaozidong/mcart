//
//  main.m
//  MCArt
//
//  Created by Derek on 15/12/18.
//  Copyright © 2015年 zzd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
