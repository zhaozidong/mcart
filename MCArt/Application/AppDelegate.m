//
//  AppDelegate.m
//  MCArt
//
//  Created by Derek on 15/12/18.
//  Copyright © 2015年 zzd. All rights reserved.
//

#import "AppDelegate.h"
#import <AlipaySDK/AlipaySDK.h>
#import "MobClick.h"
#import "AppDelegate+JPush.h"
#import <Bugly/CrashReporter.h>
#import "AYCheckManager.h"
#import "MCShareViewModel.h"


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    //Bugly
    [[CrashReporter sharedInstance] installWithAppId:@"900022638"];
    
    //极光推送
    [self registerNotificationWithLaunchOptions:launchOptions];
    
    //友盟统计
    [MobClick startWithAppkey:@"56e55fab67e58ea9f1000209" reportPolicy:BATCH channelId:nil];
    
    //clear badge
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    //新版本检测
    AYCheckManager *checkManger = [AYCheckManager sharedCheckManager];
    checkManger.openAPPStoreInsideAPP = YES;
    checkManger.debugEnable = YES;
    [checkManger checkVersionWithAlertTitle:@"发现新版本" nextTimeTitle:@"下次提示" confimTitle:@"前往更新"];
    
    [IQKeyboardManager sharedManager].enable = YES;
    
    [MCShareViewModel registerShareSDK];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    return  YES;
}

///iOS9.0以下用该方法
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if ([url.host isEqualToString:@"safepay"]) {
        //跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"payFinished" object:nil];
        }];
    }
    return  YES;
}

///iOS9.0以上用该方法
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)option{
    
    if ([url.host isEqualToString:@"safepay"]) {
        //跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"payFinished" object:nil];
        }];
    }
    return YES;
}


@end
