//
//  AppDelegate+JPush.h
//  MCArt
//
//  Created by Derek.zhao on 16/4/9.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (JPush)

- (void)registerNotificationWithLaunchOptions:(NSDictionary *)launchOptions;



@end
