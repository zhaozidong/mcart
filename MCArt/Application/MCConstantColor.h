//
//  MCConstantColor.h
//  MCArt
//
//  Created by Derek on 15/12/21.
//  Copyright © 2015年 zzd. All rights reserved.
//

#ifndef MCConstantColor_h
#define MCConstantColor_h


#ifndef kRGB
#define kRGB(r,g,b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0]
#endif

#ifndef kRGBA
#define kRGBA(r,g,b,a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#endif

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


#define kBlackColor kRGB(0, 0, 0)
#define kWhiteColor kRGB(255.0, 255.0, 255.0)










#endif /* MCConstantColor_h */
