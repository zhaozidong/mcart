//
//  MCConstantAPI.h
//  MCArt
//
//  Created by Derek on 15/12/21.
//  Copyright © 2015年 zzd. All rights reserved.
//



#ifndef MCConstantAPI_h
#define MCConstantAPI_h


#define kAppDomain @"http://115.29.112.234"
#define kAppHost @"http://115.29.112.234/MCYS/"
#define kLocalHost @"http://192.168.1.100:3000/"

#define kImagePath(path) [NSString stringWithFormat:@"%@/%@", kAppDomain, path]
#define kVideoPath(path) [NSString stringWithFormat:@"%@%@", kAppDomain, path]
#define kURLPath(path) [NSString stringWithFormat:@"%@%@", @"http://115.29.112.234/MCYS", path]


#endif /* MCConstantAPI_h */
