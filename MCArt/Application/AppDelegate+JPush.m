//
//  AppDelegate+JPush.m
//  MCArt
//
//  Created by Derek.zhao on 16/4/9.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "AppDelegate+JPush.h"
#import "JPUSHService.h"
#import "MCPushWebViewController.h"
#import "MCActivityDetailViewController.h"


@implementation AppDelegate (JPush)

- (void)registerNotificationWithLaunchOptions:(NSDictionary *)launchOptions{

//    NSString *advertisingId = [[[ASIdentifierManager sharedManager] advertisingI
//                                dentifier] UUIDString];
    
    //Required
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 8.0) {
        //       categories
    [JPUSHService registerForRemoteNotificationTypes:(UIUserNotificationTypeBadge |
                                                      UIUserNotificationTypeSound |
                                                      UIUserNotificationTypeAlert)
                                          categories:nil];
    }
//    else {
//    //categories    nil
//    [JPUSHService registerForRemoteNotificationTypes:(UIRemoteNotificationTypeBadge |
//                                                      UIRemoteNotificationTypeSound |
//                                                      UIRemoteNotificationTypeAlert)
//                                          categories:nil];
//    }

#if DEBUG
    [JPUSHService setupWithOption:launchOptions
                           appKey:@"0b52bc364fb5508b992c625a"
                          channel:@"app store"
                 apsForProduction:0];
#else
    [JPUSHService setupWithOption:launchOptions
                           appKey:@"0b52bc364fb5508b992c625a"
                          channel:@"app store"
                 apsForProduction:1];
#endif
    
    
    
//    NSMutableSet *set = [NSMutableSet set];
//    [set addObject:@"ios"];
//    [JPUSHService setTags:set aliasInbackground:nil];
//    
//    [JPUSHService setTags:set callbackSelector:@selector(finishSetTag) object:nil];
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        NSMutableSet *set = [NSMutableSet set];
        [set addObject:@"ios"];
        [JPUSHService setTags:set aliasInbackground:nil];
        [JPUSHService setTags:set callbackSelector:@selector(finishSetTag) object:nil];
    });
    
    
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    /// Required -    DeviceToken
    [JPUSHService registerDeviceToken:deviceToken];
}

//- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
//    // Required,For systems with less than or equal to iOS6
//    [JPUSHService handleRemoteNotification:userInfo];
//}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    // 取得 APNs 标准信息内容
    NSDictionary *aps = [userInfo valueForKey:@"aps"];
    NSString *content = [aps valueForKey:@"alert"]; //推送显示的内容
    NSInteger badge = [[aps valueForKey:@"badge"] integerValue]; //badge数量
    NSString *sound = [aps valueForKey:@"sound"]; //播放的声音
    
    // 取得Extras字段内容
    NSString *customizeField1 = [userInfo valueForKey:@"customizeExtras"]; //服务端中Extras字段，key是自己定义的
//    NSLog(@"content =[%@], badge=[%d], sound=[%@], customize field  =[%@]",content,badge,sound,customizeField1);
    
    // Required
    [JPUSHService handleRemoteNotification:userInfo];
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    // IOS 7 Support Required
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"receiveNotification" object:nil userInfo:userInfo];
    
    [application setApplicationIconBadgeNumber:0];
    [JPUSHService handleRemoteNotification:userInfo];
    completionHandler(UIBackgroundFetchResultNewData);
}


- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    //Optional
    NSLog(@"did Fail To Register For Remote Notifications With Error: %@", error);
}

- (void)finishSetTag{
    NSLog(@"设置tag完成");
}

@end
