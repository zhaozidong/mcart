//
//  MCConstantDevice.h
//  MCArt
//
//  Created by Derek.zhao on 16/1/31.
//  Copyright © 2016年 zzd. All rights reserved.
//

#ifndef MCConstantDevice_h
#define MCConstantDevice_h

#define iPhone6 ([UIScreen mainScreen].bounds.size.height == 667)

#define iPhone6Plus ([UIScreen mainScreen].bounds.size.height == 736)

#define iPhone4 ([UIScreen mainScreen].bounds.size.height == 480)

#define iPhone5 ([UIScreen mainScreen].bounds.size.height == 568)


#ifndef IOS7
#define IOS7 [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0
#endif

#ifndef IOS8
#define IOS8 [[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0
#endif



#ifndef kDeviceWidth
#define kDeviceWidth ([[UIScreen mainScreen] bounds].size.width)
#endif

#ifndef kDeviceHeight
#define kDeviceHeight ([[UIScreen mainScreen] bounds].size.height)
#endif

#ifndef kMainFrameWidth
#define kMainFrameWidth ([[UIScreen mainScreen]applicationFrame].size.width)
#endif

#ifndef kMainFrameHeight
#define kMainFrameHeight ([[UIScreen mainScreen]bounds].size.height)
#endif



#endif /* MCConstantDevice_h */
