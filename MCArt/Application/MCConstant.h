//
//  MCConstant.h
//  MCArt
//
//  Created by Derek on 15/12/21.
//  Copyright © 2015年 zzd. All rights reserved.
//


#import "MCConstantColor.h"
#import "MCConstantAPI.h"
#import "MCConstantDevice.h"
#import "MCConstantIconfont.h"

#ifndef MCConstant_h
#define MCConstant_h


#define MC_PAGE_SIZE @"20"


#define kAppFont(size) [UIFont systemFontOfSize:size]
#define kAppFontBold(size) [UIFont boldSystemFontOfSize:size]
#define kMCFont(sz) [UIFont fontWithName:@"ShiShangZhongHeiJianTi" size:sz]
#define kIconFont(sz) [UIFont fontWithName:@"iconfont" size:sz]


//#define kTitleFont(size) [UIFont fontWithName:@"时尚中黑简体" size:size]


#endif /* MCConstant_h */
