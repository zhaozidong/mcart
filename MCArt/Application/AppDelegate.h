//
//  AppDelegate.h
//  MCArt
//
//  Created by Derek on 15/12/18.
//  Copyright © 2015年 zzd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

