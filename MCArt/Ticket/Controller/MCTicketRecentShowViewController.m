//
//  MCTicketRecentShowViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/13.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCTicketRecentShowViewController.h"
#import "MCTicketViewModel.h"
#import "MCTicketModel.h"
#import "MCTicketListBannerCell.h"
#import "MCTicketListShowCell.h"
#import "MCTicketShowDetailViewController.h"


static NSString *const bannerIdentifier = @"bannerCell";
static NSString *const showIdentifier = @"showCell";

@interface MCTicketRecentShowViewController (){
    __block NSInteger _pageIndex;
    __block MCTicketModel *_ticket;
    __block NSMutableArray *_arrList;
    __block NSMutableArray *_banner;
}

@end

@implementation MCTicketRecentShowViewController

- (instancetype)init{
    self = [super initWithStyle:UITableViewStyleGrouped refreshType:MCTableViewRefreshTypeBottom | MCTableViewRefreshTypeTop];
    if (self) {
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"即将上演";
    _pageIndex = 1;
    _arrList = [NSMutableArray array];
    _banner = [NSMutableArray array];
    
    
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth, CGFLOAT_MIN)];
    
    [self.tableView registerClass:[MCTicketListBannerCell class] forCellReuseIdentifier:bannerIdentifier];
    [self.tableView registerClass:[MCTicketListShowCell class] forCellReuseIdentifier:showIdentifier];
    
    [self getData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.backgroundColor=kBlackColor;
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)getData{
    _pageIndex = 1;
    [_banner removeAllObjects];
    [_arrList removeAllObjects];
    
    __weak typeof(self) weakSelf = self;
    [MCMessageUtilities showProgressMessage:@""];
    [[MCTicketViewModel sharedInstance] getShowList:MCTicketShowTypeWillBegin page:_pageIndex success:^(id resquestData) {
        [MCMessageUtilities dismissHUD];
        _ticket = resquestData;
        if (_ticket.list.count > 0 || _ticket.banner.count > 0) {
            if (_ticket.banner.count > 0) {
                _banner = [_ticket.banner mutableCopy];
            }
            if (_ticket.banner.count > 0) {
                _pageIndex ++;
                _arrList = [_ticket.list mutableCopy];
            }
            [weakSelf.tableView reloadData];
        }
    } failure:^(NSError *error) {
        [MCMessageUtilities dismissHUD];
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}

#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _arrList.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
//    if (indexPath.section == 0) {
//        MCTicketListBannerCell *bannerCell = [tableView dequeueReusableCellWithIdentifier:bannerIdentifier];
//        [bannerCell updateWithTicket:_ticket];
//        return bannerCell;
//    }else{
        MCTicketShowModel *show = [_arrList objectAtIndex:indexPath.section];
        MCTicketListShowCell *showCell = [tableView dequeueReusableCellWithIdentifier:showIdentifier];
        [showCell updateWithShow:show];
        return showCell;
//    }
//    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
//    if (section == 0) {
//        return CGFLOAT_MIN;
//    }
    return 10.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
//    if (indexPath.section == 0) {
//        return [MCTicketListBannerCell cellHeight];
//    }
    return [MCTicketListShowCell cellHeight];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
//    if (indexPath.section > 0) {
        MCTicketShowModel *show = [_arrList objectAtIndex:indexPath.section];
        MCTicketListShowCell *showCell = [tableView dequeueReusableCellWithIdentifier:showIdentifier];
        [showCell updateWithShow:show];
        MCTicketShowDetailViewController *detail = [[MCTicketShowDetailViewController alloc] initWithShow:show];
        [self.navigationController pushViewController:detail animated:YES];
//    }
}


#pragma mark MJRefresh
- (void)beginRefreshHeader:(MJRefreshHeader *)header{
    [header endRefreshing];
    [self getData];
}

- (void)beginRefreshFooter:(MJRefreshFooter *)footer{
    [footer endRefreshing];
    __weak typeof(self) weakSelf = self;
    [MCMessageUtilities showProgressMessage:@""];
    [[MCTicketViewModel sharedInstance] getShowList:MCTicketShowTypeWillBegin page:_pageIndex success:^(id resquestData) {
        [MCMessageUtilities dismissHUD];
        _ticket = resquestData;
        if (_ticket.list.count > 0) {
            [_arrList addObjectsFromArray:_ticket.list];
            _pageIndex ++;
            [weakSelf.tableView reloadData];
        }
    } failure:^(NSError *error) {
        [MCMessageUtilities dismissHUD];
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
    
}

@end
