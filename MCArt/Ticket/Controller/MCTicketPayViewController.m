//
//  MCTicketPayViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/10.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCTicketPayViewController.h"
#import <AlipaySDK/AlipaySDK.h>
#import "MCTicketOrderViewModel.h"

@interface MCTicketPayViewController ()<UIAlertViewDelegate>{
    __block MCTicketOrderModel *_orderInfo;
    NSInteger _restTime;
    UIButton *_btnPay;
    NSTimer *timer;
    NSString *_orderId;
}

@end

@implementation MCTicketPayViewController

- (instancetype)initWithOrderId:(NSString *)orderId{
    self = [super init];
    if (self) {
        _orderId = orderId;
    }
    return self;
}

//- (instancetype)initWithPayInfo:(MCTicketOrderModel *)info{
//    self = [super init];
//    if (self) {
//        _orderInfo = info;
//    }
//    return self;
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"支付" withStyle:MCNavStyleWhite];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.view.backgroundColor = kWhiteColor;
    
//    [self setUpSubViews];
    
    [self getOrderData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [timer invalidate];
    timer = nil;
}

- (void)back:(id)sender{
    [self backToSeat];
}

- (void)backToSeat{
    NSArray *array = self.navigationController.viewControllers;
    if (array.count > 4) {
        UIViewController *controller = [array objectAtIndex:array.count-3];
        [self.navigationController popToViewController:controller animated:YES];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    if (self.showTicketList) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kAppShowPersonalTicketNotification" object:nil];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)getOrderData{
    __weak typeof(self) weakSelf = self;
    [[MCTicketOrderViewModel sharedInstance] getTicktPayResult:_orderId success:^(id resquestData) {
        _orderInfo = resquestData;
        [weakSelf setUpTimer];
        [weakSelf setUpSubViews];
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}

- (void)setUpTimer{
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                             target:self
                                           selector:@selector(timeFireMethod)
                                           userInfo:nil
                                            repeats:YES];
    _restTime =  [_orderInfo.resttime integerValue];
}


- (void)setUpSubViews{
    
    UILabel *_lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(8, 10, kMainFrameWidth - 2*8, 50)];
    _lblTitle.font = kAppFont(20.f);
    _lblTitle.text = _orderInfo.ordername;
    [self.view addSubview:_lblTitle];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(_lblTitle.frame)+8, kMainFrameWidth - 2 * 8, 1)];
    line.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:line];
    
    UILabel *_lblOrderNum = [[UILabel alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(line.frame)+8, kMainFrameWidth - 2*8, 20)];
    _lblOrderNum.text = [NSString stringWithFormat:@"订单金额： %@",_orderInfo.total_price];
    [_lblOrderNum sizeToFit];
    [self.view addSubview:_lblOrderNum];
    
    UILabel *_lblPayNum = [[UILabel alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(_lblOrderNum.frame)+8, kMainFrameWidth - 2*8, 20)];
    _lblPayNum.text = [NSString stringWithFormat:@"应付金额： %@",_orderInfo.total_price];
    [_lblPayNum sizeToFit];
    [self.view addSubview:_lblPayNum];
    
    UIView *line2 = [[UIView alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(_lblPayNum.frame)+18, kMainFrameWidth - 2 * 8, 1)];
    line2.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:line2];
    
    
    UILabel *lblPayStyle = [[UILabel alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(line2.frame)+8, 100, 20)];
    lblPayStyle.text = @"请选择支付方式";
    [lblPayStyle sizeToFit];
    [self.view addSubview:lblPayStyle];
    
    
    UIView *line3 = [[UIView alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(lblPayStyle.frame)+8, kMainFrameWidth - 2 * 8, 1)];
    line3.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:line3];
    
    
    UIImageView *imgPay = [[UIImageView alloc] initWithFrame:CGRectMake(16, CGRectGetMaxY(line3.frame)+8, 40, 40)];
    imgPay.image = [UIImage imageNamed:@"zhifubao"];
    [self.view addSubview:imgPay];
    
    
    _btnPay = [[UIButton alloc] initWithFrame:CGRectMake(16, CGRectGetMaxY(imgPay.frame)+50, kMainFrameWidth-16*2, 40)];
    [_btnPay setBackgroundColor:[UIColor orangeColor]];
    [_btnPay setTitle:@"确认支付" forState:UIControlStateNormal];
    [_btnPay addTarget:self action:@selector(pay) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_btnPay];
    
}


-(void)timeFireMethod{
    _restTime -= 1000;
    NSInteger sec = _restTime/1000;
    NSString *strTime = [NSString stringWithFormat:@"%@分%@秒",@(sec/60),@(sec%60)];
    [_btnPay setTitle:[NSString stringWithFormat:@"确认支付(还剩%@)",strTime] forState:UIControlStateNormal];

    if (sec < 10) {
        [MCMessageUtilities showSuccessMessage:@"订单超时，座位已被释放，请重新选座。"];
        [self backToSeat];
    }
}

- (void)pay{
    __weak typeof(self) weakSelf = self;
    [[AlipaySDK defaultService] payOrder:_orderInfo.ali_order_info fromScheme:@"mcartalipay" callback:^(NSDictionary *resultDic) {//只处理状态为9000(成功)和4000(失败)的情况
        if (resultDic) {
            if ([[resultDic objectForKey:@"resultStatus"] integerValue] == 9000) {
                [MCMessageUtilities showSuccessMessage:@"支付成功。"];
                [weakSelf backToSeat];
            }else{
                [MCMessageUtilities showErrorMessage:@"支付失败。"];
            }
        }
    }];
}



@end
