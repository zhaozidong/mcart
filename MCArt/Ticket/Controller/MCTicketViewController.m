//
//  MCTicketViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/13.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCTicketViewController.h"
#import "MCTicketRecentShowViewController.h"
#import "MCTicketAllShowViewController.h"



@interface MCTicketViewController ()

@end

@implementation MCTicketViewController


- (instancetype)init{
    NSArray *arrControls=@[[MCTicketRecentShowViewController class],[MCTicketAllShowViewController class]];
    NSArray *arrTitles=@[@"即将上演",@"演出一览"];
    self=[super initWithViewControllerClasses:arrControls andTheirTitles:arrTitles];
    if (self) {
        //        self.navStyle=MCNavStyleBlack;
        self.menuViewStyle = WMMenuViewStyleLine;
        
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    self.navStyle=MCNavStyleBlack;
//    self.title=@"掌上购票";
    
    [self setTitle:@"掌上购票" withStyle:MCNavStyleBlack];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
