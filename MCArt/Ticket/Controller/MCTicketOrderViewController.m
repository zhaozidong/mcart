//
//  MCTicketOrderViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/10.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCTicketOrderViewController.h"
#import "MCTicketViewModel.h"
#import "MCTicketAreaModel.h"
#import "MCTicketOrderViewModel.h"
#import "MCTicketOrderModel.h"
#import "MCTicketPayViewController.h"
#import "MCLoginViewModel.h"


@interface MCTicketOrderViewController (){
    NSString *_showId;
    NSString *_areaName;
    NSArray *_arrSeat;
    NSString *_showTimeId;
    BOOL _isExtAddress;
    
    __block NSString *_address;
    __block MCTicketShowModel *_show;
    
//    UITextField *_txtPhone;
    
    
    MCTicketOrderShowModel *_orderShow;
}

@end

@implementation MCTicketOrderViewController

- (instancetype)initWithShowId:(NSString *)index showTimeId:(NSString *)showTimeId areaName:(NSString *)areaName seat:(NSArray *)arrSeat extAddress:(BOOL)isExtAddress{
    self = [super init];
    if (self) {
        _showId = index;
        _areaName = areaName;
        _arrSeat = arrSeat;
        _showTimeId = showTimeId;
        _isExtAddress = isExtAddress;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTitle:@"订单确认" withStyle:MCNavStyleWhite];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.view.backgroundColor = kWhiteColor;
    
    [self getShowDetailData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//生成订单
- (void)getShowDetailData{
    __weak typeof(self) weakSelf = self;
    [[MCTicketViewModel sharedInstance] getShowDetail:_showId success:^(id resquestData) {
        MCTicketModel *ticket = resquestData;
        if (ticket.list.count > 0) {
            _show = [ticket.list objectAtIndex:0];
            if (_show.showtimes.count > 0) {
                MCTicketShowTimeModel *showTime = [_show.showtimes objectAtIndex:0];
                _address = showTime.concert_hall;
            }else{
                _address = @"";
            }
            [weakSelf setUpSubViews];
        }
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}

- (void)setUpSubViews{
    UILabel *_lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(8, 10, kMainFrameWidth - 2 * 8, 50)];
    _lblTitle.font = kAppFont(22.f);
    _lblTitle.text = _show.title;
    [self.view addSubview:_lblTitle];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(_lblTitle.frame)+8, kMainFrameWidth-2* 8, 1)];
    line.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:line];
    
    UILabel *_lblAddressTitle = [[UILabel alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(line.frame)+8, 60, 20)];
    _lblAddressTitle.text = @"地点:";
    [_lblAddressTitle sizeToFit];
    [self.view addSubview:_lblAddressTitle];
    
    UILabel *_lblAddress = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_lblAddressTitle.frame)+8, CGRectGetMinY(_lblAddressTitle.frame), 200, CGRectGetHeight(_lblAddressTitle.frame))];
    _lblAddress.textColor = [UIColor lightGrayColor];
    
    if (_isExtAddress) {
        _lblAddress.text = _areaName;
    }else{
        _lblAddress.text = _address;
    }

    [self.view addSubview:_lblAddress];
    
    UILabel *_lblTimeTitle = [[UILabel alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(_lblAddressTitle.frame)+8, 60, 20)];
    _lblTimeTitle.text = @"场次:";
    [_lblTimeTitle sizeToFit];
    [self.view addSubview:_lblTimeTitle];
    
    UILabel *_lblTime = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_lblTimeTitle.frame)+8, CGRectGetMinY(_lblTimeTitle.frame), 200, CGRectGetHeight(_lblTimeTitle.frame))];
    _lblTime.textColor = [UIColor lightGrayColor];
    _lblTime.text = _show.time;
    [self.view addSubview:_lblTime];
    
    UILabel *_lblSeatTitle = [[UILabel alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(_lblTimeTitle.frame)+8, 60, 20)];
    _lblSeatTitle.text = @"座位:";
    [_lblSeatTitle sizeToFit];
    [self.view addSubview:_lblSeatTitle];
    
    NSInteger price = 0;
    NSMutableString *strSeatId = [[NSMutableString alloc] init];
    for (MCTicketSeatDetailModel *seat in _arrSeat) {
        price += [seat.price integerValue];
        [strSeatId appendString:seat.sename];
        [strSeatId appendString:@","];
    }
    
    UILabel *_lblSeat = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_lblSeatTitle.frame)+8, CGRectGetMinY(_lblSeatTitle.frame), kMainFrameWidth - CGRectGetMaxX(_lblSeatTitle.frame)-2*8, CGRectGetHeight(_lblSeatTitle.frame)*3)];
    _lblSeat.numberOfLines = 3;
    _lblSeat.preferredMaxLayoutWidth = kMainFrameWidth - CGRectGetMaxX(_lblSeatTitle.frame)-2*8;
    
    if (_isExtAddress) {
        _lblSeat.text = [NSString stringWithFormat:@"%@",strSeatId];
    }else{
        _lblSeat.text = [NSString stringWithFormat:@"%@  %@",_areaName,strSeatId];
    }
    [_lblSeat sizeToFit];
    [self.view addSubview:_lblSeat];
    
    UILabel *_lblGet = [[UILabel alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(_lblSeat.frame)+8, kMainFrameWidth - 2* 8, 20)];
    _lblGet.text = @"取票: 票务中心";
    [_lblGet sizeToFit];
    [self.view addSubview:_lblGet];
    
    UIView *line2 = [[UIView alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(_lblGet.frame)+8, kMainFrameWidth-2*8, 1)];
    line2.backgroundColor = [UIColor lightGrayColor];
    [self.view addSubview:line2];
    
    UILabel *_lblPriceTitle = [[UILabel alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(line2.frame)+8, 80, 30)];
    _lblPriceTitle.font = kAppFont(20.f);
    _lblPriceTitle.text = @"票价：";
    [_lblPriceTitle sizeToFit];
    [self.view addSubview:_lblPriceTitle];
    
    UILabel *_lblPrice = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_lblPriceTitle.frame)+8, CGRectGetMinY(_lblPriceTitle.frame), 100, CGRectGetHeight(_lblPriceTitle.frame))];
    _lblPrice.textColor = [UIColor orangeColor];
    _lblPrice.font = kAppFont(20.f);
    _lblPrice.text = [NSString stringWithFormat:@"%ld元",(long)price];
    [self.view addSubview:_lblPrice];
    
//    _txtPhone = [[UITextField alloc] initWithFrame:CGRectMake(16, CGRectGetMaxY(_lblPrice.frame)+16, kMainFrameWidth - 2 * 16, 40)];
//    _txtPhone.keyboardType = UIKeyboardTypeNamePhonePad;
//    _txtPhone.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    _txtPhone.layer.borderWidth = 1.f;
//    _txtPhone.layer.cornerRadius = 3.f;
//    _txtPhone.keyboardType = UIKeyboardTypePhonePad;
//    _txtPhone.placeholder = @"输入接收取票号的手机号码";
//    [self.view addSubview:_txtPhone];
    
    UILabel *_lblPhone = [[UILabel alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(_lblPrice.frame)+8, kMainFrameWidth - 2 * 8, 30)];
    _lblPhone.font = kAppFont(18.f);
    _lblPhone.text = [NSString stringWithFormat:@"手机号：%@",[MCLoginViewModel sharedInstance].userInfo.cellphone];
    [self.view addSubview:_lblPhone];
    
    UIButton *btnOrder = [[UIButton alloc] initWithFrame:CGRectMake(16, CGRectGetMaxY(_lblPhone.frame)+15, kMainFrameWidth-2*16, 40)];
    [btnOrder setBackgroundColor:[UIColor orangeColor]];
    [btnOrder setTitle:@"提交订单" forState:UIControlStateNormal];
    btnOrder.layer.cornerRadius = 3.f;
    [btnOrder addTarget:self action:@selector(btnDidClickConfirm:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnOrder];
    
}

//提交订单，成功后去支付页面
- (void)btnDidClickConfirm:(id)sender{
    if (![MCLoginViewModel sharedInstance].isLogin) {
        [self showLoginViewController];
        return;
    }
    __weak typeof(self) weakSelf = self;
    [MCMessageUtilities showProgressMessage:@""];
    [[MCTicketOrderViewModel sharedInstance] buyNormalTicket:_showTimeId seat:_arrSeat phone:[MCLoginViewModel sharedInstance].userInfo.cellphone success:^(id resquestData) {//TODO:需要弹出下单成功倒计时窗口
        [MCMessageUtilities dismissHUD];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"座位锁定成功，请在15分钟内完成支付，超时系统将释放你选定的座位。" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
        [alert show];
        [weakSelf payWithOrder:resquestData];
    } failure:^(NSError *error) {
        [MCMessageUtilities dismissHUD];
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}

- (void)payWithOrder:(MCTicketOrderModel *)order{
//    __weak typeof(self) weakSelf = self;
//    MCTicketPayViewController *pay = [[MCTicketPayViewController alloc] initWithPayInfo:order];
//    [weakSelf.navigationController pushViewController:pay animated:YES];
    
    MCTicketPayViewController *pay = [[MCTicketPayViewController alloc] initWithOrderId:order.toid];
    pay.showTicketList = YES;
    [self.navigationController pushViewController:pay animated:YES];
    
}

@end
