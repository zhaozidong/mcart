//
//  MCTicketPlaceViewController.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/8.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseViewController.h"
#import "MCTicketModel.h"


@interface MCTicketPlaceViewController : MCBaseViewController

- (instancetype)initWithShowId:(NSString *)showId showTime:(MCTicketShowTimeModel *)showTime;



@end
