//
//  MCTicketSeatViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/8.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCTicketSeatViewController.h"
#import "MCTicketAreaViewModel.h"
#import "MCSeatDetailView.h"
#import "MCSeatCheckOutView.h"
#import "MCSeatSelectView.h"
#import "MCTicketOrderViewController.h"
#import "MCTicketOrderViewModel.h"
#import "MCTicketSeatDescView.h"
#import "MCTicketOrderModel.h"
#import "MCPersonalTicketViewController.h"
#import "MCLoginViewModel.h"

@interface MCTicketSeatViewController ()<MCSeatDetailDelegate,MCSeatCheckOutDelegate,MCSeatSelectViewDelegate>{
    NSString *_showTimeId;
    __block NSString *_areaId;
    __block NSString *_areaName;
    __block MCTicketSeatModel *_seat;
    MCTicketShowTimeModel *_showTime;
    BOOL _isAll;
    NSMutableArray *_selectedSeat;
    
    
    MCSeatDetailView *_detail;
    UILabel *_lblSeat;
    MCTicketSeatDescView *_descView;
}

@property (nonatomic, strong) MCSeatCheckOutView *bottomView;
@property (nonatomic, strong) MCSeatSelectView *selectView;
@property (nonatomic, strong) UIScrollView *scrollView;

@end

@implementation MCTicketSeatViewController

- (instancetype)initWithShowTime:(MCTicketShowTimeModel *)showTime isAll:(BOOL)isALl{
    self = [super init];
    if (self) {
        _showTime = showTime;
        _isAll = isALl;
        
        _showTimeId = showTime.id;
    }
    return self;
}


- (instancetype)initWithShowTimeId:(NSString *)showTimeId areaId:(NSString *)areaId areaName:(NSString *)areaName{
    self = [super init];
    if (self) {
        _showTimeId = showTimeId;
        _areaId = areaId;
        _areaName = areaName;
        _isAll = NO;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTitle:@"选择座位" withStyle:MCNavStyleWhite];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.view.backgroundColor = kWhiteColor;
    
    _selectedSeat = [NSMutableArray array];
    
    [self.view addSubview:self.scrollView];
    [self.view addSubview:self.bottomView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(presentPersonalTicket) name:@"kAppShowPersonalTicketNotification" object:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (_isAll) {
        [self getArea];
    }else{
        [self getData];
    }
    
    [self.bottomView updateWithTotal:@""];
    [self.selectView updateWithArray:[NSArray array]];
}

- (UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
        if (kMainFrameHeight < 500) {
            _scrollView.contentSize = CGSizeMake(kMainFrameWidth, kMainFrameHeight+400);
        }
    }
    return _scrollView;
}


- (MCSeatCheckOutView *)bottomView{
    if (!_bottomView) {
        _bottomView = [[MCSeatCheckOutView alloc] initWithFrame:CGRectMake(0, kMainFrameHeight - 50-64, kMainFrameWidth, 50)];
        _bottomView.delegate = self;
    }
    return _bottomView;
}

- (void)presentPersonalTicket{
    MCPersonalTicketViewController *ticket = [[MCPersonalTicketViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:ticket];
    [self presentViewController:nav animated:YES completion:nil];
}

- (void)getArea{
    __weak typeof(self ) weakSelf = self;
    [[MCTicketAreaViewModel sharedInstance] getAreaList:_showTime.id success:^(id resquestData) {
        MCTicketAreaAllModel *all = resquestData;
        MCTicketAreaModel *area = [all.list objectAtIndex:0];
        _areaName = area.area_name;
        _areaId = area.area_id;
        [weakSelf getData];
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}


- (void)getData{
    __weak typeof(self ) weakSelf = self;
    [MCMessageUtilities showProgressMessage:@""];
    [[MCTicketAreaViewModel sharedInstance] getAreaSeatByShowId:_showTimeId areaId:_areaId areaName:_areaName isAll:_isAll success:^(id resquestData) {
        [MCMessageUtilities dismissHUD];
        _seat = resquestData;
        [weakSelf processData];
    } failure:^(NSError *error) {
        [MCMessageUtilities dismissHUD];
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}

- (void)processData{
    for (UIView *view in self.scrollView.subviews) {
        [view removeFromSuperview];
    }
    
    NSInteger count = 0;
    for (NSArray *array in _seat.list) {
        for (MCTicketSeatDetailModel *detail in array) {
            if (detail.sestatus == MCSeatStatusNormal) {
                count++;
            }
        }
    }
    
    UILabel *_lblCount = [[UILabel alloc] initWithFrame:CGRectMake(0, 30, kMainFrameWidth, 30)];
    _lblCount.textAlignment = NSTextAlignmentCenter;
    if (_isAll) {//从电影院直接跳过来的情况
        _lblCount.text = [NSString stringWithFormat:@"%@(余座%@个)",_showTime.concert_hall,@(count)];
    }else{//从区域过来时的情况
        _lblCount.text = [NSString stringWithFormat:@"%@(余座%@个)",_areaName,@(count)];
    }
    [self.scrollView addSubview:_lblCount];
    
    
    _descView = [[MCTicketSeatDescView alloc] initWithFrame:CGRectMake(0, 60, kMainFrameWidth, 30)];
    [self.scrollView addSubview:_descView];
    
    
    _detail = [[MCSeatDetailView alloc] initWithFrame:CGRectMake(0, 100, kMainFrameWidth, _seat.list.count*30+2) list:_seat.list];
    _detail.seatDelegate = self;
    [self.scrollView addSubview:_detail];
    
    _lblSeat = [[UILabel alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(_detail.frame)+5, kMainFrameWidth, 20)];
    _lblSeat.font = kAppFont(14.f);
    _lblSeat.text = @"您的选座~~~";
    [self.scrollView addSubview:_lblSeat];
    
    self.selectView = [[MCSeatSelectView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_lblSeat.frame)+5, kMainFrameWidth, 80)];
    self.selectView.delegate = self;
    [self.scrollView addSubview:self.selectView];
    
    
    if (CGRectGetMaxY(_lblSeat.frame)+5+100 < kMainFrameHeight) {
        self.scrollView.contentSize = CGSizeMake(kMainFrameWidth, kMainFrameHeight+200);
    }

}

#pragma mark MCSeatDetailDelegate
- (void)clickedSeat:(NSArray *)array{
    _selectedSeat = [array mutableCopy];
    [self updateSeatWithArray:array];
}

#pragma mark MCSeatSelectViewDelegate
- (void)deleteSeatAtIndex:(NSInteger)index {
    MCTicketSeatDetailModel *seat = [_selectedSeat objectAtIndex:index];
    NSString *seatId = seat.seat_id;
    if (seatId && ![seatId isEqualToString:@""]) {
        [_selectedSeat removeObjectAtIndex:index];
        [_detail removeSeatWithId:[seatId integerValue]];
        [self updateSeatWithArray:_selectedSeat];
    }
}

- (void)updateSeatWithArray:(NSArray *)array {
    NSInteger count = 0;
    for (MCTicketSeatDetailModel *seat in array) {
        count += [seat.price integerValue];
    }
    [self.bottomView updateWithTotal:[NSString stringWithFormat:@"%@",@(count)]];
    [self.selectView updateWithArray:array];
}


//去订单确认页面
- (void)clickedCheckOut{
    NSString *areaName;
    if (_isAll) {
        areaName = _showTime.concert_hall;
    }else{
        areaName = _areaName;
    }
    
//    MCTicketOrderShowModel *show = [[MCTicketOrderShowModel alloc] init];
//    show.showId = _showId;
//    show.showTimeId = _showTimeId;
//    show.areaName = areaName;
//    show.seat = _selectedSeat;
    
//    NSLog(@"areaName=%@",areaName);
    
    if (![MCLoginViewModel sharedInstance].isLogin) {
        [self showLoginViewController];
        return;
    }
    
    MCTicketOrderViewController *order = [[MCTicketOrderViewController alloc] initWithShowId:_showId showTimeId:_showTimeId areaName:areaName seat:_selectedSeat extAddress:_isAll];
    [self.navigationController pushViewController:order animated:YES];
}

@end
