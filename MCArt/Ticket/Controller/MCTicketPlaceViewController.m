//
//  MCTicketPlaceViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/8.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCTicketPlaceViewController.h"
#import "MCTicketDetailPlaceMusicHallView.h"
#import "MCTicketAreaViewModel.h"
#import "MCTicketSeatViewController.h"
#import "MCTicketAreaModel.h"

@interface MCTicketPlaceViewController ()<MCTicketDetailPlaceDelegate>{
    MCTicketShowTimeModel *_showTime;
    __block NSMutableArray *_array;
    __block MCTicketAreaAllModel *_all;
    NSString *_showId;
    MCTicketDetailPlaceMusicHallView *hall;
}

@property (nonatomic, retain) UILabel *titleLabel;
@property (nonatomic, retain) UIScrollView *scrollView;

@end

@implementation MCTicketPlaceViewController

- (instancetype)initWithShowId:(NSString *)showId showTime:(MCTicketShowTimeModel *)showTime{
    self = [super init];
    if (self) {
        _showTime = showTime;
        _showId = showId;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self setTitle:@"选择区域" withStyle:MCNavStyleWhite];
    
    self.scrollView.backgroundColor = kWhiteColor;
    
    _array = [NSMutableArray array];
    
    [self.scrollView addSubview:self.titleLabel];
    [self.view addSubview:self.scrollView];
    
    [self getData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    }
    return _scrollView;
}

- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 20, kMainFrameWidth - 20, 35)];
        _titleLabel.font = kAppFont(20.f);
        _titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _titleLabel;
}


- (void)getData{
    __weak typeof(self) weakSelf = self;
    [MCMessageUtilities showProgressMessage:@""];
    [[MCTicketAreaViewModel sharedInstance] getAreaList:_showTime.id success:^(id resquestData) {
        [MCMessageUtilities dismissHUD];
        _all = resquestData;
        _array = [_all.list mutableCopy];
        if ([_all.concert_hall containsString:@"大剧院"]) {//areaB
            [weakSelf showAreaWithType:MCTicketDetailPlaceTypeTheatreUpStairs];
        }else if([_all.concert_hall containsString:@"音乐厅"]) {//areaA
            [weakSelf showAreaWithType:MCTicketDetailPlaceTypeMusicHall];
        }
    } failure:^(NSError *error) {
        [MCMessageUtilities dismissHUD];
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}


- (void)showAreaWithType:(MCTicketDetailPlaceType)type{
    if (kMainFrameHeight < 485) {
        self.scrollView.contentSize = CGSizeMake(kMainFrameWidth, kMainFrameHeight+200);
    }
    
    if (kMainFrameHeight > 485 && kMainFrameHeight < 570) {
        self.scrollView.contentSize = CGSizeMake(kMainFrameWidth, kMainFrameHeight+100);
    }
    
    switch (type) {
        case MCTicketDetailPlaceTypeMusicHall: {
            hall = [[MCTicketDetailPlaceMusicHallView  alloc] initWithFrame:CGRectMake((kMainFrameWidth -273)/2, 100, 273, 296) type:MCTicketDetailPlaceTypeMusicHall];
            hall.delegate = self;
            [self.scrollView addSubview:hall];
            break;
        }
        case MCTicketDetailPlaceTypeTheatreUpStairs:
        case MCTicketDetailPlaceTypeTheatreDownStairs: {
            UISegmentedControl *seg = [[UISegmentedControl alloc] initWithItems:@[@"楼上",@"楼下"]];
            seg.frame = CGRectMake((kMainFrameWidth-120)/2, CGRectGetMaxY(self.titleLabel.frame)+8, 120, 35);
            seg.selectedSegmentIndex = 0;
            [seg addTarget:self action:@selector(segValueChanged:) forControlEvents:UIControlEventValueChanged];
            [self.scrollView addSubview:seg];
            
            hall = [[MCTicketDetailPlaceMusicHallView  alloc] initWithFrame:CGRectMake((kMainFrameWidth -283)/2, CGRectGetMaxY(seg.frame)+8, 283, 260) type:MCTicketDetailPlaceTypeTheatreUpStairs];
            hall.delegate = self;
            [self.scrollView addSubview:hall];
            
            break;
        }
    }
    self.titleLabel.text = [NSString stringWithFormat:@"%@示意图",_all.concert_hall];
    
    NSMutableString *string = [[NSMutableString alloc] init];
    for (MCTicketAreaModel *area in _array) {
        [string appendString:[NSString stringWithFormat:@"%@：%@\n",area.area_name,area.area_price]];
    }
    
    UILabel *_lblPrice = [[UILabel alloc] initWithFrame:CGRectMake((kMainFrameWidth-200)/2, CGRectGetMaxY(hall.frame)+15, 200, 150)];
    _lblPrice.textAlignment = NSTextAlignmentLeft;
    _lblPrice.numberOfLines = 0;
    _lblPrice.text = string;
    [_lblPrice sizeToFit];
    CGRect frame = _lblPrice.frame;
    _lblPrice.frame = CGRectMake((kMainFrameWidth-frame.size.width)/2, frame.origin.y, frame.size.width, frame.size.height);
    [self.scrollView addSubview:_lblPrice];
    
}


- (void)segValueChanged:(UISegmentedControl *)seg{
    [hall removeFromSuperview];
    hall = nil;
    NSInteger index = seg.selectedSegmentIndex;
    if (index == 0) {
        hall = [[MCTicketDetailPlaceMusicHallView  alloc] initWithFrame:CGRectMake((kMainFrameWidth -283)/2, CGRectGetMaxY(seg.frame)+8, 283, 260) type:MCTicketDetailPlaceTypeTheatreUpStairs];
        hall.delegate = self;
        [self.scrollView addSubview:hall];
    }else if (index == 1) {
        hall = [[MCTicketDetailPlaceMusicHallView  alloc] initWithFrame:CGRectMake((kMainFrameWidth -283)/2, CGRectGetMaxY(seg.frame)+8, 283, 260) type:MCTicketDetailPlaceTypeTheatreDownStairs];
        hall.delegate = self;
        [self.scrollView addSubview:hall];
    }
}


- (void)clickedAtIndex:(NSInteger)index areaName:(NSString *)name showName:(NSString *)showName{
    NSString *areaId;
//    NSString *areaName;
    
    for (MCTicketAreaModel *area in _array) {
        if ([area.area_name containsString:name]) {
            areaId = area.area_id;
//            areaName = area.area_name;
        }
    }
    
//    NSLog(@"showName=%@",showName);
    
    MCTicketSeatViewController *seat = [[MCTicketSeatViewController alloc] initWithShowTimeId:_showTime.id areaId:areaId areaName:showName];
    seat.showId = _showId;
    [self.navigationController pushViewController:seat animated:YES];
    
    
}







@end
