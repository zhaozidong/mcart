//
//  MCTicketOrderViewController.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/10.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseViewController.h"

@interface MCTicketOrderViewController : MCBaseViewController

- (instancetype)initWithShowId:(NSString *)index showTimeId:(NSString *)showTimeId areaName:(NSString *)areaName seat:(NSArray *)arrSeat extAddress:(BOOL)isExtAddress;


@end
