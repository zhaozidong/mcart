//
//  MCTicketPayViewController.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/10.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseViewController.h"
#import "MCTicketOrderModel.h"

@interface MCTicketPayViewController : MCBaseViewController

@property (nonatomic, assign) BOOL showTicketList;

//- (instancetype)initWithPayInfo:(MCTicketOrderModel *)info;

- (instancetype)initWithOrderId:(NSString *)orderId;


@end
