//
//  MCTicketShowDetailViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/24.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCTicketShowDetailViewController.h"
#import "MCTicketDetailBottomView.h"
#import "MCTicketShowDetailTitleCell.h"
#import "MCTicketShowDetailCoverCell.h"
#import "MCTicketShowDetailTicketCell.h"
#import "MCTicketPlaceViewController.h"
#import "MCTicketSeatViewController.h"
#import "MCTicketPanicBuyingView.h"
#import "MCTicketViewModel.h"
#import "MCTicketAreaViewModel.h"
#import "MCTicketOrderViewModel.h"
#import "MCTicketPayViewController.h"
#import "MCTicketShowDetailWebCell.h"
#import "MCLoginViewModel.h"
#import "MCPersonalTicketViewController.h"
#import <MWPhotoBrowser.h>


static NSString *const titleIdentifier = @"titleCell";
static NSString *const coverIdentifier = @"coverCell";
static NSString *const ticketIdentifier = @"ticketCell";
static NSString *const webIdentifier = @"webCell";


@interface MCTicketShowDetailViewController ()<UITableViewDataSource,UITableViewDelegate,MCTicketShowDetailTicketCellDelegate,MCTicketDetailBottomDelegate,MCTicketPanicBuyDelegate,MCTicketShowDetailWebCellDelegate,UIAlertViewDelegate,MCTicketShowDetailCoverCellDelegate,MCTicketShowDetailTitleCellDelegate,UIWebViewDelegate>{
    __block MCTicketShowModel *_show;
    __block NSArray *_arrPopup;
    MCTicketShowTimeModel *_selectShowTime;
    __block MCTicketOrderModel *_order;
}

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) MCTicketDetailBottomView *bottom;

@property (nonatomic, retain) UIButton *shareButton;

@property (nonatomic, retain) UIWebView *webView;


@end

@implementation MCTicketShowDetailViewController

- (instancetype)initWithShow:(MCTicketShowModel *)show{
    self = [super init];
    if (self) {
        _show = show;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    self.title = @"演出详情";
    [self setTitle:@"演出详情" withStyle:MCNavStyleBlack];
    
    self.edgesForExtendedLayout=UIRectEdgeAll;
    self.automaticallyAdjustsScrollViewInsets = NO;
    
//    [self setTitle:@"演出详情" withColor:kWhiteColor];
    
    [self.view addSubview:self.tableView];
    
    [self.view addSubview:self.bottom];
    
    [self getShowDetail];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getShowDetail) name:@"kRefreshTicketDetailDataNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(presentPersonalTicket) name:@"kAppShowPersonalTicketNotification" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (MCTicketDetailBottomView *)bottom{
    if (!_bottom) {
        _bottom = [[MCTicketDetailBottomView alloc] initWithFrame:CGRectMake(0, kMainFrameHeight - 50, kMainFrameWidth, 50)];
        _bottom.delegate = self;
    }
    return _bottom;
}

- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth, kMainFrameHeight - 50) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource=self;
        
        _tableView.contentOffset=CGPointMake(0, 0);
        _tableView.contentInset=UIEdgeInsetsZero;
        
        _tableView.tableHeaderView=[[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth, CGFLOAT_MIN)];
        
        [_tableView registerClass:[MCTicketShowDetailTitleCell class] forCellReuseIdentifier:titleIdentifier];
        [_tableView registerClass:[MCTicketShowDetailCoverCell class] forCellReuseIdentifier:coverIdentifier];
        [_tableView registerClass:[MCTicketShowDetailTicketCell class] forCellReuseIdentifier:ticketIdentifier];
        [_tableView registerClass:[MCTicketShowDetailWebCell class] forCellReuseIdentifier:webIdentifier];
    }
    return _tableView;
}

- (UIWebView *)webView {
    if (!_webView) {
        _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth, 400)];
        _webView.delegate = self;
        _webView.scrollView.scrollEnabled = NO;
    }
    return _webView;
}

#pragma mark Notification

- (void)getShowDetail{
    __weak typeof(self) weakSelf = self;
    [MCMessageUtilities showProgressMessage:@""];
    [[MCTicketViewModel sharedInstance] getShowDetail:_show.id success:^(id resquestData) {
        [MCMessageUtilities dismissHUD];
        MCTicketModel *ticket = resquestData;
        _show = [ticket.list objectAtIndex:0];
//        [weakSelf.tableView reloadData];
        [weakSelf.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:kURLPath(_show.descr_url)]]];
    } failure:^(NSError *error) {
        [MCMessageUtilities dismissHUD];
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}

- (void)presentPersonalTicket{
    MCPersonalTicketViewController *ticket = [[MCPersonalTicketViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:ticket];
    [self presentViewController:nav animated:YES completion:nil];
}

#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        MCTicketShowDetailTitleCell *title=[tableView dequeueReusableCellWithIdentifier:titleIdentifier];
        title.delegate = self;
        [title updateWithShow:_show];
        return title;
    }else if (indexPath.section == 1) {
        MCTicketShowDetailCoverCell *coverCell = [tableView dequeueReusableCellWithIdentifier:coverIdentifier];
        coverCell.delegate = self;
        [coverCell updateWithShow:_show];
        return coverCell;
    }else if (indexPath.section == 2) {
        MCTicketShowDetailTicketCell *ticketCell = [tableView dequeueReusableCellWithIdentifier:ticketIdentifier];
        ticketCell.delegate = self;
        [ticketCell updateWithShow:_show];
        return ticketCell;
    }else if (indexPath.section == 3) {//TODO:去掉
        MCTicketShowDetailWebCell *webCell = [tableView dequeueReusableCellWithIdentifier:webIdentifier];
        webCell.delegate = self;
        [webCell updateWithShow:_show];
        return webCell;
    }
    return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return [MCTicketShowDetailTitleCell cellHeight];
    }else if (indexPath.section == 1) {
        return [MCTicketShowDetailCoverCell cellHeight];
    }else if (indexPath.section == 2) {
        return _show.showtimes.count * 80;
    }else if (indexPath.section == 3) {//TODO:去掉
        return 400.f;
    }
    return 44.f;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return CGFLOAT_MIN;
    }
    return 10.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 2) {
        return 10;
    }
    return CGFLOAT_MIN;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    if (indexPath.section == 2) {
//        MCTicketPlaceViewController *place = [[MCTicketPlaceViewController alloc] initWithShowtime:[_show.showtimes objectAtIndex:0]];
//        [self.navigationController pushViewController:place animated:YES];
//    }
}

#pragma mark UIWebViewDelegate
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    NSString *result = [webView stringByEvaluatingJavaScriptFromString:@"document.body.offsetHeight;"];
    NSInteger height = [result integerValue];
    self.webView.frame = CGRectMake(0, 0, kMainFrameWidth, height+30);
    self.tableView.tableFooterView = self.webView;
    [self.tableView reloadData];
}

#pragma mark MCTicketShowDetailTitleCellDelegate
- (void)clickedImage:(NSString *)path{
    MWPhoto *photo;
    NSMutableArray *array = [NSMutableArray array];
    if (path) {
        photo = [MWPhoto photoWithURL:[NSURL URLWithString:path]];
        photo.caption = @"";
        [array addObject:photo];
    }
    
    if (array.count > 0) {
        MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithPhotos:array];
        browser.displayActionButton = NO;
        browser.displayNavArrows = NO;
        browser.displaySelectionButtons = NO;
        browser.alwaysShowControls = NO;
        browser.zoomPhotosToFill = YES;
        browser.enableGrid = NO;
        browser.startOnGrid = NO;
        browser.enableSwipeToDismiss = NO;
        browser.autoPlayOnAppear = NO;
        [browser setCurrentPhotoIndex:0];
        [self.navigationController pushViewController:browser animated:YES];
    }
}

#pragma mark MCTicketShowDetailCoverCellDelegate

- (void)clickedItemAtIndex:(NSInteger)index {
    MWPhoto *photo;
    NSMutableArray *array = [NSMutableArray array];
    for (MCTicketShowCoverModel *cover in _show.cover) {
        if (cover.pic_url) {
            photo = [MWPhoto photoWithURL:[NSURL URLWithString:kImagePath(cover.pic_url)]];
            photo.caption = @"";
            [array addObject:photo];
        }
    }
    if (array.count > 0) {
        MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithPhotos:array];
        browser.displayActionButton = NO;
        browser.displayNavArrows = NO;
        browser.displaySelectionButtons = NO;
        browser.alwaysShowControls = NO;
        browser.zoomPhotosToFill = YES;
        browser.enableGrid = NO;
        browser.startOnGrid = NO;
        browser.enableSwipeToDismiss = NO;
        browser.autoPlayOnAppear = NO;
        [browser setCurrentPhotoIndex:0];
        [self.navigationController pushViewController:browser animated:YES];
    }
}

- (void)updateOffsety:(CGFloat)offsety{//TODO:去掉该方法,代理里面也要去掉
    CGPoint point = self.tableView.contentOffset;
    self.tableView.contentOffset = CGPointMake(point.x, point.y+offsety);
}

#pragma mark MCTicketDetailBottomDelegate
- (void)clickedChat:(id)sender{//咨询
    NSString *string = [NSString stringWithFormat:@"telprompt://%@",_show.tele];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:string]];
}

#pragma mark MCTicketShowDetailTicketCellDelegate
- (void)clickedTicket:(MCTicketShowTimeModel *)showtime{
    _selectShowTime = showtime;
    if (![showtime.concert_hall containsString:@"音乐厅"] && ![showtime.concert_hall containsString:@"大剧院"]) {
        MCTicketSeatViewController *seat = [[MCTicketSeatViewController alloc] initWithShowTime:showtime isAll:YES];
        seat.showId = _show.id;
        [self.navigationController pushViewController:seat animated:YES];
    }else{
        MCTicketPlaceViewController *place = [[MCTicketPlaceViewController alloc] initWithShowId:_show.id showTime:showtime];
        [self.navigationController pushViewController:place animated:YES];
    }
}


- (void)clickedShowPop:(MCTicketShowTimeModel *)showtime{
    _selectShowTime = showtime;
    __weak typeof(self) weakSelf = self;
    [[MCTicketAreaViewModel sharedInstance] getAreaList:showtime.id success:^(id resquestData) {
        MCTicketAreaAllModel *all = resquestData;
        _arrPopup= [all.list copy];
        [weakSelf showPopup];
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}

//弹出抢票窗口
- (void)showPopup{
    if (![MCLoginViewModel sharedInstance].isLogin) {
        [self showLoginViewController];
        return;
    }
    MCTicketPanicBuyingView *buy = [[MCTicketPanicBuyingView alloc] initWithArray:_arrPopup];
    [buy updateWithPhone:[MCLoginViewModel sharedInstance].userInfo.cellphone];
    buy.delegate = self;
    [self.view addSubview:buy];
}

///抢购票直接提交订单，然后去支付
- (void)clickedOKWithArea:(MCTicketAreaModel *)area num:(NSInteger)number phone:(NSString *)phone{
    __weak typeof(self) weakSelf = self;
    [[MCTicketOrderViewModel sharedInstance] buySpecialTicket:_selectShowTime.id areaId:area.area_id areaName:area.area_name seatNum:number phone:phone success:^(id resquestData) {
        _order = resquestData;
        [weakSelf payOrder:_order];
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}

- (void)payOrder:(MCTicketOrderModel *)orderInfo{
//    MCTicketPayViewController *pay = [[MCTicketPayViewController alloc] initWithPayInfo:orderInfo];
//    [self.navigationController pushViewController:pay animated:YES];
    
    NSMutableString *seat = [[NSMutableString alloc] initWithString:@"你抢到了 "];
    if (orderInfo && orderInfo.list.count>0) {
        for (MCTicketOrderDetailModel *detail in orderInfo.list) {
            NSString *temp = [NSString stringWithFormat:@"%@ ",detail.sename];
            [seat appendString:temp];
        }
    }
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:seat delegate:self cancelButtonTitle:@"去支付" otherButtonTitles:nil, nil];
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    MCTicketPayViewController *pay = [[MCTicketPayViewController alloc] initWithOrderId:_order.toid];
    pay.showTicketList = YES;
    [self.navigationController pushViewController:pay animated:YES];
}


@end
