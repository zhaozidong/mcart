//
//  MCTicketAllShowViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/13.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCTicketAllShowViewController.h"
#import "MCTicketShowDetailViewController.h"
#import "MCTicketViewModel.h"

#import "MCAppreciateTextCell.h"
#import "MCAppreciatePicCell.h"
#import "MCAppreciateVideoCell.h"
#import "MCAppreciateModel.h"
#import "MCBaseWebViewController.h"
#import "MCShareModel.h"
#import "MCShareWebViewController.h"

static NSString *const videoIdentifier=@"videoCell";
static NSString *const picIdentifier=@"picCell";
static NSString *const textIdentifier=@"textCell";


@interface MCTicketAllShowViewController ()<MCAppreciatePicDelegate>{
    __block NSInteger _pageIndex;
    __block NSMutableArray *_arrData;
}


@end

@implementation MCTicketAllShowViewController

- (instancetype)init{
    self = [super initWithStyle:UITableViewStyleGrouped refreshType:MCTableViewRefreshTypeBottom | MCTableViewRefreshTypeTop];
    if (self) {
        
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"演出一览";
    
    _pageIndex = 1;
    _arrData = [NSMutableArray array];
    
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth, CGFLOAT_MIN)];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"MCAppreciateVideoCell" bundle:nil] forCellReuseIdentifier:videoIdentifier];
    [self.tableView registerClass:[MCAppreciatePicCell class] forCellReuseIdentifier:picIdentifier];
    [self.tableView registerClass:[MCAppreciateTextCell class] forCellReuseIdentifier:textIdentifier];

    [self getData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.backgroundColor=kBlackColor;
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (void)getData{
    __weak typeof(self) weakSelf = self;
    [MCMessageUtilities showProgressMessage:@""];
    [[MCTicketViewModel sharedInstance] getAllShow:_pageIndex success:^(id resquestData) {
        [MCMessageUtilities dismissHUD];
        NSArray *array = resquestData;
        if (_pageIndex == 1 && array.count > 0) {
            _arrData = [array mutableCopy];
            _pageIndex ++;
        }else {
            if (array.count > 0) {
                _pageIndex ++;
                [_arrData addObjectsFromArray:array];
            }
        }
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error) {
        [MCMessageUtilities dismissHUD];
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];
    }];
}

#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _arrData.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MCAppreciateModel *appreciate = [_arrData objectAtIndex:indexPath.section];
    switch (appreciate.class_type) {
        case MCArtInfoShowTypeText: {
            MCAppreciateTextCell *textCell = [tableView dequeueReusableCellWithIdentifier:textIdentifier];
            [textCell updateWithInfo:appreciate];
            return textCell;
            break;
        }
        case MCArtInfoShowTypePic: {
            MCAppreciatePicCell *picCell = [tableView dequeueReusableCellWithIdentifier:picIdentifier];
            picCell.delegate = self;
            [picCell updateWithInfo:appreciate];
            return picCell;
            break;
        }
        case MCArtInfoShowTypeVideo: {
            MCAppreciateVideoCell *video=[tableView dequeueReusableCellWithIdentifier:videoIdentifier];
            [video updateWithInfo:appreciate];
            return video;
            break;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    MCAppreciateModel *appreciate = [_arrData objectAtIndex:indexPath.section];
    switch (appreciate.class_type) {
        case MCArtInfoShowTypeText: {
            return [MCAppreciateTextCell cellHeight];
            break;
        }
        case MCArtInfoShowTypePic: {
            return [MCAppreciatePicCell cellHeight];
            break;
        }
        case MCArtInfoShowTypeVideo: {
            return [MCAppreciateVideoCell cellHeight];
            break;
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    MCAppreciateModel *appreciate = [_arrData objectAtIndex:indexPath.section];
    switch (appreciate.class_type) {
        case MCArtInfoShowTypeText:
        case MCArtInfoShowTypeVideo: {
            MCShareModel *share = [[MCShareModel alloc] init];
            share.title = appreciate.title;
            share.image = appreciate.pic_path;
            share.url = appreciate.share_url;
            share.content = appreciate.title;
            
            MCShareWebViewController *web = [[MCShareWebViewController alloc] initWithUrl:appreciate.share_url shareInfo:share];
            [self.navigationController pushViewController:web animated:YES];
            break;
        }
        case MCArtInfoShowTypePic: {
            MCShareModel *share = [[MCShareModel alloc] init];
            share.title = appreciate.title;
            share.image = [appreciate.imgs_list firstObject];
            share.url = appreciate.share_url;
            share.content = appreciate.title;
            
            MCShareWebViewController *web = [[MCShareWebViewController alloc] initWithUrl:appreciate.share_url shareInfo:share];
            [self.navigationController pushViewController:web animated:YES];
            
            break;
        }
    }
}

#pragma mark MCAppreciatePicDelegate
- (void)clickedItem:(MCAppreciateModel *)info{
    if (info.share_url) {
        MCBaseWebViewController *webView = [[MCBaseWebViewController alloc] init];
        webView.url = info.share_url;
        [self.navigationController pushViewController:webView animated:YES];
    }
}

#pragma mark MJRefresh
- (void)beginRefreshHeader:(MJRefreshHeader *)header{
    [header endRefreshing];
    _pageIndex = 1;
    [_arrData removeAllObjects];
    [self getData];
}

- (void)beginRefreshFooter:(MJRefreshFooter *)footer{
    [footer endRefreshing];
    [self getData];
}

@end
