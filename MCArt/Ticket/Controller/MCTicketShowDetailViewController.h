//
//  MCTicketShowDetailViewController.h
//  MCArt
//
//  Created by Derek.zhao on 16/1/24.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCBaseViewController.h"
#import "MCTicketModel.h"


@interface MCTicketShowDetailViewController : MCBaseViewController




- (instancetype)initWithShow:(MCTicketShowModel *)show;



@end
