//
//  MCTicketSeatViewController.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/8.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseViewController.h"
#import "MCTicketAreaModel.h"
#import "MCTicketModel.h"



@interface MCTicketSeatViewController : MCBaseViewController

@property (nonatomic, strong) NSString *showId;


///从区域过来选作座
- (instancetype)initWithShowTimeId:(NSString *)showTimeId areaId:(NSString *)areaId areaName:(NSString *)areaName;

///电影院直接选座
- (instancetype)initWithShowTime:(MCTicketShowTimeModel *)showTime isAll:(BOOL)isALl;





@end
