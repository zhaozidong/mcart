//
//  MCTicketOrderViewModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/9.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCTicketOrderViewModel.h"
#import "MCNetworkManager.h"
#import "MCLoginViewModel.h"
#import "MCTicketOrderModel.h"
#import "MCTicketAreaModel.h"


@implementation MCTicketOrderViewModel

SINGLETON_FOR_CLASS(MCTicketOrderViewModel);


- (void)buyNormalTicket:(NSString *)index seat:(NSArray *)arrSeat phone:(NSString *)phone success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    
    NSMutableString *strSeatId = [[NSMutableString alloc] init];
    for (MCTicketSeatDetailModel *seat in arrSeat) {
        [strSeatId appendString:seat.seat_id];
        [strSeatId appendString:@","];
    }
    
    NSDictionary *dict = @{@"id":index,@"type":@(1),@"seat_id":strSeatId,@"phone":phone,@"userid":[MCLoginViewModel sharedInstance].userInfo.id};
    [[MCNetworkManager sharedInstance] postWithURL:@"appShow/buyTicket" parameter:dict success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            NSError *err;
            MCTicketOrderModel *order = [MTLJSONAdapter modelOfClass:[MCTicketOrderModel class] fromJSONDictionary: resquestData error:&err];
            if (!err) {
                if (successBlock) {
                    successBlock(order);
                }
            }else{
                if (failureBlock) {
                    failureBlock(err);
                }
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}


- (void)buySpecialTicket:(NSString *)index areaId:(NSString *)areaId areaName:(NSString *)areaName seatNum:(NSInteger)seatNum phone:(NSString *)phone success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    NSDictionary *dict = @{@"id":index,@"type":@(2),@"area_id":areaId,@"area_name":areaName,@"seat_num":@(seatNum),@"phone":phone,@"userid":[MCLoginViewModel sharedInstance].userInfo.id};
    [[MCNetworkManager sharedInstance] postWithURL:@"appShow/buyTicket" parameter:dict success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            NSError *err;
            MCTicketOrderModel *order = [MTLJSONAdapter modelOfClass:[MCTicketOrderModel class] fromJSONDictionary: resquestData error:&err];
            if (!err) {
                if (successBlock) {
                    successBlock(order);
                }
            }else{
                if (failureBlock) {
                    failureBlock(err);
                }
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}

- (void)getTicktPayResult:(NSString *)orderId success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    [[MCNetworkManager sharedInstance] postWithURL:@"appShow/getOrderPayResult" parameter:@{@"id":orderId} success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            NSError *err;
            MCTicketOrderModel *order = [MTLJSONAdapter modelOfClass:[MCTicketOrderModel class] fromJSONDictionary: resquestData error:&err];
            if (!err) {
                if (successBlock) {
                    successBlock(order);
                }
            }else{
                if (failureBlock) {
                    failureBlock(err);
                }
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}

@end
