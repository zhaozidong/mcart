//
//  MCTicketOrderViewModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/9.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MCTicketOrderViewModel : NSObject

+ (instancetype)sharedInstance;

///购买普通票
- (void)buyNormalTicket:(NSString *)index seat:(NSArray *)arrSeat phone:(NSString *)phone success:(successBlock)successBlock failure:(failureBlock)failureBlock;

///购买特价票
- (void)buySpecialTicket:(NSString *)index areaId:(NSString *)areaId areaName:(NSString *)areaName seatNum:(NSInteger)seatNum phone:(NSString *)phone success:(successBlock)successBlock failure:(failureBlock)failureBlock;

///查询购票支付结果
- (void)getTicktPayResult:(NSString *)orderId success:(successBlock)successBlock failure:(failureBlock)failureBlock;


@end
