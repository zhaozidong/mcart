//
//  MCTicketAreaViewModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/7.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCTicketAreaViewModel.h"
#import "MCNetworkManager.h"
#import "MCTicketAreaModel.h"


@implementation MCTicketAreaViewModel

SINGLETON_FOR_CLASS(MCTicketAreaViewModel);

- (void)getAreaList:(NSString *)index success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    [[MCNetworkManager sharedInstance] postWithURL:@"appShow/getShowAreaList" parameter:@{@"id":index} success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            NSError *err;
            MCTicketAreaAllModel *all = [MTLJSONAdapter modelOfClass:[MCTicketAreaAllModel class] fromJSONDictionary:resquestData error:&err];
            if (!err) {
                if (successBlock) {
                    successBlock(all);
                }
            }else{
                if (failureBlock) {
                    failureBlock(err);
                }
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"response"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}

- (void)getAreaSeatByShowId:(NSString *)showId areaId:(NSString *)areaId areaName:(NSString *)areaName isAll:(BOOL)isAll success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    
    NSDictionary *dict =@{@"id":showId,@"area_id":areaId,@"area_name":areaName,@"area_all":isAll?@(1):@(0)};
    
    [[MCNetworkManager sharedInstance] postWithURL:@"appShow/getAreaSeatList" parameter:dict success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            NSError *err;
            MCTicketSeatModel *seat = [MTLJSONAdapter modelOfClass:[MCTicketSeatModel class] fromJSONDictionary:  resquestData error:&err];
            if (!err) {
                if (successBlock) {
                    successBlock(seat);
                }
            }else{
                if (failureBlock) {
                    failureBlock(err);
                }
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"response"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}




@end
