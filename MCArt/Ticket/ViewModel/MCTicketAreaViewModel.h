//
//  MCTicketAreaViewModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/7.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MCTicketAreaViewModel : NSObject

+ (instancetype)sharedInstance;


- (void)getAreaList:(NSString *)index success:(successBlock)successBlock failure:(failureBlock)failureBlock;


- (void)getAreaSeatByShowId:(NSString *)showId areaId:(NSString *)areaId areaName:(NSString *)areaName isAll:(BOOL)isAll success:(successBlock)successBlock failure:(failureBlock)failureBlock;






@end
