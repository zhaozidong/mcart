//
//  MCTicketViewModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/28.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MCTicketModel.h"

typedef NS_ENUM(NSInteger,MCTicketShowType) {
    MCTicketShowTypeWillBegin,
    MCTicketShowTypeAll
};




@interface MCTicketViewModel : NSObject

+ (instancetype)sharedInstance;

///查询演出场次列表及详情
- (void)getShowList:(MCTicketShowType)type page:(NSInteger)page success:(successBlock)successBlock failure:(failureBlock)failureBlock;

///查询单场演出详情
- (void)getShowDetail:(NSString *)index success:(successBlock)successBlock failure:(failureBlock)failureBlock;

///查询演出一览
- (void)getAllShow:(NSInteger)pageIndex success:(successBlock)successBlock failure:(failureBlock)failureBlock;



@end
