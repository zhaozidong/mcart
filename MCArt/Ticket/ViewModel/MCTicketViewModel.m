//
//  MCTicketViewModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/28.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCTicketViewModel.h"
#import "MCNetworkManager.h"
#import "MCTicketModel.h"
#import "MCAppreciateModel.h"


@implementation MCTicketViewModel

SINGLETON_FOR_CLASS(MCTicketViewModel);

///查询演出场次列表及详情
- (void)getShowList:(MCTicketShowType)type page:(NSInteger)page success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    NSDictionary *dict;
    switch (type) {
        case MCTicketShowTypeWillBegin: {
            dict = @{@"stat":@"1",@"page":@(page),@"pageSize":MC_PAGE_SIZE};
            break;
        }
        case MCTicketShowTypeAll: {
            dict = @{@"stat":@"2",@"page":@(page),@"pageSize":MC_PAGE_SIZE};
            break;
        }
    }
    
    [[MCNetworkManager sharedInstance] postWithURL:@"appShow/getShowList" parameter:dict success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            NSError *err;
            MCTicketModel *ticket = [MTLJSONAdapter modelOfClass:[MCTicketModel class] fromJSONDictionary:resquestData error:&err];
            if (!err) {
                if (successBlock) {
                    successBlock(ticket);
                }
            }else{
                if (failureBlock) {
                    failureBlock(err);
                }
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}

///查询单场演出详情
- (void)getShowDetail:(NSString *)index success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    NSDictionary *dict = @{@"id":index,@"page":@1,@"pageSize":MC_PAGE_SIZE};
    [[MCNetworkManager sharedInstance] postWithURL:@"appShow/getShowList" parameter:dict success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            NSError *err;
            MCTicketModel *ticket = [MTLJSONAdapter modelOfClass:[MCTicketModel class] fromJSONDictionary:resquestData error:&err];
            if (!err) {
                if (successBlock) {
                    successBlock(ticket);
                }
            }else{
                if (failureBlock) {
                    failureBlock(err);
                }
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}

///查询演出一览
- (void)getAllShow:(NSInteger)pageIndex success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    NSDictionary *dict = @{@"page":@(pageIndex),@"pageSize":MC_PAGE_SIZE};
    [[MCNetworkManager sharedInstance] postWithURL:@"appNews/getYcyl" parameter:dict success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            NSError *err=nil;
            //返回的数据结构和艺术欣赏首页是一样的，所以就用同一个Model
            NSArray *array=[MTLJSONAdapter modelsOfClass:[MCAppreciateModel class] fromJSONArray:[resquestData objectForKey:@"list"] error:&err];
            if (!err) {
                successBlock(array);
            }else{
                [MCMessageUtilities showErrorMessage:err.description];
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}


@end
