//
//  MCTicketAreaModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/7.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Mantle/Mantle.h>

typedef NS_ENUM(NSInteger,MCSeatStatus) {
    MCSeatStatusNormal,
    MCSeatStatusHidden,
    MCSeatStatusLocked,
    MCSeatStatusSelected
};



@interface MCTicketAreaModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, copy) NSString *area_id;

@property (nonatomic, copy) NSString *area_name;

@property (nonatomic, copy) NSString *area_price;

@property (nonatomic, copy) NSString *area_price_special;

@property (nonatomic, copy) NSString *area_color;

@property (nonatomic, copy) NSString *rest_seat;

@end



@interface MCTicketAreaAllModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, copy) NSString *concert_hall;

@property (nonatomic, copy) NSArray *list;

@end






///////////////////////////////////////////////////////////////


@interface MCTicketSeatDetailModel : MTLModel<MTLJSONSerializing>


@property (nonatomic, copy) NSString *seat_id;

@property (nonatomic, copy) NSString *serow;


@property (nonatomic, copy) NSString *secolumn;


@property (nonatomic, copy) NSString *sename;


@property (nonatomic, assign) MCSeatStatus sestatus;


@property (nonatomic, copy) NSString *price;


@end







@interface  MCTicketSeatModel: MTLModel<MTLJSONSerializing>

@property (nonatomic, copy) NSString *area_id;

@property (nonatomic, copy) NSString *area_name;

@property (nonatomic, copy) NSString *area_price;

@property (nonatomic, copy) NSArray *list;



@end

