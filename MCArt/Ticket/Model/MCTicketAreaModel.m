//
//  MCTicketAreaModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/7.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCTicketAreaModel.h"

@implementation MCTicketAreaModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"area_id":@"area_id",
            @"area_name":@"area_name",
            @"area_price":@"area_price",
            @"area_price_special":@"area_price_special",
            @"area_color":@"area_color",
            @"rest_seat":@"rest_seat"};
}


@end

@implementation MCTicketAreaAllModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"concert_hall":@"concert_hall",
             @"list":@"list"};
}

+ (NSValueTransformer *)listJSONTransformer{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        NSArray *jsonArray = value;
        NSMutableArray *array = [NSMutableArray array];
        [jsonArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            MCTicketAreaModel *area = [MTLJSONAdapter modelOfClass:[MCTicketAreaModel class] fromJSONDictionary:obj error:error];
            [array addObject:area];
        }];
        return array;
    }];
}

@end







@implementation MCTicketSeatDetailModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"seat_id":@"seat_id",
             @"serow":@"serow",
             @"secolumn":@"secolumn",
             @"sename":@"sename",
             @"sestatus":@"sestatus",
             @"price":@"price"};
}

+ (NSValueTransformer *)sestatusJSONTransformer{
    return [NSValueTransformer mtl_valueMappingTransformerWithDictionary:@{@"0":@(MCSeatStatusNormal),
                                                                           @"1":@(MCSeatStatusHidden),
                                                                           @"2":@(MCSeatStatusLocked)}];
}



@end


@implementation MCTicketSeatModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"area_id":@"area_id",
             @"area_name":@"area_name",
             @"area_price":@"area_price",
             @"list":@"list"};
}

+ (NSValueTransformer *)listJSONTransformer{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        NSArray *jsonArray = value;
        NSMutableArray *array = [NSMutableArray array];
        [jsonArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSArray *arrDetail = [MTLJSONAdapter modelsOfClass:[MCTicketSeatDetailModel class] fromJSONArray:obj error:error];
            [array addObject:arrDetail];
        }];
        return array;
    }];
}


@end
