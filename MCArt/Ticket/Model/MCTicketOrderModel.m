//
//  MCTicketOrderModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/9.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCTicketOrderModel.h"

@implementation MCTicketOrderDetailModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"area_id":@"area_id",
             @"area_name":@"area_name",
             @"area_price":@"area_price",
             @"seat_id":@"seat_id",
             @"serow":@"serow",
             @"secolumn":@"secolumn",
             @"sename":@"sename",
             @"sestatus":@"sestatus"};
}

@end





@implementation MCTicketOrderModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"toid":@"toid",
             @"orderno":@"orderno",
             @"ordername":@"ordername",
             @"resttime":@"resttime",
             @"timestamp":@"timestamp",
             @"address":@"address",
             @"time":@"time",
             @"total_price":@"total_price",
             @"result":@"result",
             @"ali_order_info":@"ali_order_info",
             @"list":@"list"};
}

+ (NSValueTransformer *)resultJSONTransformer{
    return [NSValueTransformer mtl_valueMappingTransformerWithDictionary:@{@"0":@(MCTicketOrderPayStatusNotPay),
                                                                           @"1":@(MCTicketOrderPayStatusPaid),
                                                                           @"3":@(MCTicketOrderPayStatusOverTime)}];
}

+ (NSValueTransformer *)listJSONTransformer{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        NSArray *jsonArray = value;
        NSMutableArray *array = [NSMutableArray array];
        [jsonArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            MCTicketOrderDetailModel *detail = [MTLJSONAdapter modelOfClass:[MCTicketOrderDetailModel class] fromJSONDictionary:obj error:error];
            [array addObject:detail];
        }];
        return array;
    }];
}

@end




@implementation MCTicketOrderShowModel



@end



