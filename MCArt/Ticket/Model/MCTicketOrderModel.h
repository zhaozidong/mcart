//
//  MCTicketOrderModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/9.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Mantle/Mantle.h>

typedef NS_ENUM(NSInteger, MCTicketOrderPayStatus) {
    MCTicketOrderPayStatusNotPay,
    MCTicketOrderPayStatusPaid,
    MCTicketOrderPayStatusOverTime
};



@interface MCTicketOrderDetailModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, copy) NSString *area_id;

@property (nonatomic, copy) NSString *area_name;

@property (nonatomic, copy) NSString *area_price;

@property (nonatomic, copy) NSString *seat_id;

@property (nonatomic, copy) NSString *serow;

@property (nonatomic, copy) NSString *secolumn;

@property (nonatomic, copy) NSString *sename;

@property (nonatomic, copy) NSString *sestatus;

@end



@interface MCTicketOrderModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, copy) NSString *toid;

@property (nonatomic, copy) NSString *orderno;

@property (nonatomic, copy) NSString *ordername;

@property (nonatomic, copy) NSString *resttime;

@property (nonatomic, copy) NSString *timestamp;

@property (nonatomic, copy) NSString *address;

@property (nonatomic, copy) NSString *time;

@property (nonatomic, copy) NSString *total_price;

@property (nonatomic, assign) MCTicketOrderPayStatus result;

@property (nonatomic, copy) NSString *ali_order_info;

@property (nonatomic, copy) NSArray *list;

@end



@interface MCTicketOrderShowModel : MTLModel

@property (nonatomic, copy) NSString *showId;

@property (nonatomic, copy) NSString *showTimeId;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *address;

@property (nonatomic, copy) NSString *areaName;

@property (nonatomic, copy) NSString *time;

@property (nonatomic, copy) NSArray *seat;

@end

