//
//  MCTicketModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/25.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCTicketModel.h"

@implementation MCTicketBannerModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"id":@"id",
             @"banner_pic":@"banner_pic",
             @"is_video":@"is_video",
             @"title":@"title"
             };
}

@end



@implementation MCTicketShowTimeModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"id":@"id",
             @"type":@"type",
             @"time":@"time",
             @"concert_hall":@"concert_hall",
             @"stat":@"stat",
             @"special_limit_seat":@"special_limit_seat",
             @"special_rest_seat":@"special_rest_seat",
             @"resttime_start":@"resttime_start",
             @"resttime_end":@"resttime_end"};
}

+ (NSValueTransformer *)typeJSONTransformer{
    return [NSValueTransformer mtl_valueMappingTransformerWithDictionary:@{@"0":@(MCTicketTypeNormal),
                                                                           @"1":@(MCTicketTypeSpecialPrice),
                                                                           @"2":@(MCTicketTypeSeckilling),
                                                                           }];
}

+ (NSValueTransformer *)statJSONTransformer {
    return [NSValueTransformer mtl_valueMappingTransformerWithDictionary:@{@"0":@(MCTicketSellStatusSelling),
                                                                           @"1":@(MCTicketSellStatusSoldout)}];
}


@end


@implementation MCTicketShowCoverModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"pic_url":@"pic_url",
             @"is_video":@"is_video"};
}

@end



@implementation MCTicketShowModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"id":@"id",
             @"title":@"title",
             @"time":@"time",
             @"address":@"address",
             @"status":@"stat",//这里需要注意一下，和返回的字段名称不一样
             @"type":@"type",
             @"cover":@"cover",
             @"minprice":@"minprice",
             @"price":@"price",
             @"banner_pic":@"banner_pic",
             @"front_pic":@"front_pic",
             @"content":@"content",
             @"hint":@"hint",
             @"tele":@"tele",
             @"discount":@"discount",
             @"showtimes":@"showtimes",
             @"descr_url":@"descr_url"
             };
}

+ (NSValueTransformer *)statusJSONTransformer{
    return [NSValueTransformer mtl_valueMappingTransformerWithDictionary:@{@"0":@(MCTicketSellStatusSelling),
                                                                           @"1":@(MCTicketSellStatusSoldout)}];
    
}

+ (NSValueTransformer *)typeJSONTransformer{
    return [NSValueTransformer mtl_valueMappingTransformerWithDictionary:@{@"0":@(MCTicketTypeNormal),
                                                                           @"1":@(MCTicketTypeSpecialPrice),
                                                                           @"2":@(MCTicketTypeSeckilling),
                                                                           @"3":@(MCTicketTypeSpecialAndSeckilling)}];
}

+ (NSValueTransformer *)coverJSONTransformer{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        NSArray *jsonArray = value;
        NSMutableArray *array = [NSMutableArray array];
        [jsonArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            MCTicketShowCoverModel *banner = [MTLJSONAdapter modelOfClass:[MCTicketShowCoverModel class] fromJSONDictionary:obj error:error];
            [array addObject:banner];
        }];
        return array;
    }];
}

+ (NSValueTransformer *)showtimesJSONTransformer{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        NSArray *jsonArray = value;
        NSMutableArray *array = [NSMutableArray array];
        [jsonArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            MCTicketShowTimeModel *showTime = [MTLJSONAdapter modelOfClass:[MCTicketShowTimeModel class] fromJSONDictionary:obj error:error];
            [array addObject:showTime];
        }];
        return array;
    }];
}

@end



@implementation MCTicketModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"banner":@"banner",
             @"list":@"list"};
}

+ (NSValueTransformer *)bannerJSONTransformer{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        NSArray *jsonArray = value;
        NSMutableArray *array = [NSMutableArray array];
        [jsonArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            MCTicketBannerModel *banner = [MTLJSONAdapter modelOfClass:[MCTicketBannerModel class] fromJSONDictionary:obj error:error];
            [array addObject:banner];
        }];
        return array;
    }];
}


+ (NSValueTransformer *)listJSONTransformer{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        NSArray *jsonArray = value;
        NSMutableArray *array = [NSMutableArray array];
        [jsonArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            MCTicketShowModel *show = [MTLJSONAdapter modelOfClass:[MCTicketShowModel class] fromJSONDictionary:obj error:error];
            [array addObject:show];
        }];
        return array;
    }];
}


@end



