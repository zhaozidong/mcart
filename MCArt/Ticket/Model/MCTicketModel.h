//
//  MCTicketModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/25.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Mantle/Mantle.h>

typedef NS_ENUM(NSInteger,MCTicketSellStatus) {
    MCTicketSellStatusSelling = 1,
    MCTicketSellStatusSoldout
};


typedef NS_ENUM(NSInteger,MCTicketType) {
    MCTicketTypeNormal,
    MCTicketTypeSpecialPrice,
    MCTicketTypeSeckilling,
    MCTicketTypeSpecialAndSeckilling
};


@interface MCTicketBannerModel : MTLModel<MTLJSONSerializing>

@property(nonatomic, copy) NSString *id;

@property(nonatomic, copy) NSString *banner_pic;

@property(nonatomic, copy) NSString *is_video;

@property(nonatomic, copy) NSString *title;

@end


@interface MCTicketShowTimeModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, copy) NSString *id;

@property (nonatomic, assign) MCTicketType type;

@property (nonatomic, copy) NSString *time;

@property (nonatomic, copy) NSString *concert_hall;

@property (nonatomic, assign) MCTicketSellStatus stat;//  0：售票中      1：结束售票

@property (nonatomic, copy) NSString *special_limit_seat;

@property (nonatomic, copy) NSString *special_rest_seat;

@property (nonatomic, copy) NSString *resttime_start;

@property (nonatomic, copy) NSString *resttime_end;

@end


@interface MCTicketShowCoverModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, copy) NSString *pic_url;

@property (nonatomic, copy) NSString *is_video;

@end


@interface MCTicketShowModel : MTLModel<MTLJSONSerializing>

@property(nonatomic, copy) NSString *id;

@property(nonatomic, copy) NSString *title;

@property(nonatomic, copy) NSString *time;

@property(nonatomic, copy) NSString *address;

@property(nonatomic, assign) MCTicketSellStatus status;//  1：售票中      2：结束售票

@property(nonatomic, assign) MCTicketType type;//  0：普通票 1：特价票 2：秒杀票 3：特价+秒杀

@property(nonatomic, copy) NSArray *cover;

@property(nonatomic, copy) NSString *minprice;

@property(nonatomic, copy) NSString *price;

@property(nonatomic, copy) NSString *banner_pic;

@property(nonatomic, copy) NSString *front_pic;

@property(nonatomic, copy) NSString *content;

@property(nonatomic, copy) NSString *hint;

@property(nonatomic, copy) NSString *tele;

@property(nonatomic, copy) NSString *discount;

@property(nonatomic, copy) NSArray *showtimes;

@property(nonatomic, copy) NSString *descr_url;

@end


@interface MCTicketModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, copy) NSArray *banner;

@property (nonatomic, copy) NSArray *list;

@end

