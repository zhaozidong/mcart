//
//  MCTicketShowDetailCoverCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/7.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCTicketShowDetailCoverCell.h"

static NSString *const collectionIdentifier = @"collectionCell";


@interface MCTicketShowDetailCoverCell ()<UICollectionViewDataSource,UICollectionViewDelegate>{
    UICollectionView *_collectionView;
    MCTicketShowModel *_show;
}

@end


@implementation MCTicketShowDetailCoverCell
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (void)setUpSubViews{
    if (!_collectionView) {
        UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
        flow.itemSize = CGSizeMake(130, 100);
        flow.minimumInteritemSpacing = 8.f;
        flow.minimumLineSpacing = 8.f;
        flow.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);
        flow.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectInset(self.bounds, 8, 0) collectionViewLayout:flow];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = kWhiteColor;
        
        [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:collectionIdentifier];
    }
    [self.contentView addSubview:_collectionView];
}

- (void)updateWithShow:(MCTicketShowModel *)show{
    if (!show) {
        return;
    }
    
    _show = show;
    if (show.cover.count > 0) {
        [_collectionView reloadData];
    }
}

- (void)layoutSubviews{
    [super layoutSubviews];
    _collectionView.frame = CGRectInset(self.bounds, 8, 0) ;

}

+ (CGFloat)cellHeight{
    return 120.f;
}


#pragma mark UICollectionViewDataSource
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (_show && _show.cover.count > 0) {
        return _show.cover.count;
    }
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:collectionIdentifier forIndexPath:indexPath];
    if (![cell.contentView viewWithTag:100]) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:cell.contentView.frame];
        imageView.tag = 100;
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
        [cell.contentView addSubview:imageView];
    }
    MCTicketShowCoverModel *cover = [_show.cover objectAtIndex:indexPath.row];
    UIImageView *imgView = [cell.contentView viewWithTag:100];
    [imgView sd_setImageWithURL:[NSURL URLWithString:kImagePath(cover.pic_url)] placeholderImage:[UIImage imageNamed:@"info_loading_title"]];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedItemAtIndex:)]) {
        [self.delegate clickedItemAtIndex:indexPath.row];
    }
}

@end
