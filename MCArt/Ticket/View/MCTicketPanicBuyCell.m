//
//  MCTicketPanicBuyCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/9.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCTicketPanicBuyCell.h"

@implementation MCTicketPanicBuyCell{
    UIImageView *_imgCheck;
    UILabel *_lblArea;
//    UIButton *_btnCheck;
    UILabel *_lblPrice;
    UILabel *_lblOrginalPrice;
    UILabel *_lblRestNum;
    
    MCTicketAreaModel *_area;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
    
//    _btnCheck.selected = selected;
    if (selected){
        _imgCheck.image = [UIImage imageNamed:@"shoppingCard_checked"];
    }else{
        _imgCheck.image = [UIImage imageNamed:@"shoppingCard_noCheck"];
    }
}

- (void)setUpSubViews{
//    if (!_btnCheck) {
//        _btnCheck = [[UIButton alloc] initWithFrame:CGRectMake(8, 5, 80, 30)];
//        [_btnCheck setImage:[UIImage imageNamed:@"shoppingCard_noCheck"] forState:UIControlStateNormal];
//        [_btnCheck setImage:[UIImage imageNamed:@"shoppingCard_checked"] forState:UIControlStateSelected];
//        [_btnCheck setTitleColor:kBlackColor forState:UIControlStateNormal];
////        [_btnCheck addTarget:self action:@selector(btnDidClickCheck:) forControlEvents:UIControlEventTouchUpInside];
//    }
//    [self.contentView addSubview:_btnCheck];
    
    
    if (!_imgCheck) {
        _imgCheck = [[UIImageView alloc] initWithFrame:CGRectMake(8, 5, 30, 30)];
        _imgCheck.image = [UIImage imageNamed:@"shoppingCard_noCheck"];
    }
    [self.contentView addSubview:_imgCheck];
    
    if (!_lblArea) {
        _lblArea = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_imgCheck.frame)+2, 5, 50, 30)];
        _lblArea.font = kAppFont(16.f);
    }
    [self.contentView addSubview:_lblArea];
    
    
    if (!_lblPrice) {
        _lblPrice = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, 30, 20)];
        _lblPrice.textColor = [UIColor redColor];
    }
    [self.contentView addSubview:_lblPrice];
    
    if (!_lblOrginalPrice) {
        _lblOrginalPrice = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_lblPrice.frame)+2, 5, 50, 20)];
        _lblOrginalPrice.textColor = [UIColor lightGrayColor];
    }
    [self.contentView addSubview:_lblOrginalPrice];
    
    if (!_lblRestNum) {
        _lblRestNum = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_lblOrginalPrice.frame)+2, 5, 60, 20)];
    }
    [self.contentView addSubview:_lblRestNum];

}


- (void)updateWithArea:(MCTicketAreaModel *)area{
    if (!area) {
        return;
    }
    
    _area = area;
    
    if (area.area_name) {
//        [_btnCheck setTitle:area.area_name forState:UIControlStateNormal];
        _lblArea.text = area.area_name;
        [_lblArea sizeToFit];
    }
    
    if (area.area_price_special) {
        _lblPrice.text = area.area_price_special;
        [_lblPrice sizeToFit];
    }
    
    if (area.area_price) {
        NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"￥%@",area.area_price]];
        [attStr addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlineStyleSingle) range:NSMakeRange(0, attStr.length)];
        _lblOrginalPrice.attributedText = attStr;
        [_lblOrginalPrice sizeToFit];
    }
    
    if (area.rest_seat) {
        _lblRestNum.text = [NSString stringWithFormat:@"(余票：%@ 张)",area.rest_seat];
        [_lblRestNum sizeToFit];
    }
}


- (void)layoutSubviews{
    [super layoutSubviews];
    
    CGRect frame = _imgCheck.frame;
//    _btnCheck.frame = CGRectMake(4, (CGRectGetHeight(self.bounds)-frame.size.height)/2, frame.size.width, frame.size.height);
    
    _imgCheck.frame = CGRectMake(4, (CGRectGetHeight(self.bounds)-frame.size.height)/2, frame.size.width, frame.size.height);
    
    frame = _lblArea.frame;
    _lblArea.frame = CGRectMake(CGRectGetMaxX(_imgCheck.frame)+2, (CGRectGetHeight(self.bounds)-frame.size.height)/2, frame.size.width, frame.size.height);
    
    frame = _lblPrice.frame;
    _lblPrice.frame = CGRectMake(CGRectGetMaxX(_lblArea.frame)+2, (CGRectGetHeight(self.bounds)-frame.size.height)/2, frame.size.width, frame.size.height);
    
    frame = _lblOrginalPrice.frame;
    _lblOrginalPrice.frame =CGRectMake(CGRectGetMaxX(_lblPrice.frame)+2, (CGRectGetHeight(self.bounds)-frame.size.height)/2, frame.size.width, frame.size.height);
    
    frame = _lblRestNum.frame;
    _lblRestNum.frame = CGRectMake(CGRectGetMaxX(_lblOrginalPrice.frame)+2, (CGRectGetHeight(self.bounds)-frame.size.height)/2, frame.size.width, frame.size.height);
    
}

//- (void)btnDidClickCheck:(UIButton *)sender{
//    sender.selected = !sender.selected;
//    
//    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedChecked:)]) {
//        [self.delegate clickedChecked:_area];
//    }
//}


@end
