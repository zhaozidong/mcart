//
//  MCTicketShowDetailTitleCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/23.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCTicketShowDetailTitleCell.h"

@implementation MCTicketShowDetailTitleCell{
    UIImageView *_imgView;
    UILabel *_lblTitle;
    UILabel *_lblNumber;
    UILabel *_lblTime;
    UILabel *_lblAddress;
    UILabel *_lblPrice;
    
    MCTicketShowModel *_show;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setUpSubViews{

    if (!_imgView){
        _imgView = [[UIImageView alloc] initWithFrame:CGRectMake(16, 80, 80, 96)];
        _imgView.contentMode = UIViewContentModeScaleAspectFill;
        _imgView.clipsToBounds = YES;
        _imgView.userInteractionEnabled = YES;
    }
    [self.contentView addSubview:_imgView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapImage)];
    tap.numberOfTapsRequired = 1;
    [_imgView addGestureRecognizer:tap];
    
    
    if (!_lblTitle) {
        _lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_imgView.frame)+8, CGRectGetMinY(_imgView.frame), CGRectGetWidth(self.bounds) -  CGRectGetMaxX(_imgView.frame) - 2*8, 50)];
        _lblTitle.textColor = kBlackColor;
        _lblTitle.font = kAppFont(16.f);
        _lblTitle.preferredMaxLayoutWidth = CGRectGetWidth(self.bounds) - CGRectGetMaxX(_imgView.frame)-2*8;
        _lblTitle.numberOfLines = 3;
                                                              
    }
    [self.contentView addSubview:_lblTitle];
    
    if (!_lblTime) {
        _lblTime = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(_lblTitle.frame), 80, CGRectGetWidth(self.bounds)-130-3*8, 20)];
        _lblTime.textColor = kBlackColor;
        _lblTime.font = kAppFont(12.f);
        _lblTime.numberOfLines = 2;
        _lblTime.preferredMaxLayoutWidth = CGRectGetWidth(self.bounds)-130-24;
        _lblTime.lineBreakMode = NSLineBreakByCharWrapping;
    }
    [self.contentView addSubview:_lblTime];
    
    if (!_lblAddress) {
        _lblAddress = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(_lblTitle.frame), CGRectGetMaxY(_lblTime.frame), 100, 20)];
        _lblAddress.textColor = kBlackColor;
        _lblAddress.font = kAppFont(12.f);
    }
    [self.contentView addSubview:_lblAddress];
    
    
    if (!_lblPrice) {
        _lblPrice = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(_lblTitle.frame), CGRectGetMaxY(_lblAddress.frame)+4, 100, 20)];
        _lblPrice.textColor = kBlackColor;
        _lblPrice.font = kAppFont(12.f);
    }
    [self.contentView addSubview:_lblPrice];
    
    
}

- (void)updateWithShow:(MCTicketShowModel *)show{
    if (!show) {
        return;
    }
    _show = show;
    
    if (show.front_pic) {
        [_imgView sd_setImageWithURL:[NSURL URLWithString:kImagePath(show.front_pic)]];
    }
    
    if (show.title) {
        _lblTitle.text = show.title;
        [_lblTitle sizeToFit];
    }
    
    if (show.time) {
        _lblTime.text = [NSString stringWithFormat:@"时间：%@",show.time];
        [_lblTime sizeToFit];
    }
    
    if (show.address) {
        _lblAddress.text = [NSString stringWithFormat:@"场馆：%@",show.address];
        [_lblAddress sizeToFit];
    }
    
    if (show.price) {
        _lblPrice.text = [NSString stringWithFormat:@"票价：%@",show.price];
        [_lblPrice sizeToFit];
    }
}


- (void)layoutSubviews{
    [super layoutSubviews];
    _imgView.frame = CGRectMake(16, 80, 80, 96);
    
    CGRect frame = _lblTitle.frame;
    if (_show && _show.title) {
        NSString *title = _show.title;
        CGRect rect = [title boundingRectWithSize:CGSizeMake(kMainFrameWidth -  CGRectGetMaxX(_imgView.frame) - 2*8, 150) options:NSStringDrawingUsesLineFragmentOrigin  attributes:@{NSFontAttributeName:kAppFont(16)} context:nil];
        _lblTitle.frame = CGRectMake(CGRectGetMaxX(_imgView.frame)+8, CGRectGetMinY(_imgView.frame), kMainFrameWidth -  CGRectGetMaxX(_imgView.frame) - 2*8, rect.size.height);
    }
    
    frame = _lblTime.frame;
    _lblTime.frame = CGRectMake(CGRectGetMinX(_lblTitle.frame), CGRectGetMaxY(_lblTitle.frame)+4, CGRectGetWidth(self.bounds) -  CGRectGetWidth(_imgView.frame) - 3*8, frame.size.height);
    
    
    frame = _lblAddress.frame;
    _lblAddress.frame = CGRectMake(CGRectGetMinX(_lblTitle.frame), CGRectGetMaxY(_lblTime.frame)+4, frame.size.width, frame.size.height);
    
    frame = _lblPrice.frame;
    _lblPrice.frame = CGRectMake(CGRectGetMinX(_lblTitle.frame), CGRectGetMaxY(_lblAddress.frame)+4, frame.size.width, frame.size.height);
}


+ (CGFloat)cellHeight{
    return 190;
}

- (void)tapImage {
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedImage:)]) {
        [self.delegate clickedImage:kImagePath(_show.front_pic)];
    }
}

@end
