//
//  MCTicketPanicBuyingView.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/9.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCTicketAreaModel.h"



@protocol MCTicketPanicBuyDelegate <NSObject>

- (void)clickedOKWithArea:(MCTicketAreaModel *)area num:(NSInteger)number phone:(NSString *)phone;

@end



@interface MCTicketPanicBuyingView : UIView

@property (nonatomic, weak) id<MCTicketPanicBuyDelegate> delegate;

- (instancetype)initWithArray:(NSArray *)array;

- (void)updateWithPhone:(NSString *)phone;



@end
