//
//  MCTicketShowDetailTicketView.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/7.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCTicketModel.h"

@protocol MCTicketShowDetailTicketViewDelegate <NSObject>

- (void)clickedShowDetailTicket:(MCTicketShowTimeModel *)show;

- (void)clickedShowDetailPopup:(MCTicketShowTimeModel *)show;

@end




@interface MCTicketShowDetailTicketView : UIView


@property (nonatomic, weak) id<MCTicketShowDetailTicketViewDelegate> delegate;

- (void)updateWithShowTime:(MCTicketShowTimeModel *)info;



@end
