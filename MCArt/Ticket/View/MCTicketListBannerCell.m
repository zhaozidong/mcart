//
//  MCTicketListBannerCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/28.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCTicketListBannerCell.h"
#import "CycleScrollView.h"
#import <UIImage+AFNetworking.h>


@interface MCTicketListBannerCell ()<CycleScrollViewDelegate>{
    CycleScrollView *_cycleView;
}

@end

@implementation MCTicketListBannerCell

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setUpSubViews{

}


- (void)updateWithTicket:(MCTicketModel *)ticket{
    if (!ticket) {
        return;
    }
    
    if (ticket.banner && ticket.banner.count>0) {
        NSMutableArray *array = [NSMutableArray array];
        for (MCTicketBannerModel *banner in ticket.banner) {
            if (banner.banner_pic) {
                [array addObject:banner.banner_pic];//TODO:banner里面有视频怎么办？
            }
        }
        if (array.count> 0 && !_cycleView) {
            _cycleView = [[CycleScrollView alloc] initWithFrame:self.bounds];
            _cycleView.delegate = self;
            _cycleView.urlArray = array;
            _cycleView.autoTime = 2.5f;
        }
        [self.contentView addSubview:_cycleView];
    }
}

- (void)layoutSubviews{
    [super layoutSubviews];
    _cycleView.frame = self.bounds;
}

+ (CGFloat)cellHeight{
    return kMainFrameWidth * 0.33;
}

#pragma mark CycleScrollViewDelegate
- (void)clickImgAtIndex:(NSInteger)index{
    
    
    
    
}

@end
