//
//  MCTicketShowDetailCoverCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/7.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseTableViewCell.h"
#import "MCTicketModel.h"

@protocol MCTicketShowDetailCoverCellDelegate <NSObject>

- (void)clickedItemAtIndex:(NSInteger)index;

@end





@interface MCTicketShowDetailCoverCell : MCBaseTableViewCell


@property (nonatomic, weak) id<MCTicketShowDetailCoverCellDelegate> delegate;


- (void)updateWithShow:(MCTicketShowModel *)show;


+ (CGFloat)cellHeight;


@end
