//
//  MCTicketDetailPlaceMusicHallView.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/8.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, MCTicketDetailPlaceType) {
    MCTicketDetailPlaceTypeMusicHall,
    MCTicketDetailPlaceTypeTheatreUpStairs,
    MCTicketDetailPlaceTypeTheatreDownStairs
};



@protocol MCTicketDetailPlaceDelegate <NSObject>


- (void)clickedAtIndex:(NSInteger)index areaName:(NSString *)name showName:(NSString *)showName;


@end




@interface MCTicketDetailPlaceMusicHallView : UIView


@property (nonatomic, weak) id<MCTicketDetailPlaceDelegate> delegate;


- (instancetype)initWithType:(MCTicketDetailPlaceType)type;


- (instancetype)initWithFrame:(CGRect)frame type:(MCTicketDetailPlaceType)type;

@end
