//
//  MCTicketShowDetailWebCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/12.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCTicketShowDetailWebCell.h"

@interface MCTicketShowDetailWebCell ()<UIScrollViewDelegate>{
    UIWebView *_webView;
    CGFloat _offset;
}

@end



@implementation MCTicketShowDetailWebCell

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setUpSubViews{
    if (!_webView) {
        _webView = [[UIWebView alloc] initWithFrame:CGRectInset(self.bounds, 8, 0)];
        _webView.scrollView.delegate = self;
    }
    [self.contentView addSubview:_webView];
}

- (void)updateWithShow:(MCTicketShowModel *)show{
    if (!show) {
        return;
    }
    
    if (show.descr_url) {
        [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:kURLPath(show.descr_url)]]];
    }
}

- (void)layoutSubviews{
    [super layoutSubviews];
    _webView.frame = CGRectInset(self.bounds, 8, 0);
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.y > 0)
        scrollView.contentOffset = CGPointMake(0, scrollView.contentOffset.y);
    if (scrollView.contentOffset.y < 0){
        if (self.delegate && [self.delegate respondsToSelector:@selector(updateOffsety:)]) {
            [self.delegate updateOffsety:scrollView.contentOffset.y];
        }
        scrollView.contentOffset = CGPointMake(0, 0);
    }
    
}


@end
