//
//  MCSeatDetailView.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/8.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCTicketAreaModel.h"


@protocol MCSeatDetailDelegate <NSObject>

- (void)clickedSeat:(NSArray *)array;

@end




@interface MCSeatDetailView : UIScrollView

@property (nonatomic, weak) id<MCSeatDetailDelegate> seatDelegate;

- (instancetype)initWithFrame:(CGRect)frame list:(NSArray *)arrList;

- (void)removeSeatWithId:(NSInteger)seatId;

@end
