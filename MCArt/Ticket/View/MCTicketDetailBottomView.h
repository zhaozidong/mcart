//
//  MCTicketDetailBottomView.h
//  MCArt
//
//  Created by Derek.zhao on 16/1/23.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MCTicketDetailBottomDelegate <NSObject>

- (void)clickedChat:(id)sender;

@end




@interface MCTicketDetailBottomView : UIView

@property (nonatomic, weak) id<MCTicketDetailBottomDelegate> delegate;


@end
