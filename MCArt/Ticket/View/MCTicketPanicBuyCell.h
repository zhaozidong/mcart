//
//  MCTicketPanicBuyCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/9.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseTableViewCell.h"
#import "MCTicketAreaModel.h"

//@protocol MCTicketPanicBuyCellDelegate <NSObject>
//
//- (void)clickedChecked:(MCTicketAreaModel *)area;
//
//@end




@interface MCTicketPanicBuyCell : MCBaseTableViewCell

//@property (nonatomic, weak) id<MCTicketPanicBuyCellDelegate> delegate;

- (void)updateWithArea:(MCTicketAreaModel *)area;


@end
