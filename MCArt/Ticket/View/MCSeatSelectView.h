//
//  MCSeatSelectView.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/8.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MCSeatSelectViewDelegate <NSObject>

- (void)deleteSeatAtIndex:(NSInteger)index;

@end






@interface MCSeatSelectView : UIView

@property (nonatomic, weak) id<MCSeatSelectViewDelegate> delegate;


- (void)updateWithArray:(NSArray *)array;


@end
