//
//  MCTicketShowDetailTicketCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/7.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseTableViewCell.h"
#import "MCTicketModel.h"

@protocol MCTicketShowDetailTicketCellDelegate <NSObject>

- (void)clickedTicket:(MCTicketShowTimeModel *)showtime;

- (void)clickedShowPop:(MCTicketShowTimeModel *)showtime;

@end




@interface MCTicketShowDetailTicketCell : MCBaseTableViewCell


@property (nonatomic, weak) id<MCTicketShowDetailTicketCellDelegate> delegate;


- (void)updateWithShow:(MCTicketShowModel *)show;


@end
