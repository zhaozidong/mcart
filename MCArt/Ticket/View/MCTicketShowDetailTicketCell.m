//
//  MCTicketShowDetailTicketCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/7.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCTicketShowDetailTicketCell.h"
#import "MCTicketShowDetailTicketView.h"

@interface MCTicketShowDetailTicketCell ()<MCTicketShowDetailTicketViewDelegate>{
    MCTicketShowModel *_show;
}


@end



@implementation MCTicketShowDetailTicketCell
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setUpSubViews{
    
    
    
    
    
}

- (void)updateWithShow:(MCTicketShowModel *)show{
    if (!show) {
        return;
    }
    _show = show;
    if (show.showtimes.count > 0) {
        for (int i = 0 ; i < show.showtimes.count; i++) {
            MCTicketShowDetailTicketView *ticket = [[MCTicketShowDetailTicketView alloc] initWithFrame:CGRectMake(8, i*80+4, kMainFrameWidth - 2*8, 80-2*4)];
            ticket.delegate = self;
            [ticket updateWithShowTime:[show.showtimes objectAtIndex:i]];
            [self.contentView addSubview:ticket];
            
        }
    }
}


- (void)layoutSubviews{
    [super layoutSubviews];
}

#pragma mark MCTicketShowDetailTicketViewDelegate
- (void)clickedShowDetailTicket:(MCTicketShowTimeModel *)show{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedTicket:)]) {
        [self.delegate clickedTicket:show];
    }
}

- (void)clickedShowDetailPopup:(MCTicketShowTimeModel *)show;{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedShowPop:)]) {
        [self.delegate clickedShowPop:show];
    }
}

@end
