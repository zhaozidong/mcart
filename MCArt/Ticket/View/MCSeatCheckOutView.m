//
//  MCSeatCheckOutView.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/8.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCSeatCheckOutView.h"

@implementation MCSeatCheckOutView{
    UILabel *_lblCount;
    UIButton *_btnCheckout;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init{
    self = [super init];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}

- (void)setUpSubViews{
    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.layer.borderWidth = 0.5f;
    self.backgroundColor = kWhiteColor;
    
    if (!_lblCount) {
        _lblCount = [[UILabel alloc] init];
    }
    [self addSubview:_lblCount];
    
    if (!_btnCheckout) {
        CGFloat width = kMainFrameWidth * 0.3;
        _btnCheckout = [[UIButton alloc] initWithFrame:CGRectMake(kMainFrameWidth- width, 0, width, CGRectGetHeight(self.bounds))];
        [_btnCheckout setBackgroundColor:[UIColor redColor]];
        [_btnCheckout setTitle:@"去结算" forState:UIControlStateNormal];
        [_btnCheckout addTarget:self action:@selector(btnDidClickCheckOut:) forControlEvents:UIControlEventTouchUpInside];
    }
    [self addSubview:_btnCheckout];
    
}

- (void)updateWithTotal:(NSString *)total{
    if (total) {
        _lblCount.text = [NSString stringWithFormat:@"合计：￥%@",total];
        [_lblCount sizeToFit];
    }
}


- (void)layoutSubviews{
    [super layoutSubviews];
    
    CGFloat width = kMainFrameWidth * 0.3;
    _btnCheckout.frame = CGRectMake(kMainFrameWidth- width, 0, width, CGRectGetHeight(self.bounds));
    
    CGRect frame = _lblCount.frame;
    _lblCount.frame = CGRectMake(8, (CGRectGetHeight(self.bounds)-frame.size.height)/2, frame.size.width, frame.size.height);
}

- (void)btnDidClickCheckOut:(UIButton *)sender{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedCheckOut)]) {
        [self.delegate clickedCheckOut];
    }
}


@end
