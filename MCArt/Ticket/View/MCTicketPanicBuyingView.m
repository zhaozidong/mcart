//
//  MCTicketPanicBuyingView.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/9.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCTicketPanicBuyingView.h"
#import "PKYStepper.h"
#import "MCTicketPanicBuyCell.h"


static NSString *const buyIdentifier = @"buyCell";


@interface MCTicketPanicBuyingView ()<UITableViewDataSource,UITableViewDelegate>{
    UITableView *_tableView;
    UIView *_containerView;
    NSArray *_array;
    PKYStepper *_stepper;
    UILabel *_lblStep;
    UILabel *_lblPhone;
//    UITextField *_txtPhone;
    UIButton *_btnOK;
    UIButton *_btnCancel;
    UILabel *_lblTitle;
    UILabel *_lblPhoneNumber;
    
    __block NSInteger _ticketNum;
    MCTicketAreaModel *_selectArea;
    
    NSString *_strPhone;
    
}

@end


@implementation MCTicketPanicBuyingView
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init{
    self = [super init];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}

- (instancetype)initWithArray:(NSArray *)array{
    self = [super initWithFrame:CGRectMake(0, 0, kMainFrameWidth, kMainFrameHeight)];
    if (self) {
        _array = array;
        [self setUpSubViews];
    }
    return self;
}


- (void)setUpSubViews{
    self.backgroundColor = kRGBA(0, 0, 0, 0.3);
    _ticketNum = 0;
    
    if (!_containerView) {
        _containerView = [[UIView alloc] initWithFrame:CGRectMake((kMainFrameWidth-300)/2, kMainFrameHeight * 0.2, 300, _array.count*50+160)];
        _containerView.backgroundColor = kWhiteColor;
        _containerView.layer.cornerRadius = 5.f;
        _containerView.layer.masksToBounds = YES;
    }
    
    if (!_lblTitle) {
        _lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_containerView.frame), 40)];
        _lblTitle.textAlignment = NSTextAlignmentCenter;
        _lblTitle.font = kAppFont(20.f);
        _lblTitle.text = @"请选择抢票区域";
    }
    [_containerView addSubview:_lblTitle];
    
    
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_lblTitle.frame)+4, CGRectGetWidth(_containerView.frame), _array.count*50) style:UITableViewStylePlain];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        
        [_tableView registerClass:[MCTicketPanicBuyCell class] forCellReuseIdentifier:buyIdentifier];
    }
    [_containerView  addSubview:_tableView];
    
    
    if (!_lblStep) {
        _lblStep = [[UILabel alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(_tableView.frame)+8, 60, 20)];
        _lblStep.text = @"抢票张数：";
        [_lblStep sizeToFit];
    }
    [_containerView addSubview:_lblStep];
    
    if (!_stepper) {
        _stepper = [[PKYStepper alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_lblStep.frame)+2, CGRectGetMinY(_lblStep.frame)-4, 140, 30)];
        [_stepper setBorderColor:[UIColor colorWithWhite:0.95 alpha:1.0]];
        [_stepper setLabelTextColor:kBlackColor];
        [_stepper setButtonTextColor:kBlackColor forState:UIControlStateNormal];
        _stepper.value = 1.0f;
        _stepper.minimum = 1.0f;
        _stepper.maximum = 6.0f;
        _stepper.valueChangedCallback = ^(PKYStepper *stepper,float count) {
            stepper.countLabel.text = [NSString stringWithFormat:@"%@",@(count)];
            _ticketNum = (int)count;
        };
        [_stepper setup];
    }
    [_containerView addSubview:_stepper];
    
    if (!_lblPhone) {
        _lblPhone = [[UILabel alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(_lblStep.frame)+15, 50, 20)];
        _lblPhone.text = @"手机号：";
        [_lblPhone sizeToFit];
    }
    [_containerView addSubview:_lblPhone];
    
//    if (!_txtPhone) {
//        _txtPhone = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_lblPhone.frame)+2, CGRectGetMinY(_lblPhone.frame)-4, 180, 30)];
//        _txtPhone.layer.cornerRadius = 3.f;
//        _txtPhone.layer.borderColor = [UIColor lightGrayColor].CGColor;
//        _txtPhone.layer.borderWidth = 0.5f;
//        _txtPhone.placeholder = @"接收取票号的手机号码";
//        _txtPhone.keyboardType = UIKeyboardTypePhonePad;
//    }
//    [_containerView addSubview:_txtPhone];
    
    if (!_lblPhoneNumber) {
        _lblPhoneNumber = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_lblPhone.frame)+2, CGRectGetMinY(_lblPhone.frame)-4, 180, 30)];
        _lblPhoneNumber.font = kAppFont(18.f);
    }
    [_containerView addSubview:_lblPhoneNumber];
    
    if (!_btnCancel) {
        _btnCancel = [[UIButton alloc] initWithFrame:CGRectMake(20, CGRectGetMaxY(_lblPhoneNumber.frame)+8, 80, 40)];
        [_btnCancel setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        [_btnCancel setTitle:@"取消" forState:UIControlStateNormal];
        [_btnCancel addTarget:self action:@selector(btnDidClickCancel:) forControlEvents:UIControlEventTouchUpInside];
    }
    [_containerView addSubview:_btnCancel];
    
    if (!_btnOK) {
        _btnOK = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetWidth(_containerView.frame)-20-80, CGRectGetMinY(_btnCancel.frame), 80, 40)];
        [_btnOK setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
        [_btnOK setTitle:@"确定" forState:UIControlStateNormal];
        [_btnOK addTarget:self action:@selector(btnDidClickOK:) forControlEvents:UIControlEventTouchUpInside];
    }
    [_containerView addSubview:_btnOK];
    
    
    [self addSubview:_containerView];
}

- (void)dismiss{
    [self removeFromSuperview];
}

- (void)updateWithPhone:(NSString *)phone{
    if (phone) {
        _strPhone = phone;
        _lblPhoneNumber.text = phone;
    }
}


#pragma --mark table
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MCTicketAreaModel *area = [_array objectAtIndex:indexPath.row];
    MCTicketPanicBuyCell *buyCell = [tableView dequeueReusableCellWithIdentifier:buyIdentifier];
    [buyCell updateWithArea:area];
    return buyCell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell setSelected:YES animated:NO];
    
    _selectArea = [_array objectAtIndex:indexPath.row];
}

- (void)btnDidClickOK:(id)sender{
//    if ([_txtPhone.text  isEqualToString:@""]) {
//        return;
//    }
    
    if (!_selectArea) {
        [MCMessageUtilities showTotast:@"请先选择需要抢票的区域!"];
        return;
    }
    
    if (!_ticketNum) {
        [MCMessageUtilities showTotast:@"请先选择需要抢票的数量!"];
        return;
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedOKWithArea:num:phone:)]) {
        [self.delegate clickedOKWithArea:_selectArea num:_ticketNum phone:_strPhone];
    }
    [self dismiss];
}

- (void)btnDidClickCancel:(id)sender{
    [self dismiss];
}



@end
