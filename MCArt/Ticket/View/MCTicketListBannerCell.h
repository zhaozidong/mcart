//
//  MCTicketListBannerCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/28.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseTableViewCell.h"
#import "MCTicketModel.h"


@interface MCTicketListBannerCell : MCBaseTableViewCell


- (void)updateWithTicket:(MCTicketModel *)ticket;


+ (CGFloat)cellHeight;

@end
