//
//  MCSeatSelectView.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/8.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCSeatSelectView.h"
#import "MCTicketAreaModel.h"


@implementation MCSeatSelectView {
    NSArray *_array;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init{
    self = [super init];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}

- (void)setUpSubViews{
    
 
    
    
    
}

- (void)updateWithArray:(NSArray *)array{
    for (UIView *view in self.subviews) {
        [view removeFromSuperview];
    }
    
    if (!array || array.count == 0) {
        return;
    }
    _array = array;
    
    CGFloat width = (kMainFrameWidth - 4*8)/3;
    
    for (int i = 0; i < array.count; i++) {
        MCTicketSeatDetailModel *detail = [array objectAtIndex:i];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(i, 0, width, 25)];
        label.layer.cornerRadius = 3.f;
        label.layer.borderColor = [UIColor lightGrayColor].CGColor;
        label.layer.borderWidth = 1.f;
        label.backgroundColor = [UIColor yellowColor];
        label.textAlignment = NSTextAlignmentLeft;
        label.text = detail.sename;
        label.tag = i;
        label.userInteractionEnabled = YES;

        UIButton *btnClose = [[UIButton alloc] init];
        [btnClose.titleLabel setFont:kIconFont(16)];
        [btnClose setTitleColor:kBlackColor forState:UIControlStateNormal];
        [btnClose setTitle:kIcon_closecircleo forState:UIControlStateNormal];
        [btnClose addTarget:self action:@selector(btnDidClickClose:) forControlEvents:UIControlEventTouchUpInside];
        btnClose.tag = i;

        if (i < 3) {
            label.frame = CGRectMake((i+1)*8+i*width, 0, width, 25);
            btnClose.frame = CGRectMake(CGRectGetMaxX(label.frame)-25, 0, 25, 25);
        }else if (i>=3){
            int j = i-3;
            label.frame = CGRectMake((j+1)*8+j*width, 8+25, width, 25);
            btnClose.frame = CGRectMake(CGRectGetMaxX(label.frame)-25, 8+25, 25, 25);
        }

        
        [self addSubview:label];
        [self addSubview:btnClose];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapLabel:)];
        tap.numberOfTapsRequired = 1;
        [label addGestureRecognizer:tap];
        
    }
}

- (void)btnDidClickClose:(UIButton *)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(deleteSeatAtIndex:)]) {
        [self.delegate deleteSeatAtIndex:sender.tag];
    }
}

- (void)tapLabel:(UIGestureRecognizer *)gesture {
    UIView *view = gesture.view;
    if (view && self.delegate && [self.delegate respondsToSelector:@selector(deleteSeatAtIndex:)]) {
        [self.delegate deleteSeatAtIndex:view.tag];
    }
}


@end
