//
//  MCTicketDetailBottomView.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/23.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCTicketDetailBottomView.h"

@implementation MCTicketDetailBottomView{
    UIButton *_btnChat;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init{
    self = [super init];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}

- (void)setUpSubViews{
    self.backgroundColor = kWhiteColor;
    self.layer.borderWidth = 0.5f;
    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    if (!_btnChat) {
        _btnChat = [[UIButton alloc] initWithFrame:self.bounds];
        [_btnChat.titleLabel setFont:kAppFont(18.f)];
        [_btnChat setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [_btnChat setImage:[UIImage imageNamed:@"chat"] forState:UIControlStateNormal];
        [_btnChat setTitle:@"咨询" forState:UIControlStateNormal];
        [_btnChat addTarget:self action:@selector(btnDidClickChat:) forControlEvents:UIControlEventTouchUpInside];
    }
    [self addSubview:_btnChat];
    

}

- (void)layoutSubviews{
    [super layoutSubviews];
    _btnChat.frame = self.bounds;
}

- (void)btnDidClickChat:(id)sender{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedChat:)]) {
        [self.delegate clickedChat:sender];
    }
}


@end
