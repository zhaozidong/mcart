//
//  MCTicketDetailPlaceMusicHallView.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/8.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCTicketDetailPlaceMusicHallView.h"

@implementation MCTicketDetailPlaceMusicHallView{
    NSArray *_arrAreaName;
    NSArray *_arrShowAreaName;
    MCTicketDetailPlaceType _type;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithType:(MCTicketDetailPlaceType)type{
    self = [super init];
    if (self) {
        _type = type;
        switch (type) {
            case MCTicketDetailPlaceTypeMusicHall: {
                _arrAreaName = @[@"D区",@"",@"D区",@"C区",@"A区",@"C区",@"B区",@"B区",@"B区"];
                _arrShowAreaName = @[@"D区左",@"",@"D区右",@"C区左",@"A区",@"C区右",@"B区",@"B区",@"B区"];
                break;
            }
            case MCTicketDetailPlaceTypeTheatreUpStairs: {
                _arrAreaName = @[@"F区",@"F",@"F区",@"E区",@"E区",@"E区",@"D区",@"",@"D区",@"",@"",@""];
                _arrShowAreaName = @[@"F区",@"F",@"F区",@"E区",@"E区",@"E区",@"D区",@"",@"D区",@"",@"",@""];
                break;
            }
            case MCTicketDetailPlaceTypeTheatreDownStairs: {
                _arrAreaName = @[@"A区",@"A区",@"A区",@"B区",@"B区",@"B区",@"C区",@"C区",@"C区",@"",@"",@""];
                _arrShowAreaName = @[@"A区",@"A区",@"A区",@"B区",@"B区",@"B区",@"C区",@"C区",@"C区",@"",@"",@""];
                break;
            }
        }
        [self setUpSubView];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame type:(MCTicketDetailPlaceType)type{
    self = [super initWithFrame:frame];
    if (self) {
        _type = type;
        switch (type) {
            case MCTicketDetailPlaceTypeMusicHall: {
                _arrAreaName = @[@"D区",@"",@"D区",@"C区",@"A区",@"C区",@"B区",@"B区",@"B区"];
                _arrShowAreaName = @[@"D区左",@"",@"D区右",@"C区左",@"A区",@"C区右",@"B区",@"B区",@"B区"];
                break;
            }
            case MCTicketDetailPlaceTypeTheatreUpStairs: {
                _arrAreaName = @[@"F区",@"F",@"F区",@"E区",@"E区",@"E区",@"D区",@"",@"D区",@"",@"",@""];
                _arrShowAreaName = @[@"F区",@"F",@"F区",@"E区",@"E区",@"E区",@"D区",@"",@"D区",@"",@"",@""];
                break;
            }
            case MCTicketDetailPlaceTypeTheatreDownStairs: {
                _arrAreaName = @[@"C区",@"C区",@"C区",@"B区",@"B区",@"B区",@"A区",@"A区",@"A区",@"",@"",@""];
                _arrShowAreaName = @[@"C区",@"C区",@"C区",@"B区",@"B区",@"B区",@"A区",@"A区",@"A区",@"",@"",@""];
                break;
            }
        }
        [self setUpSubView];
    }
    return self;
}

- (void)setUpSubView{
    switch (_type) {
        case MCTicketDetailPlaceTypeMusicHall: {
            [self setUpAreaA];
            break;
        }
        case MCTicketDetailPlaceTypeTheatreUpStairs: {
            [self setUpAreaB1];
            break;
        }
        case MCTicketDetailPlaceTypeTheatreDownStairs: {
            [self setUpAreaB2];
            break;
        }
    }
    
}

- (void)setUpAreaA{
    CGFloat left = 0;
    CGFloat top = 0;
    
    for (int i = 1; i < 10; i++) {
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"area_0%d",i]];
        CGFloat imgWidth = image.size.width;
        CGFloat imgHeight = image.size.height;
        
        UIImageView *_imgView = [[UIImageView alloc] initWithFrame:CGRectMake(left, top, imgWidth, imgHeight)];
        _imgView.image = image;
        _imgView.tag = i;
        [self addSubview:_imgView];
        
        _imgView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickedView:)];
        tap.numberOfTapsRequired = 1;
        [_imgView addGestureRecognizer:tap];
        
        if (i ==3 || i ==6) {
            left = 0;
            top += imgHeight;
        }else{
            left += imgWidth;
        }
    }
}

- (void)setUpAreaB1{
    CGFloat left = 0;
    CGFloat top = 0;
    
    for (int i = 1; i <= 12; i++) {
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"area_b%d",i]];
        CGFloat imgWidth = image.size.width;
        CGFloat imgHeight = image.size.height;
        
        UIImageView *_imgView = [[UIImageView alloc] initWithFrame:CGRectMake(left, top, imgWidth, imgHeight)];
        _imgView.image = image;
        _imgView.tag = i;
        [self addSubview:_imgView];
        
        _imgView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickedView:)];
        tap.numberOfTapsRequired = 1;
        [_imgView addGestureRecognizer:tap];
        
        if (i ==3 || i ==6 || i == 9) {
            left = 0;
            top += imgHeight;
        }else{
            left += imgWidth;
        }
    }
}

- (void)setUpAreaB2{
    CGFloat left = 0;
    CGFloat top = 0;
    
    for (int i = 1; i <= 12; i++) {
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"area_c%d",i]];
        CGFloat imgWidth = image.size.width;
        CGFloat imgHeight = image.size.height;
        
        UIImageView *_imgView = [[UIImageView alloc] initWithFrame:CGRectMake(left, top, imgWidth, imgHeight)];
        _imgView.image = image;
        _imgView.tag = i;
        [self addSubview:_imgView];
        
        _imgView.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickedView:)];
        tap.numberOfTapsRequired = 1;
        [_imgView addGestureRecognizer:tap];
        
        if (i ==3 || i ==6 || i == 9) {
            left = 0;
            top += imgHeight;
        }else{
            left += imgWidth;
        }
    }
}



- (void)layoutSubviews{
    [super layoutSubviews];

}


- (void)clickedView:(UIGestureRecognizer *)gesture{
    UIImageView *_imageView = (UIImageView *)gesture.view;
    NSInteger index = _imageView.tag;
    NSString *areaName = [_arrAreaName objectAtIndex:index - 1];
    NSString *showName = [_arrShowAreaName objectAtIndex:index - 1];
    if (areaName && ![areaName isEqualToString:@""] && self.delegate && [self.delegate respondsToSelector:@selector(clickedAtIndex:areaName:showName:)]) {
        [self.delegate clickedAtIndex:_imageView.tag areaName:areaName showName:showName];
    }
}

@end
