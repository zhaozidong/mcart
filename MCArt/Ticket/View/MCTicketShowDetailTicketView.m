//
//  MCTicketShowDetailTicketView.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/7.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCTicketShowDetailTicketView.h"

@implementation MCTicketShowDetailTicketView{
    UILabel *_lblTime;
    UILabel *_lblNum;
    MCTicketShowTimeModel *_show;
    NSTimer *_timer;
    long long _second;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init{
    self = [super init];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}

- (void)setUpSubViews{
    self.backgroundColor = [UIColor yellowColor];
    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.layer.borderWidth = 1.f;
    self.layer.cornerRadius = 3.f;
    
    if (!_lblTime) {
        _lblTime = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.bounds), 20)];
        _lblTime.textAlignment = NSTextAlignmentCenter;
    }
    [self addSubview:_lblTime];
    
    if (!_lblNum) {
        _lblNum = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, CGRectGetWidth(self.bounds), 20)];
        _lblNum.textAlignment = NSTextAlignmentCenter;
        _lblNum.numberOfLines = 2;
        _lblNum.font = kAppFont(14.f);
    }
    [self addSubview:_lblNum];
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickedTicket:)];
    tap.numberOfTapsRequired = 1;
    [self addGestureRecognizer:tap];
    
}

- (void)updateWithShowTime:(MCTicketShowTimeModel *)info{
    if (!info) {
        return;
    }
    
    _show = info;
    
    if (info.time) {
        _lblTime.text = info.time;
        [_lblTime sizeToFit];
    }
    
    
    if (info.stat == MCTicketSellStatusSoldout) {
        _lblNum.text = @"网上售票已停止，需购票请在演出前到售票窗口购票";
        [_lblNum sizeToFit];
        return;
    }
    
    
    if (info.type == MCTicketTypeNormal) {
        _lblNum.text = [NSString stringWithFormat:@"普通票（剩余%@张）",_show.special_rest_seat];
    }else if (info.type == MCTicketTypeSpecialPrice) {
        if ([info.special_rest_seat integerValue] > 0) {
            _lblNum.text = [NSString stringWithFormat:@"限量%@张特价,还剩%@张",info.special_limit_seat,info.special_rest_seat];
        }else if (info.stat == MCTicketSellStatusSoldout) {
            _lblNum.text = [NSString stringWithFormat:@"限时优惠已结束，点击按普通票购买（剩余%@张）",_show.special_rest_seat];
        }
    }else if (info.type == MCTicketTypeSeckilling) {
        if ([info.resttime_start longLongValue] > 0) {
//            _lblNum.text = [NSString stringWithFormat:@"限时优惠进行中，倒计时%@",info.resttime_start];
//
//            long long sec = [info.resttime_start longLongValue]/1000;
//            long long day = sec / (24*3600);
//            long long hour = (sec - day*(24*3600))/3600;
//            long long min = (sec - day*(24*3600) - hour*3600)/60;
//            long long s = sec - day*(24*3600) - hour*3600 - 60*min;
//            NSString *strTime = [NSString stringWithFormat:@"距结束:%@天%@小时%@分%@秒",@(day),@(hour),@(min),@(s)];
//            NSLog(@"%lld小时%lld分%lld秒\n\r",hour,min,s);
            
            _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timer) userInfo:nil repeats:YES];
            [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
            _second = [info.resttime_start longLongValue]/1000;
            
        }else if ([info.resttime_end longLongValue] > 0) {
//            _lblNum.text = [NSString stringWithFormat:@"限时优惠进行中，距结束%@",info.resttime_end];
//            long long sec = [info.resttime_end longLongValue]/1000;
//            long long day = sec / (24*3600);
//            long long hour = (sec - day*(24*3600))/3600;
//            long long min = (sec - day*(24*3600) - hour*3600)/60;
//            long long s = sec - day*(24*3600) - hour*3600 - 60*min;
//            NSLog(@"%@天%@小时%@分%@秒\n\r",@(day),@(hour),@(min),@(s));
            
            _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timer) userInfo:nil repeats:YES];
            [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
            _second = [info.resttime_end longLongValue]/1000;
            
        }else{
            _lblNum.text = [NSString stringWithFormat:@"限时优惠已结束，点击按普通票购买（剩余%@张）",_show.special_rest_seat];
        }
    }
    [_lblNum sizeToFit];
}


- (void)timer{
    if (_second > 0) {
        _second --;
    }else{
        if (_timer) {
            [_timer invalidate];
            _timer = nil;
        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"kRefreshTicketDetailDataNotification" object:nil];
        });
    }
    
    long long day = _second / (24*3600);
    long long hour = (_second - day*(24*3600))/3600;
    long long min = (_second - day*(24*3600) - hour*3600)/60;
    long long s = _second - day*(24*3600) - hour*3600 - 60*min;
    
    NSString *strTime;
    if ([_show.resttime_start longLongValue] > 0) {
        strTime = [NSString stringWithFormat:@"限时优惠进行中，倒计时%@天%@小时%@分%@秒（剩余%@张）",@(day),@(hour),@(min),@(s),_show.special_rest_seat];
    }else{
        strTime = [NSString stringWithFormat:@"限时优惠进行中，距结束%@天%@小时%@分%@秒（剩余%@张）",@(day),@(hour),@(min),@(s),_show.special_rest_seat];
    }
//    NSLog(@"strTime=%@",strTime);
    _lblNum.text = strTime;
}



- (void)layoutSubviews{
    [super layoutSubviews];
    
    CGRect frame = _lblTime.frame;
    _lblTime.frame = CGRectMake((CGRectGetWidth(self.bounds)-frame.size.width)/2, 5, frame.size.width, frame.size.height);
    
//    frame = _lblNum.frame;
//    _lblNum.frame = CGRectMake((CGRectGetWidth(self.bounds)-frame.size.width)/2, CGRectGetMaxY(_lblTime.frame)+8, frame.size.width, frame.size.height);
    _lblNum.frame = CGRectMake(0, CGRectGetMaxY(_lblTime.frame)+8, CGRectGetWidth(self.bounds), 35);
}


- (void)clickedTicket:(UIGestureRecognizer *)recognizer{
    
    if (_show.stat == MCTicketSellStatusSoldout ) {
        return;
    }
    
    if (_show.type == MCTicketTypeSpecialPrice) {//特价进行中，直接弹窗
        if ([_show.special_rest_seat longLongValue] > 0) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(clickedShowDetailPopup:)]) {
                [self.delegate clickedShowDetailPopup:_show];
            }
            return;
        }
    }else if (_show.type == MCTicketTypeSeckilling) {//秒杀进行中，直接弹窗
        if ([_show.resttime_end longLongValue] > 0) {
            if (self.delegate && [self.delegate respondsToSelector:@selector(clickedShowDetailPopup:)]) {
                [self.delegate clickedShowDetailPopup:_show];
            }
            return;
        }
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedShowDetailTicket:)]) {
        [self.delegate clickedShowDetailTicket:_show];
    }
    
}


@end
