//
//  MCTicketSeatDescView.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/13.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCTicketSeatDescView.h"

@implementation MCTicketSeatDescView{
    UIImageView *_seat1;
    UIImageView *_seat2;
    UIImageView *_seat3;
    
    UILabel *_lblSeat1;
    UILabel *_lblSeat2;
    UILabel *_lblSeat3;
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init{
    self = [super init];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}

- (void)setUpSubViews{
    
    if (!_seat1) {
        _seat1 = [[UIImageView alloc] initWithFrame:CGRectMake(kMainFrameHeight*0.1, 0, 20, 20)];
        _seat1.image = [UIImage imageNamed:@"chair_small_gray"];
    }
    [self addSubview:_seat1];
    
    if (!_lblSeat1) {
        _lblSeat1 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_seat1.frame)+4, 2, 40, 20)];
        _lblSeat1.font = kAppFont(12.f);
        _lblSeat1.text = @"可选";
    }
    [self addSubview:_lblSeat1];
    
    if (!_seat2) {
        _seat2 = [[UIImageView alloc] initWithFrame:CGRectMake(kMainFrameWidth*0.4, 0, 20, 20)];
        _seat2.image = [UIImage imageNamed:@"chair_small_blue"];
    }
    [self addSubview:_seat2];
    
    if (!_lblSeat2) {
        _lblSeat2 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_seat2.frame)+4, 2, 40, 20)];
        _lblSeat2.font = kAppFont(12.f);
        _lblSeat2.text = @"已选";
    }
    [self addSubview:_lblSeat2];
    
    if (!_seat3) {
        _seat3 = [[UIImageView alloc] initWithFrame:CGRectMake(kMainFrameWidth*0.7, 0, 20, 20)];
        _seat3.image = [UIImage imageNamed:@"chair_small_red"];
    }
    [self addSubview:_seat3];
    
    if (!_lblSeat3) {
        _lblSeat3 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_seat3.frame)+4, 2, 40, 20)];
        _lblSeat3.font = kAppFont(12.f);
        _lblSeat3.text = @"已售";
    }
    [self addSubview:_lblSeat3];
    
}



@end
