//
//  MCTicketListShowCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/7.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCTicketListShowCell.h"

@implementation MCTicketListShowCell{
    UIImageView *_imageView;
    UILabel *_lblTitle;
    UILabel *_lblTime;
    UILabel *_lblAddress;
    UILabel *_lblPrice;
    UIButton *_btnStatus;
    UIImageView *_imgViewTicket;
    
    MCTicketShowModel *_show;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setUpSubViews{
    if (!_imageView) {//海报图
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, CGRectGetHeight(self.bounds) - 2*10, CGRectGetHeight(self.bounds) - 2*10)];
        _imageView.contentMode = UIViewContentModeScaleAspectFill;
        _imageView.clipsToBounds = YES;
    }
    [self.contentView addSubview:_imageView];
    
    
    if (!_imgViewTicket) {//特价票
        _imgViewTicket = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 67, 67)];
    }
    [self.contentView addSubview:_imgViewTicket];
    
    
    if (!_lblTitle) {
        _lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_imageView.frame), 10, kMainFrameWidth - CGRectGetMaxX(_imageView.frame) - 2*8, 60)];
        _lblTitle.font = kAppFont(16.f);
        _lblTitle.preferredMaxLayoutWidth = kMainFrameWidth - CGRectGetMaxX(_imageView.frame) - 2*8;
        _lblTitle.numberOfLines = 2;
        _lblTitle.lineBreakMode = NSLineBreakByCharWrapping;
    }
    [self.contentView addSubview:_lblTitle];
    
    if (!_lblTime) {
        _lblTime = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(_lblTitle.frame), CGRectGetMaxY(_lblTitle.frame)+8, CGRectGetWidth(_lblTitle.frame), 20)];
        _lblTime.font = kAppFont(12.f);
        _lblTime.textColor = [UIColor lightGrayColor];
    }
    [self.contentView addSubview:_lblTime];
    
    if (!_lblAddress) {
        _lblAddress = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(_lblTitle.frame), CGRectGetMaxY(_lblTime.frame)+4, CGRectGetWidth(_lblTitle.frame), 25)];
        _lblAddress.font = kAppFont(12.f);
        _lblAddress.textColor = [UIColor lightGrayColor];
    }
    [self.contentView addSubview:_lblAddress];
    
    if (!_lblPrice) {
        _lblPrice = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(_lblTitle.frame), CGRectGetMaxY(_lblAddress.frame)+4, 60, 30)];
        _lblPrice.textColor = [UIColor redColor];
        _lblPrice.font = kAppFont(16.f);
    }
    [self.contentView addSubview:_lblPrice];
    
    
    if (!_btnStatus) {
        _btnStatus = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_lblPrice.frame)+8, CGRectGetMinY(_lblPrice.frame), 50, 20)];
        _btnStatus.layer.cornerRadius = 3.f;
        _btnStatus.layer.borderColor = [UIColor redColor].CGColor;
        _btnStatus.layer.borderWidth = 1.f;
        [_btnStatus setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [_btnStatus.titleLabel setFont:kAppFont(12.f)];
    }
    [self.contentView addSubview:_btnStatus];
    
}

- (void)updateWithShow:(MCTicketShowModel *)show{
    if (!show) {
        return;
    }
    _show = show;
    
    if (show.front_pic) {
        [_imageView sd_setImageWithURL:[NSURL URLWithString:kImagePath(show.front_pic)] placeholderImage:[UIImage imageNamed:@"info_loading_title"]];
    }
    
    if (show.title) {
        _lblTitle.text = show.title;
        [_lblTitle sizeToFit];
    }
    
    if (show.time) {
        _lblTime.text = [NSString stringWithFormat:@"时间：%@",show.time];
        [_lblTime sizeToFit];
    }
    
    if (show.address) {
        _lblAddress.text = [NSString stringWithFormat:@"场馆：%@",show.address];
        [_lblAddress sizeToFit];
    }
    
    if (show.minprice) {
        _lblPrice.text = [NSString stringWithFormat:@"%@ 起",show.minprice];
        [_lblPrice sizeToFit];
    }
    
    _imgViewTicket.hidden = NO;
    if (show.type == MCTicketTypeSpecialPrice ) {
        _imgViewTicket.image = [UIImage imageNamed:@"ticketSpecialPrice"];
    }else if (show.type == MCTicketTypeSeckilling) {
        _imgViewTicket.image = [UIImage imageNamed:@"ticketSeckilling"];
    }else if (show.type == MCTicketTypeSpecialAndSeckilling) {
        _imgViewTicket.image = [UIImage imageNamed:@"ticketSpecialAndSeckilling"];
    }else if (show.type == MCTicketTypeNormal) {
        _imgViewTicket.hidden = YES;
    }
    
    if (show.status == MCTicketSellStatusSelling) {
        [_btnStatus setTitle:@"售票中" forState:UIControlStateNormal];
    }else if (show.status == MCTicketSellStatusSoldout) {
        [_btnStatus setTitle:@"结束售票" forState:UIControlStateNormal];
    }

}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    CGFloat height = CGRectGetHeight(self.bounds) - 2*10;
    _imageView.frame = CGRectMake(10, 10, height*0.83, height);
    
    CGRect frame = _lblTitle.frame;
    
    if (_show && _show.title) {
        NSString *title = _show.title;
        CGRect rect = [title boundingRectWithSize:CGSizeMake(kMainFrameWidth - CGRectGetMaxX(_imageView.frame) - 2*8, 200) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:kAppFont(16.f)} context:nil];
        _lblTitle.frame = CGRectMake(CGRectGetMaxX(_imageView.frame)+8, 8, kMainFrameWidth - CGRectGetMaxX(_imageView.frame) - 2*8, rect.size.height);
    }
    
    frame = _lblTime.frame;
    _lblTime.frame = CGRectMake(CGRectGetMinX(_lblTitle.frame), 55, frame.size.width, frame.size.height);
    
    frame = _lblAddress.frame;
    _lblAddress.frame = CGRectMake(CGRectGetMinX(_lblTitle.frame), CGRectGetMaxY(_lblTime.frame)+2, frame.size.width, frame.size.height);
    
    frame = _lblPrice.frame;
    _lblPrice.frame = CGRectMake(CGRectGetMinX(_lblTitle.frame), CGRectGetMaxY(_lblAddress.frame)+2, frame.size.width, frame.size.height);
    
    _imgViewTicket.frame = CGRectMake(0, 0, 67, 67);
    
    _btnStatus.frame = CGRectMake(CGRectGetMaxX(_lblPrice.frame)+8, CGRectGetMinY(_lblPrice.frame), 50, 20);
    
}

+ (CGFloat) cellHeight{
    return 120.f;
}


@end
