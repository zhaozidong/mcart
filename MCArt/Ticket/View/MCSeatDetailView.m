//
//  MCSeatDetailView.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/8.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCSeatDetailView.h"
#import "MCTicketAreaModel.h"
#import "MCSeatButton.h"
#import "MCMessageUtilities.h"


static NSInteger const width = 30;

@implementation MCSeatDetailView{
    NSArray *_arrList;
    NSInteger _selCount;
    NSMutableArray *_arrSelect;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame list:(NSArray *)arrList{
    self = [super initWithFrame:frame];
    if (self) {
        _arrList = arrList;
        [self setUpSubViews];
    }
    return self;
}

- (void)setUpSubViews{
    _selCount = 0;
    _arrSelect = [NSMutableArray array];
    
    NSInteger colCount = 0;
    
    //找出宽度最大的一排有多少个座位
    for (NSArray *array in _arrList) {
        MCTicketSeatDetailModel *detail = [array lastObject];
        if ([detail.secolumn integerValue] > colCount) {
            colCount = [detail.secolumn integerValue];
        }
    }
    
    self.contentSize = CGSizeMake((colCount+1) * width, _arrList.count * width);
    
    for (int i = 0; i < _arrList.count; i++) {
        NSArray *array = [_arrList objectAtIndex:i];
        for (int j = 0; j < array.count; j++) {
            MCTicketSeatDetailModel *detail = [array objectAtIndex:j];
            if (detail.sestatus != MCSeatStatusHidden) {
                NSInteger column = [detail.secolumn integerValue];
                MCSeatButton *btnChair = [[MCSeatButton alloc] initWithFrame:CGRectMake(column*width, i*width, width, width)];
                btnChair.tag=[detail.seat_id integerValue];
                if (detail.sestatus == MCSeatStatusNormal) {
                    btnChair.status = MCSeatStatusNormal;
                    btnChair.seat = detail;
                }else if (detail.sestatus == MCSeatStatusLocked) {
                    btnChair.status = MCSeatStatusLocked;
                }
                [btnChair addTarget:self action:@selector(clickedChair:) forControlEvents:UIControlEventTouchUpInside];
                [self addSubview:btnChair];
            }
        }
    }
}

- (void)clickedChair:(MCSeatButton *)sender{
    sender.selected = !sender.selected;
    if (sender.selected && _selCount == 6) {
        [MCMessageUtilities showTotast:@"一次最多只能选择6个座位"];
        sender.selected = NO;
        return;
    }
    
    if (!sender.selected) {
        _selCount--;
        [_arrSelect removeObject:sender.seat];
        if (self.seatDelegate && [self.seatDelegate respondsToSelector:@selector(clickedSeat:)]) {
            [self.seatDelegate clickedSeat:_arrSelect];
        }
        return;
    }
    
    _selCount++;
    [_arrSelect addObject:sender.seat];
    if (self.seatDelegate && [self.seatDelegate respondsToSelector:@selector(clickedSeat:)]) {
        [self.seatDelegate clickedSeat:_arrSelect];
    }
}

- (void)removeSeatWithId:(NSInteger)seatId {
    for (MCSeatButton *seat in self.subviews) {
        if (seat.tag == seatId) {
            seat.status = MCSeatStatusNormal;
            break;
        }
    }
    
    NSInteger index = -1;
    for (NSInteger i=0; i<_arrSelect.count; i++) {
        MCTicketSeatDetailModel *seat = [_arrSelect objectAtIndex:i];
        if (seatId == [seat.seat_id integerValue]) {
            index = i;
            break;
        }
    }
    if (index >= 0) {
        _selCount--;
        [_arrSelect removeObjectAtIndex:index];
    }
}



@end
