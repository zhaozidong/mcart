//
//  MCTicketShowDetailTitleCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/1/23.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseTableViewCell.h"
#import "MCTicketModel.h"

@protocol MCTicketShowDetailTitleCellDelegate <NSObject>

- (void)clickedImage:(NSString *)path;

@end




@interface MCTicketShowDetailTitleCell : MCBaseTableViewCell

@property (nonatomic, weak) id<MCTicketShowDetailTitleCellDelegate> delegate;

- (void)updateWithShow:(MCTicketShowModel *)show;

+ (CGFloat)cellHeight;

@end
