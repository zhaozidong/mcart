//
//  MCTicketShowDetailWebCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/12.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseTableViewCell.h"
#import "MCTicketModel.h"

@protocol MCTicketShowDetailWebCellDelegate <NSObject>

- (void)updateOffsety:(CGFloat)offsety;

@end






@interface MCTicketShowDetailWebCell : MCBaseTableViewCell

@property (nonatomic, weak) id<MCTicketShowDetailWebCellDelegate> delegate;

- (void)updateWithShow:(MCTicketShowModel *)show;


@end
