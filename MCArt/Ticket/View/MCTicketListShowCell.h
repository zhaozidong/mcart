//
//  MCTicketListShowCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/7.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseTableViewCell.h"
#import "MCTicketModel.h"



@interface MCTicketListShowCell : MCBaseTableViewCell


- (void)updateWithShow:(MCTicketShowModel *)show;

+ (CGFloat) cellHeight;

@end
