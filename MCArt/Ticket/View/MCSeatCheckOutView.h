//
//  MCSeatCheckOutView.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/8.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MCSeatCheckOutDelegate <NSObject>

- (void)clickedCheckOut;

@end



@interface MCSeatCheckOutView : UIView

@property (nonatomic, weak) id<MCSeatCheckOutDelegate> delegate;


- (void)updateWithTotal:(NSString *)total;

@end
