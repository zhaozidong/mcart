//
//  MCMainCategoryView.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/10.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, MCMainCategoryTitleStyle) {
    MCMainCategoryTitleStyleLeftTop,
    MCMainCategoryTitleStyleLeft,
    MCMainCategoryTitleStyleLeftBottom,
    MCMainCategoryTitleStyleRightTop,
    MCMainCategoryTitleStyleRight,
    MCMainCategoryTitleStyleRightBottom,
    MCMainCategoryTitleStyleBottom
};


@protocol MCMainCategoryViewDelegate <NSObject>

- (void)clickedWithTitle:(NSString *)title;

@end



@interface MCMainCategoryView : UIView


@property (nonatomic, weak) id<MCMainCategoryViewDelegate> delegate;

- (instancetype)initWithFrame:(CGRect)frame image:(NSString *)image title:(NSString *)title position:(MCMainCategoryTitleStyle)style;




@end
