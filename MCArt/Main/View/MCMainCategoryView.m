//
//  MCMainCategoryView.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/10.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMainCategoryView.h"

@implementation MCMainCategoryView{
    UIImageView *_imageView;
    UILabel *_lblTitle;
    
    NSString *_strImageName;
    NSString *_strTitle;
    MCMainCategoryTitleStyle _style;
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (instancetype)initWithFrame:(CGRect)frame image:(NSString *)image title:(NSString *)title position:(MCMainCategoryTitleStyle)style{
    self = [super initWithFrame:frame];
    if (self) {
        _strImageName = image;
        _strTitle = title;
        _style = style;
        
        [self setUpSubViews];
    }
    return self;
}

- (void)setUpSubViews{
//    self.backgroundColor = [UIColor orangeColor];
    self.userInteractionEnabled = YES;
    
    if (!_imageView) {
        _imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:_strImageName]];
    }
    [self addSubview:_imageView];
    
    if (!_lblTitle) {
        _lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 25, 100)];
        _lblTitle.numberOfLines = 4;
        _lblTitle.font = kAppFont(14.f);
        _lblTitle.text = _strTitle;
    }
    [self addSubview:_lblTitle];
    
    
    switch (_style) {
        case MCMainCategoryTitleStyleLeftTop: {
            _lblTitle.frame = CGRectMake(0, 0, 25, 70);
            _imageView.frame = CGRectMake(15, 5, 60, 80);
            break;
        }
        case MCMainCategoryTitleStyleLeft: {
            _lblTitle.frame = CGRectMake(0, 0, 25, 70);
            _imageView.frame = CGRectMake(25, 0, 90, 70);
            break;
        }
        case MCMainCategoryTitleStyleLeftBottom: {
            _lblTitle.frame = CGRectMake(0, 60, 25, 70);
            _imageView.frame = CGRectMake(30, 0, 90, 70);
            break;
        }
        case MCMainCategoryTitleStyleRightTop: {
            _imageView.frame = CGRectMake(0, 25, 60, 75);
            _lblTitle.frame = CGRectMake(65, 20, 25, 70);
            break;
        }
        case MCMainCategoryTitleStyleRight: {
            _imageView.frame = CGRectMake(0, 0, 103, 67);
            _lblTitle.frame = CGRectMake(100, 10, 25, 70);
            break;
        }
        case MCMainCategoryTitleStyleRightBottom: {
            _imageView.frame = CGRectMake(0, 0, 90, 70);
            _lblTitle.frame = CGRectMake(90, 30, 25, 70);
            break;
        }case MCMainCategoryTitleStyleBottom: {
            _imageView.frame = CGRectMake(0, 0, 67, 108);
            _lblTitle.frame = CGRectMake(25, 75, 25, 70);
            break;
        }
    }
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickView)];
    tap.numberOfTapsRequired = 1;
    [self addGestureRecognizer:tap];
    
}


- (void)clickView{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedWithTitle:)]) {
        [self.delegate clickedWithTitle:_strTitle];
    }
}


@end
