//
//  MCAdsModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/4/27.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseModel.h"

@interface MCAdsModel : MCBaseModel

@property (nonatomic, copy) NSString *pic;

@end
