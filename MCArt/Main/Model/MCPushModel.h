//
//  MCPushModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/4/18.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseModel.h"


typedef NS_ENUM(NSInteger, MCPushType) {
    MCPushTypeNone,
    MCPushTypeInfo,
    MCPushTypeActivity
};

typedef NS_ENUM(NSInteger, NSPushInfoShowType) {
    MCPushInfoShowTypeText,
    MCPushInfoShowTypePic,
    MCPushInfoShowTypeVideo
};



@interface MCPushDetailModel : MTLModel

@property (nonatomic, copy) NSString *share_title;

@property (nonatomic, copy) NSString *url;

@property (nonatomic, copy) NSString *share_url;

@property (nonatomic, copy) NSString *share_content;

@property (nonatomic, copy) NSString *class_type;

@end




@interface MCPushModel : MTLModel

@property (nonatomic, copy) NSString *type;

@property (nonatomic, copy) NSString *id;

@property (nonatomic, copy) NSString *title;

@property (nonatomic, copy) NSString *content;

@property (nonatomic, strong) MCPushDetailModel *bean;


@end
