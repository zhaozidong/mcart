//
//  MCPushModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/4/18.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCPushModel.h"

@implementation MCPushDetailModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"share_title":@"share_title",
             @"url":@"url",
             @"share_url":@"share_url",
             @"share_content":@"share_content",
             @"class_type":@"class_type"};
}

@end



@implementation MCPushModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"type":@"type",
             @"id":@"id",
             @"title":@"title",
             @"content":@"content",
             @"newBean":@"bean"};
}

@end




