//
//  MCMainViewModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/4/27.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMainViewModel.h"
#import "MCNetworkManager.h"
#import "MCAdsModel.h"

@implementation MCMainViewModel

SINGLETON_FOR_CLASS(MCMainViewModel);

///获取最新广告信息
- (void)getLatestAds:(NSInteger)width height:(NSInteger)height success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    NSDictionary *dict = @{@"width":@(width),@"height":@(height)};
    [[MCNetworkManager sharedInstance] postWithURL:@"app/getAd" parameter:dict success:^(id resquestData) {
        NSError *error = nil;
        MCAdsModel *ads = [MTLJSONAdapter modelOfClass:[MCAdsModel class] fromJSONDictionary:resquestData error:&error];
        if (!error) {
            if (successBlock) {
                successBlock(ads);
            }
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}


@end
