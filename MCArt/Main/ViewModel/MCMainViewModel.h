//
//  MCMainViewModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/4/27.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MCMainViewModel : NSObject

+ (instancetype)sharedInstance;

///获取最新广告信息
- (void)getLatestAds:(NSInteger)width height:(NSInteger)height success:(successBlock)successBlock failure:(failureBlock)failureBlock;



@end
