//
//  ViewController.m
//  MCArt
//
//  Created by Derek on 15/12/18.
//  Copyright © 2015年 zzd. All rights reserved.
//

#import "ViewController.h"
#import "MCLoginViewController.h"
#import "RegisterViewController.h"
#import "MCArtInfoViewController.h"
#import "MCMallViewController.h"
#import "MCTicketViewController.h"
#import "MCAppreciateViewController.h"
#import "MCTicketShowDetailViewController.h"
#import "MCActivityViewController.h"
#import "MCPersonalViewController.h"
#import "MCMainCategoryView.h"
#import "MCPersonalTicketViewController.h"
#import "MCPushModel.h"
#import "MCPushWebViewController.h"
#import "MCActivityDetailViewController.h"
#import "MCAdsModel.h"
#import "MCMainViewModel.h"
#import <SDWebImageManager.h>
#import "BBLaunchAdMonitor.h"
#import "MCUserDefaultUtils.h"
#import "MCLoginViewModel.h"

@interface ViewController ()<MCMainCategoryViewDelegate>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    UIView *statusBar=[[UIView alloc] initWithFrame:CGRectMake(0, 0, kDeviceWidth, 20)];
    statusBar.backgroundColor=kBlackColor;
    [self.view addSubview:statusBar];
    
    
    MCMainCategoryView *appreciate = [[MCMainCategoryView alloc] initWithFrame:CGRectMake(kMainFrameWidth - 130, 80, 120, 100) image:@"main_appreciate" title:@"艺术欣赏" position:MCMainCategoryTitleStyleLeft];
    appreciate.delegate = self;
    [self.view addSubview:appreciate];
    
    MCMainCategoryView *ticket = [[MCMainCategoryView alloc] initWithFrame:CGRectMake(kMainFrameWidth/2 - 40, kMainFrameHeight*0.33, 120, 100) image:@"main_ticket" title:@"掌上购票" position:MCMainCategoryTitleStyleRight];
    ticket.delegate = self;
    [self.view addSubview:ticket];
    
    MCMainCategoryView *info = [[MCMainCategoryView alloc] initWithFrame:CGRectMake(20, CGRectGetMinY(ticket.frame)+20, 100, 100) image:@"main_info" title:@"艺术资讯" position:MCMainCategoryTitleStyleLeftTop];
    info.delegate = self;
    [self.view addSubview:info];
    
    
    MCMainCategoryView *mall = [[MCMainCategoryView alloc] initWithFrame:CGRectMake(CGRectGetMinX(ticket.frame)+10, kMainFrameHeight*0.45, 90, 100) image:@"main_mall" title:@"艺术商城" position:MCMainCategoryTitleStyleRightTop];
    mall.delegate = self;
    [self.view addSubview:mall];
    
    
    MCMainCategoryView *activity = [[MCMainCategoryView alloc] initWithFrame:CGRectMake(CGRectGetMinX(mall.frame)-60, kMainFrameHeight*0.72, 90, 100) image:@"main_activity" title:@"艺术活动" position:MCMainCategoryTitleStyleRightBottom];
    activity.delegate = self;
    [self.view addSubview:activity];
    
    MCMainCategoryView *personal = [[MCMainCategoryView alloc] initWithFrame:CGRectMake(kMainFrameWidth - 110, kMainFrameHeight*0.67, 90, 100) image:@"main_personal" title:@"个人中心" position:MCMainCategoryTitleStyleBottom];
    personal.delegate = self;
    [self.view addSubview:personal];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(payFinished) name:@"payFinished" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotification:) name:@"receiveNotification" object:nil];
    
    [self showADS];
    
    [self getLatestAds];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden=YES;
}


- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)payFinished{
    MCPersonalTicketViewController *ticket = [[MCPersonalTicketViewController alloc] init];
    [self.navigationController pushViewController:ticket animated:YES];
}


- (IBAction)clickLogin:(id)sender {
    MCLoginViewController *login=[[MCLoginViewController alloc] initWithNibName:@"MCLoginViewController" bundle:nil];
    [self.navigationController pushViewController:login animated:YES];
}

- (IBAction)clickRegister:(id)sender {
    RegisterViewController *reg=[[RegisterViewController alloc] initWithNibName:@"RegisterViewController" bundle:nil];
    [self.navigationController pushViewController:reg animated:YES];
}

- (void)clickedWithTitle:(NSString *)title{
    if ([title containsString:@"欣赏"]) {
        MCAppreciateViewController *appreciate=[[MCAppreciateViewController alloc] initWithStyle:UITableViewStyleGrouped refreshType:MCTableViewRefreshTypeTop | MCTableViewRefreshTypeBottom];
        [self.navigationController pushViewController:appreciate animated:YES];
    }else if ([title containsString:@"购票"]) {
        MCTicketViewController *ticket=[[MCTicketViewController alloc] init];
        [self.navigationController pushViewController:ticket animated:YES];
    }else if ([title containsString:@"资讯"]) {
        MCArtInfoViewController *artInfo=[[MCArtInfoViewController alloc] init];
        [self.navigationController pushViewController:artInfo animated:YES];
    }else if ([title containsString:@"商城"]) {
        MCMallViewController *mall=[[MCMallViewController alloc] init];
        [self.navigationController pushViewController:mall animated: YES];
    }else if ([title containsString:@"活动"]) {
        MCActivityViewController *activity=[[MCActivityViewController alloc] initWithStyle:UITableViewStyleGrouped refreshType:MCTableViewRefreshTypeTop | MCTableViewRefreshTypeBottom];
        [self.navigationController pushViewController:activity animated:YES];
    }else if ([title containsString:@"个人中心"]) {
        if (![MCLoginViewModel sharedInstance].isLogin) {
            MCLoginViewController *login = [[MCLoginViewController alloc] init];
            [self.navigationController pushViewController:login animated:YES];
        }else {
            MCPersonalViewController *personal = [[MCPersonalViewController alloc] initWithStyle:UITableViewStyleGrouped];
            [self.navigationController pushViewController:personal animated:YES];
        }
    }
}

- (void)receiveNotification:(NSNotification *)notification{
    NSDictionary *userInfo = notification.userInfo;
    NSInteger type = [[userInfo objectForKey:@"type"] integerValue];
    if (type == 1) {//是文本消息，点击打开APP
        
    }else if (type == 2) {//是艺术资讯，点击打开资讯详情页面 参考newBean
        NSDictionary *newBean = [userInfo objectForKey:@"newBean"];
        NSString *title = [userInfo objectForKey:@"title"];
        NSError *error = nil;
        MCPushDetailModel *detail = [[MCPushDetailModel alloc] initWithDictionary:newBean error:&error];
        if (!error) {
            MCPushWebViewController *web = [[MCPushWebViewController alloc] initWithTitle:title url:detail.url];
            web.share_url = detail.share_url;
            web.share_content = detail.share_content;
            [self.navigationController pushViewController:web animated:YES];
        }
    }else if (type == 3) {//是艺术活动，点击打开对应id 的艺术活动详情页
        NSString *index = [userInfo objectForKey:@"id"];
        MCActivityDetailViewController *detail = [[MCActivityDetailViewController alloc] initWithId:index];
        [self.navigationController pushViewController:detail animated:YES];
    }
}

- (void)showADS {
    NSString *path = [MCUserDefaultUtils valueWithKey:kUserDefaultShowAds];
    SDImageCache *imageCache = [SDImageCache sharedImageCache];
    [imageCache queryDiskCacheForKey:path done:^(UIImage *image, SDImageCacheType cacheType) {
        if (image) {
            [self showAdsWihtImage:image];
        }
    }];
}

- (void)showAdsWihtImage:(UIImage *)image {
    [BBLaunchAdMonitor showImage:image onView:self.view forTime:3];
}

//获取最新的广告
- (void)getLatestAds {
    NSInteger scale = [UIScreen mainScreen].scale;
    NSInteger width = [UIScreen mainScreen].bounds.size.width*scale;
    NSInteger height = [UIScreen mainScreen].bounds.size.height*scale;

    __weak typeof(self) weakSelf = self;
    [[MCMainViewModel sharedInstance] getLatestAds:width height:height success:^(id resquestData) {
        MCAdsModel *ads = resquestData;
        if (ads.pic) {
            [weakSelf downloadAdsPic:kImagePath(ads.pic)];
        }
    } failure:^(NSError *error) {

    }];
}

- (void)downloadAdsPic:(NSString *)picPath {
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:[NSURL URLWithString:picPath] options:SDWebImageRetryFailed progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
        if (finished) {
            NSString *string = imageURL.absoluteString;
            [MCUserDefaultUtils saveValue:string forKey:kUserDefaultShowAds];
        }
    }];

}



@end
