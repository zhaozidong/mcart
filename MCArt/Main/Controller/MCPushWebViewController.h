//
//  MCPushWebViewController.h
//  MCArt
//
//  Created by Derek.zhao on 16/4/11.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseWebViewController.h"

@interface MCPushWebViewController : MCBaseWebViewController

@property (nonatomic, strong) NSString *share_url;

@property (nonatomic, strong) NSString *share_content;






- (instancetype)initWithTitle:(NSString *)title url:(NSString *)url;






@end
