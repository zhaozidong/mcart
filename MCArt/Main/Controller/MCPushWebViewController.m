//
//  MCPushWebViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/4/11.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCPushWebViewController.h"
#import "MCShareView.h"
#import "MCShareViewModel.h"



@interface MCPushWebViewController (){
    NSString *_title;
}

@property (nonatomic, retain) UIButton *shareButton;


@end

@implementation MCPushWebViewController

- (instancetype)initWithTitle:(NSString *)title url:(NSString *)url{
    self = [super init];
    if (self) {
        _title = title;
        self.url = url;
        
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTitle:_title withStyle:MCNavStyleWhite];
    
    
    UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithCustomView:self.shareButton];
    self.navigationItem.rightBarButtonItem = right;
    
    
    [self updateWithURL:self.url];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (UIButton *)shareButton{
    if (!_shareButton) {
        _shareButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 45, 45)];
        [_shareButton setImage:[UIImage imageNamed:@"titleShare_white"] forState:UIControlStateNormal];
        [_shareButton addTarget:self action:@selector(btnDidClickedShare:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _shareButton;
}

- (void)btnDidClickedShare:(id)sender{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    MCShareView *share = [[MCShareView alloc] initWithFrame:self.view.bounds];
    //    share.delegate = self;
    [share updateWithTitle:_title image:nil url:self.url content:self.share_content];
    [window addSubview:share];
}


@end
