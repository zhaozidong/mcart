//
//  MCActivityDemeanourModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/2.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Mantle/Mantle.h>

typedef NS_ENUM(NSInteger,MCActivityDemeanourType) {
    MCActivityDemeanourTypeAll,
    MCActivityDemeanourTypePic = 2,
    MCActivityDemeanourTypeVideo
};


@interface MCActivityDemeanourModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, strong) NSString *id;

@property (nonatomic, strong) NSString *code;

@property (nonatomic, strong) NSString *real_name;

@property (nonatomic, strong) NSString *vote_num;

@property (nonatomic, assign) MCActivityDemeanourType type;

@property (nonatomic, strong) NSString *title;

@property (nonatomic, strong) NSString *pic_path;

@property (nonatomic, strong) NSString *content;

@end
