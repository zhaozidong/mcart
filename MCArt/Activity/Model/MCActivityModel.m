//
//  MCActivityModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/1.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCActivityModel.h"

@implementation MCActivityModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"id" : @"id",
             @"title" : @"title",
             @"pic_path" : @"pic_path",
             @"enter_time_start" : @"enter_time_start",
             @"enter_time_end" : @"enter_time_end",
             @"match_time_start" : @"match_time_start",
             @"match_time_end" : @"match_time_end",
             @"vote_time_start" : @"vote_time_start",
             @"vote_time_end" : @"vote_time_end",
             @"states" : @"states",
             @"content" : @"content"};
}

+ (NSValueTransformer *)statesJSONTransformer{
    return [NSValueTransformer mtl_valueMappingTransformerWithDictionary:@{@"1": @(MCActivityTypeNotStart),
                                                                           @"2": @(MCActivityTypeApplying),
                                                                           @"3": @(MCActivityTypeRacing),
                                                                           @"4": @(MCActivityTypeVoting),
                                                                           @"5": @(MCActivityTypeFinished),
                                                                           @"11":@(MCActivityTypeApplyFinished),
                                                                           @"12":@(MCActivityTypeRaceFinished)}];
}



@end
