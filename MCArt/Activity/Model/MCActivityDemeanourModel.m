//
//  MCActivityDemeanourModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/2.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCActivityDemeanourModel.h"

@implementation MCActivityDemeanourModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"id": @"id",
             @"code" : @"code",
             @"real_name" : @"real_name",
             @"vote_num" : @"vote_num",
             @"type" : @"type",
             @"title" : @"title",
             @"pic_path" : @"pic_path",
             @"content" : @"content"
             };
}

+ (NSValueTransformer *)typeJSONTransformer{
    return [NSValueTransformer mtl_valueMappingTransformerWithDictionary:@{@"0":@(MCActivityDemeanourTypeAll),
                                                                           @"2":@(MCActivityDemeanourTypePic),
                                                                           @"3":@(MCActivityDemeanourTypeVideo)}];
}


@end
