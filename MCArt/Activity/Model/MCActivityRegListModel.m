//
//  MCActivityRegListModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/4.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCActivityRegListModel.h"

@implementation MCActivityRegDetailModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"id" : @"id" ,
             @"name" : @"name",
             @"age" : @"age",
             @"states" : @"states",
             @"pic_url" : @"pic_url"
             };
}

+ (NSValueTransformer *)statesJSONTransformer{
    return [NSValueTransformer mtl_valueMappingTransformerWithDictionary:@{@"1" : @(MCActivityRegStatusReview),
                                                                           @"2" : @(MCActivityRegStatusSuccess),
                                                                           @"3" : @(MCActivityRegStatusFailure)
                                                                           }];
}
@end



@implementation MCActivityRegListModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"total" : @"total",
             @"checking" : @"checking",
             @"no_pass" : @"no_pass",
             @"list" : @"list"};
}

+ (NSValueTransformer *)listJSONTransformer{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        NSArray *jsonArray = value;
        NSMutableArray *listArray = [NSMutableArray array];
        [jsonArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            MCActivityRegDetailModel *detail = [MTLJSONAdapter modelOfClass:[MCActivityRegDetailModel class]fromJSONDictionary:obj error:error];
            [listArray addObject:detail];
        }];
        return listArray;
    }];
}

@end
