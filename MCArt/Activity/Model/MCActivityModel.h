//
//  MCActivityModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/1.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Mantle/Mantle.h>

typedef NS_ENUM(NSInteger, MCActivityType) {
    MCActivityTypeNotStart = 1,
    MCActivityTypeApplying,
    MCActivityTypeRacing,
    MCActivityTypeVoting,
    MCActivityTypeFinished,
    MCActivityTypeApplyFinished,
    MCActivityTypeRaceFinished
};




@interface MCActivityModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, strong) NSString *id;

@property (nonatomic, strong) NSString *title;

@property (nonatomic, strong) NSString *pic_path;

@property (nonatomic, strong) NSString *enter_time_start;

@property (nonatomic, strong) NSString *enter_time_end;

@property (nonatomic, strong) NSString *match_time_start;

@property (nonatomic, strong) NSString *match_time_end;

@property (nonatomic, strong) NSString *vote_time_start;

@property (nonatomic, strong) NSString *vote_time_end;

@property (nonatomic, assign) MCActivityType states;

@property (nonatomic, strong) NSString *content;



@end
