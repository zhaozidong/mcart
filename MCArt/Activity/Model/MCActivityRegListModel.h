//
//  MCActivityRegListModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/4.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Mantle/Mantle.h>
typedef NS_ENUM(NSInteger, MCActivityRegStatus) {
    MCActivityRegStatusReview =1,
    MCActivityRegStatusSuccess,
    MCActivityRegStatusFailure
};


@interface MCActivityRegDetailModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, strong) NSString *id;

@property (nonatomic, strong) NSString *name;

@property (nonatomic, strong) NSString *age;

@property (nonatomic, assign) MCActivityRegStatus states;

@property (nonatomic, strong) NSString *pic_url;

@end



@interface MCActivityRegListModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, strong) NSString *total;

@property (nonatomic, strong) NSString *checking;

@property (nonatomic, strong) NSString *no_pass;

@property (nonatomic, strong) NSArray *list;


@end
