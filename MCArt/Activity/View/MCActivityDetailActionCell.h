//
//  MCActivityDetailActionCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/5/14.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseTableViewCell.h"
#import "MCActivityDetailActionView.h"

@protocol MCActivityDetailActionCellDelegate <NSObject>

- (void)clickedDetailDemeanour;

- (void)clickedDetailEnroll;

- (void)clickedDetailList;


@end





@interface MCActivityDetailActionCell : MCBaseTableViewCell

@property (nonatomic, weak) id<MCActivityDetailActionCellDelegate> delegate;


+ (CGFloat)cellHeight;



@end
