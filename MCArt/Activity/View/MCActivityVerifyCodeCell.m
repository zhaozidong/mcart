//
//  MCActivityVerifyCodeCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/12.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCActivityVerifyCodeCell.h"



@implementation MCActivityVerifyCodeCell

@synthesize delegate;
@synthesize stringValue;
@synthesize textField;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (void)initalizeInputView {
    // Initialization code
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.textField = [[UITextField alloc] initWithFrame:CGRectZero];
    self.textField.autocorrectionType = UITextAutocorrectionTypeDefault;
    self.textField.autocapitalizationType = UITextAutocapitalizationTypeWords;
    self.textField.textAlignment = NSTextAlignmentRight;
    self.textField.textColor = [UIColor darkTextColor];
    self.textField.font = [UIFont systemFontOfSize:17.0f];
    self.textField.clearButtonMode = UITextFieldViewModeNever;
    self.textField.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    [self addSubview:self.textField];
    
    self.actionButton = [[UIButton alloc] initWithFrame:CGRectZero];
    [self.actionButton.titleLabel setFont:kAppFont(14.f)];
    [self.actionButton setBackgroundColor:UIColorFromRGB(0x3ca400)];
    [self.actionButton setTitle:@"获取验证码" forState:UIControlStateNormal];
    self.actionButton.layer.cornerRadius = 3.f;
    [self.actionButton addTarget:self action:@selector(btnDidClickSend:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.actionButton];
    
    
    self.accessoryType = UITableViewCellAccessoryNone;
    
    self.textField.delegate = self;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self initalizeInputView];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initalizeInputView];
    }
    return self;
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    if (selected) {
        [self.textField becomeFirstResponder];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (selected) {
        [self.textField becomeFirstResponder];
    }
}

- (void)setStringValue:(NSString *)value {
    self.textField.text = value;
}

- (NSString *)stringValue {
    return self.textField.text;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (delegate && [delegate respondsToSelector:@selector(tableViewCell:didEndEditingWithString:)]) {
        [delegate tableViewCell:self didEndEditingWithString:self.stringValue];
    }
    UITableView *tableView = nil;
    if ([self.superview isKindOfClass:[UITableView class]]) {
        tableView = (UITableView *)self.superview;
    } else if ([self.superview.superview isKindOfClass:[UITableView class]]) {
        tableView = (UITableView *)self.superview.superview;
    }
    [tableView deselectRowAtIndexPath:[tableView indexPathForCell:self] animated:YES];
}

- (void)btnDidClickSend:(UIButton *)sender{
    if (self.delegate && [self.delegate respondsToSelector:@selector(tableViewCell:didClickSendButton:)]) {
        [self.delegate tableViewCell:self didClickSendButton:sender];
    }
}


- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.actionButton.frame = CGRectMake(CGRectGetWidth(self.bounds)-10-100, (CGRectGetHeight(self.bounds)-34)/2, 100, 34);
    
    CGRect editFrame = CGRectInset(self.contentView.frame, 10, 10);
    
    if (self.textLabel.text && [self.textLabel.text length] != 0) {
        CGSize textSize = [self.textLabel sizeThatFits:CGSizeZero];
        editFrame.origin.x += textSize.width + 10;
        editFrame.size.width -= textSize.width + 120;
        self.textField.textAlignment = NSTextAlignmentRight;
    } else {
        self.textField.textAlignment = NSTextAlignmentLeft;
    }
    
    self.textField.frame = editFrame;
}


@end
