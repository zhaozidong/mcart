//
//  MCDemeanourVoteView.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/12.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MCDemeanourVoteViewDelegate <NSObject>

- (void)clickedVote:(NSString *)phone verify:(NSString *)code;

- (void)clickedVerifyCode:(NSString *)phone button:(id)sender;


@end




@interface MCDemeanourVoteView : UIView


@property (nonatomic, strong) id<MCDemeanourVoteViewDelegate> delegate;

- (void)updateWithName:(NSString *)name;


@end
