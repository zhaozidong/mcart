//
//  MCActivityDetailBannerCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/5/14.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseTableViewCell.h"
#import "MCActivityModel.h"



@interface MCActivityDetailBannerCell : MCBaseTableViewCell

- (void)updateWithActivity:(MCActivityModel *)activity;

+ (CGFloat)cellHeight;


@end
