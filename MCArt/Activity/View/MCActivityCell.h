//
//  MCActivityCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/1.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseTableViewCell.h"
#import "MCActivityModel.h"


@interface MCActivityCell : MCBaseTableViewCell

- (void)updateWithInfo:(MCActivityModel *)info;

+ (CGFloat)cellHeight;

@end
