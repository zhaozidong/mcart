//
//  MCActivityRegListCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/4.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCActivityRegListCell.h"
#import <UIImageView+WebCache.h>


@implementation MCActivityRegListCell{
    UIImageView *_imgHead;
    UILabel *_lblName;
    UILabel *_lblAge;
    UILabel *_lblStatus;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setUpSubViews{
    if (!_imgHead) {
        _imgHead = [[UIImageView alloc] initWithFrame:CGRectMake(16, 8, 50, 50)];
        _imgHead.layer.cornerRadius = 25.f;
        _imgHead.layer.masksToBounds = YES;
    }
    [self.contentView addSubview:_imgHead];
    
    if (!_lblName) {
        _lblName = [[UILabel alloc] init];
    }
    [self.contentView addSubview:_lblName];
    
    if (!_lblAge) {
        _lblAge = [[UILabel  alloc] init];
    }
    [self.contentView addSubview:_lblAge];
    
    if (!_lblStatus) {
        _lblStatus = [[UILabel alloc] init];
    }
    [self.contentView addSubview:_lblStatus];
}


- (void)updateWithInfo:(MCActivityRegDetailModel *)info{
    if (!info) {
        return;
    }

    if (info.pic_url) {
        [_imgHead sd_setImageWithURL:[NSURL URLWithString:kImagePath(info.pic_url)]];
    }
    
    
    if (info.name) {
        _lblName.text = info.name;
        [_lblName sizeToFit];
    }
    
    if (info.age) {
        _lblAge.text = [NSString stringWithFormat:@"%@岁",info.age];
        [_lblAge sizeToFit];
    }
    
    switch (info.states) {
        case MCActivityRegStatusReview: {
            _lblStatus.text = @"审核中";
            _lblStatus.textColor = [UIColor lightGrayColor];
            break;
        }
        case MCActivityRegStatusSuccess: {
            _lblStatus.text = @"报名成功";
            _lblStatus.textColor = UIColorFromRGB(0x009965);
            break;
        }
        case MCActivityRegStatusFailure: {
            _lblStatus.text = @"审核未通过";
            break;
        }
    }
    [_lblStatus sizeToFit];
}


- (void)layoutSubviews{
    [super layoutSubviews];
    _imgHead.frame = CGRectMake(16, 8, 50, 50);
    
    
    CGRect frame = _lblName.frame;
    _lblName.frame = CGRectMake(50+16+8, 10, frame.size.width, frame.size.height);
    
    frame = _lblAge.frame;
    _lblAge.frame = CGRectMake(50+16+8, CGRectGetMaxY(_lblName.frame)+8, frame.size.width, frame.size.height);
    
    frame = _lblStatus.frame;
    _lblStatus.frame = CGRectMake(CGRectGetWidth(self.bounds) - frame.size.width - 16, (CGRectGetHeight(self.bounds)-frame.size.height)/2 , frame.size.width, frame.size.height);
    
}

+ (CGFloat)cellHeight{
    return 50+2*8;
}

@end
