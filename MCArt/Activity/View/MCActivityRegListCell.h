//
//  MCActivityRegListCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/4.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseTableViewCell.h"
#import "MCActivityRegListModel.h"


@interface MCActivityRegListCell : MCBaseTableViewCell

- (void)updateWithInfo:(MCActivityRegDetailModel *)info;

+ (CGFloat)cellHeight;

@end
