//
//  MCActivityDetailActionView.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/1.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCActivityDetailActionView.h"



@implementation MCActivityDetailActionView{
    UIButton *_btnDemeanour;
    UIButton *_btnEnroll;
    UIButton *_btnList;
    UILabel *_lblDemeanour;
    UILabel *_lblEnroll;
    UILabel *_lblList;
}


- (instancetype)init{
    self = [super init];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}


- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}



- (void)setUpSubViews{
    self.backgroundColor=UIColorFromRGB(0xe4e0df);

    if (!_btnDemeanour) {
        _btnDemeanour = [[UIButton alloc] initWithFrame:CGRectMake((kMainFrameWidth - 44*3)/4, 16, 44, 44)];
        [_btnDemeanour setBackgroundImage:[UIImage imageNamed:@"activityDetailDemeanour"] forState:UIControlStateNormal];
        [_btnDemeanour addTarget:self action:@selector(btnDidClickDemeanour:) forControlEvents:UIControlEventTouchUpInside];
    }
    [self addSubview:_btnDemeanour];
    
    if (!_lblDemeanour) {
        _lblDemeanour = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(_btnDemeanour.frame), CGRectGetMaxY(_btnDemeanour.frame)+8, CGRectGetWidth(_btnDemeanour.frame), 20)];
        _lblDemeanour.text = @"活动风采";
        [_lblDemeanour sizeToFit];
    }
    [self addSubview:_lblDemeanour];
    
    
    if (!_btnEnroll) {
        _btnEnroll = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_btnDemeanour.frame) + (kMainFrameWidth - 44*3)/4, 16, 44, 44)];
        [_btnEnroll setBackgroundImage:[UIImage imageNamed:@"activityDetailEnroll"] forState:UIControlStateNormal];
        [_btnEnroll addTarget:self action:@selector(btnDidClickEnroll:) forControlEvents:UIControlEventTouchUpInside];
    }
    [self addSubview:_btnEnroll];
    
    if (!_lblEnroll) {
        _lblEnroll = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(_btnEnroll.frame), CGRectGetMaxY(_btnEnroll.frame)+8, CGRectGetWidth(_btnEnroll.frame), 20)];
        _lblEnroll.text = @"我要报名";
        [_lblEnroll sizeToFit];
    }
    [self addSubview:_lblEnroll];
    
    
    if (!_btnList) {
        _btnList = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_btnEnroll.frame) + (kMainFrameWidth - 44*3)/4, 16, 44, 44)];
        [_btnList setBackgroundImage:[UIImage imageNamed:@"activityDetailList"] forState:UIControlStateNormal];
        [_btnList addTarget:self action:@selector(btnDidClickList:) forControlEvents:UIControlEventTouchUpInside];
    }
    [self addSubview:_btnList];
    
    if (!_lblList) {
        _lblList = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(_lblList.frame), CGRectGetMaxY(_btnList.frame)+8, CGRectGetWidth(_btnList.frame), 20)];
        _lblList.text = @"参赛列表";
        [_lblList sizeToFit];
    }
    [self addSubview:_lblList];
    
}


- (void)layoutSubviews{
    [super layoutSubviews];
    
    _btnDemeanour.frame = CGRectMake((kMainFrameWidth - 44*3)/4, 16, 44, 44);
    _btnEnroll.frame = CGRectMake(CGRectGetMaxX(_btnDemeanour.frame) + (kMainFrameWidth - 44*3)/4, 16, 44, 44);
    _btnList.frame = CGRectMake(CGRectGetMaxX(_btnEnroll.frame) + (kMainFrameWidth - 44*3)/4, 16, 44, 44);
    
    
    CGRect frame = _lblDemeanour.frame;
//    _lblDemeanour.frame = CGRectMake(CGRectGetMinX(_btnDemeanour.frame), CGRectGetMaxY(_btnDemeanour.frame)+8, frame.size.width, frame.size.height);
    
    _lblDemeanour.center = CGPointMake(CGRectGetMidX(_btnDemeanour.frame), CGRectGetMaxY(_btnDemeanour.frame)+8+frame.size.height/2);
    
    frame = _lblEnroll.frame;
//    _lblEnroll.frame = CGRectMake(CGRectGetMinX(_btnEnroll.frame), CGRectGetMaxY(_btnEnroll.frame)+8, frame.size.width, frame.size.height);
    
    _lblEnroll.center = CGPointMake(CGRectGetMidX(_btnEnroll.frame), CGRectGetMaxY(_btnEnroll.frame)+8+frame.size.height/2);
    
    frame = _lblList.frame;
//    _lblList.frame = CGRectMake(CGRectGetMinX(_btnList.frame), CGRectGetMaxY(_btnList.frame)+8, frame.size.width, frame.size.height);
    _lblList.center = CGPointMake(CGRectGetMidX(_btnList.frame), CGRectGetMaxY(_btnList.frame)+8+frame.size.height/2);
}


- (void)btnDidClickDemeanour:(id)sender{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedDemeanour)]) {
        [self.delegate clickedDemeanour];
    }
}

- (void)btnDidClickEnroll:(id)sender{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedEnroll)]) {
        [self.delegate clickedEnroll];
    }
}

- (void)btnDidClickList:(id)sender{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedList)]) {
        [self.delegate clickedList];
    } 
}





@end
