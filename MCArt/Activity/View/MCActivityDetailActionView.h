//
//  MCActivityDetailActionView.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/1.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MCActivityDetailActionDelegate <NSObject>

- (void)clickedDemeanour;

- (void)clickedEnroll;

- (void)clickedList;

@end


@interface MCActivityDetailActionView : UIView


@property(nonatomic, strong) id<MCActivityDetailActionDelegate> delegate;


@end
