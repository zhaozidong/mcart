//
//  MCDemeanourVoteView.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/12.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCDemeanourVoteView.h"


@implementation MCDemeanourVoteView{
    UILabel *_lblName;
    UITextField *_txtPhone;
    UITextField *_txtVerify;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}

- (void)setUpSubViews{
    self.backgroundColor = kRGBA(0, 0, 0, 0.3);
    
    UIView *container = [[UIView alloc] initWithFrame:CGRectMake(0, kMainFrameHeight - 300, kMainFrameWidth, 300)];
    container.backgroundColor = kWhiteColor;
    [self addSubview:container];
    
    _lblName = [[UILabel alloc] initWithFrame:CGRectMake(8, 8, CGRectGetWidth(container.frame)-80, 20)];
    [container addSubview:_lblName];
    
    UIButton *btnClose = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetWidth(container.frame)-8-25, 8, 25, 25)];
    [btnClose setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
    [btnClose addTarget:self action:@selector(btnDidClose) forControlEvents:UIControlEventTouchUpInside];
    [container addSubview:btnClose];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(_lblName.frame)+8, kMainFrameWidth-16, 2)];
    line.backgroundColor = UIColorFromRGB(0xed5e71);
    [container addSubview:line];
    
    _txtPhone = [[UITextField alloc] initWithFrame:CGRectMake(30, CGRectGetMaxY(line.frame)+16, CGRectGetWidth(container.frame)-60, 30)];
    _txtPhone.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _txtPhone.layer.borderWidth = 1.f;
    _txtPhone.layer.cornerRadius = 2.f;
    _txtPhone.keyboardType = UIKeyboardTypePhonePad;
    _txtPhone.placeholder = @"手机号";
    [container addSubview:_txtPhone];
    
    _txtVerify = [[UITextField alloc] initWithFrame:CGRectMake(30, CGRectGetMaxY(_txtPhone.frame)+10, CGRectGetWidth(_txtPhone.frame)-100, 30)];
    _txtVerify.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _txtVerify.layer.borderWidth = 1.f;
    _txtVerify.layer.cornerRadius = 2.f;
    _txtVerify.placeholder = @"短信验证码";
    [container addSubview:_txtVerify];
    
    UIButton *btnVerify = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_txtVerify.frame)+20, CGRectGetMinY(_txtVerify.frame), CGRectGetWidth(_txtPhone.frame)-CGRectGetWidth(_txtVerify.frame)-30, 30)];
    btnVerify.layer.cornerRadius = 2.f;
    [btnVerify.titleLabel setFont:kAppFont(14.f)];
    [btnVerify setBackgroundColor:UIColorFromRGB(0xed5e71)];
    [btnVerify setTitle:@"获取验证码" forState:UIControlStateNormal];
    [btnVerify addTarget:self action:@selector(btnDidClickVerify:) forControlEvents:UIControlEventTouchUpInside];
    [container addSubview:btnVerify];
    
    UIButton *btnVote = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMinX(_txtPhone.frame), CGRectGetMaxY(_txtVerify.frame)+10, CGRectGetWidth(_txtPhone.frame), 30)];
    btnVote.layer.cornerRadius = 2.f;
    btnVote.titleLabel.font = kAppFont(16.f);
    [btnVote setBackgroundColor:UIColorFromRGB(0xed5e71)];
    [btnVote setTitle:@"确认投票" forState:UIControlStateNormal];
    [btnVote addTarget:self action:@selector(btnDidClickVote) forControlEvents:UIControlEventTouchUpInside];
    [container addSubview:btnVote];
    
    UILabel *lblTip = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(btnVote.frame)+10, CGRectGetWidth(container.frame), 20)];
    lblTip.textAlignment = NSTextAlignmentCenter;
    lblTip.font = kAppFont(12.f);
    lblTip.text = @"注意：一个手机号码只能对一个活动投票";
    [container addSubview:lblTip];
}

- (void)updateWithName:(NSString *)name{
    _lblName.text = [NSString stringWithFormat:@"给 %@ 投票",name];
}

- (void)btnDidClose{
    [self removeFromSuperview];
}

- (void)btnDidClickVerify:(id)sender{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedVerifyCode:button:)]) {
        [self.delegate clickedVerifyCode:_txtPhone.text button:sender];
    }
}

- (void)btnDidClickVote{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedVote:verify:)]) {
        [self.delegate clickedVote:_txtPhone.text verify:_txtVerify.text];
    }
}


@end
