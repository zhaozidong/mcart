//
//  MCActivityCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/1.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCActivityCell.h"
#import <UIImageView+WebCache.h>


@implementation MCActivityCell{
    UIImageView *_imgView;
    UIImageView *_imgViewStatus;
}


- (void)setUpSubViews{
    if (!_imgView) {
        _imgView = [[UIImageView alloc] initWithFrame:self.bounds];
        _imgView.contentMode = UIViewContentModeScaleAspectFill;
        _imgView.clipsToBounds = YES;
    }
    [self.contentView addSubview:_imgView];
    
    if (!_imgViewStatus) {
        _imgViewStatus = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 47, 47)];
    }
    [self.contentView addSubview:_imgViewStatus];
}


- (void)updateWithInfo:(MCActivityModel *)info{
    if (!info) {
        return;
    }
    
    if (info.pic_path) {
        [_imgView sd_setImageWithURL:[NSURL URLWithString:kImagePath(info.pic_path)]];
    }
    
    switch (info.states) {
        case MCActivityTypeNotStart: {
            _imgViewStatus.image = [UIImage imageNamed:@"activity_notStart"];
            break;
        }
        case MCActivityTypeApplying: {
            _imgViewStatus.image = [UIImage imageNamed:@"activity_applying"];
            break;
        }
        case MCActivityTypeRacing: {
            _imgViewStatus.image = [UIImage imageNamed:@"activity_racing"];
            break;
        }
        case MCActivityTypeVoting: {
            _imgViewStatus.image = [UIImage imageNamed:@"activity_voting"];
            break;
        }
        case MCActivityTypeFinished: {
            _imgViewStatus.image = [UIImage imageNamed:@"activity_finished"];
            break;
        }
        case MCActivityTypeApplyFinished: {
            _imgViewStatus.image = [UIImage imageNamed:@"activity_applyFinished"];
            break;
        }
        case MCActivityTypeRaceFinished: {
            _imgViewStatus.image = [UIImage imageNamed:@"activity_raceFinished"];
            break;
        }
    }
}

- (void)layoutSubviews{
    [super layoutSubviews];
    _imgView.frame = self.bounds;
    _imgViewStatus.frame = CGRectMake(0, 0, 47, 47);
}


+ (CGFloat)cellHeight{
    return kMainFrameWidth*0.476;
}

@end
