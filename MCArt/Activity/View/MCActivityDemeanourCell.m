//
//  MCActivityDemeanourCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/12.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCActivityDemeanourCell.h"
#import <UIImageView+WebCache.h>


@implementation MCActivityDemeanourCell{
    UIImageView *_imageView;
    UILabel *_lblName;
    UILabel *_lblNumber;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}

- (void)setUpSubViews{
    self.backgroundColor = kWhiteColor;
    self.layer.cornerRadius = 5.f;
    self.layer.masksToBounds = YES;
    
    if (!_imageView) {
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.bounds), CGRectGetHeight(self.bounds)-8-50)];
    }
    [self.contentView addSubview:_imageView];
    
    if (!_lblName) {
        _lblName = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(_imageView.frame)+8, 60, 30)];
    }
    [self.contentView addSubview:_lblName];
    
    if (!_lblNumber) {
        _lblNumber = [[UILabel alloc] initWithFrame:CGRectMake(70, CGRectGetMaxY(_imageView.frame)+8, 60, 30)];
    }
    [self.contentView addSubview:_lblNumber];
    
}

- (void)updateWithDemeanour:(MCActivityDemeanourModel *)info{
    if (!info) {
        return;
    }
    
    if (info.pic_path) {
        [_imageView sd_setImageWithURL:[NSURL URLWithString:kImagePath(info.pic_path)]];
    }
    
    if (info.code && info.real_name) {
        _lblName.text = [NSString stringWithFormat:@"%@ %@",info.code,info.real_name];
    }else{
        _lblName.text = [NSString stringWithFormat:@"%@",info.real_name];
    }
    [_lblName sizeToFit];
    
    if (info.vote_num) {
        _lblNumber.text = [NSString stringWithFormat:@"%@票",info.vote_num];
        [_lblNumber sizeToFit];
    }
    
}


- (void)layoutSubviews{
    [super layoutSubviews];
    
    _imageView.frame = CGRectMake(0, 0, CGRectGetWidth(self.bounds), CGRectGetWidth(self.bounds)/4*3);
    
    CGRect frame = _lblName.frame;
    _lblName.frame = CGRectMake(4, CGRectGetMaxY(_imageView.frame)+8, frame.size.width, frame.size.height);
    
    frame = _lblNumber.frame;
    _lblNumber.frame = CGRectMake(CGRectGetWidth(self.bounds)-frame.size.width-8, CGRectGetMaxY(_imageView.frame)+8, frame.size.width, frame.size.height);
    
}


@end
