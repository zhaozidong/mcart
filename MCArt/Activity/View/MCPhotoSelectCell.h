//
//  MCPhotoSelectCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/3.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseTableViewCell.h"

@protocol MCPhotoSelectCellDelegate <NSObject>

- (void)finishedImageWithPath:(NSString *)imagePath;

@end




@interface MCPhotoSelectCell : MCBaseTableViewCell

@property (nonatomic, copy) NSString *imgPath;

@property (nonatomic, weak) id<MCPhotoSelectCellDelegate> delegate;

@property (nonatomic, weak) UIViewController *parentViewContrller;

@end
