//
//  MCPhotoSelectCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/3.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCPhotoSelectCell.h"
#import "MCActivityViewModel.h"
#import "MCMessageUtilities.h"

@interface  MCPhotoSelectCell ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate>

@end


@implementation MCPhotoSelectCell{
    UIImageView *_imgView;
}


- (void)setUpSubViews{
    if (!_imgView) {
        _imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"photoSelect"]];
        _imgView.frame = CGRectMake(CGRectGetWidth(self.bounds)-8-80, 8, 80, 80);
        _imgView.layer.borderWidth = 1.f;
        _imgView.layer.borderColor = [UIColor lightGrayColor].CGColor;
        _imgView.userInteractionEnabled = YES;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapImgView)];
        tap.numberOfTapsRequired = 1;
        [_imgView addGestureRecognizer:tap];
    }
    [self.contentView addSubview:_imgView];
}

- (void)setImgPath:(NSString *)imgPath{
    _imgPath = imgPath;
    [_imgView sd_setImageWithURL:[NSURL URLWithString:kImagePath(_imgPath)]];
}


- (void)layoutSubviews{
    [super layoutSubviews];
    _imgView.frame = CGRectMake(CGRectGetWidth(self.bounds)-8-80, 8, 80, 80);
}


+ (CGFloat)cellHeight{
    return 96.f;
}


- (void)tapImgView{
    UIActionSheet *action = [[UIActionSheet alloc] initWithTitle:@"上传头像" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"拍照",@"从相册选择", nil];
    [action showInView:self.parentViewContrller.view];
}

#pragma mark UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    UIImagePickerController * picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    if (buttonIndex == 0) {//拍照
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        }else{
            NSLog(@"模拟器无法打开相机");
        }
        [self.parentViewContrller presentViewController:picker animated:YES completion:nil];
    }else if (buttonIndex == 1){//从相册选择
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self.parentViewContrller presentViewController:picker animated:YES completion:nil];
    }
}


#pragma mark UIImagePickerControllerDelegate
//- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(nullable NSDictionary<NSString *,id> *)editingInfo{
//    [picker dismissViewControllerAnimated:YES completion:nil];
//    if (image) {
//        _imgView.image = image;
//    }
//}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    [picker dismissViewControllerAnimated:YES completion:nil];
    if ([info objectForKey:@"UIImagePickerControllerOriginalImage"]) {
        UIImage *img = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        _imgView.image = img;
        __weak typeof(self) weakSelf = self;
        [[MCActivityViewModel sharedInstance] uploadImage:img progress:nil success:^(id resquestData) {
            [weakSelf postSuccess:resquestData];
        } failure:^(NSError *error) {
            [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
        }];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (void)postSuccess:(NSDictionary *)dict{
    if (!dict || ![dict objectForKey:@"path"]) {
        return;
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(finishedImageWithPath:)]) {
        [self.delegate finishedImageWithPath:[dict objectForKey:@"path"]];
    }
}


@end
