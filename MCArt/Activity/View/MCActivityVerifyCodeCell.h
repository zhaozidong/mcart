//
//  MCActivityVerifyCodeCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/12.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "StringInputTableViewCell.h"



@class MCActivityVerifyCodeCell;

@protocol MCActivityVerifyCodeCellDelegate <NSObject>
@optional

- (void)tableViewCell:(MCActivityVerifyCodeCell *)cell didEndEditingWithString:(NSString *)value;
- (void)tableViewCell:(MCActivityVerifyCodeCell *)cell didClickSendButton:(UIButton *)sender;
@end



@interface MCActivityVerifyCodeCell : UITableViewCell<UITextFieldDelegate>{
    UITextField *textField;
}

@property (nonatomic, assign) StringInputCellType cellType;
@property (nonatomic, strong) NSString *stringValue;
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, retain) UIButton *actionButton;


@property (nonatomic, weak)  id<MCActivityVerifyCodeCellDelegate> delegate;


@end
