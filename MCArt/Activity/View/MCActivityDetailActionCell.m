//
//  MCActivityDetailActionCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/5/14.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCActivityDetailActionCell.h"

@interface MCActivityDetailActionCell()<MCActivityDetailActionDelegate>{
    MCActivityDetailActionView *_actionView;
}
@end


@implementation MCActivityDetailActionCell
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (void)setUpSubViews {
    if (!_actionView) {
        _actionView = [[MCActivityDetailActionView alloc] initWithFrame:self.bounds];
        _actionView.delegate = self;
    }
    [self.contentView addSubview:_actionView];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    _actionView.frame = self.bounds;
}

+ (CGFloat)cellHeight {
    return 100;
}

- (void)clickedDemeanour{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedDetailDemeanour)]) {
        [self.delegate clickedDetailDemeanour];
    }
}

- (void)clickedEnroll{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedDetailEnroll)]) {
        [self.delegate clickedDetailEnroll];
    }
}

- (void)clickedList{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedDetailList)]) {
        [self.delegate clickedDetailList];
    }
}




@end
