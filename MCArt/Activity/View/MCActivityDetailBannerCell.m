//
//  MCActivityDetailBannerCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/5/14.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCActivityDetailBannerCell.h"
#import <UIImageView+WebCache.h>



@implementation MCActivityDetailBannerCell{
    UIImageView *_imageView;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/



- (void)setUpSubViews {
    
    if (!_imageView) {
        _imageView = [[UIImageView alloc] initWithFrame:self.bounds];
    }
    [self.contentView addSubview:_imageView];

}

- (void)updateWithActivity:(MCActivityModel *)activity {
    if (!activity) {
        return;
    }
    
    if (activity.pic_path) {
        [_imageView sd_setImageWithURL:[NSURL URLWithString:kImagePath(activity.pic_path)]];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    _imageView.frame = self.bounds;
}

+ (CGFloat)cellHeight {
    return kMainFrameWidth * 0.476;
}

@end
