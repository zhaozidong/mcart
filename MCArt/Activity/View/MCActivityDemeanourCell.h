//
//  MCActivityDemeanourCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/12.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCActivityDemeanourModel.h"

@protocol MCActivityDemeanourCellDelegate <NSObject>

- (void)clickedDemeanour:(MCActivityDemeanourModel *)info;

@end



@interface MCActivityDemeanourCell : UICollectionViewCell

@property (nonatomic, weak) id<MCActivityDemeanourCellDelegate> delegate;

- (void)updateWithDemeanour:(MCActivityDemeanourModel *)info;

@end
