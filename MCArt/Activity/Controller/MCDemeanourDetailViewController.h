//
//  MCDemeanourDetailViewController.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/12.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseViewController.h"
#import "MCActivityDemeanourModel.h"


@interface MCDemeanourDetailViewController : MCBaseViewController

- (instancetype)initWithDemeanour:(MCActivityDemeanourModel *)demeanour;



@end
