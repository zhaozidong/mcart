//
//  MCActivityEnrollViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/3.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCActivityEnrollViewController.h"
#import "CustomUIPickerView.h"
#import "DateInputTableViewCell.h"
#import "PickerInputTableViewCell.h"
#import "SimplePickerInputTableViewCell.h"
#import "StringInputTableViewCell.h"
#import "MCPhotoSelectCell.h"
#import "DateInputTableViewCell.h"
#import "MCLoginViewModel.h"
#import "NSString+Utilities.h"
#import "MCActivityVerifyCodeCell.h"
#import "MCActivityViewModel.h"



static NSString *const stringIdentifier = @"stringCell";
static NSString *const simplePickerIdentifier = @"simplePickerCell";
static NSString *const datePickerIdentifier = @"datePickerCell";
static NSString *const photoIdentifier = @"photoCell";
static NSString *const verifyIdentifier = @"verifyCell";


@interface MCActivityEnrollViewController ()<MCPhotoSelectCellDelegate,StringInputTableViewCellDelegate,SimplePickerInputTableViewCellDelegate,DateInputTableViewCellDelegate,MCActivityVerifyCodeCellDelegate>{
    NSString *_activityId;
    
    NSString *_imagePath;
    NSString *_name;
    NSString *_interest;
    NSString *_skill;
    NSString *_phoneNumber;
    NSString *_gender;
    NSString *_date;
    NSString *_verifyCode;
    
    NSTimer *_timer;
    NSInteger _timeCount;
    UIButton *_verifyButton;

}

@property(nonatomic, retain) UIView *footerView;

@end


@implementation MCActivityEnrollViewController

- (instancetype)initWithActivityId:(NSString *)index{
    self = [super init];
    if (self) {
        _activityId = index;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTitle:@"我要报名" withStyle:MCNavStyleBlack];
    
    _gender = @"男";
    
    self.tableView.tableFooterView = self.footerView;
    
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    if (_timer) {
        [_timer invalidate];
        _timer = nil;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIView *)footerView{
    if (!_footerView) {
        _footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth, 150)];
        UIButton *btnPost = [[UIButton alloc] initWithFrame:CGRectMake(24, 60, kMainFrameWidth - 24*2, 40)];
        [btnPost setBackgroundColor:[UIColor redColor]];
        [btnPost setTitleColor:kWhiteColor forState:UIControlStateNormal];
        [btnPost setTitle:@"提交" forState:UIControlStateNormal];
        [btnPost addTarget:self action:@selector(btnDidPost) forControlEvents:UIControlEventTouchUpInside];
        btnPost.layer.cornerRadius = 5.f;
        [_footerView addSubview:btnPost];
    }
    return _footerView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 8;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){//上传头像
        MCPhotoSelectCell *photoCell = [[MCPhotoSelectCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:photoIdentifier];
        photoCell.delegate = self;
        photoCell.parentViewContrller = self;
        if (_imagePath) {
            photoCell.imgPath = _imagePath;
        }
        photoCell.textLabel.text = @"上传头像";
        return photoCell;
    }else if (indexPath.row == 1){//真实姓名
        StringInputTableViewCell *stringCell = [[StringInputTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:stringIdentifier];
        stringCell.delegate = self;
        stringCell.cellType = StringInputCellTypeName;
        stringCell.textLabel.text = @"真实姓名";
        return stringCell;
    }else if (indexPath.row == 2){//性别
        SimplePickerInputTableViewCell *simpleCell = [[SimplePickerInputTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:simplePickerIdentifier];
        simpleCell.delegate = self;
        simpleCell.textLabel.text = @"性       别";
        simpleCell.values = @[@"男",@"女"];
        simpleCell.value = @"男";
        return simpleCell;
    }else if (indexPath.row == 3){//出生年月
        DateInputTableViewCell *dateCell = [[DateInputTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:datePickerIdentifier];
        dateCell.delegate = self;
        dateCell.textLabel.text = @"出生年月";
        return dateCell;
    }else if (indexPath.row == 4){//手机号码
        StringInputTableViewCell *stringCell = [[StringInputTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:stringIdentifier];
        stringCell.delegate = self;
        stringCell.cellType = StringInputCellTypePhone;
        stringCell.textLabel.text = @"手机号码";
        return stringCell;
    }else if (indexPath.row == 5){//验证码
//        StringInputTableViewCell *stringCell = [[StringInputTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:stringIdentifier];
//        stringCell.delegate = self;
//        
//        stringCell.textLabel.text = @"验  证  码";
//        return stringCell;
        
        MCActivityVerifyCodeCell *verifyCell = [[MCActivityVerifyCodeCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:verifyIdentifier];
        verifyCell.delegate = self;
        verifyCell.cellType = StringInputCellTypeVerifyCode;
        verifyCell.textLabel.text = @"验  证  码";
        return verifyCell;
        
    }else if (indexPath.row == 6){//个人爱好
        StringInputTableViewCell *stringCell = [[StringInputTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:stringIdentifier];
        stringCell.delegate = self;
        stringCell.cellType = StringInputCellTypeInterest;
        stringCell.textLabel.text = @"个人爱好";
        return stringCell;
    }else if (indexPath.row == 7){//参赛才艺
        StringInputTableViewCell *stringCell = [[StringInputTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:stringIdentifier];
        stringCell.delegate = self;
        stringCell.cellType = StringInputCellTypeSkill;
        stringCell.textLabel.text = @"参赛才艺";
        return stringCell;
    }
    return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return 96.f;
    }
    return 44.f;
}

//实现提交保存按钮
- (void)btnDidPost{
    if (![MCLoginViewModel sharedInstance].isLogin) {
        [self showLoginViewController];
    }
    
    if (!_imagePath || [_imagePath isEqualToString:@""]) {
        [MCMessageUtilities showTotast:@"头像不能为空"];
        return;
    }
    
    if (!_name || [_name isEqualToString:@""]) {
        [MCMessageUtilities showTotast:@"姓名不正确"];
        return;
    }
    
    if (!_date || [_date isEqualToString:@""]) {
        [MCMessageUtilities showTotast:@"出生日期不正确"];
        return;
    }
    
    if (!_gender || [_gender isEqualToString:@""]) {
        [MCMessageUtilities showTotast:@"性别不正确"];
        return;
    }
    
    if (!_phoneNumber || [_phoneNumber isEqualToString:@""]) {
        [MCMessageUtilities showTotast:@"出生日期不正确"];
        return;
    }
    
    if (!_verifyCode || [_verifyCode isEqualToString:@""]) {
        [MCMessageUtilities showTotast:@"验证码不正确"];
        return;
    }
    
    if (!_interest || [_interest isEqualToString:@""]) {
        [MCMessageUtilities showTotast:@"兴趣爱好不能为空"];
        return;
    }
    
    if (!_skill || [_skill isEqualToString:@""]) {
        [MCMessageUtilities showTotast:@"技能不能为空"];
        return;
    }
    
    
    NSInteger gen = [_gender isEqualToString:@"男"] ? 1 : 2;
    
    NSDictionary *dict = @{@"id":_activityId,
                           @"name":_name,
                           @"head_img":_imagePath,
                           @"gender":@(gen),
                           @"birth":_date,
                           @"phone":_phoneNumber,
                           @"hobby":_interest,
                           @"talent":_skill,
                           @"user_id":[MCLoginViewModel sharedInstance].userInfo.id,
                           @"phone_code":_verifyCode
                           };
    
    __weak typeof(self) weakSelf = self;
    [[MCActivityViewModel sharedInstance] addMemberByPara:dict success:^(id resquestData) {
        [MCMessageUtilities showSuccessMessage:@"报名成功"];
        [weakSelf.navigationController popViewControllerAnimated:YES];
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}

#pragma mark MCPhotoSelectCellDelegate
- (void)finishedImageWithPath:(NSString *)imagePath{
    _imagePath = imagePath;
}

#pragma mark StringInputTableViewCellDelegate
- (void)tableViewCell:(StringInputTableViewCell *)cell didEndEditingWithString:(NSString *)value{
    switch (cell.cellType) {
        case StringInputCellTypeName: {
            _name = value;
            break;
        }
        case StringInputCellTypeInterest: {
            _interest = value;
            break;
        }
        case StringInputCellTypeSkill: {
            _skill = value;
            break;
        }
        case StringInputCellTypePhone: {
            _phoneNumber = value;
            break;
        }case StringInputCellTypeVerifyCode: {
            _verifyCode = value;
            break;
        }
    }
}

#pragma mark DateInputTableViewCellDelegate
- (void)tableViewCell:(DateInputTableViewCell *)cell didEndEditingWithDate:(NSDate *)value{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"YYYY-MM-dd";
    _date = [formatter stringFromDate:value];
}

#pragma mark SimplePickerInputTableViewCellDelegate
- (void)tableViewCell:(SimplePickerInputTableViewCell *)cell didEndEditingWithValue:(NSString *)value{
    _gender = value;
}


///获取验证码
- (void)tableViewCell:(MCActivityVerifyCodeCell *)cell didClickSendButton:(UIButton *)sender{
    [self.view becomeFirstResponder];
    
    if (!_phoneNumber || [_phoneNumber isEqualToString:@""]) {
        [MCMessageUtilities showTotast:@"电话号码不正确！"];
        return;
    }
    _verifyButton = sender;
    _timeCount = 60;
    _timer = nil;
    if (!_timer) {
        _timer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(timerAction) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSDefaultRunLoopMode];
    }
    sender.enabled = NO;
    sleep(1);
    
    [[MCLoginViewModel sharedInstance] getVerifyCodeByPhone:_phoneNumber success:^(id resquestData) {
        [MCMessageUtilities showSuccessMessage:@"验证码发送成功"];
        
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}

- (void)timerAction {
    if (_timeCount > 0) {
        _timeCount --;
        NSString *title = [NSString stringWithFormat:@"还剩%ld秒",_timeCount];
        [_verifyButton setTitle:title forState:UIControlStateDisabled];
        [_verifyButton setBackgroundColor:[UIColor lightGrayColor]];
        _verifyButton.enabled = NO;
    }else {
        [_timer invalidate];
        _timer = nil;
        _timeCount = 60;
        [_verifyButton setTitle:@"获取验证码" forState:UIControlStateNormal];
        [_verifyButton setBackgroundColor:UIColorFromRGB(0x3ca400)];
        _verifyButton.enabled = YES;
    }
}



@end
