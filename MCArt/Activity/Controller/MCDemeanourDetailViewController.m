//
//  MCDemeanourDetailViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/12.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCDemeanourDetailViewController.h"
#import "MCStyleButton.h"
#import "MCDemeanourVoteView.h"
#import "MCLoginViewModel.h"
#import "MCActivityViewModel.h"

@interface MCDemeanourDetailViewController ()<MCDemeanourVoteViewDelegate>{
    MCActivityDemeanourModel *_demeanour;
    NSTimer *_timer;
    NSInteger _timeCount;
    UIButton *_verifyButton;
}

@property (nonatomic, retain)UIWebView *webView;

@property (nonatomic, retain)MCStyleButton *voteButton;

@end

@implementation MCDemeanourDetailViewController

- (instancetype)initWithDemeanour:(MCActivityDemeanourModel *)demeanour{
    self = [super init];
    if (self) {
        _demeanour = demeanour;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"活动风采" withStyle:MCNavStyleWhite];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.voteButton];
    
    [self.view addSubview:self.webView];
    
    NSString *strURL = [NSString stringWithFormat:@"%@%@?id=%@",kAppHost,@"appActivity/activityShowContent",_demeanour.id];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:strURL]]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    if (_timer) {
        [_timer invalidate];
        _timer = nil;
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (MCStyleButton *)voteButton{
    if (!_voteButton) {
        _voteButton = [[MCStyleButton alloc] initWithFrame:CGRectMake(0, 0, 50, 25)];
        [_voteButton setTitle:@"投票" forState:UIControlStateNormal];
        [_voteButton addTarget:self action:@selector(clickedVote:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _voteButton;
}


- (UIWebView *)webView{
    if (!_webView) {
        _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth, kMainFrameHeight-64)];
    }
    return _webView;
}

- (void)clickedVote:(UIButton *)sender{
    MCDemeanourVoteView *vote = [[MCDemeanourVoteView alloc] initWithFrame:self.view.bounds];
    vote.delegate = self;
    [vote updateWithName:_demeanour.real_name];
    [self.view addSubview:vote];
}

- (void)clickedVote:(NSString *)phone verify:(NSString *)code{
    [[MCActivityViewModel sharedInstance] voteWithId:_demeanour.id phone:phone verify:code success:^(id resquestData) {
        [MCMessageUtilities showSuccessMessage:resquestData];
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}

- (void)clickedVerifyCode:(NSString *)phone button:(id)sender{
    _verifyButton = sender;
    _timeCount = 60;
    _timer = nil;
    if (!_timer) {
        _timer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(timerAction) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSDefaultRunLoopMode];
    }
    _verifyButton.enabled = NO;
    sleep(1);
    [[MCLoginViewModel sharedInstance] getVerifyCodeByPhone:phone success:^(id resquestData) {
        [MCMessageUtilities showSuccessMessage:@"验证码发送成功"];
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}


- (void)timerAction{
    if (_timeCount > 0) {
        _timeCount --;
        NSString *title = [NSString stringWithFormat:@"还剩%ld秒",_timeCount];
        [_verifyButton setTitle:title forState:UIControlStateDisabled];
        [_verifyButton setBackgroundColor:[UIColor lightGrayColor]];
        _verifyButton.enabled = NO;
    }else {
        [_timer invalidate];
        _timer = nil;
        _timeCount = 60;
        [_verifyButton setTitle:@"获取验证码" forState:UIControlStateNormal];
        [_verifyButton setBackgroundColor:UIColorFromRGB(0xed5e71)];
        _verifyButton.enabled = YES;
    }
}





@end
