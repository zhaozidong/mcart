//
//  MCActivityListViewController.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/2.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseTableViewController.h"

@interface MCActivityListViewController : MCBaseTableViewController

- (instancetype)initWithActivityID:(NSString *)activityId isApply:(BOOL)isApply;


@end
