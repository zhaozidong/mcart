//
//  MCActivityListViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/2.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCActivityListViewController.h"
#import "MCActivityViewModel.h"
#import "MCActivityRegListModel.h"
#import "MCActivityRegListCell.h"
#import "MCStyleButton.h"
#import "MCActivityEnrollViewController.h"

static NSString *const detailIdentifier = @"detailCell";

@implementation MCActivityListViewController{
    NSString *_activityId;
    __block NSInteger _pageIndex;
    __block MCActivityRegListModel *_list;
    BOOL _isApply;
}

- (instancetype)initWithActivityID:(NSString *)activityId isApply:(BOOL)isApply{
    self = [super init];
    if (self) {
        _activityId = activityId;
        _isApply = isApply;
    }
    return self;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTitle:@"报名列表" withStyle:MCNavStyleBlack];
    
    if (_isApply) {
        MCStyleButton *btnEnroll = [[MCStyleButton alloc] initWithFrame:CGRectMake(0, 0, 45, 22)];
        [btnEnroll setTitle:@"报名" forState:UIControlStateNormal];
        [btnEnroll addTarget:self action:@selector(btnDidClickEnroll:) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:btnEnroll];
    }
    
    [self.tableView registerClass:[MCActivityRegListCell class] forCellReuseIdentifier:detailIdentifier];
    
    [self getData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)getData{
    _pageIndex = 1;
    __weak typeof(self) weakSelf = self;
    [[MCActivityViewModel sharedInstance] getRegistrationList:_activityId index:_pageIndex success:^(id resquestData) {
        _list = resquestData;
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}

- (void)btnDidClickEnroll:(id)sender{
    MCActivityEnrollViewController *enroll = [[MCActivityEnrollViewController alloc] init];
    [self.navigationController pushViewController:enroll animated:YES];
}


#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _list.list.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MCActivityRegDetailModel *detail = [_list.list objectAtIndex:indexPath.row];
    MCActivityRegListCell *cell = [tableView dequeueReusableCellWithIdentifier:detailIdentifier];
    [cell updateWithInfo:detail];
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [MCActivityRegListCell cellHeight];
}






@end
