//
//  MCActivityDemeanourViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/2.
//  Copyright © 2016年 zzd. All rights reserved.
//  活动风采

#import "MCActivityDemeanourViewController.h"
#import "FL_WaterFallLayout.h"
#import "MCActivityViewModel.h"
#import "MCActivityDemeanourModel.h"
#import "MCActivityDemeanourCell.h"
#import <MJRefresh.h>
#import "MCDemeanourDetailViewController.h"

static NSString *const demeanourIdentifier = @"demeanourCell";

@interface MCActivityDemeanourViewController () <FL_WaterFallLayoutDelegate,UICollectionViewDataSource,UICollectionViewDelegate>{
    NSString *_activityId;
    __block NSArray *_arrList;
    __block NSInteger _pageIndex;
    MCActivityDemeanourType _type;
}

@property (nonatomic, strong) UISegmentedControl *titleView;

@property (nonatomic, strong)UICollectionView *collectionView;

@end


@implementation MCActivityDemeanourViewController
- (instancetype)initWithActivityId:(NSString *)activityId{
    self = [super init];
    if (self) {
        _activityId = activityId;
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    [self setTitle:@"活动风采" withStyle:MCNavStyleBlack];
    
    self.navigationItem.titleView = self.titleView;
    
    self.titleView.selectedSegmentIndex = 0;
    _pageIndex = 1;
    _type = MCActivityDemeanourTypePic;
    
    [self.view addSubview:self.collectionView];
    
    [self getData];
}


- (UISegmentedControl *)titleView{
    if (!_titleView) {
        _titleView  =  [[UISegmentedControl alloc] initWithItems:@[@"图片",@"视频"]];
        _titleView.frame = CGRectMake(0, 0, 60, 30);
        [_titleView addTarget:self action:@selector(clickedSeg:) forControlEvents:UIControlEventValueChanged];
    }
    return _titleView;
}


- (UICollectionView *)collectionView{
    if (!_collectionView) {
        FL_WaterFallLayout *waterFall = [[FL_WaterFallLayout alloc] init];
        waterFall.delegate = self;
        
        _collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:waterFall];
        _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _collectionView.dataSource = self;
        _collectionView.delegate = self;
        
        _collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(beginRefreshHeader:)];
        _collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(beginRefreshFooter:)];
        
        [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"basicCell"];
        [_collectionView registerClass:[MCActivityDemeanourCell class] forCellWithReuseIdentifier:demeanourIdentifier];
    }
    return _collectionView;
}


- (void)getData{
    __weak typeof(self) weakSelf = self;
    [[MCActivityViewModel sharedInstance] getActivityShowList:_activityId type:_type page:_pageIndex success:^(id resquestData) {
        _arrList = resquestData;
        [weakSelf.collectionView reloadData];
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _arrList.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MCActivityDemeanourModel *demeanour = [_arrList objectAtIndex:indexPath.row];
    MCActivityDemeanourCell *demeanourCell = [collectionView dequeueReusableCellWithReuseIdentifier:demeanourIdentifier forIndexPath:indexPath];
    [demeanourCell updateWithDemeanour:demeanour];
    return demeanourCell;
}

//计算高度
-(CGFloat)waterFlowLayout:(FL_WaterFallLayout*)waterFlowLayout heightForWidth:(CGFloat)width indexPath:(NSIndexPath*)indexPath{
//    if (indexPath.row%3 == 0) {
//        return 190;
//    }else if (indexPath.row%3 == 1){
//        return 170;
//    }else{
//        return 160;
//    }
    return ((kMainFrameWidth-3*8)/2)/4*3+30;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    MCActivityDemeanourModel *demeanour = [_arrList objectAtIndex:indexPath.row];
    MCDemeanourDetailViewController *detail = [[MCDemeanourDetailViewController alloc] initWithDemeanour:demeanour];
    [self.navigationController pushViewController:detail animated:YES];
}


#pragma mark MJRefresh

- (void)beginRefreshHeader:(MJRefreshHeader *)header{
    [header endRefreshing];
    _pageIndex = 1;
    [self getData];
}

- (void)beginRefreshFooter:(MJRefreshFooter *)footer{
    [footer endRefreshing];
    [self getData];
}


- (void)clickedSeg:(UISegmentedControl *)seg{
    if (seg.selectedSegmentIndex == 0) {
        _type = MCActivityDemeanourTypePic;
    }else if (seg.selectedSegmentIndex == 1) {
        _type = MCActivityDemeanourTypeVideo;
    }
    _pageIndex = 1;
    [self getData];
}

@end
