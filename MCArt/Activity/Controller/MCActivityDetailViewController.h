//
//  MCActivityDetailViewController.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/1.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseViewController.h"

@interface MCActivityDetailViewController : MCBaseViewController


- (instancetype)initWithId:(NSString *)index;


@end
