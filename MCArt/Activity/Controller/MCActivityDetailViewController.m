//
//  MCActivityDetailViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/1.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCActivityDetailViewController.h"
#import "MCActivityDetailActionView.h"
#import "MCActivityViewModel.h"
#import "MCActivityModel.h"
#import <UIImageView+WebCache.h>
#import "MCActivityDemeanourViewController.h"
#import "MCActivityEnrollViewController.h"
#import "MCActivityListViewController.h"
#import "MCActivityDetailViewController.h"
#import "MCActivityDetailBannerCell.h"
#import "MCActivityDetailActionCell.h"

static NSString *const bannerIdentifier = @"bannerCell";
static NSString *const actionIdentifier = @"actionCell";

@interface MCActivityDetailViewController ()<UITableViewDelegate,UITableViewDataSource, MCActivityDetailActionCellDelegate,UIWebViewDelegate>{
    NSString *_index;
    __block MCActivityModel *_detailData;
}
@property(nonatomic, strong)UITableView *tableView;
@property(nonatomic, strong)UIWebView *webView;
@end

@implementation MCActivityDetailViewController
- (instancetype)initWithId:(NSString *)index{
    self = [super init];
    if (self) {
        _index = index;
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self setTitle:@"活动详情" withStyle:MCNavStyleBlack];
    self.view.backgroundColor=kWhiteColor;

    [self.view addSubview:self.tableView];
    
    [self getData];
}

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth
                                                                   , kMainFrameHeight-64)];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        
        [_tableView registerClass:[MCActivityDetailBannerCell class] forCellReuseIdentifier:bannerIdentifier];
        [_tableView registerClass:[MCActivityDetailActionCell class] forCellReuseIdentifier:actionIdentifier];
        
        _tableView.tableFooterView = [UIView new];
    }
    
    return _tableView;
}

- (UIWebView *)webView{
    if (!_webView) {
        _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth, 400)];
        _webView.delegate = self;
        _webView.scrollView.scrollEnabled = NO;
    }
    return _webView;
}

- (void)getData{
    __weak typeof(self) weakSelf = self;
    [[MCActivityViewModel sharedInstance] getActivityDetail:_index success:^(id resquestData) {
        _detailData = [resquestData objectAtIndex:0];
        [weakSelf loadDetailData];
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}

- (void)loadDetailData{
    if (!_detailData) {
        return;
    }
    
    NSString *strURL = [NSString stringWithFormat:@"%@/appActivity/activityContentPage?id=%@",kAppHost,_detailData.id];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:strURL]]];

}

#pragma makr UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        MCActivityDetailBannerCell *bannerCell = [tableView dequeueReusableCellWithIdentifier:@"bannerCell"];
        [bannerCell updateWithActivity:_detailData];
        return bannerCell;
    }else if (indexPath.row == 1) {
        MCActivityDetailActionCell *actionCell = [tableView dequeueReusableCellWithIdentifier:@"actionCell"];
        actionCell.delegate = self;
        return actionCell;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return [MCActivityDetailBannerCell cellHeight];
    }else if (indexPath.row == 1) {
        return [MCActivityDetailActionCell cellHeight];
    }
    return 44;
}


#pragma mark UIWebViewDelegate
- (void)webViewDidFinishLoad:(UIWebView *)webView {
    NSString *result = [webView stringByEvaluatingJavaScriptFromString:@"document.body.offsetHeight;"];
    NSInteger height = [result integerValue];
    self.webView.frame = CGRectMake(0, 0, kMainFrameWidth, height+20);
    self.tableView.tableFooterView = self.webView;
}


#pragma mark MCActivityDetailActionDelegate
- (void)clickedDetailDemeanour{
    if (_detailData.states == MCActivityTypeVoting || _detailData.states == MCActivityTypeFinished) {
        MCActivityDemeanourViewController *demeanour = [[MCActivityDemeanourViewController alloc] initWithActivityId:_index];
        [self.navigationController pushViewController:demeanour animated:YES];
    }else {
        [MCMessageUtilities showTotast:@"等待活动投票或者结束才可查看"];
        return;
    }
}

- (void)clickedDetailEnroll{
    if (_detailData.states == MCActivityTypeApplying) {
        MCActivityEnrollViewController *enroll = [[MCActivityEnrollViewController alloc] initWithActivityId:_index];
        [self.navigationController pushViewController:enroll animated:YES];
    }else {
        [MCMessageUtilities showTotast:@"该活动不在报名期间，不可报名"];
        return;
    }
}

- (void)clickedDetailList{
    if (_detailData.states == MCActivityTypeApplying) {
        MCActivityListViewController *list = [[MCActivityListViewController alloc] initWithActivityID:_index isApply:_detailData.states == MCActivityTypeApplying];
        [self.navigationController pushViewController:list animated:YES];
    }else {
        [MCMessageUtilities showTotast:@"该活动不在报名期间，不可查看"];
        return;
    }
}


@end
