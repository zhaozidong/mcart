//
//  MCActivityViewModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/1.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MCActivityDemeanourModel.h"

@interface MCActivityViewModel : NSObject

+ (instancetype)sharedInstance;

///活动列表
- (void)getActivityList:(NSInteger)pageIndex success:(successBlock)successBlock failure:(failureBlock)failureBlock;

///活动详情
- (void)getActivityDetail:(NSString *)index success:(successBlock)successBlock failure:(failureBlock)failure;

///活动风采列表
- (void)getActivityShowList:(NSString *)index type:(MCActivityDemeanourType)type page:(NSInteger)pageIndex success:(successBlock)successBlock failure:(failureBlock)failureBlock;

///上传图片
- (void)uploadImage:(UIImage *)image progress:(uploadBlock)uploadBlock success:(successBlock)successBlock failure:(failureBlock)failureBlock;

///活动参赛列表
- (void)getRegistrationList:(NSString *)index index:(NSInteger)pageIndex success:(successBlock)successBlock failure:(failureBlock)failureBlock;

///活动报名
- (void)addMemberByPara:(NSDictionary *)para success:(successBlock)successBlock failure:(failureBlock)failureBlock;

///活动投票
- (void)voteWithId:(NSString *)index phone:(NSString *)phone verify:(NSString *)verify success:(successBlock)successBlock failure:(failureBlock)failureBlock;


@end
