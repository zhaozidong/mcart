//
//  MCActivityViewModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/1.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCActivityViewModel.h"
#import "MCNetworkManager.h"
#import "MCActivityModel.h"
#import "MCActivityRegListModel.h"


@implementation MCActivityViewModel

SINGLETON_FOR_CLASS(MCActivityViewModel);

//获取活动列表
- (void)getActivityList:(NSInteger)pageIndex success:(successBlock)successBlock failure:(failureBlock)failureBlock {
    NSString *page = [NSString stringWithFormat:@"%ld",(long)pageIndex];
    NSDictionary *dict = @{@"page": page , @"pageSize" : MC_PAGE_SIZE};
    [[MCNetworkManager sharedInstance] postWithURL:@"appActivity/getActivityList" parameter:dict success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            NSError *err=nil;
            NSArray *array=[MTLJSONAdapter modelsOfClass:[MCActivityModel class] fromJSONArray:[resquestData objectForKey:@"list"] error:&err];
            if (!err) {
                successBlock(array);
            }else{
                if (failureBlock) {
                    failureBlock(err);
                }
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}

//获取活动详情
- (void)getActivityDetail:(NSString *)index success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    [[MCNetworkManager sharedInstance] postWithURL:@"appActivity/getActivityContent" parameter:@{@"id": index} success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            NSError *err=nil;
            NSArray *array=[MTLJSONAdapter modelsOfClass:[MCActivityModel class] fromJSONArray:[resquestData objectForKey:@"list"] error:&err];
            if (!err) {
                successBlock(array);
            }else{
                if (failureBlock) {
                    failureBlock(err);
                }
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}

///活动风采列表
- (void)getActivityShowList:(NSString *)index type:(MCActivityDemeanourType)type page:(NSInteger)pageIndex success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    NSDictionary *dict = @{@"id":index,@"type":@(type),@"page":@(pageIndex),@"pageSize":MC_PAGE_SIZE};
    [[MCNetworkManager sharedInstance] postWithURL:@"appActivity/getActivityShowList" parameter:dict success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            NSError *err=nil;
            NSArray *array=[MTLJSONAdapter modelsOfClass:[MCActivityDemeanourModel class] fromJSONArray:[resquestData objectForKey:@"list"] error:&err];
            if (!err) {
                successBlock(array);
            }else{
                if (failureBlock) {
                    failureBlock(err);
                }
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}


//上传头像照片
- (void)uploadImage:(UIImage *)image progress:(uploadBlock)uploadBlock success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    NSData *imgData= UIImageJPEGRepresentation(image,0.1);
    [[MCNetworkManager sharedInstance] uploadWithURL:@"appUser/uploadPic" data:imgData uploadProgress:^(NSProgress *uploadProgress) {
        if (uploadBlock) {
            uploadBlock(uploadProgress);
        }
    } success:^(id resquestData) {
        if (successBlock) {
            successBlock(resquestData);
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}


//获取报名列表
- (void)getRegistrationList:(NSString *)index index:(NSInteger)pageIndex success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    NSDictionary *dict = @{@"id" : index, @"page" :[NSString stringWithFormat:@"%ld",(long)pageIndex], @"pageSize" : MC_PAGE_SIZE};
    [[MCNetworkManager sharedInstance] postWithURL:@"appActivity/getMemberList" parameter:dict success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]){
            NSError *err;
            MCActivityRegListModel *list = [MTLJSONAdapter modelOfClass:[MCActivityRegListModel class] fromJSONDictionary:resquestData error:&err];
            if (!err) {
                if (successBlock) {
                    successBlock(list);
                }
            }else{
                if (failureBlock) {
                    failureBlock(err);
                }
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}


//报名
- (void)addMemberByPara:(NSDictionary *)para success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    [[MCNetworkManager sharedInstance] postWithURL:@"appActivity/addMember" parameter:para success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            if (successBlock) {
                successBlock([resquestData objectForKey:@"info"]);
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}


///活动投票
- (void)voteWithId:(NSString *)index phone:(NSString *)phone verify:(NSString *)verify success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    NSDictionary *dict = @{@"showid":index,@"phone":phone,@"phonecode":verify};
    [[MCNetworkManager sharedInstance] postWithURL:@"appActivity/appAddVote" parameter:dict success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            if (successBlock) {
                successBlock([resquestData objectForKey:@"info"]);
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}


@end
