//
//  MCAppreciateModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/1/27.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Mantle/Mantle.h>

//typedef NS_ENUM(NSInteger, MCAppreciateType){
//    MCAppreciateType
//};

typedef NS_ENUM(NSInteger,MCAppreciateShowType) {
    MCArtInfoShowTypeText = 1,
    MCArtInfoShowTypePic,
    MCArtInfoShowTypeVideo
};


@interface MCAppreciatePicModel : MTLModel<MTLJSONSerializing>

@property(nonatomic, strong)NSString *src;

@property(nonatomic, strong)NSString *descr;

@end


@interface MCAppreciateVideoModel : MTLModel<MTLJSONSerializing>

@property(nonatomic, strong)NSString *src;

@property(nonatomic, strong)NSString *type;

@end


@interface MCAppreciateModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, strong) NSString *id;

@property (nonatomic, strong) NSString *title;

@property (nonatomic, strong) NSString *time;

//0为全部、1为音舞、2为美术、3为文学、4为戏曲、5为名家
@property (nonatomic, strong) NSString *type;

//展示类型  1文本资讯 2图集 3视频
@property (nonatomic, assign) MCAppreciateShowType class_type;

//文本资讯内容，图集介绍，视频介绍,相当于副标题
@property (nonatomic, strong) NSString *content;

//封面图片
@property (nonatomic, strong) NSString *pic_path;

//图集时有值
@property (nonatomic, strong) NSArray *imgs_list;

//视频地址，视频时有值
@property (nonatomic, strong) MCAppreciateVideoModel *video;

//分享用的url
@property (nonatomic, strong) NSString *share_url;

//分享用的url
@property (nonatomic, strong) NSString *share_content;



@end
