//
//  MCAppreciateModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/27.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCAppreciateModel.h"

@implementation MCAppreciatePicModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"src": @"src", @"descr":@"descr"};
}
@end



@implementation MCAppreciateVideoModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"src": @"src",@"type":@"type"};
}
@end


@implementation MCAppreciateModel
+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"id" : @"id",
             @"title" : @"title",
             @"time" : @"time",
             @"type" : @"type",
             @"class_type" : @"class_type",
             @"content" : @"content",
             @"pic_path" : @"pic_path",
             @"imgs_list" : @"imgs_list",
             @"video" : @"video",
             @"share_url" : @"share_url",
             @"share_content" : @"share_content"
             };
}

//+ (NSValueTransformer *)typeJSONTransformer {
//    return [NSValueTransformer mtl_valueMappingTransformerWithDictionary:@{
//                                                                           @"1": @(MCArtInfoNewestTypeLocal),
//                                                                           @"2": @(MCArtInfoNewestTypeInternal),
//                                                                           @"3": @(MCArtInfoNewestTypeForeign),
//                                                                           @"4": @(MCArtInfoNewestTypeTopic)
//                                                                           }];
//}


+ (NSValueTransformer *)class_typeJSONTransformer {
    return [NSValueTransformer mtl_valueMappingTransformerWithDictionary:@{
                                                                           @"1": @(MCArtInfoShowTypeText),
                                                                           @"2": @(MCArtInfoShowTypePic),
                                                                           @"3": @(MCArtInfoShowTypeVideo)
                                                                           }];
}


+ (NSValueTransformer *)imgs_listJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        NSArray *jsonArray = value;
        NSMutableArray *imgArray = [NSMutableArray array];
        [jsonArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            MCAppreciatePicModel *picture = [MTLJSONAdapter modelOfClass:[MCAppreciatePicModel class] fromJSONDictionary:obj error:error];
            [imgArray addObject:picture];
        }];
        return imgArray;
    }];
}


+ (NSValueTransformer *)videoJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        MCAppreciateVideoModel *video = [MTLJSONAdapter modelOfClass:[MCAppreciateVideoModel class] fromJSONDictionary:value error:error];
        return video;
    }];
}

@end
