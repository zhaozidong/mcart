//
//  MCAppreciatePicCollectionCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/31.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCAppreciatePicCollectionCell.h"
#import <UIImageView+WebCache.h>


@implementation MCAppreciatePicCollectionCell{
    UIImageView *_imgView;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}

- (void)setUpSubViews{
    if (!_imgView) {
        _imgView = [[UIImageView  alloc] initWithFrame:self.bounds];
    }
    [self.contentView addSubview:_imgView];
}

- (void)updateWithURL:(NSString *)strURL{
    if (strURL) {
        [_imgView sd_setImageWithURL:[NSURL URLWithString:kImagePath(strURL)]];
    }
}


@end
