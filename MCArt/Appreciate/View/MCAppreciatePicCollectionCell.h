//
//  MCAppreciatePicCollectionCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/1/31.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MCAppreciatePicCollectionCell : UICollectionViewCell


- (void)updateWithURL:(NSString *)strURL;


@end
