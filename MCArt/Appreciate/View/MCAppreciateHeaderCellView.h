//
//  MCAppreciateHeaderCellView.h
//  MCArt
//
//  Created by Derek.zhao on 16/1/7.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MCAppreciateHeaderCellView : UIView

- (void)updateWithTitle:(NSString *)title path:(NSString *)strPath;

@end
