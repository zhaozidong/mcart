//
//  MCAppreciateLiteratureCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/20.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCAppreciateModel.h"


@interface MCAppreciateLiteratureCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;


@property (weak, nonatomic) IBOutlet UILabel *lblTime;



- (void)updateWithInfo:(MCAppreciateModel *)info;



@end
