//
//  MCAppreciateVideoCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/1/12.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCAppreciateModel.h"



@interface MCAppreciateVideoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@property (weak, nonatomic) IBOutlet UIImageView *videoImageView;


- (void)updateWithInfo:(MCAppreciateModel *)info;


+ (CGFloat)cellHeight;

@end
