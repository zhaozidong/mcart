//
//  MCAppreciateTextCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/1/31.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseTableViewCell.h"
#import "MCAppreciateModel.h"


@interface MCAppreciateTextCell : MCBaseTableViewCell

- (void)updateWithInfo:(MCAppreciateModel *)info;

+ (CGFloat)cellHeight;

@end
