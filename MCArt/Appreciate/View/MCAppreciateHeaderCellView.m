//
//  MCAppreciateHeaderCellView.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/7.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCAppreciateHeaderCellView.h"

@implementation MCAppreciateHeaderCellView{
    UIImageView *_imgView;
    UILabel *_lblTitle;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
//        self.backgroundColor=[UIColor greenColor];
        [self setUpSubViews];
    }
    return self;
}

- (void)setUpSubViews{
    if (!_imgView) {
        _imgView=[[UIImageView alloc] init];
    }
    [self addSubview:_imgView];
    
    if (!_lblTitle) {
        _lblTitle=[[UILabel alloc] init];
        
    }
    [self addSubview:_lblTitle];
}


- (void)updateWithTitle:(NSString *)title path:(NSString *)strPath{
    if (title) {
        _lblTitle.text=title;
        [_lblTitle sizeToFit];
    }
    
    if (strPath) {
        UIImage *img=[UIImage imageNamed:strPath];
        _imgView.image=img;
        _imgView.layer.cornerRadius=img.size.width/2;
    }
}


- (void)layoutSubviews{
    [super layoutSubviews];
    
    _imgView.frame=CGRectMake(16, 16, CGRectGetWidth(self.bounds)-2*16, CGRectGetWidth(self.bounds)-2*16);
    
    CGRect frame=_lblTitle.frame;
    _lblTitle.frame=CGRectMake((CGRectGetWidth(self.bounds)-frame.size.width)/2, CGRectGetMaxY(_imgView.frame)+8, frame.size.width, frame.size.height);
    
}

@end
