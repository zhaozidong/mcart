//
//  MCAppreciateHeaderContainerView.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/11.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCAppreciateHeaderContainerView.h"
#import "MCAppreciateHeaderCellView.h"

@implementation MCAppreciateHeaderContainerView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}

- (void)setUpSubViews{
    CGFloat width=kMainFrameWidth/5;
    CGFloat height=width+20;
    NSArray *arrTitle=@[@"音舞",@"美术",@"文学",@"戏曲",@"名家"];
    
    for (int i =0 ; i < 5 ; i++) {
        MCAppreciateHeaderCellView *cell=[[MCAppreciateHeaderCellView alloc] initWithFrame:CGRectMake(i * width, 0, width, height)];
        cell.tag=i;
        NSString *strPath=[NSString stringWithFormat:@"xinsh%d",i + 1];
        [cell updateWithTitle:[arrTitle objectAtIndex:i] path:strPath];
        [self addSubview:cell];
    }
    
    UITapGestureRecognizer *gesture=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickedHeaderCell:)];
    gesture.numberOfTapsRequired=1;
    gesture.cancelsTouchesInView=YES;
    [self addGestureRecognizer:gesture];
}

- (void)clickedHeaderCell:(UITapGestureRecognizer *)recognizer{
    CGPoint pt=[recognizer locationInView:self];
    for (UIView *vi in self.subviews) {
        if(CGRectContainsPoint([vi frame], pt)){
            if (self.clickBlock) {
                self.clickBlock(vi.tag);
            }
        }
    }
}


@end
