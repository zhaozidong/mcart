//
//  MCAppreciatePicCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/31.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCAppreciatePicCell.h"
#import "MCAppreciatePicCollectionCell.h"


@interface MCAppreciatePicCell ()<UICollectionViewDataSource,UICollectionViewDelegate>{
    UILabel *_lblTitle;
    UILabel *_lblTime;
    UICollectionView *_collectionView;
    NSInteger _itemCount;
    MCAppreciateModel *_info;
}
@end


@implementation MCAppreciatePicCell

- (void)setUpSubViews{
    _itemCount = 0;
    
    if (!_lblTitle) {
        _lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(8, 8, kMainFrameWidth-16, 30)];
        _lblTitle.font = kAppFontBold(20.f);
    }
    [self.contentView addSubview:_lblTitle];
    
    
    if (!_lblTime) {
        _lblTime = [[UILabel alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(_lblTitle.frame)+8, kMainFrameWidth - 16, 15)];
        _lblTime.textColor = [UIColor lightGrayColor];
    }
    [self.contentView addSubview:_lblTime];
    
    
    if (!_collectionView) {
        
        CGFloat width = (kMainFrameWidth - 4*8)/3;
        CGFloat height = width/4*3;
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.minimumLineSpacing = 1.f;
        layout.itemSize = CGSizeMake(width, height);
        layout.minimumInteritemSpacing = 1.f;
        layout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);

        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(_lblTime.frame) + 8, kMainFrameWidth - 16, height) collectionViewLayout:layout];
        _collectionView.backgroundColor = kWhiteColor;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        
        [_collectionView registerClass:[MCAppreciatePicCollectionCell class] forCellWithReuseIdentifier:@"picCell"];
    }
    
    [self.contentView addSubview:_collectionView];
}

- (void)updateWithInfo:(MCAppreciateModel *)info{
    if (!info) {
        return;
    }
    
    _info = info;
    
    if (info.title) {
        _lblTitle.text =info.title;
        [_lblTitle sizeToFit];
    }
    
    if (info.time) {
        _lblTime.text = info.time;
        [_lblTime sizeToFit];
    }
    
    if (info.imgs_list.count > 0) {
        _itemCount = info.imgs_list.count;
        [_collectionView reloadData];
    }
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    CGRect frame = _lblTitle.frame;
    _lblTitle.frame = CGRectMake(8, 16, frame.size.width > kMainFrameWidth - 16 ? kMainFrameWidth - 16 : frame.size.width, frame.size.height);
    
    frame = _lblTime.frame;
    _lblTime.frame = CGRectMake(8, CGRectGetMaxY(_lblTitle.frame)+5, frame.size.width, frame.size.height);
    
    frame = _collectionView.frame;
    _collectionView.frame = CGRectMake(8, CGRectGetMaxY(_lblTime.frame) + 8, frame.size.width, frame.size.height);
    
}

+ (CGFloat)cellHeight{
    CGFloat _collectionHeight = ((kMainFrameWidth - 4*8)/3)/4*3;
    return 66 + _collectionHeight + 2*8;
}


#pragma mark UICollectionDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    if (_itemCount > 3) {
        return 3;
    }
    return _itemCount;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MCAppreciatePicCollectionCell *picCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"picCell" forIndexPath:indexPath];
    MCAppreciatePicModel *pic = [_info.imgs_list objectAtIndex:indexPath.row];
    [picCell updateWithURL:pic.src];
    return picCell;
}

#pragma mark UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedItem:)]) {
        [self.delegate clickedItem:_info];
    }
}

@end
