//
//  MCAppreciateTextCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/31.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCAppreciateTextCell.h"
#import <UIImageView+WebCache.h>


@implementation MCAppreciateTextCell{
    UILabel *_lblTitle;
    UILabel *_lblSubTitle;
    UIImageView *_imgView;
    UILabel *_lblTime;
}

- (void)setUpSubViews{
    if (!_imgView) {
        _imgView = [[UIImageView alloc] initWithFrame:CGRectMake(8, 8, 80/3*4, 80)];
    }
    [self.contentView addSubview:_imgView];
    
    
    if (!_lblTitle) {
        _lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_imgView.frame) + 8 , 8, kMainFrameWidth - 16 - 118, 30)];
        _lblTitle.font = kAppFontBold(20.f);
    }
    [self.contentView addSubview:_lblTitle];
    
    if (!_lblSubTitle) {
        _lblSubTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_imgView.frame) + 8, CGRectGetMaxY(_lblTitle.frame)+8, kMainFrameWidth - 16 - 118, 20)];
        _lblSubTitle.textColor = [UIColor lightGrayColor];
    }
    [self.contentView addSubview:_lblSubTitle];
    
    if (!_lblTime) {
        _lblTime = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_imgView.frame) + 8, CGRectGetMaxY(_lblSubTitle.frame)+8, kMainFrameWidth - 16 - 118, 20)];
        _lblTime.textAlignment = NSTextAlignmentRight;
        _lblTime.textColor = [UIColor lightGrayColor];
    }
    [self.contentView addSubview:_lblTime];
    
}

- (void)updateWithInfo:(MCAppreciateModel *)info{
    
    if (!info) {
        return;
    }
    
    if (info.pic_path) {
        [_imgView sd_setImageWithURL:[NSURL URLWithString:kImagePath(info.pic_path)]];
    }
    
    
    if (info.title) {
        _lblTitle.text = info.title;
//        [_lblTitle sizeToFit];
    }
    
    if (info.content) {
        _lblSubTitle.text = info.content;
//        [_lblSubTitle sizeToFit];
    }
    
    if (info.time) {
        _lblTime.text = info.time;
    }
}

//- (void)layoutSubviews {
//    [super layoutSubviews];
//}

+ (CGFloat)cellHeight{
    return 80+2*8;
}



@end
