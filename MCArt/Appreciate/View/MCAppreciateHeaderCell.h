//
//  MCAppreciateHeaderCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/1/11.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseTableViewCell.h"

@protocol MCAppreciateHeaderDelegate <NSObject>

- (void)clickedItemAtIndex:(NSInteger)index;

@end


@interface MCAppreciateHeaderCell : MCBaseTableViewCell

@property(nonatomic, weak) id<MCAppreciateHeaderDelegate> delegate;

+ (CGFloat)cellHeight;

@end
