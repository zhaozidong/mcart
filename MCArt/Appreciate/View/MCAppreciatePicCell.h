//
//  MCAppreciatePicCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/1/31.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseTableViewCell.h"
#import "MCAppreciateModel.h"


@protocol MCAppreciatePicDelegate <NSObject>

- (void)clickedItem:(MCAppreciateModel *)info;

@end





@interface MCAppreciatePicCell : MCBaseTableViewCell


@property (nonatomic, strong) id<MCAppreciatePicDelegate> delegate;

- (void)updateWithInfo:(MCAppreciateModel *)info;


+ (CGFloat)cellHeight;



@end
