//
//  MCAppreciateLiteratureCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/20.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCAppreciateLiteratureCell.h"
#import <UIImageView+WebCache.h>



@implementation MCAppreciateLiteratureCell

- (void)awakeFromNib {
    // Initialization code
    self.backgroundColor = kWhiteColor;
}

- (void)updateWithInfo:(MCAppreciateModel *)info{
    if (!info) {
        return;
    }
    
    if (info.pic_path) {
        [self.imgView sd_setImageWithURL:[NSURL URLWithString:kImagePath(info.pic_path)] placeholderImage:[UIImage imageNamed:@"info_loading_title"]];
    }
    
    if (info.title) {
        self.lblTitle.text = info.title;
    }
    
    if (info.time) {
        self.lblTime.text = info.time;
    }
}




@end
