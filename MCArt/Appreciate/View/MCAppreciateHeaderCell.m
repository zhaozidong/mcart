//
//  MCAppreciateHeaderCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/11.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCAppreciateHeaderCell.h"
#import "MCAppreciateHeaderContainerView.h"

@implementation MCAppreciateHeaderCell{
    MCAppreciateHeaderContainerView *_container;
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (void)setUpSubViews{
    _container=[[MCAppreciateHeaderContainerView alloc] initWithFrame:self.bounds];
    
    __weak typeof(MCAppreciateHeaderCell *) weakSelf=self;
    _container.clickBlock=^(NSInteger index){
        if (weakSelf.delegate && [weakSelf.delegate respondsToSelector:@selector(clickedItemAtIndex:)]) {
            [weakSelf.delegate clickedItemAtIndex:index];
        }
    };
    [self.contentView addSubview:_container];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    _container.frame=self.bounds;
}

+ (CGFloat)cellHeight{
    return kMainFrameWidth/5+20;
}


@end
