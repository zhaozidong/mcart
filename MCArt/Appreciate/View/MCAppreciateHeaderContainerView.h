//
//  MCAppreciateHeaderContainerView.h
//  MCArt
//
//  Created by Derek.zhao on 16/1/11.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^clickImageView)(NSInteger index);

@interface MCAppreciateHeaderContainerView : UIView

@property (nonatomic, strong) clickImageView clickBlock;

@end
