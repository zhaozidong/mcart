//
//  MCAppreciateVideoCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/12.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCAppreciateVideoCell.h"
#import <UIImageView+WebCache.h>


@implementation MCAppreciateVideoCell{
    UIImageView *_imgVideo;
}

- (void)awakeFromNib {
    // Initialization code
    
    if (!_imgVideo) {
        _imgVideo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"video"]];
    }
    [self.videoImageView addSubview:_imgVideo];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateWithInfo:(MCAppreciateModel *)info{
    if (!info) {
        return;
    }
    
    if (info.title) {
        self.titleLabel.text = info.title;
    }
    
    if (info.time) {
        self.timeLabel.text = info.time;
    }
    
    if (info.pic_path) {
        [self.videoImageView sd_setImageWithURL:[NSURL URLWithString:kImagePath(info.pic_path)]];
    }
}

- (void)layoutSubviews{
    [super layoutSubviews];
    CGRect frame = self.videoImageView.frame;
    
    _imgVideo.frame = CGRectMake((CGRectGetWidth(self.bounds)-70)/2, CGRectGetHeight(self.bounds)*0.3, 70, 70);
}

+ (CGFloat)cellHeight {
    return kMainFrameWidth/4*3+65;
}

@end
