//
//  MCAppreciateLiteratureViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/13.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCAppreciateLiteratureViewController.h"
#import <MJRefresh.h>
#import "MCAppreciateViewModel.h"
#import "MCAppreciateModel.h"
#import "MCAppreciateLiteratureCell.h"
#import "MCBaseWebViewController.h"
#import "MCShareWebViewController.h"
#import "MCShareModel.h"



NSString *const literatureIdentifier = @"literatureCell";


@interface MCAppreciateLiteratureViewController ()<UICollectionViewDataSource,UICollectionViewDelegate>{
    __block NSInteger _pageIndex;
    __block NSMutableArray *_arrData;
    
}

@property (nonatomic, strong) UICollectionView *collectionView;


@end

@implementation MCAppreciateLiteratureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navStyle=MCNavStyleBlack;
    self.title=@"文学";
    
    _pageIndex = 1;
    _arrData = [NSMutableArray array];
    
    [self.view addSubview:self.collectionView];
    
    [self getData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
        flow.itemSize = CGSizeMake((kMainFrameWidth - 3*8 -1)/2, 190);
        flow.minimumLineSpacing = 8.f;
        flow.minimumInteritemSpacing = 8.f;
        flow.sectionInset=UIEdgeInsetsMake(8, 8, 8, 8);
        
        _collectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:flow];
        _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        
        [_collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([MCAppreciateLiteratureCell class]) bundle:nil] forCellWithReuseIdentifier:literatureIdentifier];
        
        _collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(beginRefreshHeader:)];
        _collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(beginRefreshFooter:)];
        
    }
    return _collectionView;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _arrData.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MCAppreciateModel *appreciate = [_arrData objectAtIndex:indexPath.row];
    MCAppreciateLiteratureCell *literatureCell = [collectionView dequeueReusableCellWithReuseIdentifier:literatureIdentifier forIndexPath:indexPath];
    [literatureCell updateWithInfo:appreciate];
    return literatureCell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView deselectItemAtIndexPath:indexPath animated:NO];
    MCAppreciateModel *appreciate = [_arrData objectAtIndex:indexPath.row];
    MCShareModel *share = [[MCShareModel alloc] init];
    share.title = appreciate.title;
    share.image = [appreciate.imgs_list firstObject];
    share.url = appreciate.share_url;
    share.content = appreciate.title;
    
    MCShareWebViewController *web = [[MCShareWebViewController alloc] initWithUrl:appreciate.share_url shareInfo:share];
    [self.navigationController pushViewController:web animated:YES];
}


- (void)getData{
    __weak typeof(self) weakSelf = self;
    [[MCAppreciateViewModel sharedInstance] getAppreciateWithType:MCAppreciateTypeLiterature page:_pageIndex success:^(id resquestData) {
        if (_pageIndex == 1) {
            _arrData = [(NSArray *)resquestData mutableCopy];
        }else{
            [_arrData addObjectsFromArray:resquestData];
        }
        _pageIndex ++;
        [weakSelf.collectionView reloadData];
    } failure:^(NSError *error) {
        [MCMessageUtilities  showErrorMessage:error.description];
    }];
}


#pragma mark MJRefresh

- (void)beginRefreshHeader:(MJRefreshHeader *)header{
    [header endRefreshing];
    _pageIndex = 1;
    [self getData];
}

- (void)beginRefreshFooter:(MJRefreshFooter *)footer{
    [footer endRefreshing];
    [self getData];
}

@end
