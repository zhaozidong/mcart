//
//  MCAppreciateDanceViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/13.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCAppreciateDanceViewController.h"
#import "MCAppreciateViewModel.h"
#import "MCAppreciatePicCell.h"
#import "MCAppreciateVideoCell.h"
#import "MCAppreciateTextCell.h"
#import "MCBaseWebViewController.h"
#import "MCShareWebViewController.h"
#import "MCShareModel.h"


static NSString *const videoIdentifier=@"videoCell";
static NSString *const picIdentifier=@"picCell";
static NSString *const textIdentifier=@"textCell";



@interface MCAppreciateDanceViewController ()<MCAppreciatePicDelegate>{
    __block NSInteger _pageIndex;
    __block NSMutableArray *_arrData;
}

@end

@implementation MCAppreciateDanceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTitle:@"音舞" withStyle:MCNavStyleBlack];
    
    _arrData = [NSMutableArray array];
    
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameHeight, CGFLOAT_MIN)];
    
    [self.tableView registerNib:[UINib nibWithNibName:@"MCAppreciateVideoCell" bundle:nil] forCellReuseIdentifier:videoIdentifier];
    [self.tableView registerClass:[MCAppreciatePicCell class] forCellReuseIdentifier:picIdentifier];
    [self.tableView registerClass:[MCAppreciateTextCell class] forCellReuseIdentifier:textIdentifier];
    
    [self getData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (void)getData{
    _pageIndex = 1;
    __weak typeof(self) weakSelf = self;
    [[MCAppreciateViewModel sharedInstance] getAppreciateWithType:MCAppreciateTypeDance page:_pageIndex success:^(id resquestData) {
        _pageIndex++ ;
        _arrData = [(NSArray *)resquestData mutableCopy];
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}


#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return _arrData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MCAppreciateModel *appreciate = [_arrData objectAtIndex:indexPath.section];
    switch (appreciate.class_type) {
        case MCArtInfoShowTypeText: {
            MCAppreciateTextCell *textCell = [tableView dequeueReusableCellWithIdentifier:textIdentifier];
            [textCell updateWithInfo:appreciate];
            return textCell;
            break;
        }
        case MCArtInfoShowTypePic: {
            MCAppreciatePicCell *picCell = [tableView dequeueReusableCellWithIdentifier:picIdentifier];
            picCell.delegate = self;
            [picCell updateWithInfo:appreciate];
            return picCell;
            break;
        }
        case MCArtInfoShowTypeVideo: {
            MCAppreciateVideoCell *video=[tableView dequeueReusableCellWithIdentifier:videoIdentifier];
            [video updateWithInfo:appreciate];
            return video;
            break;
        }
    }
    return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    MCAppreciateModel *appreciate = [_arrData objectAtIndex:indexPath.section];
    switch (appreciate.class_type) {
        case MCArtInfoShowTypeText: {
            return [MCAppreciateTextCell cellHeight];
            break;
        }
        case MCArtInfoShowTypePic: {
            return [MCAppreciatePicCell cellHeight];
            break;
        }
        case MCArtInfoShowTypeVideo: {
            return [MCAppreciateVideoCell cellHeight];
            break;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10.f;
}


#pragma mark UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    MCAppreciateModel *appreciate = [_arrData objectAtIndex:indexPath.section];
    switch (appreciate.class_type) {
        case MCArtInfoShowTypeText:
        case MCArtInfoShowTypeVideo: {
            
            MCShareModel *share = [[MCShareModel alloc] init];
            share.title = appreciate.title;
            share.image = appreciate.pic_path;
            share.url = appreciate.share_url;
            share.content = appreciate.title;
            
            MCShareWebViewController *web = [[MCShareWebViewController alloc] initWithUrl:appreciate.share_url shareInfo:share];
            [self.navigationController pushViewController:web animated:YES];
            break;
        }
        case MCArtInfoShowTypePic: {
            MCShareModel *share = [[MCShareModel alloc] init];
            share.title = appreciate.title;
            share.image = [appreciate.imgs_list firstObject];
            share.url = appreciate.share_url;
            share.content = appreciate.title;
            
            MCShareWebViewController *web = [[MCShareWebViewController alloc] initWithUrl:appreciate.share_url shareInfo:share];
            [self.navigationController pushViewController:web animated:YES];
            
            break;
        }
    }
}


#pragma mark MCAppreciatePicDelegate
- (void)clickedItem:(MCAppreciateModel *)info{
    if (info.share_url) {
        MCBaseWebViewController *webView = [[MCBaseWebViewController alloc] init];
        webView.url = info.share_url;
        [self.navigationController pushViewController:webView animated:YES];
    }
}


- (void)beginRefreshHeader:(MJRefreshHeader *)header{
    [header endRefreshing];
    [self getData];
}

- (void)beginRefreshFooter:(MJRefreshFooter *)footer{
    [footer endRefreshing];
    __weak typeof(self) weakSelf = self;
    [[MCAppreciateViewModel sharedInstance] getAppreciateWithType:MCAppreciateTypeDance page:_pageIndex success:^(id resquestData) {
        _pageIndex++;
        [_arrData addObjectsFromArray:(NSArray *)resquestData];
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}


@end
