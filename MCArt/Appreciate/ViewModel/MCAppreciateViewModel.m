//
//  MCAppreciateViewModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/31.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCAppreciateViewModel.h"
#import "MCNetworkManager.h"
#import "MCAppreciateModel.h"

@implementation MCAppreciateViewModel

SINGLETON_FOR_CLASS(MCAppreciateViewModel)

- (void)getAppreciateWithType:(MCAppreciateType)type page:(NSInteger)pageIndex success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    NSDictionary *dict;
    NSString *index = [NSString stringWithFormat:@"%ld",pageIndex];
    switch (type) {
        case MCAppreciateTypeAll: {
            dict =@{@"type": @"0", @"page" : index ,@"pageSize" : MC_PAGE_SIZE};
            break;
        }
        case MCAppreciateTypeDance: {
            dict =@{@"type": @"1", @"page" : index ,@"pageSize" : MC_PAGE_SIZE};
            break;
        }
        case MCAppreciateTypeArt: {
            dict =@{@"type": @"2", @"page" : index ,@"pageSize" : MC_PAGE_SIZE};
            break;
        }
        case MCAppreciateTypeLiterature: {
            dict =@{@"type": @"3", @"page" : index ,@"pageSize" : MC_PAGE_SIZE};
            break;
        }
        case MCAppreciateTypeOpera: {
            dict =@{@"type": @"4", @"page" : index ,@"pageSize" : MC_PAGE_SIZE};
            break;
        }
        case MCAppreciateTypeFamous: {
            dict =@{@"type": @"5", @"page" : index ,@"pageSize" : MC_PAGE_SIZE};
            break;
        }
    }
    
    [[MCNetworkManager sharedInstance] postWithURL:@"appNews/getYsxs" parameter:dict success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            NSError *err=nil;
            NSArray *array=[MTLJSONAdapter modelsOfClass:[MCAppreciateModel class] fromJSONArray:[resquestData objectForKey:@"list"] error:&err];
            if (!err) {
                successBlock(array);
            }else{
                [MCMessageUtilities showErrorMessage:err.description];
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}

@end
