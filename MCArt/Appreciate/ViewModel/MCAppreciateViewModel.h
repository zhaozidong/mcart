//
//  MCAppreciateViewModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/1/31.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, MCAppreciateType) {
    MCAppreciateTypeAll,
    MCAppreciateTypeDance,
    MCAppreciateTypeArt,
    MCAppreciateTypeLiterature,
    MCAppreciateTypeOpera,
    MCAppreciateTypeFamous
};





@interface MCAppreciateViewModel : NSObject


+ (instancetype)sharedInstance;


- (void)getAppreciateWithType:(MCAppreciateType)type page:(NSInteger)pageIndex success:(successBlock)successBlock failure:(failureBlock)failureBlock;






@end
