//
//  MCMallOrderViewModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/11.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMallOrderViewModel.h"
#import "MCNetworkManager.h"
#import "MCLoginViewModel.h"
#import "MCMallOrderModel.h"

@implementation MCMallOrderViewModel

SINGLETON_FOR_CLASS(MCMallOrderViewModel);

- (void)buyGoods:(NSString *)orderInfo selfFetch:(BOOL)slefFetch express:(MCPersonalExpressDetailModel *)expressInfo success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    NSDictionary *dict;
    if (slefFetch) {//自取
        dict = @{@"orderInfo":orderInfo,
            @"userid":[MCLoginViewModel sharedInstance].userInfo.id,
            @"shipMethod":@(0),
            @"name":expressInfo.name,
            @"cell":expressInfo.cell};
    }else {//物流
        dict = @{@"orderInfo":orderInfo,
                 @"userid":[MCLoginViewModel sharedInstance].userInfo.id,
                 @"shipMethod":@(1),
                 @"name":expressInfo.name,
                 @"address":expressInfo.address,
                 @"cell":expressInfo.cell};
    }
    
    [[MCNetworkManager sharedInstance] postWithURL:@"appGoods/buyGoods" parameter:dict success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            NSError *err;
            MCMallOrderModel *order = [MTLJSONAdapter modelOfClass:[MCMallOrderModel class] fromJSONDictionary:resquestData error:&err];
            if (!err) {
                if (successBlock) {
                    successBlock(order);
                }
            }else{
                if (failureBlock) {
                    failureBlock(err);
                }
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}

- (void)getOrderResult:(NSString *)orderId success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    [[MCNetworkManager sharedInstance] postWithURL:@"appGoods/getOrderPayResult" parameter:@{@"id":orderId} success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            NSError *err;
            MCMallOrderModel *order = [MTLJSONAdapter modelOfClass:[MCMallOrderModel class] fromJSONDictionary:resquestData error:&err];
            if (!err) {
                if (successBlock) {
                    successBlock(order);
                }
            }else{
                if (failureBlock) {
                    failureBlock(err);
                }
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}


@end
