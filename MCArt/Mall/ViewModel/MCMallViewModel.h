//
//  MCMallViewModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/21.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger,MCMallGoodsType) {
    MCMallGoodsTypeNewest,
    MCMallGoodsTypeHotest,
    MCMallGoodsTypeImmediately
};





@interface MCMallViewModel : NSObject

+ (instancetype)sharedInstance;

@property (nonatomic, assign, readonly) long long currentTime;

///商城获取分类接口
- (void)getCategoryList:(successBlock)successBlock failure:(failureBlock)failureBlock;


///商城首页列表接口
- (void)getAllGoodsByType:(MCMallGoodsType)type page:(NSInteger)pageIndex success:(successBlock)successBlock failure:(failureBlock)failureBlock;



@end
