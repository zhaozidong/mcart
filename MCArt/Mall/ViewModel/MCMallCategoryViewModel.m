//
//  MCMallCategoryViewModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/24.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMallCategoryViewModel.h"
#import "MCMallCategoryModel.h"
#import "MCNetworkManager.h"
#import "NSString+Utilities.h"


@implementation MCMallCategoryViewModel

SINGLETON_FOR_CLASS(MCMallCategoryViewModel);



- (void)getCategoryDetail:(NSString *)class_id keyWord:(NSString *)keyWord order:(NSString *)order pageIndex:(NSInteger)page success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    
    //    NSDictionary *dict = @{@"class_id":class_id,@"keyword":keyWord,@"order":order,@"page":@(page),@"pageSize":MC_PAGE_SIZE};
    
    NSMutableDictionary *dict = [@{@"page":@(page),@"pageSize":MC_PAGE_SIZE} mutableCopy];
    if (class_id && ![NSString isNullOrEmpty:class_id]) {
        [dict setObject:class_id forKey:@"class_id"];
    }
    
    if (keyWord && ![NSString isNullOrEmpty:keyWord]) {
        [dict setObject:keyWord forKey:@"keyword"];
    }
    
    if (order && ![NSString isNullOrEmpty:order]) {
        [dict setObject:order forKey:@"order"];
    }

    [[MCNetworkManager sharedInstance] postWithURL:@"appGoods/getGoodsByCateList" parameter:dict success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            NSError *err;
            NSArray *array= [MTLJSONAdapter modelsOfClass:[MCMallCategoryDetailModel class] fromJSONArray:[resquestData objectForKey:@"list"] error:&err];
            if (!err) {
                if (successBlock) {
                    successBlock(array);
                }
            }else{
                if (failureBlock) {
                    failureBlock(err);
                }
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}


@end
