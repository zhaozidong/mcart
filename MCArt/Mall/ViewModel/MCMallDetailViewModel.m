//
//  MCMallDetailViewModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/27.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMallDetailViewModel.h"
#import "MCNetworkManager.h"
#import "MCMallDetailModel.h"
#import "MCLoginViewModel.h"

@implementation MCMallDetailViewModel


SINGLETON_FOR_CLASS(MCMallDetailViewModel);


- (void)getGoodsDetailById:(NSString *)index success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    NSDictionary *dict = @{@"id":index,@"page":@1,@"pageSize":MC_PAGE_SIZE};
    [[MCNetworkManager sharedInstance] postWithURL:@"appGoods/getGoodsDetailById" parameter:dict success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            NSError *err;
            MCMallDetailModel *detail = [MTLJSONAdapter modelOfClass:[MCMallDetailModel class] fromJSONDictionary:resquestData error:&err];
            if (!err) {
                if (successBlock) {
                    successBlock(detail);
                }
            }else{
                if (failureBlock) {
                    failureBlock(err);
                }
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}


- (void)addToShoppingCard:(NSString *)index success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    NSDictionary *dict = @{@"id":index,@"userid":[MCLoginViewModel sharedInstance].userInfo.id};
    [[MCNetworkManager sharedInstance] postWithURL:@"appGoods/addToShoppingChart" parameter:dict success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            if (successBlock) {
                successBlock([resquestData objectForKey:@"info"]);
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}

@end
