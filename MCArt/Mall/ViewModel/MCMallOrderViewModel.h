//
//  MCMallOrderViewModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/11.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MCPersonalModel.h"

@interface MCMallOrderViewModel : NSObject

+ (instancetype)sharedInstance;


- (void)buyGoods:(NSString *)orderInfo selfFetch:(BOOL)slefFetch express:(MCPersonalExpressDetailModel *)expressInfo success:(successBlock)successBlock failure:(failureBlock)failureBlock;


///查询订单支付结果
- (void)getOrderResult:(NSString *)orderId success:(successBlock)successBlock failure:(failureBlock)failureBlock;



@end
