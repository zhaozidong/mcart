//
//  MCMallCardViewModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/28.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MCMallCardViewModel : NSObject

+ (instancetype)sharedInstance;


///获取购物车列表
- (void)getShoppingCardList:(successBlock)successBlock failure:(failureBlock)failureBlock;


///从购物车删除商品
- (void)deleteGoodsFromCard:(NSString *)index success:(successBlock)successBlock failure:(failureBlock)failureBlock;




@end
