//
//  MCMallViewModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/21.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMallViewModel.h"
#import "MCNetworkManager.h"
#import "MCMallModel.h"
#import "MCMallCategoryModel.h"

@implementation MCMallViewModel{
    __block long long  _currentTime;
}

SINGLETON_FOR_CLASS(MCMallViewModel);

- (void)getCategoryList:(successBlock)successBlock failure:(failureBlock)failureBlock{
    [[MCNetworkManager sharedInstance] postWithURL:@"appGoods/getCategoryList" parameter:nil success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            NSError *err;
            NSArray *array = [MTLJSONAdapter modelsOfClass:[MCMallCategoryModel class] fromJSONArray:[resquestData objectForKey:@"list"] error:&err];
            if (!err) {
                if (successBlock) {
                    successBlock(array);
                }
            }else{
                if (failureBlock) {
                    failureBlock(err);
                }
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}

- (long long )currentTime{
    return _currentTime;
}

- (void)getAllGoodsByType:(MCMallGoodsType)type page:(NSInteger)pageIndex success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    NSDictionary *dict;
    switch (type) {
        case MCMallGoodsTypeNewest: {
            dict = @{@"type":@1,@"page":@(pageIndex),@"pageSize":MC_PAGE_SIZE};
            break;
        }
        case MCMallGoodsTypeHotest: {
            dict = @{@"type":@2,@"page":@(pageIndex),@"pageSize":MC_PAGE_SIZE};
            break;
        }
        case MCMallGoodsTypeImmediately: {
            dict = @{@"type":@3,@"page":@(pageIndex),@"pageSize":MC_PAGE_SIZE};
            break;
        }
    }
    
    [[MCNetworkManager sharedInstance] postWithURL:@"appGoods/getGoodsAllList" parameter:dict success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            _currentTime = [[resquestData objectForKey:@"timestamp"] longLongValue];
            NSError *err;
            NSArray *array = [MTLJSONAdapter modelsOfClass:[MCMallModel class] fromJSONArray:[resquestData objectForKey:@"list"] error:&err];
            if (!err) {
                if (successBlock) {
                    successBlock(array);
                }
            }else{
                if (failureBlock) {
                    failureBlock(err);
                }
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}



@end
