//
//  MCMallCardViewModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/28.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMallCardViewModel.h"
#import "MCNetworkManager.h"
#import "MCMallCardModel.h"
#import "MCLoginViewModel.h"
#import "MCLoginViewModel.h"

@implementation MCMallCardViewModel

SINGLETON_FOR_CLASS(MCMallCardViewModel);

- (void)getShoppingCardList:(successBlock)successBlock failure:(failureBlock)failureBlock{
    [[MCNetworkManager sharedInstance] postWithURL:@"appGoods/myShoppingChart" parameter:@{@"userid":[MCLoginViewModel sharedInstance].userInfo.id} success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            NSError *err;
            NSArray *array= [MTLJSONAdapter modelsOfClass:[MCMallCardModel class] fromJSONArray:[resquestData objectForKey:@"goodschart"] error:&err];
            if (!err) {
                if (successBlock) {
                    successBlock(array);
                }
            }else{
                if (failureBlock) {
                    failureBlock(err);
                }
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}

- (void)deleteGoodsFromCard:(NSString *)index success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    NSDictionary *dict = @{@"id":index, @"userid":[MCLoginViewModel sharedInstance].userInfo.id};
    [[MCNetworkManager sharedInstance] postWithURL:@"appGoods/deleteFromShoppingChart" parameter:dict success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            if (successBlock) {
                successBlock([resquestData objectForKey:@"info"]);
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}


@end
