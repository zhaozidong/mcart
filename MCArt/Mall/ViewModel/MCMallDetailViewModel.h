//
//  MCMallDetailViewModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/27.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MCMallDetailViewModel : NSObject

+ (instancetype)sharedInstance;

///获取商品详情
- (void)getGoodsDetailById:(NSString *)index success:(successBlock)successBlock failure:(failureBlock)failureBlock;

///添加商品到购物车
- (void)addToShoppingCard:(NSString *)index success:(successBlock)successBlock failure:(failureBlock)failureBlock;



@end
