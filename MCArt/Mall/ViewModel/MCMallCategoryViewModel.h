//
//  MCMallCategoryViewModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/24.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MCMallCategoryViewModel : NSObject

+ (instancetype)sharedInstance;



- (void)getCategoryDetail:(NSString *)class_id keyWord:(NSString *)keyWord order:(NSString *)order pageIndex:(NSInteger)page success:(successBlock)successBlock failure:(failureBlock)failureBlock;


@end
