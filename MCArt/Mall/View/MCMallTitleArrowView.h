//
//  MCMallTitleArrowView.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/21.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol MCMallTitleArrowViewDelegate <NSObject>

- (void)clickTitle;

@end



@interface MCMallTitleArrowView : UIView

@property (nonatomic, retain) NSString *title;

@property (nonatomic, weak) id<MCMallTitleArrowViewDelegate> delegate;

- (instancetype)initWithTitle:(NSString *)title;

@end
