//
//  MCMallOrderAddressView.m
//  MCArt
//
//  Created by Derek.zhao on 16/4/15.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMallOrderAddressView.h"
#import "TNCheckBoxGroup.h"


@implementation MCMallOrderAddressView{
    UILabel *_lblTitle;
    UIView *_container;
    UITextField *_txtReceiver;
    UITextField *_txtAddress;
    UITextField *_txtPhone;
    BOOL _selfFetch;
    MCPersonalExpressDetailModel *_expressInfo;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}

- (void)setUpSubViews{
    self.backgroundColor = [UIColor clearColor];
    _selfFetch = NO;
    
    
    _lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, kMainFrameHeight*0.6, kMainFrameWidth, 40)];
    _lblTitle.textAlignment = NSTextAlignmentCenter;
    _lblTitle.backgroundColor = UIColorFromRGB(0x999999);
    _lblTitle.textColor = kWhiteColor;
    _lblTitle.text = @"收货信息确认";
    
    [self addSubview:_lblTitle];
    
    
    _container = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_lblTitle.frame), kMainFrameWidth, kMainFrameHeight-CGRectGetHeight(_lblTitle.frame))];
    _container.backgroundColor = kWhiteColor;
    [self addSubview:_container];
    
    UILabel *_lblReceiver = [[UILabel alloc] initWithFrame:CGRectMake(8, 8, 90, 30)];
    _lblReceiver.text = @"收货人：";
    [_container addSubview:_lblReceiver];
    
    _txtReceiver = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_lblReceiver.frame)+8, 8, 100, 30)];
//    _txtReceiver.borderStyle = UITextBorderStyleLine;
    _txtReceiver.layer.borderWidth = 1.f;
    _txtReceiver.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _txtReceiver.layer.cornerRadius = 2.f;
    [_container addSubview:_txtReceiver];
    
    UILabel *_lblAddress = [[UILabel alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(_lblReceiver.frame)+8, CGRectGetWidth(_lblReceiver.frame), 30)];
    _lblAddress.text = @"收货地址：";
    [_container addSubview:_lblAddress];
    
    _txtAddress = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMinX(_txtReceiver.frame), CGRectGetMinY(_lblAddress.frame), kMainFrameWidth-CGRectGetWidth(_lblAddress.frame)-3*8, 30)];
    _txtAddress.layer.borderWidth = 1.f;
    _txtAddress.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _txtAddress.layer.cornerRadius = 2.f;
    [_container addSubview:_txtAddress];
    
    UILabel *_lblPhone = [[UILabel alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(_lblAddress.frame)+8, CGRectGetWidth(_lblReceiver.frame), 30)];
    _lblPhone.text = @"手机号：";
    [_container addSubview:_lblPhone];
    
    _txtPhone = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMinX(_txtReceiver.frame), CGRectGetMinY(_lblPhone.frame), kMainFrameWidth - CGRectGetWidth(_lblPhone.frame)-3*8, 30)];
    _txtPhone.layer.borderWidth = 1.f;
    _txtPhone.layer.borderColor = [UIColor lightGrayColor].CGColor;
    _txtPhone.layer.cornerRadius = 2.f;
    _txtPhone.keyboardType = UIKeyboardTypePhonePad;
    [_container addSubview:_txtPhone];
    
    
    TNImageCheckBoxData *manData = [[TNImageCheckBoxData alloc] init];
    manData.identifier = @"selfPick";
    manData.labelText = @"我要自取";
    manData.checkedImage = [UIImage imageNamed:@"checked"];
    manData.uncheckedImage = [UIImage imageNamed:@"unchecked"];

    TNCheckBoxGroup * group = [[TNCheckBoxGroup alloc] initWithCheckBoxData:@[manData] style:TNCheckBoxLayoutVertical];
    [group create];
    group.position = CGPointMake(25, CGRectGetMaxY(_lblPhone.frame)+30);
    [_container addSubview:group];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(groupChanged:) name:GROUP_CHANGED object:group];
    
    
    UIButton *_btnConfirm = [[UIButton alloc] initWithFrame:CGRectMake(kMainFrameWidth/2, CGRectGetMaxY(_txtPhone.frame)+25, 70, 30)];
    [_btnConfirm setBackgroundColor:[UIColor redColor]];
    _btnConfirm.layer.cornerRadius = 3.f;
    [_btnConfirm setTitle:@"确认" forState:UIControlStateNormal];
    [_btnConfirm addTarget:self action:@selector(btnDidClickConfirm) forControlEvents:UIControlEventTouchUpInside];
    [_container addSubview:_btnConfirm];
    
    UIButton *_btnCancel = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_btnConfirm.frame)+8, CGRectGetMinY(_btnConfirm.frame), 70, 30)];
    [_btnCancel setBackgroundColor:[UIColor lightGrayColor]];
    _btnCancel.layer.cornerRadius = 3.f;
    [_btnCancel setTitle:@"取消" forState:UIControlStateNormal];
    [_btnCancel addTarget:self action:@selector(btnDidClickCancel) forControlEvents:UIControlEventTouchUpInside];
    [_container addSubview:_btnCancel];
}


- (void)groupChanged:(NSNotification *)notification {
    _selfFetch = !_selfFetch;
}

- (void)updateWithExpressDetail:(MCPersonalExpressDetailModel *)info{
    
    _expressInfo = info;
    
    if (info.name) {
        _txtReceiver.text = info.name;
//        _txtReceiver.enabled = NO;
    }
    
    if (info.address) {
        _txtAddress.text = info.address;
//        _txtAddress.enabled = NO;
    }
    
    if (info.cell) {
        _txtPhone.text = info.cell;
//        _txtPhone.enabled = NO;
    }
}

- (void)btnDidClickConfirm{
    if (!_txtReceiver.text || [_txtReceiver.text isEqualToString:@""]) {
        [MCMessageUtilities showTotast:@"姓名不能为空"];
        return;
    }
    
    if (!_txtPhone.text || [_txtPhone.text isEqualToString:@""]) {
        [MCMessageUtilities showTotast:@"电话不能为空"];
        return;
    }
    
    if (_txtPhone.text.length != 11) {
        [MCMessageUtilities showTotast:@"请输入正确的电话号码"];
        return;
    }
    
    if (!_selfFetch) {
        if (!_txtAddress.text || [_txtAddress.text isEqualToString:@""]) {
            [MCMessageUtilities showTotast:@"地址不能为空"];
            return;
        }
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedConfirm:expressInfo:)]) {
        _expressInfo.name = _txtReceiver.text;
        _expressInfo.address = _txtAddress.text;
        _expressInfo.cell = _txtPhone.text;
        [self.delegate clickedConfirm:_selfFetch expressInfo:_expressInfo];
    }
    [self removeFromSuperview];
}

- (void)btnDidClickCancel{
    [self removeFromSuperview];
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range {
    if (textField.text.length > 11) {
        return NO;
    }
    return YES;
}

@end
