//
//  MCMallDetailActionView.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/27.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MCMallDetailActionDelegate <NSObject>

- (void)clickedChat;

- (void)clickedCard;

@end






@interface MCMallDetailActionView : UIView


@property(nonatomic, weak) id<MCMallDetailActionDelegate> delegate;


- (instancetype)initWithFrame:(CGRect)frame seckiling:(BOOL)isSeckiling;


@end
