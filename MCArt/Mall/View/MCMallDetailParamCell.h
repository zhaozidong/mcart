//
//  MCMallDetailParamCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/28.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseTableViewCell.h"
#import "MCMallDetailModel.h"



@interface MCMallDetailParamCell : MCBaseTableViewCell


- (void)updateWithParam:(MCMallDetailParamModel *)param;


+ (CGFloat)cellHeightWithParam:(MCMallDetailParamModel *)param;

@end
