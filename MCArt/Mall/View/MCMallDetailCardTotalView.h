//
//  MCMallDetailCardTotalView.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/29.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol MCMallDetailTotalDelegate <NSObject>

- (void)clickedAll:(BOOL)isSelect;

- (void)clickedPay;

@end


@interface MCMallDetailCardTotalView : UIView


@property (nonatomic, assign) BOOL selected;

@property (nonatomic, weak) id<MCMallDetailTotalDelegate> delegate;

- (void)updateWithTotal:(NSString *)total;

@end
