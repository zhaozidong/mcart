//
//  MCMallNewestCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/24.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseTableViewCell.h"
#import "MCMallModel.h"


@interface MCMallNewestCell : MCBaseTableViewCell

- (void)updateWithInfo:(MCMallModel *)info;

+ (CGFloat)cellHeight;


@end
