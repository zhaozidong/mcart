//
//  MCMallImmediatelyCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/26.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCMallModel.h"


@interface MCMallImmediatelyCell : UICollectionViewCell


- (void)updateWithInfo:(MCMallModel *)info;




@end
