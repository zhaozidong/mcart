//
//  MCMallDetailCommentCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/27.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMallDetailCommentCell.h"
#import "NSString+Utilities.h"

@implementation MCMallDetailCommentCell{
    UIImageView *_imgViewHead;
    UILabel *_lblName;
    UILabel *_lblTime;
    UILabel *_lblContent;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setUpSubViews{
    if (!_imgViewHead) {
        _imgViewHead = [[UIImageView alloc] initWithFrame:CGRectMake(8, 8, 30, 30)];
        _imgViewHead.layer.cornerRadius = 15.f;
    }
    [self.contentView addSubview:_imgViewHead];
    
    if (!_lblName) {
        _lblName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_imgViewHead.frame)+8, 15, 80, 20)];
        _lblName.font = kAppFont(16.f);
    }
    [self.contentView addSubview:_lblName];
    
    if (!_lblTime) {
        _lblTime = [[UILabel alloc] initWithFrame:CGRectMake(200, 15, 100, 20)];
        _lblTime.textColor = [UIColor lightGrayColor];
        _lblTime.textAlignment = NSTextAlignmentRight;
        _lblTime.font = kAppFont(12.f);
    }
    [self.contentView addSubview:_lblTime];
    
    if (!_lblContent) {
        _lblContent = [[UILabel alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(_imgViewHead.frame)+8, kMainFrameWidth - 2*8, 150)];
        _lblContent.numberOfLines = 0;
        _lblContent.preferredMaxLayoutWidth = kMainFrameWidth-2*8;
        _lblContent.font = kAppFont(14.f);
    }
    [self.contentView addSubview:_lblContent];
    
}

- (void)updateWithComment:(MCMallDetailCommentModel *)comment{
    if (!comment) {
        return;
    }
    
    if (comment.head_img && ![NSString isNullOrEmpty:comment.head_img]) {
        [_imgViewHead sd_setImageWithURL:[NSURL URLWithString:kImagePath(comment.head_img)]];
    }
    
    if (comment.account) {
        _lblName.text = comment.account;
        [_lblName sizeToFit];
    }
    
    if (comment.time) {
        _lblTime.text = comment.time;
        [_lblTime sizeToFit];
    }
    
    if (comment.content) {
        _lblContent.text = comment.content;
        [_lblContent sizeToFit];
    }
    
    
    
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    CGRect frame;
    _imgViewHead.frame = CGRectMake(8, 8, 30, 30);
    
    frame = _lblName.frame;
    _lblName.frame = CGRectMake(CGRectGetMaxX(_imgViewHead.frame)+8, (30-frame.size.height)/2+8, frame.size.width, frame.size.height);
    
    frame = _lblTime.frame;
    _lblTime.frame = CGRectMake(CGRectGetWidth(self.bounds)-8 - frame.size.width, 15, frame.size.width, frame.size.height);
    
    frame = _lblContent.frame;
    _lblContent.frame = CGRectMake(8, CGRectGetMaxY(_imgViewHead.frame)+8, frame.size.width, frame.size.height);
    
}



+ (CGFloat)cellHeight:(MCMallDetailCommentModel *)comment{
    NSString *content = comment.content;
    NSDictionary *attr = @{NSFontAttributeName:kAppFont(14.f)};
    CGRect rect = [content boundingRectWithSize:CGSizeMake(kMainFrameWidth - 2*8, 200)
                                        options:NSStringDrawingUsesLineFragmentOrigin
                                     attributes:attr
                                        context:nil];
    return 30+2*8+rect.size.height+8;
}


@end
