//
//  MCMallCategoryTitleView.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/21.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMallCategoryTitleView.h"
#import "MCArrowButton.h"


@implementation MCMallCategoryTitleView{
    NSArray *_arrTitle;
    NSArray *_arrArrow;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init{
    self = [super init];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}

- (instancetype)initWithTitle:(NSArray *)arrTitle arrow:(NSArray *)arrArrow{
    self = [super init];
    if (self) {
        _arrTitle = arrTitle;
        _arrArrow = arrArrow;
        [self setUpSubViews];
    }
    return self;
}

- (void)setUpSubViews{
    self.backgroundColor = [UIColor groupTableViewBackgroundColor];
    if (_arrTitle) {
        CGFloat width = (kMainFrameWidth - 3 - (_arrTitle.count +1)*1)/(_arrTitle.count);
        for (NSInteger i = 0; i < _arrTitle.count; i++) {
            if ([_arrArrow containsObject:@(i)]) {
                MCArrowButton *arrowButton = [[MCArrowButton alloc] initWithFrame:CGRectMake(width*i+i, 0, width, 40)];
                arrowButton.tag = i;
                [arrowButton.titleLabel setFont:kAppFont(14.f)];
                [arrowButton setTitleColor:kBlackColor forState:UIControlStateNormal];
                [arrowButton setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
                [arrowButton setTitle:[_arrTitle objectAtIndex:i] forState:UIControlStateNormal];
                [arrowButton addTarget:self action:@selector(btnDidClickTitle:) forControlEvents:UIControlEventTouchUpInside];
                [self addSubview:arrowButton];
            }else{
                UIButton *btnTitle = [[UIButton alloc] initWithFrame:CGRectMake(width*i+i, 0, width, 40)];
                btnTitle.tag = i;
                [btnTitle.titleLabel setFont:kAppFont(14.f)];
                [btnTitle setTitleColor:kBlackColor forState:UIControlStateNormal];
                [btnTitle setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
                [btnTitle setTitle:[_arrTitle objectAtIndex:i] forState:UIControlStateNormal];
                [btnTitle addTarget:self action:@selector(btnDidClickTitle:) forControlEvents:UIControlEventTouchUpInside];
                [self addSubview:btnTitle];
                if (i == 0 ) {
                    btnTitle.selected = YES;
                }
            }
        }
    }
}

- (void)btnDidClickTitle:(id)sender{
    MCArrowButtonStatus arrowStatus = 0;
    UIButton *button = (UIButton *)sender;
    for (UIView *view in self.subviews) {
        if ([view isKindOfClass:[UIButton class]]) {
            UIButton *tempButton = (UIButton *)view;
            if (tempButton.tag != button.tag) {
                tempButton.selected = NO;
            }else{
                tempButton.selected = YES;
                if ([tempButton isKindOfClass:[MCArrowButton class]]) {
                    MCArrowButton *arrowButton = (MCArrowButton *)tempButton;
                    arrowStatus = [arrowButton changeToOtherStatus];
                }
            }
        }
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedItemAtIndex:order:)]) {
        [self.delegate clickedItemAtIndex:button.tag order:arrowStatus];
    }
}



@end
