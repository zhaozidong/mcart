//
//  MCMallTitleArrowView.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/21.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMallTitleArrowView.h"

@implementation MCMallTitleArrowView{
    UILabel *_titleLabel;
    UIImageView *_arrowImageView;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init{
    self=[super init];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self=[super initWithFrame:frame];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}


- (instancetype)initWithTitle:(NSString *)title{
    CGSize sz=[[self class] sizeForText:title];
    self=[super initWithFrame:CGRectMake(0, 0, sz.width, sz.height)];
    if (self) {
        [self setUpSubViews];
        [self setTitle:title];
    }
    return self;
}

- (void)setUpSubViews{
    self.userInteractionEnabled= YES;
    
    UITapGestureRecognizer *tap=[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickTitleLabel)];
    tap.numberOfTapsRequired=1;
    [self addGestureRecognizer:tap];
    
    
    if (!_titleLabel) {
        _titleLabel=[[UILabel alloc] init];
        //        _titleLabel.textColor=kRGBA(97, 97, 97, 1.0f);
    }
    [self addSubview:_titleLabel];
    
    if (!_arrowImageView) {
        _arrowImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"titleArrow"]];
    }
    [self addSubview:_arrowImageView];
}

- (void)setTitle:(NSString *)title{
    if (_titleLabel) {
        _titleLabel.textColor = kWhiteColor;
        _titleLabel.font = kMCFont(20);
        _titleLabel.text=title;
        [_titleLabel sizeToFit];
    }
}

- (void)layoutSubviews{
    [super layoutSubviews];
    CGRect frame=_titleLabel.frame;
    _titleLabel.frame=CGRectMake(0, 0, frame.size.width, frame.size.height);
    _arrowImageView.frame=CGRectMake(frame.size.width+3, 8, 15, 10);
}


+ (CGSize)sizeForText:(NSString *)text{
    CGSize sz=[text sizeWithAttributes:@{NSFontAttributeName:kAppFont(16.f)}];
    return CGSizeMake(sz.width+20, sz.height);
}

- (void)clickTitleLabel{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickTitle)]) {
        [self.delegate clickTitle];
    }
}



@end
