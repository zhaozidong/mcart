//
//  MCMallDetailActionView.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/27.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMallDetailActionView.h"

@implementation MCMallDetailActionView{
    UIButton *_btnChat;
    UIButton *_btnAddToCard;
    
    BOOL _isSeckiling;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init{
    self = [super init];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame seckiling:(BOOL)isSeckiling{
    self = [super initWithFrame:frame];
    if (self) {
        _isSeckiling = isSeckiling;
        [self setUpSubViews];
    }
    return self;
}


- (void)setUpSubViews{
    
    if (!_btnChat) {
        _btnChat = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth/2, 50)];
        [_btnChat setBackgroundColor:[UIColor orangeColor]];
        [_btnChat setTitle:@"购买咨询" forState:UIControlStateNormal];
        [_btnChat addTarget:self action:@selector(btnDidClickChat:) forControlEvents:UIControlEventTouchUpInside];
    }
    [self addSubview:_btnChat];
    
    
    if (!_btnAddToCard) {
        _btnAddToCard = [[UIButton alloc] initWithFrame:CGRectMake(kMainFrameWidth/2, 0, kMainFrameWidth/2, 50)];
        [_btnAddToCard setBackgroundColor:[UIColor redColor]];
        if (_isSeckiling) {
            [_btnAddToCard setTitle:@"立即购买" forState:UIControlStateNormal];
        }else{
            [_btnAddToCard setTitle:@"加入购物车" forState:UIControlStateNormal];
        }
        [_btnAddToCard addTarget:self action:@selector(btnDidClickCard:) forControlEvents:UIControlEventTouchUpInside];
    }
    [self addSubview:_btnAddToCard];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
}

- (void)btnDidClickChat:(id)sender{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedChat)]) {
        [self.delegate clickedChat];
    }
}

- (void)btnDidClickCard:(id)sender{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedCard)]) {
        [self.delegate clickedCard];
    }
}


@end
