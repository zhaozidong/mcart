//
//  MCMallDetailCommentCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/27.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseTableViewCell.h"
#import "MCMallDetailModel.h"



@interface MCMallDetailCommentCell : MCBaseTableViewCell


- (void)updateWithComment:(MCMallDetailCommentModel *)comment;


+ (CGFloat)cellHeight:(MCMallDetailCommentModel *)comment;


@end
