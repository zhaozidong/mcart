//
//  MCMallDetailCardTotalView.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/29.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMallDetailCardTotalView.h"

@implementation MCMallDetailCardTotalView{
    UIButton *_btnAll;
    UILabel *_lblAll;
    UIButton *_btnPay;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)init{
    self = [super init];
    if (self) {
        [self setUpSubView];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpSubView];
    }
    return self;
}

- (void)setUpSubView{
    self.backgroundColor = UIColorFromRGB(0xe7e8e7);
    if (!_btnAll) {
        _btnAll = [[UIButton alloc] initWithFrame:CGRectMake(8, (CGRectGetHeight(self.bounds)-25)/2, 80, 25)];
        [_btnAll setImage:[UIImage imageNamed:@"shoppingCard_noCheck"] forState:UIControlStateNormal];
        [_btnAll setImage:[UIImage imageNamed:@"shoppingCard_checked"] forState:UIControlStateSelected];
        [_btnAll setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        
        [_btnAll setTitle:@"全选" forState:UIControlStateNormal];
        [_btnAll addTarget:self action:@selector(btnDidClickAll:) forControlEvents:UIControlEventTouchUpInside];
    }
    [self addSubview:_btnAll];
    
    if (!_lblAll) {
        _lblAll = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_btnAll.frame)+8, CGRectGetMinY(_btnAll.frame), 80, 30)];
    }
    [self addSubview:_lblAll];
    
    if (!_btnPay) {
        _btnPay = [[UIButton alloc] initWithFrame:CGRectMake(kMainFrameWidth*0.75, 0, kMainFrameWidth * 0.25, CGRectGetHeight(self.bounds))];
        [_btnPay.titleLabel setFont:kAppFont(16.f)];
        [_btnPay setBackgroundColor:[UIColor redColor]];
        [_btnPay setTitle:@"去付款" forState:UIControlStateNormal];
        [_btnPay addTarget:self action:@selector(btnDidClickPay:) forControlEvents:UIControlEventTouchUpInside];
    }
    [self addSubview:_btnPay];
}

- (void)setSelected:(BOOL)selected{
    _selected = selected;
    _btnAll.selected = selected;
}

- (void)updateWithTotal:(NSString *)total{
    if (total) {
        _lblAll.text = [NSString stringWithFormat:@"合计：￥%@",total];
        [_lblAll sizeToFit];
    }
}


- (void)layoutSubviews{
    [super layoutSubviews];
    
    
    
}


- (void)btnDidClickAll:(id)sender{
    UIButton *button = (UIButton *)sender;
    button.selected = !button.selected;
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedAll:)]) {
        [self.delegate clickedAll:button.selected];
    }
}


- (void)btnDidClickPay:(id)sender{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedPay)]) {
        [self.delegate clickedPay];
    }
}


@end
