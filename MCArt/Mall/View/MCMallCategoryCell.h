//
//  MCMallCategoryCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/21.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseTableViewCell.h"
#import "MCMallModel.h"
#import "MCMallCategoryModel.h"


@protocol MCMallCategoryCellDelegate <NSObject>

@optional
- (void)clickedCategoryInfo:(MCMallCategoryDetailModel *)info;

- (void)clickedWithMallInfo:(MCMallModel *)info;

@end





@interface MCMallCategoryCell : MCBaseTableViewCell

@property (nonatomic, weak) id<MCMallCategoryCellDelegate> delegate;

- (void)updateWithMallInfo:(MCMallModel *)info;

- (void)updateWithCategoryInfo:(MCMallCategoryDetailModel *)info;


+ (CGFloat)cellHeight;

@end
