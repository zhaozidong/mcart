//
//  MCMallDetailMoreCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/27.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMallDetailMoreCell.h"
#import "SegmentTapView.h"
#import "FlipTableView.h"
#import "MCMallDetailWebViewController.h"
#import "MCMallDetailCellParamViewController.h"
#import "MCMallDetailCellCommentViewController.h"


@interface MCMallDetailMoreCell()<SegmentTapViewDelegate,FlipTableViewDelegate>{
    SegmentTapView *_tapView;
    FlipTableView *_flipView;
    MCMallDetailWebViewController *_detail;
    MCMallDetailCellParamViewController *_param;
    MCMallDetailCellCommentViewController *_comment;
    
}
    
@end


@implementation MCMallDetailMoreCell

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setUpSubViews{
    if (!_tapView) {
        _tapView = [[SegmentTapView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth, 40) withDataArray:@[@"商品详情",@"产品参数",@"买家评论"] withFont:14];
        _tapView.delegate = self;
    }
    [self.contentView addSubview:_tapView];
    
    if (!_detail) {
        _detail = [[MCMallDetailWebViewController alloc] init];
    }
    
    if (!_param) {
        _param = [[MCMallDetailCellParamViewController alloc] init];
    }

    if (!_comment) {
        _comment = [[MCMallDetailCellCommentViewController alloc] init];
    }
    
    if (!_flipView) {
        _flipView = [[FlipTableView alloc] initWithFrame:CGRectMake(0, 40, kMainFrameWidth, 500) withArray:@[_detail,_param,_comment]];
        _flipView.delegate = self;
    }
    [self.contentView addSubview:_flipView];
}
- (void)setSelectIndex:(NSInteger)selectIndex{
    _selectIndex = selectIndex;
    [_flipView selectIndex:selectIndex];
    [_tapView selectIndex:selectIndex+1];
}

- (void)updateWithDetailInfo:(MCMallDetailModel *)info{
    if (!info) {
        return;
    }
    
    if (info.descr_url) {
        [_detail updateWithURL:kURLPath(info.descr_url)];
        [_flipView selectIndex:0];
    }
    
    if (info.param) {
        _param.param = info.param;
    }
    
    if (info.comment) {
        _comment.comment = info.comment;
    }
}

+ (CGFloat)cellHeight{
    return 540;
}

#pragma mark SegmentTapViewDelegate
-(void)selectedIndex:(NSInteger)index{
    [_flipView selectIndex:index];
}

#pragma mark FlipTableViewDelegate
-(void)scrollChangeToIndex:(NSInteger)index{
    [_tapView selectIndex:index];
}

@end
