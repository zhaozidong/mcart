//
//  MCMallDetailParamCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/28.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMallDetailParamCell.h"

@implementation MCMallDetailParamCell{
    UILabel *_lblTitle;
    UILabel *_lblDetail;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (void)setUpSubViews{
    if (!_lblTitle) {
        _lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(8, 8, 70, 30)];
        _lblTitle.textColor = [UIColor lightGrayColor];
        _lblTitle.font = kAppFont(14.f);
    }
    [self.contentView addSubview:_lblTitle];
    
    if (!_lblDetail) {
        _lblDetail = [[UILabel alloc] initWithFrame:CGRectMake(88, 8, kMainFrameWidth - 88 -8, 200)];
        _lblDetail.numberOfLines = 0;
        _lblDetail.preferredMaxLayoutWidth = kMainFrameWidth - 88 -8;
        _lblDetail.font = kAppFont(14.f);
    }
    [self.contentView addSubview:_lblDetail];
}


- (void)updateWithParam:(MCMallDetailParamModel *)param{
    
    if (!param) {
        return;
    }
    
    if (param.key) {
        _lblTitle.text = param.key;
        [_lblTitle sizeToFit];
    }
    
    if (param.value) {
        _lblDetail.text = param.value;
        [_lblDetail sizeToFit];
    }
    
    
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    CGRect frame = _lblTitle.frame;
    _lblTitle.frame = CGRectMake(8, 8, 70, frame.size.height);
    
    frame = _lblDetail.frame;
    _lblDetail.frame = CGRectMake(88, 8, kMainFrameWidth - 88 - 8, frame.size.height);
    
}


+ (CGFloat)cellHeightWithParam:(MCMallDetailParamModel *)param{
    CGRect rect = [param.value boundingRectWithSize:CGSizeMake(kMainFrameWidth - 88-8, 300)
                                            options:NSStringDrawingUsesLineFragmentOrigin
                                         attributes:@{NSFontAttributeName:kAppFont(14.f)}
                                            context:nil];
    return rect.size.height + 8*2;
}




@end
