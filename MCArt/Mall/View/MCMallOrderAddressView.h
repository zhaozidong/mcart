//
//  MCMallOrderAddressView.h
//  MCArt
//
//  Created by Derek.zhao on 16/4/15.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCPersonalModel.h"


@protocol MCMallOrderAddressViewDelegate <NSObject>

- (void)clickedConfirm:(BOOL)selfFetch expressInfo:(MCPersonalExpressDetailModel *)info;

@end



@interface MCMallOrderAddressView : UIView

@property (nonatomic, weak) id<MCMallOrderAddressViewDelegate> delegate;

- (void)updateWithExpressDetail:(MCPersonalExpressDetailModel *)info;


@end
