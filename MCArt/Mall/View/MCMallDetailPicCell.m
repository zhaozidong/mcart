//
//  MCMallDetailPicCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/27.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMallDetailPicCell.h"
#import "CycleScrollView.h"


@interface MCMallDetailPicCell ()<CycleScrollViewDelegate>{
    CycleScrollView *_scrollView;
}
@end

@implementation MCMallDetailPicCell
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setUpSubViews{
    self.backgroundColor = kWhiteColor;
}

- (void)updateWithDetailInfo:(MCMallDetailModel *)info{
    
    if (!info) {
        return;
    }
    
    if (info.cover && info.cover.count > 0) {
        NSMutableArray *array = [NSMutableArray array];
        for (MCMallDetailCoverModel *cv in info.cover) {
            [array addObject:kImagePath(cv.pic_url)];
        }
        
        _scrollView = [[CycleScrollView alloc]initWithFrame:self.bounds];
        _scrollView.urlArray = array;
        _scrollView.delegate = self;
        _scrollView.autoScroll = YES;
        _scrollView.autoTime = 2.5f;
        //    cycleScroll.pageControlCurrentPageIndicatorTintColor = [UIColor redColor];
        [self.contentView addSubview:_scrollView];
    }
}

+ (CGFloat)cellHeight{
    return kMainFrameWidth*0.875;
}


- (void)clickImgAtIndex:(NSInteger)index{
    
    
    
    
}

- (void)removeScroll {
    _scrollView.delegate = nil;
    [_scrollView disableTimer];
}


@end
