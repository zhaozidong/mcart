//
//  MCMallCategoryCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/21.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMallCategoryCell.h"
#import <Masonry.h>

NSInteger const PADDING = 8;


@implementation MCMallCategoryCell{
    UIImageView *_imageView;
    UILabel *_lblTitle;
    UILabel *_lblSellNum;
    UILabel *_lblPice;
    UILabel *_lblOrignalPrice;
    UIImageView *_imgViewCard;
    BOOL isCategory;
    MCMallModel *_mallInfo;
    MCMallCategoryDetailModel *_categoryInfo;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setUpSubViews{
    if (!_imageView) {
        _imageView = [[UIImageView alloc] init];
        _imageView.contentMode = UIViewContentModeScaleAspectFill;
        _imageView.clipsToBounds = YES;
    }
    [self.contentView addSubview:_imageView];
    
    if (!_lblTitle) {
        _lblTitle = [[UILabel alloc] init];
        _lblTitle.font = kAppFont(18.f);
        _lblTitle.numberOfLines = 2;
    }
    [self.contentView addSubview:_lblTitle];
    
    if (!_lblSellNum) {
        _lblSellNum = [[UILabel alloc] init];
        _lblSellNum.font = kAppFont(12.f);
        _lblSellNum.textColor = [UIColor lightGrayColor];
    }
    [self.contentView addSubview:_lblSellNum];
    
    if (!_lblPice) {
        _lblPice = [[UILabel alloc] init];
        _lblPice.textColor = [UIColor redColor];
        _lblPice.font = kAppFont(22.f);
    }
    [self.contentView addSubview:_lblPice];

    if (!_lblOrignalPrice) {
        _lblOrignalPrice = [[UILabel alloc] init];
        _lblOrignalPrice.textColor = [UIColor lightGrayColor];
    }
    [self.contentView addSubview:_lblOrignalPrice];
    
    
    if (!_imgViewCard) {
        _imgViewCard = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"shoppingCard"]];
        _imgViewCard.userInteractionEnabled = YES;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapCard)];
        tap.numberOfTapsRequired = 1;
        [_imgViewCard addGestureRecognizer:tap];
    }
    [self.contentView addSubview:_imgViewCard];
    
}


- (void)updateWithMallInfo:(MCMallModel *)info{
    
    if (!info) {
        return;
    }
    
    isCategory = NO;
    _mallInfo = info;
    
    if (info.pic_B) {
        [_imageView sd_setImageWithURL:[NSURL URLWithString:kImagePath(info.pic_B)] placeholderImage:[UIImage imageNamed:@"info_loading_title"]];
    }
    
    
    if (info.title) {
        _lblTitle.text = info.title;
    }
    
    if (info.discount) {
        _lblPice.text = [NSString stringWithFormat:@"￥%@",info.discount];
        [_lblPice sizeToFit];
    }
    
    if (info.price) {
        NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"￥%@",info.price]];
        [attStr addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlineStyleSingle) range:NSMakeRange(0, attStr.length)];
        _lblOrignalPrice.attributedText = attStr;
        [_lblOrignalPrice sizeToFit];
        
    }
}

- (void)updateWithCategoryInfo:(MCMallCategoryDetailModel *)info{
    
    if (!info) {
        return;
    }
    
    isCategory = YES;
    _categoryInfo = info;
    
    if (info.pic_A) {
        [_imageView sd_setImageWithURL:[NSURL URLWithString:kImagePath(info.pic_A)] placeholderImage:[UIImage imageNamed:@"info_loading_title"]];
    }
    
    if (info.title) {
        _lblTitle.text = info.title;
    }
    
    if (info.sell_num) {
        _lblSellNum.text = [NSString stringWithFormat:@"已售%@件",info.sell_num];
        [_lblSellNum sizeToFit];
    }
    
    if (info.discount) {
        _lblPice.text = [NSString stringWithFormat:@"￥%@",info.discount];
        [_lblPice sizeToFit];
    }
    
    if (info.price) {
        NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"￥%@",info.price]];
        [attStr addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlineStyleSingle) range:NSMakeRange(0, attStr.length)];
        _lblOrignalPrice.attributedText = attStr;
        [_lblOrignalPrice sizeToFit];
        
    }
}



- (void)layoutSubviews{
    [super layoutSubviews];
    
    _imageView.frame = CGRectMake(8, 8, 100, CGRectGetHeight(self.bounds) - 2* 8);
    
    _lblTitle.frame = CGRectMake(CGRectGetMaxX(_imageView.frame) + 8, 8, kMainFrameWidth - CGRectGetWidth(_imageView.frame) - 3* 8, 50);
    
    _lblSellNum.frame = CGRectMake(CGRectGetMinX(_lblTitle.frame), CGRectGetMaxY(_lblTitle.frame)+8, CGRectGetWidth(_lblTitle.frame), 20);
    
    
    CGRect frame;
//    if (isCategory) {
//        frame = _lblSellNum.frame;
//        _lblSellNum.frame = CGRectMake(CGRectGetMinX(_lblTitle.frame), CGRectGetMaxY(), <#CGFloat width#>, <#CGFloat height#>)
//        
//        
//    }
    
    frame = _lblPice.frame;
    _lblPice.frame = CGRectMake(CGRectGetMinX(_lblTitle.frame), CGRectGetMaxY(_lblSellNum.frame), frame.size.width, frame.size.height);
    
    
    frame = _lblOrignalPrice.frame;
    _lblOrignalPrice.frame = CGRectMake(CGRectGetMaxX(_lblPice.frame)+8, CGRectGetMaxY(_lblPice.frame)-frame.size.height, frame.size.width, frame.size.height);
    
    
    frame = _imgViewCard.frame;
    _imgViewCard.frame =CGRectMake(CGRectGetWidth(self.bounds)-8-frame.size.width, CGRectGetHeight(self.bounds)-8-frame.size.height, frame.size.width, frame.size.height);
    
}

+ (CGFloat)cellHeight{
    return 130.f;
}


- (void)tapCard{
    if (isCategory) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(clickedCategoryInfo:)]) {
            [self.delegate clickedCategoryInfo:_categoryInfo];
        }
    }else{
        if (self.delegate && [self.delegate respondsToSelector:@selector(clickedWithMallInfo:)]) {
            [self.delegate clickedWithMallInfo:_mallInfo];
        }
    }
}

@end
