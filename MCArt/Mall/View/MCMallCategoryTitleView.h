//
//  MCMallCategoryTitleView.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/21.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCArrowButton.h"


@protocol MCMallCategoryTitleDelegate <NSObject>

- (void)clickedItemAtIndex:(NSInteger)index order:(MCArrowButtonStatus)order;

@end





@interface MCMallCategoryTitleView : UIView

@property(nonatomic, weak) id<MCMallCategoryTitleDelegate> delegate;

- (instancetype)initWithTitle:(NSArray *)arrTitle arrow:(NSArray *)arrArrow;


@end
