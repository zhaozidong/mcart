//
//  MCMallImmediatelyCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/26.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMallImmediatelyCell.h"
#import <UIImageView+WebCache.h>
#import "MCMallViewModel.h"


@implementation MCMallImmediatelyCell{
    UIImageView *_imageView;
    UIView *_timeContainerView;
    UILabel *_lblTitle;
    UILabel *_lblPrice;
    UILabel *_lblOrginalPrice;
    UILabel *_lblBuyNum;
    UILabel *_lblTime;
    
    long long _sec;
    NSTimer *_timer;
    
}

- (instancetype)init{
    self = [super init];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}

- (void)setUpSubViews{
    self.contentView.backgroundColor = kWhiteColor;
    
    if (!_imageView) {
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(4, 4, CGRectGetWidth(self.bounds) - 2*4, (CGRectGetWidth(self.bounds) - 2*4)/1.19)];
        
    }
    [self.contentView addSubview:_imageView];
    
    if (!_timeContainerView) {
        _timeContainerView = [[UIView alloc] initWithFrame:CGRectMake(4, CGRectGetMaxY(_imageView.frame), CGRectGetWidth(self.bounds)-2*4, 60)];
        _timeContainerView.backgroundColor = [UIColor orangeColor];
    }
    [self.contentView addSubview:_timeContainerView];
    
    if (!_lblTime) {
        _lblTime = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(_timeContainerView.frame), CGRectGetHeight(_timeContainerView.frame))];
        _lblTime.textAlignment = NSTextAlignmentCenter;
        _lblTime.font = kAppFont(12.f);
    }
    [_timeContainerView addSubview:_lblTime];
    
    
    if (!_lblTitle) {
        _lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(4, CGRectGetMaxY(_timeContainerView.frame)+8, CGRectGetWidth(self.bounds)-2*4, 50)];
        _lblTitle.font = kAppFont(16.f);
    }
    [self.contentView addSubview:_lblTitle];
    
    if (!_lblPrice) {
        _lblPrice = [[UILabel alloc] initWithFrame:CGRectMake(4, CGRectGetMaxY(_lblPrice.frame)+10, 80, 50)];
        _lblPrice.textColor = [UIColor redColor];
        _lblPrice.font = kAppFont(15.f);
    }
    [self.contentView addSubview:_lblPrice];
    
    if (!_lblOrginalPrice) {
        _lblOrginalPrice = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_lblPrice.frame)+1, CGRectGetMaxY(_lblOrginalPrice.frame)+40, 80, 20)];
        _lblOrginalPrice.textColor = [UIColor lightGrayColor];
        _lblOrginalPrice.font = kAppFont(11.f);
    }
    [self.contentView addSubview:_lblOrginalPrice];
    
    if (!_lblBuyNum) {
        _lblBuyNum = [[UILabel alloc] init];
        _lblBuyNum.textColor = [UIColor lightGrayColor];
        _lblBuyNum.font = kAppFont(11.f);
    }
    [self.contentView addSubview:_lblBuyNum];
    
}

- (void)updateWithInfo:(MCMallModel *)info{
    
    if (!info) {
        return;
    }
    
    if (_timer) {
        [_timer invalidate];
    }
    
    
    if (info.pic_B) {
        [_imageView sd_setImageWithURL:[NSURL URLWithString:kImagePath(info.pic_B)] placeholderImage:[UIImage imageNamed:@"info_loading_title"]];
    }
    
    if (info.title) {
        _lblTitle.text = info.title;
    }
    
    if (info.price) {
        _lblPrice.text = [NSString stringWithFormat:@"￥%@",info.price];
        [_lblPrice sizeToFit];
    }
    
    if (info.discount) {
        NSMutableAttributedString *attStr = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"￥%@",info.discount]];
        [attStr addAttribute:NSStrikethroughStyleAttributeName value:@(NSUnderlineStyleSingle) range:NSMakeRange(0, attStr.length)];
        _lblOrginalPrice.attributedText = attStr;
        [_lblOrginalPrice sizeToFit];
        
    }
    
    if (info.sell_num) {
        _lblBuyNum.text = [NSString stringWithFormat:@"%@人付款",info.sell_num];
        [_lblBuyNum sizeToFit];
    }
    
    if (info.end_time) {
        long long sec = ([info.end_time longLongValue] - [MCMallViewModel sharedInstance].currentTime)/1000;
        
        if (sec > 0) {
            _sec = sec;
            _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timer) userInfo:nil repeats:YES];
            [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
        }
        
//        NSLog(@"sec = %ld秒",(long)sec);
//        NSInteger day = (NSInteger)(sec / (24*3600));
//        NSInteger hour = (sec - day*(24*3600))/3600;
//        NSInteger min = (sec - day*(24*3600) - hour*3600)/60;
//        NSInteger s = sec - day*(24*3600) - hour*3600 - 60*min;
//        NSLog(@"%@天%@小时%@分%@秒",@(day),@(hour),@(min),@(s));
        
    }
}


- (void)timer{
    if (_sec > 0) {
        _sec --;
    }else{
        if (_timer) {
            [_timer invalidate];
            _timer = nil;
        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"kMallDetailRefreshDataNotification" object:nil];
        });
        return;
    }
    
//    long long day = (NSInteger)(_sec / (24*3600));
//    long long hour = (_sec - day*(24*3600))/3600;
//    long long min = (_sec - day*(24*3600) - hour*3600)/60;
//    long long s = _sec - day*(24*3600) - hour*3600 - 60*min;
//    NSString *strTime = [NSString stringWithFormat:@"距结束:%@天%@小时%@分%@秒",@(day),@(hour),@(min),@(s)];
    
    long long hour = _sec / 3600;
    long long min = (_sec - hour*3600) / 60;
    long long s = _sec - hour*3600 - 60*min;
    NSString *strTime = [NSString stringWithFormat:@"距结束:%@小时%@分%@秒",@(hour),@(min),@(s)];
    
    _lblTime.text = strTime;
}


- (void)layoutSubviews{
    [super layoutSubviews];
    
    
    CGRect frame;
    _imageView.frame = CGRectMake(4, 4, CGRectGetWidth(self.bounds) - 2*4, (CGRectGetWidth(self.bounds) - 2*4)/1.19);
    
    _timeContainerView.frame = CGRectMake(4, CGRectGetMaxY(_imageView.frame), CGRectGetWidth(self.bounds)-2*4, 30);
    
    _lblTime.frame = CGRectMake(0, 0, CGRectGetWidth(_timeContainerView.frame), CGRectGetHeight(_timeContainerView.frame));
    
    _lblTitle.frame = CGRectMake(4, CGRectGetMaxY(_timeContainerView.frame)+1, CGRectGetWidth(self.bounds)-2*4, 30);
    
    frame = _lblPrice.frame;
    _lblPrice.frame = CGRectMake(4, CGRectGetHeight(self.bounds)-8-frame.size.height, frame.size.width, frame.size.height);
    
    
    frame = _lblOrginalPrice.frame;
    _lblOrginalPrice.frame = CGRectMake(CGRectGetMaxX(_lblPrice.frame)+1, CGRectGetMaxY(_lblPrice.frame)-frame.size.height, frame.size.width, frame.size.height);
    
    frame = _lblBuyNum.frame;
    _lblBuyNum.frame = CGRectMake(CGRectGetWidth(self.bounds)-4-frame.size.width, CGRectGetHeight(self.bounds)-8-frame.size.height, frame.size.width, frame.size.height);
}





@end
