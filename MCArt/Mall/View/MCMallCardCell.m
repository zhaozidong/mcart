//
//  MCMallCardCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/29.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMallCardCell.h"
#import "PKYStepper.h"


@implementation MCMallCardCell{
    UIButton *_btnCheck;
    UIImageView *_imageView;
    UILabel *_lblTitle;
    UILabel *_lblPrice;
    MCMallCardModel *_info;
    PKYStepper *_stepper;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setUpSubViews{
    
    if (!_btnCheck) {
        _btnCheck = [[UIButton alloc] initWithFrame:CGRectMake(8, (CGRectGetHeight(self.bounds)-25)/2, 25, 25)];
        [_btnCheck setImage:[UIImage imageNamed:@"shoppingCard_noCheck"] forState:UIControlStateNormal];
        [_btnCheck setImage:[UIImage imageNamed:@"shoppingCard_checked"] forState:UIControlStateSelected];
        [_btnCheck addTarget:self action:@selector(btnDidClickCheck:) forControlEvents:UIControlEventTouchUpInside];
    }
    [self.contentView addSubview:_btnCheck];
    
    if (!_imageView) {
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_btnCheck.frame)+8, 8, CGRectGetHeight(self.bounds)-2*8, CGRectGetHeight(self.bounds)-2*8)];
        _imageView.contentMode = UIViewContentModeScaleAspectFill;
        _imageView.clipsToBounds = YES;
    }
    [self.contentView addSubview:_imageView];
    
    
    if (!_lblTitle) {
        _lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_imageView.frame)+8, 8, kMainFrameWidth - CGRectGetMaxX(_imageView.frame) - 2* 8, 50)];
        _lblTitle.preferredMaxLayoutWidth = kMainFrameWidth - CGRectGetMaxX(_imageView.frame) - 2*8;
        _lblTitle.numberOfLines = 3;
        _lblTitle.font = kAppFont(14.f);
    }
    [self.contentView addSubview:_lblTitle];
    
    
    if (!_lblPrice) {
        _lblPrice = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_imageView.frame) + 8, CGRectGetMaxY(_lblTitle.frame)+10, 100, 30)];
        _lblPrice.textColor = [UIColor redColor];
        _lblPrice.font = kAppFont(16.f);
    }
    [self.contentView addSubview:_lblPrice];
    
    if (!_stepper) {
        _stepper = [[PKYStepper alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.bounds)-8-120, CGRectGetMinY(_lblPrice.frame)-5, 120, 30)];
        [_stepper setCornerRadius:3.0f];
        [_stepper setBorderColor:[UIColor colorWithWhite:0.95 alpha:1.0]];
        [_stepper setLabelTextColor:kBlackColor];
        [_stepper setButtonTextColor:kBlackColor forState:UIControlStateNormal];
        _stepper.value = 1.0f;
        __weak typeof(self) weakSelf = self;
        _stepper.valueChangedCallback = ^(PKYStepper *stepper,float count) {
            stepper.countLabel.text = [NSString stringWithFormat:@"%@",@(count)];
            [weakSelf updateNumber:(int)count];
        };
        
        [_stepper setup];
    }
    [self.contentView addSubview:_stepper];
}

- (void)updateWithCardInfo:(MCMallCardModel *)info{
    
    if (!info) {
        return;
    }
    
    _info = info;
    
    if (info.pic_url) {
        [_imageView sd_setImageWithURL:[NSURL URLWithString:kImagePath(info.pic_url)]];
    }
    
    if (info.gname) {
        _lblTitle.text = info.gname;
//        [_lblTitle sizeToFit];
    }
    
    if (info.price) {
        _lblPrice.text = [NSString stringWithFormat:@"￥%@",info.price];
        [_lblPrice sizeToFit];
    }
    
    if (info.gnum) {
        _stepper.value = [info.gnum integerValue];
    }
    
    
    if (info.selected) {
        _btnCheck.selected = YES;
    }else{
        _btnCheck.selected = NO;
    }
    
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    _btnCheck.frame = CGRectMake(8, (CGRectGetHeight(self.bounds)-25)/2, 25, 25);
    
    _imageView.frame = CGRectMake(CGRectGetMaxX(_btnCheck.frame)+8, 8, CGRectGetHeight(self.bounds)-2*8, CGRectGetHeight(self.bounds)-2*8);
    
    _lblTitle.frame = CGRectMake(CGRectGetMaxX(_imageView.frame)+8, 8, kMainFrameWidth - CGRectGetMaxX(_imageView.frame) - 2* 8, 40);
    
    CGRect frame = _lblPrice.frame;
    _lblPrice.frame = CGRectMake(CGRectGetMaxX(_imageView.frame) + 8, CGRectGetMaxY(_lblTitle.frame)+2, frame.size.width, frame.size.height);
    
    _stepper.frame = CGRectMake(CGRectGetWidth(self.bounds)-8-130, CGRectGetHeight(self.bounds)-30-4, 130, 30);
    
}

+ (CGFloat)cellHeight{
    return 100.f;
}

- (void)updateNumber:(NSInteger)count{
    if (_info) {
        _info.gnum = [NSString stringWithFormat:@"%@",@(count)];
        if (self.delegate && [self.delegate respondsToSelector:@selector(clickedNumber:)]) {
            [self.delegate clickedNumber:_info];
        }
    }
}


- (void)btnDidClickCheck:(id)sender{
    UIButton *button = (UIButton *)sender;
    button.selected = !button.selected;
    _info.selected = button.selected;
    if (_info && self.delegate && [self.delegate respondsToSelector:@selector(clickedCheck:)]) {
        [self.delegate clickedCheck:_info];
    }
}

@end
