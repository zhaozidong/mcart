//
//  MCMallDetailTitleCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/27.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMallDetailTitleCell.h"

@implementation MCMallDetailTitleCell{
    UILabel *_lblTitle;
    UILabel *_lblPrice;
    UILabel *_lblExpress;
    UILabel *_lblSellNum;
    UILabel *_lblSendFrom;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setUpSubViews{
    
    if (!_lblTitle) {
        _lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(8, 8, CGRectGetWidth(self.bounds) - 2*8, 30)];
        _lblTitle.font = kAppFont(18.f);
    }
    [self.contentView addSubview:_lblTitle];
    
    if (!_lblPrice) {
        _lblPrice = [[UILabel alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(_lblTitle.frame)+8, 100, 40)];
        _lblPrice.textColor = [UIColor redColor];
        _lblPrice.font = kAppFont(18.f);
    }
    [self.contentView addSubview:_lblPrice];
    
    if (!_lblExpress) {
        _lblExpress = [[UILabel alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(_lblPrice.frame)+8, 100, 20)];
        _lblExpress.textColor = [UIColor lightGrayColor];
        _lblExpress.font = kAppFont(12.f);
    }
    [self.contentView addSubview:_lblExpress];
    
    if (!_lblSellNum) {
        _lblSellNum = [[UILabel alloc] initWithFrame:CGRectMake(100, CGRectGetMinY(_lblExpress.frame), 100, 20)];
        _lblSellNum.textColor = [UIColor lightGrayColor];
        _lblSellNum.font = kAppFont(12.f);
    }
    [self.contentView addSubview:_lblSellNum];
    
    if (!_lblSendFrom) {
        _lblSendFrom = [[UILabel alloc] initWithFrame:CGRectMake(300, CGRectGetMinY(_lblExpress.frame), 100, 20)];
        _lblSendFrom.textColor = [UIColor lightGrayColor];
        _lblSendFrom.font = kAppFont(12.f);
    }
    [self.contentView addSubview:_lblSendFrom];
    
    
}

- (void)updateWithDetailInfo:(MCMallDetailModel *)info{
    if (!info) {
        return;
    }
    
    if (info.title) {
        _lblTitle.text = info.title;
    }
    
    if (info.discount) {
        _lblPrice.text = [NSString stringWithFormat:@"￥%@",info.discount];
        [_lblPrice sizeToFit];
    }
    
    if (info.express) {
        _lblExpress.text = [NSString stringWithFormat:@"快递：%@",info.express];
        [_lblExpress sizeToFit];
    }
    
    if (info.sell_num) {
        _lblSellNum.text = [NSString stringWithFormat:@"已售%@件",info.sell_num];
        [_lblSellNum sizeToFit];
    }
    
    if (info.sendfrom) {
        _lblSendFrom.text = info.sendfrom;
        [_lblSendFrom sizeToFit];
    }
    
}


- (void)layoutSubviews{
    [super layoutSubviews];
    
    CGRect frame;
    _lblTitle.frame = CGRectMake(8, 8, kMainFrameWidth - 2*8, 30);
    
    frame = _lblPrice.frame;
    _lblPrice.frame = CGRectMake(8, CGRectGetMaxY(_lblTitle.frame) + 8, frame.size.width, frame.size.height);
    
    frame = _lblExpress.frame;
    _lblExpress.frame = CGRectMake(8, CGRectGetMaxY(_lblPrice.frame) + 8, frame.size.width, frame.size.height);
    
    frame = _lblSellNum.frame;
    _lblSellNum.frame = CGRectMake((CGRectGetWidth(self.bounds)-frame.size.width)/2, CGRectGetMinY(_lblExpress.frame), frame.size.width, frame.size.height);
    
    frame = _lblSendFrom.frame;
    _lblSendFrom.frame = CGRectMake(CGRectGetWidth(self.bounds)-8-frame.size.width, CGRectGetMinY(_lblExpress.frame), frame.size.width, frame.size.height);
    
}


+ (CGFloat)cellHeight{
    return 100.f;
}

@end
