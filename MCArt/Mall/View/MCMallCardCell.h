//
//  MCMallCardCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/29.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseTableViewCell.h"
#import "MCMallCardModel.h"


@protocol MCMallCardCellDelegate <NSObject>

- (void)clickedCheck:(MCMallCardModel *)goodsInfo;

- (void)clickedNumber:(MCMallCardModel *)goodsInfo;


@end


@interface MCMallCardCell : MCBaseTableViewCell

@property(nonatomic, weak) id<MCMallCardCellDelegate> delegate;

- (void)updateWithCardInfo:(MCMallCardModel *)info;

+ (CGFloat)cellHeight;

@end
