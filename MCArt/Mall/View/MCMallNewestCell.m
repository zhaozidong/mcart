//
//  MCMallNewestCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/24.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMallNewestCell.h"

@implementation MCMallNewestCell{
    UIImageView *_imageView;
    UIView *_bottomView;
    UILabel *_lblTitle;
    UILabel *_lblPrice;
    
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setUpSubViews{
    if (!_imageView) {
        _imageView = [[UIImageView alloc] initWithFrame:self.bounds];
        _imageView.contentMode = UIViewContentModeScaleAspectFill;
        _imageView.clipsToBounds = YES;
    }
    [self.contentView addSubview:_imageView];
    
    
    if (!_bottomView) {
        _bottomView = [[UIView alloc] init];
        _bottomView.backgroundColor = kRGBA(0, 0, 0, 0.54);
    }
    [self.contentView addSubview:_bottomView];
    
    
    if (!_lblTitle) {
        _lblTitle = [[UILabel alloc] init];
        _lblTitle.textColor = kWhiteColor;
    }
    [_bottomView addSubview:_lblTitle];
    
    
    if (!_lblPrice) {
        _lblPrice = [[UILabel alloc] init];
        _lblPrice.textColor = [UIColor redColor];
        _lblPrice.textAlignment = NSTextAlignmentRight;
    }
    [_bottomView addSubview:_lblPrice];
    
    [self autoLayout];
}


- (void)autoLayout{
    
    [_imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
    
    [_bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.bottom.equalTo(@0);
        make.height.equalTo(@40);
    }];
    
    [_lblTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(@16);
        make.centerY.equalTo(_bottomView);
        make.height.equalTo(@30);
        make.width.equalTo(@(kMainFrameWidth - 100));
    }];
    
    [_lblPrice mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.equalTo(@-4);
        make.centerY.equalTo(_bottomView);
        make.height.equalTo(_lblTitle);
        make.width.equalTo(@100);
    }];
    
}


- (void)updateWithInfo:(MCMallModel *)info{
    if (!info) {
        return;
    }
    if (info.pic_A){
        [_imageView sd_setImageWithURL:[NSURL URLWithString:kImagePath(info.pic_A)] placeholderImage:[UIImage imageNamed:@"info_loading_banner"]];
    }
    
    if (info.title){
        _lblTitle.text = info.title;
    }
    
    if (info.discount) {
        _lblPrice.text = [NSString stringWithFormat:@"￥%@",info.discount];
    }
    
}

+ (CGFloat)cellHeight{
    return 180.f;
}


@end
