//
//  MCMallDetailWebViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/23.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMallDetailWebViewController.h"

@interface MCMallDetailWebViewController ()<UIWebViewDelegate,UIScrollViewDelegate>

@property(nonatomic, strong)UIWebView *webView;

@end


@implementation MCMallDetailWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view addSubview:self.webView];
    
    if (self.url) {
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (UIWebView *)webView{
    if (!_webView) {
        _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth, CGRectGetHeight(self.view.bounds))];
        _webView.scrollView.contentSize = CGSizeMake(kMainFrameWidth, CGRectGetHeight(self.view.bounds)+150);
        _webView.backgroundColor = kWhiteColor;
        _webView.delegate =self;
        _webView.scrollView.delegate = self;

    }
    return _webView;
}

- (void)updateWithURL:(NSString *)url{
    self.url = url;
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
//    self.webView.frame = self.view.bounds;
}


#pragma mark UIWebViewDelegate

//- (void)webViewDidStartLoad:(UIWebView *)webView{
//
//    //    NSLog(@"start");
//}
//- (void)webViewDidFinishLoad:(UIWebView *)webView{
//
//    //    NSLog(@"finish");
//}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(nullable NSError *)error{
    
//    NSLog(@"fail desc=%@, code=%ld",error.description,error.code);
    
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGPoint offset = scrollView.contentOffset;
    if (offset.y < -15) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kMallDetailScrollToTopNotification" object:nil];
    }
}

@end
