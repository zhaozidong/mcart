//
//  MCMallDetailWebViewController.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/23.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseWebViewController.h"

@interface MCMallDetailWebViewController : MCBaseViewController

@property (nonatomic, strong) NSString *url;

- (void)updateWithURL:(NSString *)url;



@end
