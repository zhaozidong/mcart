//
//  MCMallDetailCellCommentViewController.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/27.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseTableViewController.h"

@interface MCMallDetailCellCommentViewController : MCBaseTableViewController

@property (nonatomic, retain) NSArray *comment;


@end
