//
//  MCMallPayViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/11.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMallPayViewController.h"
#import <AlipaySDK/AlipaySDK.h>


@interface MCMallPayViewController (){
    MCMallOrderModel *_orderInfo;
    NSInteger _restTime;
    UIButton *_btnPay;
    NSTimer *timer;
}

@end

@implementation MCMallPayViewController
- (instancetype)initWithOrderInfo:(MCMallOrderModel *)info{
    self = [super init];
    if (self) {
        _orderInfo = info;
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"订单支付" withStyle:MCNavStyleWhite];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.view.backgroundColor = kWhiteColor;
    
    [self setUpSubViews];
    
    
    timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                      target:self
                                                    selector:@selector(timeFireMethod)
                                                    userInfo:nil
                                                     repeats:YES];
    
    _restTime =  [_orderInfo.resttime integerValue];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [timer invalidate];
    timer = nil;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (void)back:(id)sender{
    [self backToCard];
}

- (void)backToCard{
    [self.navigationController popViewControllerAnimated:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kAppShowPersonalTicketNotification" object:nil];
}

- (void)setUpSubViews{
    NSMutableString *strGoods = [[NSMutableString alloc] init];
    for (MCMallOrderDetailModel *detail in _orderInfo.orderInfo) {
        NSString *string = [NSString stringWithFormat:@"%@   ￥%@*%@",detail.gname,detail.unitprice,detail.gnum];
        [strGoods appendString:string];
        [strGoods appendString:@"\n\n"];
    }
    CGRect rect = [strGoods boundingRectWithSize:CGSizeMake(kMainFrameWidth - 32, 1000)
                                         options:NSStringDrawingUsesLineFragmentOrigin
                                      attributes:@{NSFontAttributeName:kAppFont(14.f)}
                                         context:nil];
    
    UIScrollView *scroll = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    if (rect.size.height+350>kMainFrameHeight) {
        scroll.contentSize = CGSizeMake(kMainFrameWidth, rect.size.height+450);
    }
    [self.view addSubview:scroll];
    
    
    UILabel *_lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(8, 10, kMainFrameWidth - 2*8, 50)];
    _lblTitle.font = kAppFont(20.f);
    _lblTitle.text = _orderInfo.ordername;
    [scroll addSubview:_lblTitle];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(_lblTitle.frame)+8, kMainFrameWidth - 2 * 8, 1)];
    line.backgroundColor = [UIColor lightGrayColor];
    [scroll addSubview:line];
    
    UILabel *_lblOrderNum = [[UILabel alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(line.frame)+8, kMainFrameWidth - 2*8, 20)];
    _lblOrderNum.text = [NSString stringWithFormat:@"订单金额： %@",_orderInfo.total_price];
    [_lblOrderNum sizeToFit];
    [scroll addSubview:_lblOrderNum];
    
    UILabel *_lblPayNum = [[UILabel alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(_lblOrderNum.frame)+8, kMainFrameWidth - 2*8, 20)];
    _lblPayNum.text = [NSString stringWithFormat:@"应付金额： %@",_orderInfo.total_price];
    [_lblPayNum sizeToFit];
    [scroll addSubview:_lblPayNum];
    
    UIView *line2 = [[UIView alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(_lblPayNum.frame)+18, kMainFrameWidth - 2 * 8, 1)];
    line2.backgroundColor = [UIColor lightGrayColor];
    [scroll addSubview:line2];
    
    //商品
    UILabel *lblGoods = [[UILabel alloc] initWithFrame:CGRectMake(16, CGRectGetMaxY(line2.frame)+16, kMainFrameWidth - 32, rect.size.height)];
    lblGoods.numberOfLines = 0;
    lblGoods.font = kAppFont(14.f);
    lblGoods.text = strGoods;
    [scroll addSubview:lblGoods];
    
    
    UILabel *lblPayStyle = [[UILabel alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(lblGoods.frame)+8, 100, 20)];
    lblPayStyle.text = @"请选择支付方式";
    [lblPayStyle sizeToFit];
    [scroll addSubview:lblPayStyle];
    
    
    
    UIView *line3 = [[UIView alloc] initWithFrame:CGRectMake(8, CGRectGetMaxY(lblPayStyle.frame)+8, kMainFrameWidth - 2 * 8, 1)];
    line3.backgroundColor = [UIColor lightGrayColor];
    [scroll addSubview:line3];
    
    
    UIImageView *imgPay = [[UIImageView alloc] initWithFrame:CGRectMake(16, CGRectGetMaxY(line3.frame)+8, 40, 40)];
    imgPay.image = [UIImage imageNamed:@"zhifubao"];
    [scroll addSubview:imgPay];
    
    
    _btnPay = [[UIButton alloc] initWithFrame:CGRectMake(16, CGRectGetMaxY(imgPay.frame)+50, kMainFrameWidth-16*2, 40)];
    [_btnPay setBackgroundColor:[UIColor orangeColor]];
    [_btnPay setTitle:@"确认支付" forState:UIControlStateNormal];
    [_btnPay addTarget:self action:@selector(pay) forControlEvents:UIControlEventTouchUpInside];
    [scroll addSubview:_btnPay];
}

-(void)timeFireMethod{
    _restTime -= 1000;
    NSInteger sec = _restTime/1000;
    NSString *strTime = [NSString stringWithFormat:@"%@分%@秒",@(sec/60),@(sec%60)];
    [_btnPay setTitle:[NSString stringWithFormat:@"确认支付(还剩%@)",strTime] forState:UIControlStateNormal];
    
    if (sec < 20) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


- (void)pay{
    __weak typeof(self) weakSelf = self;
    [[AlipaySDK defaultService] payOrder:_orderInfo.ali_order_info fromScheme:@"mcartalipay" callback:^(NSDictionary *resultDic) {//只处理状态为9000(成功)和4000(失败)的情况
        if (resultDic) {
            if ([[resultDic objectForKey:@"resultStatus"] integerValue] == 9000) {
                [MCMessageUtilities showSuccessMessage:@"支付成功"];
                [weakSelf backToCard];
            }else{
                [MCMessageUtilities showSuccessMessage:@"支付失败"];
            }

        }
    }];
}

@end
