//
//  MCMallCategoryViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/13.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMallCategoryViewController.h"
#import "MCMallCategoryTitleView.h"
#import "MCMallCategoryCell.h"
#import "MCMallCategoryViewModel.h"
#import "MCMallSearchViewController.h"
#import "MCMallDetailViewController.h"
#import "MCMallShoppingCardViewController.h"
#import "MCMallDetailViewModel.h"
#import "MCLoginViewModel.h"


@interface MCMallCategoryViewController ()<UITableViewDataSource,UITableViewDelegate,MCMallCategoryTitleDelegate,MCMallCategoryCellDelegate>{
    MCMallCategoryModel *_category;
    __block NSMutableArray *_arrData;
    __block NSInteger _pageIndex;
    
    NSString *_classId;
    NSString *_order;
    
}

@property(nonatomic, retain) MCMallCategoryTitleView *titleView;

@property(nonatomic, retain) UIButton *searchButton;

@property(nonatomic, retain) UITableView *tableView;

@property (nonatomic, retain) UIButton *btnCard;

@end

@implementation MCMallCategoryViewController

- (instancetype)initWithCategory:(MCMallCategoryModel *)category{
    self = [super init];
    if (self) {
        _category = category;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.edgesForExtendedLayout=UIRectEdgeNone;
    
    [self setTitle:_category.name withStyle:MCNavStyleBlack];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.searchButton];
    
    [self.view addSubview:self.titleView];
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.btnCard];
    
    _classId = _category.class_id;
    _order = nil;
    _pageIndex = 1;
    _arrData = [NSMutableArray array];
    
    [self getData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (UIButton *)searchButton{
    if (!_searchButton) {
        _searchButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
        [_searchButton setImage:[UIImage imageNamed:@"search"] forState:UIControlStateNormal];
        [_searchButton addTarget:self action:@selector(btnDidClickSearch:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _searchButton;
}

- (MCMallCategoryTitleView *)titleView{
    if (!_titleView) {
        NSArray *array = @[@"综合",@"销量",@"价格",@"人气",@"最新"];
        _titleView = [[MCMallCategoryTitleView alloc] initWithTitle:array arrow:@[@(1),@(2),@(3)]];
        _titleView.frame = CGRectMake(0, 0, kMainFrameWidth, 40);
        _titleView.delegate = self;
    }
    return _titleView;
}

- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 40, kMainFrameWidth, kMainFrameHeight-40-64) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.scrollEnabled = YES;
        
        _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth, CGFLOAT_MIN)];
        
        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(beginRefreshHeader:)];
        _tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(beginRefreshFooter:)];
        
        [_tableView registerClass:[MCMallCategoryCell class] forCellReuseIdentifier:@"categoryCell"];
    }
    return _tableView;
}

- (UIButton *)btnCard{
    if (!_btnCard) {
        _btnCard = [[UIButton alloc] initWithFrame:CGRectMake(0, kMainFrameHeight - 64-40, 40, 40)];
        [_btnCard setImage:[UIImage imageNamed:@"jumpToCard"] forState:UIControlStateNormal];
        [_btnCard addTarget:self action:@selector(btnDidClickJumpToCard:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _btnCard;
}

- (void)btnDidClickSearch:(UIButton *)sender{
    MCMallSearchViewController *search = [[MCMallSearchViewController alloc] initWithCategory:_category];
    [self.navigationController pushViewController:search animated:YES];
}

- (void)btnDidClickJumpToCard:(UIButton *)sender{
    if (![MCLoginViewModel sharedInstance].isLogin) {
        [self showLoginViewController];
        return;
    }
    
    MCMallShoppingCardViewController *card = [[MCMallShoppingCardViewController alloc] init];
    [self.navigationController pushViewController:card animated:YES];
}


- (void)getData{
    __weak typeof(self) weakSelf = self;
    [[MCMallCategoryViewModel sharedInstance] getCategoryDetail:_classId keyWord:nil order:_order pageIndex:_pageIndex success:^(id resquestData) {
        if (_pageIndex == 1) {
            _arrData = [resquestData mutableCopy];
        }else{
            [_arrData addObjectsFromArray:resquestData];
        }
        _pageIndex ++;
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}


#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _arrData.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MCMallCategoryDetailModel *category = [_arrData objectAtIndex:indexPath.row];
    MCMallCategoryCell *categoryCell = [tableView dequeueReusableCellWithIdentifier:@"categoryCell"];
    categoryCell.delegate = self;
    [categoryCell updateWithCategoryInfo:category];
    return categoryCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [MCMallCategoryCell cellHeight];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    MCMallCategoryDetailModel *category = [_arrData objectAtIndex:indexPath.row];
    MCMallDetailViewController *detail = [[MCMallDetailViewController alloc] initWithId:category.id seckiling:NO];
    [self.navigationController pushViewController:detail animated:YES];
}


#pragma mark MCMallCategoryTitleDelegate
- (void)clickedItemAtIndex:(NSInteger)index order:(MCArrowButtonStatus)order{
    if (index == 0) {//综合
        _order = nil;
    }else if (index == 1) {//销量
        if (order == MCArrowButtonStatusDown) {
            _order = @"12";
        }else{
            _order = @"11";
        }
    }else if (index == 2) {//人气
        if (order == MCArrowButtonStatusDown) {
            _order = @"22";
        }else{
            _order = @"21";
        }
    }else if (index == 3) {//价格
        if (order == MCArrowButtonStatusDown) {
            _order = @"32";
        }else{
            _order = @"31";
        }
    }else if (index == 4) {//最新
        _order = @"41";
    }
    _pageIndex = 1;
    [self getData];
}


#pragma mark MJRefresh

- (void)beginRefreshHeader:(MJRefreshHeader *)header{
    [header endRefreshing];
    _pageIndex = 1;
    [self getData];
}

- (void)beginRefreshFooter:(MJRefreshFooter *)footer{
    [footer endRefreshing];
    [self getData];
}

#pragma mark MCMallCategoryCellDelegate
- (void)clickedCategoryInfo:(MCMallCategoryDetailModel *)info{
    if (![MCLoginViewModel sharedInstance].isLogin) {
        [self showLoginViewController];
        return;
    }
    
    [[MCMallDetailViewModel sharedInstance] addToShoppingCard:info.id  success:^(id resquestData) {
        [MCMessageUtilities showTotast:resquestData];
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}


@end
