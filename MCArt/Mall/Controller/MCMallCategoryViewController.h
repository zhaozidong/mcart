//
//  MCMallCategoryViewController.h
//  MCArt
//
//  Created by Derek.zhao on 16/1/13.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseViewController.h"
#import "MCMallCategoryModel.h"


@interface MCMallCategoryViewController : MCBaseViewController

- (instancetype)initWithCategory:(MCMallCategoryModel *)category;


@end
