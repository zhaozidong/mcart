//
//  MCMallSearchViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/13.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMallSearchViewController.h"
#import "MCMallCategoryViewModel.h"
#import "MCMallCategoryCell.h"
#import "MCMallDetailViewController.h"
#import "MCMallShoppingCardViewController.h"
#import "MCMallDetailViewModel.h"
#import "MCLoginViewModel.h"

@interface MCMallSearchViewController ()<UISearchBarDelegate,MCMallCategoryCellDelegate>{
    MCMallCategoryModel *_category;
    
    __block NSMutableArray *_arrData;
    __block NSInteger _pageIndex;
}


@property (nonatomic, retain) UISearchBar *searchBar;


@end

@implementation MCMallSearchViewController

- (instancetype)initWithCategory:(MCMallCategoryModel *)category{
    self = [super init];
    if (self) {
        _category = category;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTitle:@"" withStyle:MCNavStyleBlack];
    
    self.navigationItem.titleView = self.searchBar;
    
    _pageIndex = 1;
    _arrData = [NSMutableArray array];
    
    [self.tableView registerClass:[MCMallCategoryCell class] forCellReuseIdentifier:@"categoryCell"];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (UISearchBar *)searchBar{
    if (!_searchBar) {
        _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth-100, 35)];
        _searchBar.delegate = self;
    }
    return _searchBar;
}


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    __weak typeof(self) weakSelf = self;
    [[MCMallCategoryViewModel sharedInstance] getCategoryDetail:_category.class_id keyWord:searchText order:nil pageIndex:1 success:^(id resquestData) {
        _arrData = resquestData;
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _arrData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MCMallCategoryDetailModel *category = [_arrData objectAtIndex:indexPath.row];
    MCMallCategoryCell *categoryCell = [tableView dequeueReusableCellWithIdentifier:@"categoryCell"];
    categoryCell.delegate = self;
    [categoryCell updateWithCategoryInfo:category];
    return categoryCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [MCMallCategoryCell cellHeight];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    MCMallCategoryDetailModel *category = [_arrData objectAtIndex:indexPath.row];
    MCMallDetailViewController *detail = [[MCMallDetailViewController alloc] initWithId:category.id seckiling:NO];
    [self.navigationController pushViewController:detail animated:YES];
}

#pragma mark MCMallCategoryCellDelegate

- (void)clickedCategoryInfo:(MCMallCategoryDetailModel *)info{
    if (![MCLoginViewModel sharedInstance].isLogin) {
        [self showLoginViewController];
        return;
    }
    
    [[MCMallDetailViewModel sharedInstance] addToShoppingCard:info.id  success:^(id resquestData) {
        [MCMessageUtilities showTotast:resquestData];
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}

@end
