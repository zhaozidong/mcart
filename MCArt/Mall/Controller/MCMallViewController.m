//
//  MCMallViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/12.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMallViewController.h"
#import "MCMallNewestViewController.h"
#import "MCMallHotestViewController.h"
#import "MCMallImmediatelyViewController.h"
#import "MCMallTitleArrowView.h"
#import "MLKMenuPopover.h"
#import "MCMallCategoryViewController.h"
#import "MCMallCategoryModel.h"
#import "MCMallViewModel.h"

@interface MCMallViewController ()<MLKMenuPopoverDelegate,MCMallTitleArrowViewDelegate>{
    MLKMenuPopover *_popOver;
    MCMallTitleArrowView *_titleView;
    __block NSArray *_arrCategoryList;
}


@end

@implementation MCMallViewController

- (instancetype)init{
    NSArray *arrControls=@[[MCMallNewestViewController class],[MCMallHotestViewController class],[MCMallImmediatelyViewController class],];
    NSArray *arrTitles=@[@"最新上线",@"热卖商品",@"限时秒杀"];
    self=[super initWithViewControllerClasses:arrControls andTheirTitles:arrTitles];
    if (self) {
        //        self.navStyle=MCNavStyleBlack;
    }
    return self;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title=@"艺术商城";
    self.navStyle=MCNavStyleBlack;
    
    _titleView = [[MCMallTitleArrowView alloc] initWithTitle:@"艺术商城"];
    _titleView.delegate = self;
    self.navigationItem.titleView = _titleView;
    
    _arrCategoryList = [NSArray array];
    [self getCategory];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//- (void)viewWillAppear:(BOOL)animated{
//    [super viewWillAppear:animated];
//    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
//    self.navigationController.navigationBar.backgroundColor=kBlackColor;
//    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
//}

//- (void)viewDidAppear:(BOOL)animated{
//    [super viewDidAppear:animated];
//    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
//    self.navigationController.navigationBar.backgroundColor=kBlackColor;
//    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
//}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark IWWebTitleArrowDelegate
- (void)clickTitle{
    if (!_popOver) {
        NSMutableArray *arrTitle=[NSMutableArray array];
        if (_arrCategoryList) {
            for (MCMallCategoryModel *category in _arrCategoryList) {
                [arrTitle addObject:category.name];
            }
        }
        _popOver=[[MLKMenuPopover alloc] initWithFrame:CGRectMake((kMainFrameWidth-115)/2, 0, 115, arrTitle.count*44) menuItems:arrTitle imgItems:nil];
        _popOver.menuPopoverDelegate=self;
    }
    [_popOver showInView:self.view];
}

#pragma mark MLKMenuPopoverDelegate
- (void)menuPopover:(MLKMenuPopover *)menuPopover didSelectMenuItemAtIndex:(NSInteger)selectedIndex{
    MCMallCategoryViewController *category = [[MCMallCategoryViewController alloc] initWithCategory:[_arrCategoryList objectAtIndex:selectedIndex]];
    [self.navigationController pushViewController:category animated:YES];
}


- (void)getCategory{
    [[MCMallViewModel sharedInstance] getCategoryList:^(id resquestData) {
        _arrCategoryList = [resquestData copy];
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}


@end
