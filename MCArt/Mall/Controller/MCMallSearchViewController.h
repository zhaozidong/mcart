//
//  MCMallSearchViewController.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/13.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseTableViewController.h"
#import "MCMallCategoryModel.h"


@interface MCMallSearchViewController : MCBaseTableViewController

- (instancetype)initWithCategory:(MCMallCategoryModel *)category;


@end
