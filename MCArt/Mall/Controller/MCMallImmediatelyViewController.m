//
//  MCMallImmediatelyViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/13.
//  Copyright © 2016年 zzd. All rights reserved.
//  限时秒杀

#import "MCMallImmediatelyViewController.h"
#import <MJRefresh.h>
#import "MCMallViewModel.h"
#import "MCMallImmediatelyCell.h"
#import "MCMallDetailViewController.h"


static NSString *const immediatelyIdentifier = @"immediatelyCell";

@interface MCMallImmediatelyViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>{
    __block NSMutableArray *_arrData;
    __block NSInteger _pageIndex;
}

@property(nonatomic, strong) UICollectionView *collectionView;

@end


//限时秒杀
@implementation MCMallImmediatelyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _arrData = [NSMutableArray array];
    _pageIndex = 1;
    
    [self.view addSubview:self.collectionView];
    
    [self getData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.backgroundColor=kBlackColor;
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)getData{
    __weak typeof(self) weakSelf = self;
    [MCMessageUtilities showProgressMessage:@""];
    [[MCMallViewModel sharedInstance] getAllGoodsByType:MCMallGoodsTypeImmediately page:_pageIndex success:^(id resquestData) {
        [MCMessageUtilities dismissHUD];
        if (_pageIndex == 1) {
            _arrData = [resquestData mutableCopy];
        }else{
            [_arrData addObjectsFromArray:resquestData];
        }
        _pageIndex++;
        [weakSelf.collectionView reloadData];
    } failure:^(NSError *error) {
        [MCMessageUtilities dismissHUD];
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}


//TODO:考虑写一个UICollectionViewController的基类

- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
        layout.minimumLineSpacing = 8.f;
        layout.minimumInteritemSpacing = 8.f;
        
        CGFloat width = (kMainFrameWidth-3*8-1)/2;
        layout.itemSize = CGSizeMake(width, width*1.33);
        layout.sectionInset = UIEdgeInsetsMake(8, 8, 8, 8);
        
        _collectionView=[[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout: layout];
        _collectionView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        
        _collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(beginRefreshHeader:)];
        _collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(beginRefreshFooter:)];
        
        
        [_collectionView registerClass:[MCMallImmediatelyCell class] forCellWithReuseIdentifier:immediatelyIdentifier];
        
        
    }
    return _collectionView;
}


#pragma mark UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return _arrData.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MCMallModel *info = [_arrData objectAtIndex:indexPath.row];
    MCMallImmediatelyCell *immediatelyCell = [collectionView dequeueReusableCellWithReuseIdentifier:immediatelyIdentifier forIndexPath:indexPath];
    [immediatelyCell updateWithInfo:info];
    return immediatelyCell;
}

#pragma mark UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    [collectionView deselectItemAtIndexPath:indexPath animated:NO];
    MCMallModel *info = [_arrData objectAtIndex:indexPath.row];
    MCMallDetailViewController *detail = [[MCMallDetailViewController alloc] initWithId:info.id seckiling:YES];
    [self.navigationController pushViewController:detail animated:YES];
}


#pragma mark MJRefresh

- (void)beginRefreshHeader:(MJRefreshHeader *)header{
    [header endRefreshing];
    _pageIndex = 1;
    [self getData];
}

- (void)beginRefreshFooter:(MJRefreshFooter *)footer{
    [footer endRefreshing];
    [self getData];
}





@end
