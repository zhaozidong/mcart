//
//  MCMallPayViewController.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/11.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseViewController.h"
#import "MCMallOrderModel.h"

@interface MCMallPayViewController : MCBaseViewController

- (instancetype)initWithOrderInfo:(MCMallOrderModel *)info;


@end
