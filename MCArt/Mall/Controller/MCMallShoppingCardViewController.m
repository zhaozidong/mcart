//
//  MCMallShoppingCardViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/28.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMallShoppingCardViewController.h"
#import "MCMallCardViewModel.h"
#import "MCMallCardCell.h"
#import "MCMallDetailCardTotalView.h"
#import "MCTicketPayViewController.h"
#import "MCMallPayViewController.h"
#import "MCMallOrderViewModel.h"
#import "MCMallOrderModel.h"
#import "MCPersonalOrderViewController.h"
#import "MCMallOrderAddressView.h"
#import "MCPersonalViewModel.h"

static NSString *const cardIdentifier = @"cardCell";


@interface MCMallShoppingCardViewController ()<MCMallCardCellDelegate,MCMallDetailTotalDelegate,UITableViewDataSource,UITableViewDelegate,MCMallOrderAddressViewDelegate>{
    __block NSMutableArray *_arrCardList;
    __block MCMallOrderModel *_orderInfo;
    __block MCPersonalExpressModel *_express;
}

@property(nonatomic, retain) MCMallDetailCardTotalView *bottomView;

@property(nonatomic, retain) UITableView *tableView;

@end

@implementation MCMallShoppingCardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTitle:@"购物车" withStyle:MCNavStyleWhite];
    
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.bottomView];
    
    [self getCardData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(presentMyOrder) name:@"kAppShowPersonalTicketNotification" object:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)presentMyOrder{
    MCPersonalOrderViewController *order = [[MCPersonalOrderViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:order];
    [self presentViewController:nav animated:YES completion:nil];
}

- (MCMallDetailCardTotalView *)bottomView{
    if (!_bottomView) {
        _bottomView = [[MCMallDetailCardTotalView alloc] initWithFrame:CGRectMake(0, kMainFrameHeight -60, kMainFrameWidth, 60)];
        _bottomView.delegate = self;
    }
    return _bottomView;
}

- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth, kMainFrameHeight-60)];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        
        _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth, CGFLOAT_MIN)];
        
        [_tableView registerClass:[MCMallCardCell class] forCellReuseIdentifier:cardIdentifier];
    }
    return _tableView;
}


- (void)getCardData{
    __weak typeof(self) weakSelf = self;
    [[MCMallCardViewModel sharedInstance] getShoppingCardList:^(id resquestData) {
        _arrCardList = resquestData;
        [weakSelf.tableView reloadData];
        [weakSelf updateTotal];
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}


- (void)deleteGoods:(MCMallCardModel *)goodsEntity{
    __weak typeof(self) weakSelf = self;
    [MCMessageUtilities showProgressMessage:@"正在删除"];
    [[MCMallCardViewModel sharedInstance] deleteGoodsFromCard:goodsEntity.gcid success:^(id resquestData) {
        [MCMessageUtilities showSuccessMessage:resquestData];
        [_arrCardList removeObject:goodsEntity];
        [weakSelf.tableView reloadData];
        [weakSelf updateTotal];
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}

#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _arrCardList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MCMallCardModel *card = [_arrCardList objectAtIndex:indexPath.row];
    MCMallCardCell *cardCell = [tableView dequeueReusableCellWithIdentifier:cardIdentifier];
    cardCell.delegate = self;
    [cardCell updateWithCardInfo:card];
    return cardCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [MCMallCardCell cellHeight];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        MCMallCardModel *card = [_arrCardList objectAtIndex:indexPath.row];
        [self deleteGoods:card];
    }
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"删除";
}

#pragma mark MCMallDetailTotalDelegate
//全选
- (void)clickedAll:(BOOL)isSelect{
    [_arrCardList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MCMallCardModel *cardEntity = (MCMallCardModel *)obj;
        if (isSelect) {
            cardEntity.selected = YES;
        }else{
            cardEntity.selected = NO;
        }
    }];
    [self.tableView reloadData];
    [self updateTotal];
}

//去付款
- (void)clickedPay{
    __block BOOL isEmpty = YES;
    [_arrCardList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MCMallCardModel *cardEntity = (MCMallCardModel *)obj;
        if (cardEntity.selected) {
            isEmpty = NO;
        }
    }];
    
    if (isEmpty) {
        [MCMessageUtilities showErrorMessage:@"你还没有选择商品，不能支付！"];
        return;
    }
    
    __weak typeof(self) weakSelf = self;
    [[MCPersonalViewModel sharedInstance] getExpressInfo:^(id resquestData) {
        _express = resquestData;
        if (!_express.response) {
            [weakSelf showAddressView:_express];
        }else{
            [MCMessageUtilities showErrorMessage:_express.info];
        }
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}


- (void)showAddressView:(MCPersonalExpressModel *)expressInfo{
    MCPersonalExpressDetailModel *detail = [expressInfo.receiver objectAtIndex:0];
    MCMallOrderAddressView *address = [[MCMallOrderAddressView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth, kMainFrameHeight)];
    address.delegate = self;
    [address updateWithExpressDetail:detail];
    [self.view addSubview:address];
}

#pragma mark MCMallOrderAddressViewDelegate
- (void)clickedConfirm:(BOOL)selfFetch expressInfo:(MCPersonalExpressDetailModel *)info{
    NSMutableString *string = [[NSMutableString alloc] initWithString:@"["];
    [_arrCardList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MCMallCardModel *cardEntity = (MCMallCardModel *)obj;
        if (cardEntity.selected) {
            [string appendString:[NSString stringWithFormat:@"{\"gid\":%@,\"gnum\":%@},",cardEntity.gid,cardEntity.gnum]];
        }
    }];
    [string appendString:@"]"];
    
    __weak typeof(self) weakSelf = self;
    [[MCMallOrderViewModel sharedInstance] buyGoods:string selfFetch:selfFetch express:info success:^(id resquestData) {
        [MCMessageUtilities showSuccessMessage:@"下单成功!"];
        _orderInfo = resquestData;
        [weakSelf payOrder];
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];
    }];
}

///订单支付
- (void)payOrder{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"请在15分钟内完成支付" delegate:self cancelButtonTitle:@"关闭" otherButtonTitles:nil, nil];
    [alert show];
    MCMallPayViewController *pay = [[MCMallPayViewController alloc] initWithOrderInfo:_orderInfo];
    [self.navigationController pushViewController:pay animated:YES];
}


#pragma mark MCMallCardCellDelegate
- (void)clickedCheck:(MCMallCardModel *)goodsInfo{
    __block BOOL isNotSelectAll;
    [_arrCardList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MCMallCardModel *cardEntity = (MCMallCardModel *)obj;
        if (!cardEntity.selected) {
            isNotSelectAll = YES;
            *stop  = YES;
        }
    }];
    if (isNotSelectAll) {
        self.bottomView.selected = NO;
    }else{
        self.bottomView.selected = YES;
    }
    
    [self updateTotal];
}

- (void)clickedNumber:(MCMallCardModel *)goodsInfo{
    [self updateTotal];
}

- (void)updateTotal{
    __block CGFloat total=0;
    [_arrCardList enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        MCMallCardModel *cardEntity = (MCMallCardModel *)obj;
        if (cardEntity.selected) {
            CGFloat price = [cardEntity.price floatValue];
            NSInteger num = [cardEntity.gnum integerValue];
            total += price*num;
        }
    }];
    [self.bottomView updateWithTotal:[NSString stringWithFormat:@"%.f",total]];
}


@end
