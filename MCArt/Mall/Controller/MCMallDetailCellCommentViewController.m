//
//  MCMallDetailCellCommentViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/27.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMallDetailCellCommentViewController.h"
#import "MCMallDetailCommentCell.h"


static NSString *const commentIdentifier = @"commentCell";

@interface MCMallDetailCellCommentViewController (){
    
}

@end

@implementation MCMallDetailCellCommentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth, CGFLOAT_MIN)];
    
    [self.tableView registerClass:[MCMallDetailCommentCell class] forCellReuseIdentifier:commentIdentifier];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (void)setComment:(NSArray *)comment{
    _comment = comment;
    [self.tableView reloadData];
}


#pragma mark UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _comment.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MCMallDetailCommentModel *comment = [_comment objectAtIndex:indexPath.row];
    MCMallDetailCommentCell *commentCell = [tableView dequeueReusableCellWithIdentifier:commentIdentifier];
    [commentCell updateWithComment:comment];
    return commentCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    MCMallDetailCommentModel *comment = [_comment objectAtIndex:indexPath.row];
    return [MCMallDetailCommentCell cellHeight:comment];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGPoint offset = scrollView.contentOffset;
    if (offset.y < -15) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kMallDetailScrollToTopNotification" object:nil];
    }
}



@end
