//
//  MCMallHotestViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/12.
//  Copyright © 2016年 zzd. All rights reserved.
//  热卖商品

#import "MCMallHotestViewController.h"
#import "MCMallCategoryCell.h"
#import "MCMallViewModel.h"
#import "MCMallDetailViewController.h"
#import "MCMallShoppingCardViewController.h"
#import "MCMallDetailViewModel.h"
#import "MCLoginViewModel.h"

static NSString *const categoryIdentifier = @"categoryCell";

@interface MCMallHotestViewController ()<UITableViewDataSource,UITableViewDelegate,MCMallCategoryCellDelegate>{
    __block NSMutableArray *_arrData;
    __block NSInteger _pageIndex;
}

@property (nonatomic, retain) UITableView *tableView;

@property (nonatomic, retain) UIButton *btnCard;


@end

@implementation MCMallHotestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _arrData = [NSMutableArray array];
    _pageIndex = 1;
    
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.btnCard];

    [self getData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.backgroundColor=kBlackColor;
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (UIButton *)btnCard{
    if (!_btnCard) {
        _btnCard = [[UIButton alloc] initWithFrame:CGRectMake(0, kMainFrameHeight - 64-30-40, 40, 40)];
        [_btnCard setImage:[UIImage imageNamed:@"jumpToCard"] forState:UIControlStateNormal];
        [_btnCard addTarget:self action:@selector(btnDidClickJumpToCard:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _btnCard;
}

- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth, kMainFrameHeight-64) style:UITableViewStyleGrouped];
        _tableView.dataSource = self;
        _tableView.delegate = self;
        
        _tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(beginRefreshHeader:)];
        _tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(beginRefreshFooter:)];
        
        _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth, CGFLOAT_MIN)];
        [_tableView registerClass:[MCMallCategoryCell class] forCellReuseIdentifier:categoryIdentifier];
    }
    return _tableView;
}

- (void)btnDidClickJumpToCard:(UIButton *)sender{
    if (![MCLoginViewModel sharedInstance].isLogin) {
        [self showLoginViewController];
        return;
    }
    
    MCMallShoppingCardViewController *card = [[MCMallShoppingCardViewController alloc] init];
    [self.navigationController pushViewController:card animated:YES];
}


- (void)getData{
    __weak typeof(self) weakSelf = self;
    [MCMessageUtilities showProgressMessage:@""];
    [[MCMallViewModel sharedInstance] getAllGoodsByType:MCMallGoodsTypeHotest page:_pageIndex success:^(id resquestData) {
        [MCMessageUtilities dismissHUD];
        if (_pageIndex == 1) {
            _arrData = [resquestData mutableCopy];
        }else{
            [_arrData addObjectsFromArray:resquestData];
        }
        _pageIndex ++;
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error) {
        [MCMessageUtilities dismissHUD];
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _arrData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MCMallModel *info = [_arrData objectAtIndex:indexPath.row];
    MCMallCategoryCell *categoryCell = [tableView dequeueReusableCellWithIdentifier:categoryIdentifier];
    categoryCell.delegate = self;
    [categoryCell updateWithMallInfo:info];
    return categoryCell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [MCMallCategoryCell cellHeight];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    MCMallModel *info = [_arrData objectAtIndex:indexPath.row];
    
    MCMallDetailViewController *detail = [[MCMallDetailViewController alloc] initWithId:info.id seckiling:NO];
    [self.navigationController pushViewController:detail animated:YES];
    
}




#pragma mark MJRefresh
- (void)beginRefreshHeader:(MJRefreshHeader *)header{
    [header endRefreshing];
    _pageIndex = 1;
    [self getData];
 
}

- (void)beginRefreshFooter:(MJRefreshFooter *)footer{
    [footer endRefreshing];
    [self getData];
}


#pragma mark MCMallCategoryCellDelegate
- (void)clickedWithMallInfo:(MCMallModel *)info{
    if (![MCLoginViewModel sharedInstance].isLogin) {
        [self showLoginViewController];
        return;
    }
    
    [[MCMallDetailViewModel sharedInstance] addToShoppingCard:info.id  success:^(id resquestData) {
        [MCMessageUtilities showTotast:resquestData];
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}

@end
