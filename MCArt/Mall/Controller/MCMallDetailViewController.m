//
//  MCMallDetailViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/27.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMallDetailViewController.h"
#import "MCMallDetailActionView.h"
#import "MCMallDetailModel.h"
#import "MCMallDetailViewModel.h"
#import "MCMallDetailPicCell.h"
#import "MCMallDetailTitleCell.h"
#import "MCMallDetailMoreCell.h"
#import "MCLoginViewModel.h"
#import "MCMallShoppingCardViewController.h"
#import "MCShareView.h"
#import "MCMallOrderViewModel.h"
#import "MCMallPayViewController.h"
#import "MCMallOrderAddressView.h"
#import "MCPersonalViewModel.h"
#import "MCShareModel.h"
#import "MCShareWebViewController.h"


static NSString *const picIdentifier = @"picCell";
static NSString *const titleIdentifier = @"titleCell";
static NSString *const basicIdentifier = @"basicCell";
static NSString *const moreIdentifier = @"moreCell";

@interface MCMallDetailViewController ()<MCMallDetailActionDelegate,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,MCMallOrderAddressViewDelegate>{
    NSString *_goodsId;
    __block MCMallDetailModel *_detailInfo;
    MCShareView *_shareView;
    BOOL _isSeckiling;
    
    __block MCMallOrderModel *_orderInfo;
}

@property(nonatomic, strong) UITableView *tableView;
@property(nonatomic, strong) MCMallDetailActionView *actionView;
@property(nonatomic, strong) UIButton *backButton;
@property(nonatomic, strong) UIButton *cardButton;
@property(nonatomic, strong) UIButton *shareButton;

@end

@implementation MCMallDetailViewController

- (instancetype)initWithId:(NSString *)index seckiling:(BOOL)isSeckiling {
    self = [super init];
    if (self) {
        _goodsId = index;
        _isSeckiling = isSeckiling;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    
    [self.view addSubview:self.tableView];
    [self.view addSubview:self.actionView];
    
    [self.view addSubview:self.backButton];
    [self.view addSubview:self.cardButton];
    [self.view addSubview:self.shareButton];
    
    [self getDetailData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(scrollTopTop) name:@"kMallDetailScrollToTopNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(getDetailData) name:@"kMallDetailRefreshDataNotification" object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
//    self.navigationController.navigationBar.shadowImage = [UIImage new];
//    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
//    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    
    self.navigationController.navigationBarHidden = YES;
}


- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
//    [self.navigationController.navigationBar setBackgroundImage:nil forBarMetrics:UIBarMetricsDefault];
//    if (IOS7) {
//        self.navigationController.navigationBar.backIndicatorImage = nil;
//    }
//    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    
    self.navigationController.navigationBarHidden = NO;
}


- (UIButton *)backButton{
    if (!_backButton) {
        _backButton = [[UIButton alloc] initWithFrame:CGRectMake(8, 22, 50, 50)];
        [_backButton setImage:[UIImage imageNamed:@"titleBack"] forState: UIControlStateNormal];
        [_backButton addTarget:self action:@selector(btnDidClickBack:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _backButton;
}

- (UIButton *)cardButton{
    if (!_cardButton) {
        _cardButton = [[UIButton alloc] initWithFrame:CGRectMake(kMainFrameWidth - 100, 22, 50, 50)];
        [_cardButton setImage:[UIImage imageNamed:@"titleCard"] forState:UIControlStateNormal];
        [_cardButton addTarget:self action:@selector(btnDidClickCard:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cardButton;
}

- (UIButton *)shareButton{
    if (!_shareButton) {
        _shareButton = [[UIButton alloc] initWithFrame:CGRectMake(kMainFrameWidth - 50, 22, 50, 50)];
        [_shareButton setImage:[UIImage imageNamed:@"titleShare"] forState:UIControlStateNormal];
        [_shareButton addTarget:self action:@selector(btnDidClickShare:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _shareButton;
}

- (void)btnDidClickBack:(UIButton *)sender{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)btnDidClickCard:(UIButton *)sender{
    if (![MCLoginViewModel sharedInstance].isLogin) {
        [self showLoginViewController];
        return;
    }
    
    MCMallShoppingCardViewController *shoppingCard = [[MCMallShoppingCardViewController alloc] init];
    [self.navigationController pushViewController:shoppingCard animated:YES];
}

- (void)btnDidClickShare:(UIButton *)sender{
    if (_detailInfo.cover && _detailInfo.cover.count > 0) {
        UIWindow *window = [UIApplication sharedApplication].keyWindow;
        _shareView = [[MCShareView alloc] initWithFrame:self.view.bounds];
        MCMallDetailCoverModel *cover = [_detailInfo.cover objectAtIndex:0];
        [_shareView updateWithTitle:_detailInfo.title image:cover.pic_url url:_detailInfo.share_url content:_detailInfo.share_content];
        [window addSubview:_shareView];
    }
}

- (UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 20, kMainFrameWidth, kMainFrameHeight -50-20) style:UITableViewStyleGrouped];
        _tableView.delegate = self;
        _tableView.dataSource = self;

        _tableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth, CGFLOAT_MIN)];
        
        [_tableView registerClass:[MCMallDetailPicCell class] forCellReuseIdentifier:picIdentifier];
        [_tableView registerClass:[MCMallDetailTitleCell class] forCellReuseIdentifier:titleIdentifier];
        [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:basicIdentifier];
        [_tableView registerClass:[MCMallDetailMoreCell class] forCellReuseIdentifier:moreIdentifier];
    }
    return _tableView;
}

- (MCMallDetailActionView *)actionView{
    if (!_actionView) {
        _actionView = [[MCMallDetailActionView alloc] initWithFrame:CGRectMake(0, kMainFrameHeight - 50, kMainFrameWidth, 50) seckiling:_isSeckiling];
        _actionView.delegate = self;
    }
    return _actionView;
}

- (void)getDetailData{
    __weak typeof(self) weakSelf = self;
    [[MCMallDetailViewModel sharedInstance] getGoodsDetailById:_goodsId success:^(id resquestData) {
        _detailInfo = resquestData;
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}

#pragma mark UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 2;
    }else if (section == 1) {
        return 1;
    }else if (section == 2) {
        return 1;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            MCMallDetailPicCell *picCell = [tableView dequeueReusableCellWithIdentifier:picIdentifier];
            [picCell updateWithDetailInfo:_detailInfo];
            return picCell;
        }else if (indexPath.row == 1){
            MCMallDetailTitleCell *titleCell = [tableView dequeueReusableCellWithIdentifier:titleIdentifier];
            [titleCell updateWithDetailInfo:_detailInfo];
            return titleCell;
        }
    }else if (indexPath.section == 1){
        UITableViewCell *basicCell = [tableView dequeueReusableCellWithIdentifier:basicIdentifier];
        basicCell.textLabel.text = @"查看评价";
        basicCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return basicCell;
    }else if (indexPath.section == 2) {
        MCMallDetailMoreCell *moreCell = [tableView dequeueReusableCellWithIdentifier:moreIdentifier];
        [moreCell updateWithDetailInfo:_detailInfo];
        return moreCell;
    }
    return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            return [MCMallDetailPicCell cellHeight];
        }else if (indexPath.row == 1) {
            return [MCMallDetailTitleCell cellHeight];
        }
    }else if (indexPath.section == 2){
        return [MCMallDetailMoreCell cellHeight];
    }
    return 44.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 10.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (indexPath.section == 1) {
        MCMallDetailMoreCell *moreCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
        if (moreCell) {
            moreCell.selectIndex = 2;
            [UIView animateWithDuration:0.5 animations:^{
                self.tableView.contentOffset = CGPointMake(0, kMainFrameWidth*0.875 + 100);
            }];
        }
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
//    CGRect rect = [self.tableView rectForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
//    
//    CGFloat height = kMainFrameWidth*0.875 + 400;
    CGPoint offset = scrollView.contentOffset;
//    if (offset.y > height) {
//        scrollView.contentOffset = CGPointMake(0, height);
//    }
    

    
    if (offset.y > 350) {
        [UIView animateWithDuration:0.5 animations:^{
            scrollView.contentOffset = CGPointMake(0, 380);
//            self.tableView.contentOffset = CGPointMake(0, rect.origin.y);
        }];
    }
}

- (void)scrollTopTop{
    [UIView animateWithDuration:0.5 animations:^{
        self.tableView.contentOffset = CGPointMake(0, 0);
    }];
}


#pragma mark MCMallDetailActionDelegate
- (void)clickedChat{
    NSString *string = [NSString stringWithFormat:@"telprompt://%@",_detailInfo.consultation_phone];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:string]];
}

- (void)clickedCard{
    if (![MCLoginViewModel sharedInstance].isLogin) {
        [self showLoginViewController];
        return;
    }
    
    if (_isSeckiling) {
        __weak typeof(self) weakSelf = self;
        [[MCPersonalViewModel sharedInstance] getExpressInfo:^(id resquestData) {
            MCPersonalExpressModel *express = resquestData;
            if (!express.response) {
                [weakSelf showAddressView:express];
            }else{
                [MCMessageUtilities showErrorMessage:express.info];
            }
        } failure:^(NSError *error) {
            [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
        }];
    }else{
        __weak typeof(self) weakSelf = self;
        [[MCMallDetailViewModel sharedInstance] addToShoppingCard:_goodsId success:^(id resquestData) {
            [MCMessageUtilities showSuccessMessage:resquestData];
            [weakSelf gotoCard];
        } failure:^(NSError *error) {
            [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
        }];
    }
}

- (void)showAddressView:(MCPersonalExpressModel *)expressInfo{
    MCPersonalExpressDetailModel *detail = [expressInfo.receiver objectAtIndex:0];
    MCMallOrderAddressView *address = [[MCMallOrderAddressView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth, kMainFrameHeight)];
    address.delegate = self;
    [address updateWithExpressDetail:detail];
    [self.view addSubview:address];
}


#pragma mark MCMallOrderAddressViewDelegate
- (void)clickedConfirm:(BOOL)selfFetch expressInfo:(MCPersonalExpressDetailModel *)info{
    NSString *string = [NSString stringWithFormat:@"[{\"gid\":%@,\"gnum\":%@}]",_detailInfo.id,@"1"];
    
    __weak typeof(self) weakSelf = self;
    [[MCMallOrderViewModel sharedInstance] buyGoods:string selfFetch:selfFetch express:info success:^(id resquestData) {
        [MCMessageUtilities showSuccessMessage:@"下单成功!"];
        _orderInfo = resquestData;
        [weakSelf payOrder];
    } failure:^(NSError *error) {
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];
    }];
}

///跳转购物车
- (void)gotoCard{
    MCMallShoppingCardViewController *shoppingCard = [[MCMallShoppingCardViewController alloc] init];
    [self.navigationController pushViewController:shoppingCard animated:YES];
}


///订单支付
- (void)payOrder{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"请在15分钟内完成支付" delegate:self cancelButtonTitle:@"关闭" otherButtonTitles:nil, nil];
    [alert show];
    MCMallPayViewController *pay = [[MCMallPayViewController alloc] initWithOrderInfo:_orderInfo];
    [self.navigationController pushViewController:pay animated:YES];
}

@end
