//
//  MCMallDetailCellParamViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/27.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMallDetailCellParamViewController.h"
#import "MCMallDetailParamCell.h"
#import "MCMallDetailModel.h"

static NSString *const paramIdentifier = @"paramCell";


@interface MCMallDetailCellParamViewController ()

@end

@implementation MCMallDetailCellParamViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [self.tableView registerClass:[MCMallDetailParamCell class] forCellReuseIdentifier:paramIdentifier];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (void)setParam:(NSArray *)param{
    _param = param;
    [self.tableView reloadData];
}


#pragma mark UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _param.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MCMallDetailParamModel *param = [_param objectAtIndex:indexPath.row];
    MCMallDetailParamCell *paramCell = [tableView dequeueReusableCellWithIdentifier:paramIdentifier];
    [paramCell updateWithParam:param];
    return paramCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    MCMallDetailParamModel *param = [_param objectAtIndex:indexPath.row];
    return [MCMallDetailParamCell cellHeightWithParam:param];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGPoint offset = scrollView.contentOffset;
    if (offset.y < -15) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kMallDetailScrollToTopNotification" object:nil];
    }
}


@end
