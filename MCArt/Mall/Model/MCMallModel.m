//
//  MCMallModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/24.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMallModel.h"

@implementation MCMallModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"id":@"id",
             @"title":@"title",
             @"price":@"price",
             @"discount":@"discount",
             @"type":@"type",
             @"pic_A":@"pic_A",
             @"pic_B":@"pic_B",
             @"content":@"content",
             @"view_num":@"view_num",
             @"start_time":@"start_time",
             @"end_time":@"end_time",
             @"sell_num":@"sell_num"
             };
}


@end
