//
//  MCMallCardModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/28.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMallCardModel.h"

@implementation MCMallCardModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"gcid":@"gcid",
             @"gnum":@"gnum",
             @"gid":@"gid",
             @"gname":@"gname",
             @"price":@"price",
             @"pic_url":@"pic_url"};
}

@end
