//
//  MCMallCategoryModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/21.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMallCategoryModel.h"

@implementation MCMallCategoryModel

+(NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"class_id":@"class_id",
             @"name":@"name"
             };
}


@end



@implementation MCMallCategoryDetailModel

+(NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"id":@"id",
             @"title":@"title",
             @"price":@"price",
             @"discount":@"discount",
             @"class_id":@"class_id",
             @"pic_A":@"pic_A",
             @"pic_B":@"pic_B",
             @"content":@"content",
             @"view_num":@"view_num",
             @"sell_num":@"sell_num"
             };
}

@end