//
//  MCMallCategoryModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/21.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface MCMallCategoryModel : MTLModel<MTLJSONSerializing>

@property(nonatomic, strong) NSString *class_id;

@property(nonatomic, strong) NSString *name;

@end





@interface MCMallCategoryDetailModel : MTLModel<MTLJSONSerializing>

@property(nonatomic, strong) NSString *id;

@property(nonatomic, strong) NSString *title;

@property(nonatomic, strong) NSString *price;

@property(nonatomic, strong) NSString *discount;

@property(nonatomic, strong) NSString *class_id;

@property(nonatomic, strong) NSString *pic_A;

@property(nonatomic, strong) NSString *pic_B;

@property(nonatomic, strong) NSString *content;

@property(nonatomic, strong) NSString *view_num;

@property(nonatomic, strong) NSString *sell_num;

@end

