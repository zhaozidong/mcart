//
//  MCMallDetailModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/27.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMallDetailModel.h"

@implementation MCMallDetailCoverModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"pic_url":@"pic_url"};
}

@end


@implementation MCMallDetailParamModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"key":@"key",
             @"value":@"value"
             };
}

@end


@implementation MCMallDetailCommentModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"id":@"id",
             @"head_img":@"head_img",
             @"account":@"account",
             @"content":@"content",
             @"time":@"time"};
}

@end


@implementation MCMallDetailModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"id":@"id",
             @"title":@"title",
             @"price":@"price",
             @"discount":@"discount",
             @"class_id":@"class_id",
             @"cover":@"cover",
             @"content":@"content",
             @"detail":@"detail",
             @"express":@"express",
             @"sendfrom":@"sendfrom",
             @"view_num":@"view_num",
             @"start_time":@"start_time",
             @"end_time":@"end_time",
             @"sell_num":@"sell_num",
             @"consultation_phone":@"consultation_phone",
             @"param":@"param",
             @"comment":@"comment",
             @"share_url":@"share_url",
             @"share_content":@"share_content",
             @"descr_url":@"descr_url"
             };
}

+ (NSValueTransformer *)coverJSONTransformer{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        NSArray *jsonArray = value;
        NSMutableArray *listArray = [NSMutableArray array];
        [jsonArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            MCMallDetailCoverModel *cover = [MTLJSONAdapter modelOfClass:[MCMallDetailCoverModel class]fromJSONDictionary:obj error:error];
            [listArray addObject:cover];
        }];
        return listArray;
    }];
}

+ (NSValueTransformer *)paramJSONTransformer{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        NSArray *jsonArray = value;
        NSMutableArray *paramArray = [NSMutableArray array];
        [jsonArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            MCMallDetailParamModel *param = [MTLJSONAdapter modelOfClass:[MCMallDetailParamModel class] fromJSONDictionary:obj error:error];
            [paramArray addObject:param];
        }];
        return paramArray;
    }];
}

+ (NSValueTransformer *)commentJSONTransformer{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        NSArray *jsonArray = value;
        NSMutableArray *commentArray = [NSMutableArray array];
        [jsonArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            MCMallDetailCommentModel *comment = [MTLJSONAdapter modelOfClass:[MCMallDetailCommentModel class] fromJSONDictionary:obj error:error];
            [commentArray addObject:comment];
        }];
        return commentArray;
    }];
}


@end
