//
//  MCMallDetailModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/27.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface MCMallDetailCoverModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, copy) NSString *pic_url;

@end



@interface MCMallDetailParamModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, copy) NSString *key;

@property (nonatomic, copy) NSString *value;

@end


@interface MCMallDetailCommentModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, copy) NSString *id;

@property (nonatomic, copy) NSString *head_img;

@property (nonatomic, copy) NSString *account;

@property (nonatomic, copy) NSString *content;

@property (nonatomic, copy) NSString *time;

@end



@interface MCMallDetailModel : MTLModel<MTLJSONSerializing>


@property (nonatomic, copy) NSString *id;


@property (nonatomic, copy) NSString *title;


@property (nonatomic, copy) NSString *price;


@property (nonatomic, copy) NSString *discount;

///类型id
@property (nonatomic, copy) NSString *class_id;

@property (nonatomic, copy) NSArray *cover;

///商品简介
@property (nonatomic, copy) NSString *content;

///商品详情
@property (nonatomic, copy) NSString *detail;

///免运费
@property (nonatomic, copy) NSString *express;

///发货地
@property (nonatomic, copy) NSString *sendfrom;

///浏览量
@property (nonatomic, copy) NSString *view_num;

///秒杀开始时间
@property (nonatomic, copy) NSString *start_time;

///秒杀结束时间
@property (nonatomic, copy) NSString *end_time;

///售出数量
@property (nonatomic, copy) NSString *sell_num;

///咨询电话
@property (nonatomic, copy) NSString *consultation_phone;

///产品参数
@property (nonatomic, copy) NSArray *param;

///买家评论
@property (nonatomic, copy) NSArray *comment;

///分享URL
@property (nonatomic, copy) NSString *share_url;

///分享内容
@property (nonatomic, copy) NSString *share_content;

///商品详情url
@property (nonatomic, copy) NSString *descr_url;


@end
