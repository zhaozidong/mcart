//
//  MCMallCardModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/28.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface MCMallCardModel : MTLModel<MTLJSONSerializing>

///购物车商品关联ID
@property (nonatomic, copy) NSString *gcid;

///商品数量
@property (nonatomic, copy) NSString *gnum;

///商品ID
@property (nonatomic, copy) NSString *gid;

///商品名称
@property (nonatomic, copy) NSString *gname;

///商品单价
@property (nonatomic, copy) NSString *price;

///商品图片
@property (nonatomic, copy) NSString *pic_url;

///在购物车中是否选中
@property (nonatomic, assign) BOOL selected;

@end
