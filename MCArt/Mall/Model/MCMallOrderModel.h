//
//  MCMallOrderModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/11.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Mantle/Mantle.h>





@interface MCMallOrderDetailModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, copy) NSString *gid;

@property (nonatomic, copy) NSString *gname;

@property (nonatomic, copy) NSString *gnum;

@property (nonatomic, copy) NSString *unitprice;

@end




@interface MCMallOrderModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, copy) NSString *goid;

@property (nonatomic, copy) NSString *orderno;

@property (nonatomic, copy) NSString *ordername;

@property (nonatomic, copy) NSString *ali_order_info;

@property (nonatomic, copy) NSString *result;

@property (nonatomic, copy) NSArray *orderInfo;

@property (nonatomic, copy) NSString *resttime;

@property (nonatomic, copy) NSString *total_price;

@property (nonatomic, copy) NSString *timestamp;

@end
