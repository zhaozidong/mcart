//
//  MCMallOrderModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/11.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMallOrderModel.h"



@implementation MCMallOrderDetailModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"gid":@"gid",
             @"gname":@"gname",
             @"gnum":@"gnum",
             @"unitprice":@"unitprice"};
}

@end



@implementation MCMallOrderModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey{
    return @{@"goid":@"goid",
             @"orderno":@"orderno",
             @"ordername":@"ordername",
             @"ali_order_info":@"ali_order_info",
             @"result":@"result",
             @"orderInfo":@"orderInfo",
             @"resttime":@"resttime",
             @"total_price":@"total_price",
             @"timestamp":@"timestamp"};
}

+ (NSValueTransformer *)orderInfoJSONTransformer{
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        NSArray *jsonArray = value;
        NSMutableArray *array = [NSMutableArray array];
        [jsonArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            MCMallOrderDetailModel *detail = [MTLJSONAdapter modelOfClass:[MCMallOrderDetailModel class] fromJSONDictionary:obj error:error];
            [array addObject:detail];
        }];
        return array;
    }];
}

@end
