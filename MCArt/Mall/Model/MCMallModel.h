//
//  MCMallModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/24.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface MCMallModel : MTLModel<MTLJSONSerializing>

@property (nonatomic,copy) NSString *id;

@property (nonatomic,copy) NSString *title;

@property (nonatomic,copy) NSString *price;

@property (nonatomic,copy) NSString *discount;

@property (nonatomic,copy) NSString *type;

@property (nonatomic,copy) NSString *pic_A;

@property (nonatomic,copy) NSString *pic_B;

@property (nonatomic,copy) NSString *content;

@property (nonatomic,copy) NSString *view_num;

@property (nonatomic,copy) NSString *start_time;

@property (nonatomic,copy) NSString *end_time;

@property (nonatomic,copy) NSString *sell_num;

@end
