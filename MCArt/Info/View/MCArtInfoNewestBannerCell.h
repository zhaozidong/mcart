//
//  MCArtInfoNewestBannerCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/1/6.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCArtInfoNewestModel.h"


@protocol MCArtInfoNewestBannerCellDelegate <NSObject>

- (void)clickedImageAtIndex:(NSInteger)index;

@end


@interface MCArtInfoNewestBannerCell : UITableViewCell

@property (nonatomic, weak) id<MCArtInfoNewestBannerCellDelegate> delegate;

- (void)updateWithInfo:(NSArray *)info;

+ (CGFloat)cellHeight;

@end
