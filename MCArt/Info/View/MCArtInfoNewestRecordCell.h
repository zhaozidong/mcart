//
//  MCArtInfoNewestRecordCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/1/7.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseTableViewCell.h"
#import "MCArtInfoNewestModel.h"


@interface MCArtInfoNewestRecordCell : MCBaseTableViewCell

- (void)updateWithInfo:(MCArtInfoNewestModel *)info;


+ (CGFloat)cellHeight;

@end
