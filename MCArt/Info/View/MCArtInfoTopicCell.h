//
//  MCArtInfoTopicCell.h
//  MCArt
//
//  Created by Derek.zhao on 16/1/5.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseTableViewCell.h"
#import "MCArtInfoTopicModel.h"

@interface MCArtInfoTopicCell : MCBaseTableViewCell


- (void)updateWithInfo:(MCArtInfoTopicModel *)info;


@end
