//
//  MCArtInfoNewSummaryCell.m
//  MCArt
//
//  Created by Derek.zhao on 15/12/28.
//  Copyright © 2015年 zzd. All rights reserved.
//

#import "MCArtInfoNewSummaryCell.h"

@implementation MCArtInfoNewSummaryCell{
    UIImageView *_imgView;
    UIImageView *_imgViewArea;
    UILabel *_lblTitle;
    UILabel *_lblSubTitle;
    UILabel *_lblTime;
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}

- (void)setUpSubViews{
    
    if (!_imgView) {
        _imgView=[[UIImageView alloc] initWithFrame:CGRectMake(8, 8, 110, 80)];
        
    }
    [self.contentView addSubview:_imgView];
    
    if (!_imgViewArea) {
        _imgViewArea=[[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_imgView.frame)+10, 10, 20, 12)];
        _imgViewArea.image=[UIImage imageNamed:@"areaLocal"];
    }
    [self.contentView addSubview:_imgViewArea];
    
    
    
    if (!_lblTitle) {
        _lblTitle=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_imgViewArea.frame)+6, 10, CGRectGetWidth(self.bounds)-CGRectGetMaxX(_imgViewArea.frame)-20, 30)];
        
        
    }
    
    [self.contentView addSubview:_lblTitle];
    
    
    if (_lblSubTitle) {
        _lblSubTitle=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(_imgViewArea.frame), CGRectGetMaxY(_lblTitle.frame), CGRectGetWidth(self.bounds)-CGRectGetWidth(_imgView.frame)-20, 30)];
    }
    [self.contentView addSubview:_lblSubTitle];
    
}

- (void)updateCell{
    
    
    
    
    
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    
    
}


@end
