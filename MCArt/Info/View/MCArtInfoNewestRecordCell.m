//
//  MCArtInfoNewestRecordCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/7.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCArtInfoNewestRecordCell.h"
#import <UIImageView+WebCache.h>

@implementation MCArtInfoNewestRecordCell{
    UIImageView *_imgViewTitle;
    UIImageView *_imgViewType;
    UILabel *_lblTitle;
    UILabel *_lblSubTitle;
    UILabel *_lblTime;
    UIImageView *_imgVideo;
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (void)setUpSubViews{
    if (!_imgViewTitle) {
        _imgViewTitle=[[UIImageView alloc] initWithFrame:CGRectMake(8, 8, 80/3*4, 80)];
        _imgViewTitle.contentMode = UIViewContentModeScaleAspectFill;
        _imgViewTitle.clipsToBounds = YES;
    }
    [self.contentView addSubview:_imgViewTitle];
    
    if (!_imgViewType) {
        _imgViewType=[[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_imgViewTitle.frame)+8, 8, 28, 16)];
    }
    [self.contentView addSubview:_imgViewType];
    
    if (!_lblTitle) {
        _lblTitle=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_imgViewType.frame)+8, 8, 100, 20)];
    }
    [self.contentView addSubview:_lblTitle];
    
    if (!_lblSubTitle) {
        _lblSubTitle=[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_imgViewTitle.frame)+8, 50, 200, 20)];
        _lblSubTitle.font=kAppFont(12.f);
        _lblSubTitle.textColor=[UIColor lightGrayColor];
    }
    [self.contentView addSubview:_lblSubTitle];
    
    
    if (!_lblTime) {
        _lblTime = [[UILabel alloc] initWithFrame:CGRectMake(120, 70, 100, 20)];
        _lblTime.textAlignment=NSTextAlignmentRight;
        _lblTime.font=kAppFont(12.f);
        _lblTime.textColor=[UIColor lightGrayColor];
    }
    [self.contentView addSubview:_lblTime];
    
    
    if (!_imgVideo) {
        _imgVideo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"video"]];
    }
    [_imgViewTitle addSubview:_imgVideo];
}

- (void)updateWithInfo:(MCArtInfoNewestModel *)info{
    if (!info) {
        return;
    }
    
    if (info.pic_path) {
        [_imgViewTitle sd_setImageWithURL:[NSURL URLWithString:kImagePath(info.pic_path)]];
    }
    
    if (info.video) {
        _imgVideo.hidden = NO;
    }else{
        _imgVideo.hidden = YES;
    }
    
    if (info.type) {
        switch (info.type) {
            case MCArtInfoNewestTypeLocal:
            {
                _imgViewType.image=[UIImage imageNamed:@"areaLocal"];
            }
                break;
            case MCArtInfoNewestTypeInternal:
            {
                _imgViewType.image=[UIImage imageNamed:@"areaInternal"];
            }
                break;
            case MCArtInfoNewestTypeForeign:
            {
                _imgViewType.image=[UIImage imageNamed:@"areaForeign"];
            }
                break;
            default:
            {
                _imgViewType.image = nil;
            }
                break;
        }
    }
    
    if (info.title) {
        _lblTitle.text = info.title;
        [_lblTitle sizeToFit];
    }
    
    if (info.content) {
        _lblSubTitle.text = info.content;
        [_lblSubTitle sizeToFit];
    }
    
    if (info.time) {
        _lblTime.text = info.time;
        [_lblTime sizeToFit];
    }
}

- (void)layoutSubviews{
    [super layoutSubviews];
    
    
    CGRect frame=_imgViewTitle.frame;
    CGFloat width = 80/3*4;
    CGFloat height = 80;
    _imgViewTitle.frame=CGRectMake(8, 8, width, height);
    _imgVideo.frame = CGRectMake((width-70)/2, (height-70)/2, 70, 70);
//    _imgVideo.center = _imgViewTitle.center;
    
    frame=_imgViewType.frame;
    _imgViewType.frame=CGRectMake(CGRectGetMaxX(_imgViewTitle.frame)+8, 15, 28, 16);
    
    frame=_lblTitle.frame;
    _lblTitle.frame=CGRectMake(CGRectGetMaxX(_imgViewType.frame) + 8, 13, CGRectGetWidth(self.bounds) - CGRectGetMaxX(_imgViewType.frame) - 2*8, frame.size.height);
    
    frame = _lblSubTitle.frame;
    _lblSubTitle.frame=CGRectMake(CGRectGetMinX(_imgViewType.frame), CGRectGetMaxY(_lblTitle.frame) + 10, CGRectGetWidth(self.bounds) - CGRectGetWidth(_imgViewTitle.frame)-2*8, frame.size.height);
    
    frame = _lblTime.frame;
    _lblTime.frame=CGRectMake(CGRectGetWidth(self.bounds) - 8 -frame.size.width, CGRectGetHeight(self.bounds) - 8 - frame.size.height, frame.size.width, frame.size.height);
    
}

+ (CGFloat)cellHeight{
    return 80+2*8;
}

@end
