//
//  MCArtInfoBannerTableViewCell.m
//  MCArt
//
//  Created by Derek.zhao on 15/12/28.
//  Copyright © 2015年 zzd. All rights reserved.
//

#import "MCArtInfoBannerTableViewCell.h"

@implementation MCArtInfoBannerTableViewCell{
    UIImageView *_imgViewBanner;
    UILabel *_lblTitle;
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setUpSubViews];
    }
    return self;
}


- (void)setUpSubViews{
    if (!_imgViewBanner) {
        _imgViewBanner=[[UIImageView alloc] initWithFrame:self.contentView.bounds];
        _imgViewBanner.image=[UIImage imageNamed:@"new@2x.jpg"];
    }
    [self.contentView addSubview:_imgViewBanner];
    
    if (!_lblTitle) {
        _lblTitle=[[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.contentView.bounds)-30, CGRectGetWidth(self.contentView.bounds), 30)];
        _lblTitle.textAlignment=NSTextAlignmentCenter;
        _lblTitle.textColor=kWhiteColor;
        _lblTitle.backgroundColor=kRGBA(0, 0, 0, 0.54f);
        _lblTitle.text=@"《意外之旅》压轴登场";;
    }
    [self.contentView addSubview:_lblTitle];
}


- (void)layoutSubviews{
    [super layoutSubviews];
    _imgViewBanner.frame=self.contentView.bounds;
    _lblTitle.frame=CGRectMake(0, CGRectGetHeight(self.contentView.bounds)-30, CGRectGetWidth(self.contentView.bounds), 30);
}


@end
