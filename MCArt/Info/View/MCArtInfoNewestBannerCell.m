//
//  MCArtInfoNewestBannerCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/6.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCArtInfoNewestBannerCell.h"
#import "SDAdScrollView.h"


@interface MCArtInfoNewestBannerCell (){
    UILabel *_lblTitle;
    NSMutableArray *_arrTitle;
    SDAdScrollView *_scroll;
}

@end

@implementation MCArtInfoNewestBannerCell

- (void)awakeFromNib {
    // Initialization code
    
    _scroll = [[SDAdScrollView alloc] initWithFrame:CGRectMake(0, 0, kMainFrameWidth, kMainFrameWidth*0.476)];
    [self.contentView addSubview:_scroll];
    
    
    _lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.bounds)-30, kMainFrameWidth, 30)];
    _lblTitle.backgroundColor = kRGBA(0, 0, 0, 0.54);
    _lblTitle.textAlignment = NSTextAlignmentCenter;
    _lblTitle.textColor = kWhiteColor;

    [self.contentView addSubview:_lblTitle];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateWithInfo:(NSArray *)bannerList{
    if (!bannerList) {
        return;
    }
    
//    if (info.pic_path) {
//        NSString *strPath = [NSString stringWithFormat:@"%@%@",kAppDomain,info.pic_path];
//        [self.imgView sd_setImageWithURL:[NSURL URLWithString:strPath]];
//    }
//    
//    if (info.title) {
//        self.lblTitle.text = info.title;
//    }
    
    _arrTitle = [NSMutableArray array];
    NSMutableArray *arrPath = [NSMutableArray array];
    for (MCArtInfoNewestModel *info in bannerList) {
        if (info.pic_path) {
            [arrPath addObject:kImagePath(info.pic_path)];
        }else{
            [arrPath addObject:@"info_loading_banner"];
        }
        if (info.title) {
            [_arrTitle addObject:info.title];
        }
    }

    _scroll.adList = arrPath;
    __weak typeof(self) weakSelf = self;
    _scroll.scrollBlock = ^(NSInteger index) {
        [weakSelf scrollToIndex:index];
    };
    _scroll.tapActionBlock=^(SDAdScrollView *adScrollView){
        [weakSelf clickImgAtIndex:adScrollView.currentPage];
    };
    
    _lblTitle.text = [_arrTitle objectAtIndex:0];
}


+ (CGFloat)cellHeight{
    return kMainFrameWidth*0.476;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    _scroll.frame = CGRectMake(0, 0, kMainFrameWidth, kMainFrameWidth*0.476);
    _lblTitle.frame = CGRectMake(0, CGRectGetHeight(self.bounds)-30, kMainFrameWidth, 30);
}

- (void)scrollToIndex:(NSInteger)index {
    _lblTitle.text = [_arrTitle objectAtIndex:index];
}

- (void)clickImgAtIndex:(NSInteger)index{
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickedImageAtIndex:)]) {
        [self.delegate clickedImageAtIndex:index];
    }
}

@end
