//
//  MCArtInfoTopicCell.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/5.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCArtInfoTopicCell.h"
#import <UIImageView+WebCache.h>


@implementation MCArtInfoTopicCell{
    UIImageView *_imgView;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (void)setUpSubViews{
    [super setUpSubViews];
    if (!_imgView) {
        _imgView=[[UIImageView alloc] initWithFrame:CGRectInset(self.bounds, 8, 4)];
    }
    [self.contentView addSubview:_imgView];
}

- (void)updateWithInfo:(MCArtInfoTopicModel *)info{
    
    if (!info) {
        return;
    }
    
    if (info.pic) {
        [_imgView sd_setImageWithURL:[NSURL URLWithString:kImagePath(info.pic)]];
    }else{
        _imgView.image = [UIImage imageNamed:@"info_loading_topic"];
    }
}

- (void)layoutSubviews{
    [super layoutSubviews];
    _imgView.frame=CGRectInset(self.bounds, 8, 4);
}


@end
