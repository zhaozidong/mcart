//
//  MCArtInfoNewestModel.m
//  MCArt
//
//  Created by Derek.zhao on 15/12/30.
//  Copyright © 2015年 zzd. All rights reserved.
//

#import "MCArtInfoNewestModel.h"

@implementation MCArtInfoNewestImageModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"src":@"src",
             @"descr":@"descr"};
}

@end




@implementation MCArtInfoNewestVideoModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"src":@"src",
             @"type":@"type"};
}

@end




@implementation MCArtInfoNewestModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"id" : @"id",
             @"title" : @"title",
             @"time" : @"time",
             @"type" : @"type",
             @"class_type" : @"class_type",
             @"content" : @"content",
             @"pic_path" : @"pic_path",
             @"imgs_list" : @"imgs_list",
             @"video" : @"video",
             @"share_url" : @"share_url",
             @"share_content" : @"share_content"
             };
}

+ (NSValueTransformer *)typeJSONTransformer {
    return [NSValueTransformer mtl_valueMappingTransformerWithDictionary:@{
                                                                           @"1": @(MCArtInfoNewestTypeLocal),
                                                                           @"2": @(MCArtInfoNewestTypeInternal),
                                                                           @"3": @(MCArtInfoNewestTypeForeign),
                                                                           @"4": @(MCArtInfoNewestTypeTopic)
                                                                           }];
}

+ (NSValueTransformer *)class_typeJSONTransformer {
    return [NSValueTransformer mtl_valueMappingTransformerWithDictionary:@{
                                                                           @"1": @(MCArtInfoShowTypeText),
                                                                           @"2": @(MCArtInfoShowTypePic),
                                                                           @"3": @(MCArtInfoShowTypeVideo)
                                                                           }];
}


+ (NSValueTransformer *)imgs_listJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        NSArray *jsonArray = value;
        NSMutableArray *array = [NSMutableArray array];
        [jsonArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            MCArtInfoNewestImageModel *image = [MTLJSONAdapter modelOfClass:[MCArtInfoNewestImageModel class] fromJSONDictionary:obj error:error];
            [array addObject:image];
//            if (!error) {
//                [array addObject:image];
//            }
//            else {
//                NSLog(@"error = %@",*error);
//            }
        }];
        return array;
    }];
}

@end


//@implementation MCArtInfoNewestAllEntity
//
//+ (NSDictionary *)JSONKeyPathsByPropertyKey {
//    
//    
//    
//}
//
//@end

@implementation MCArtInfoModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"banner":@"banner",
             @"list":@"list"};
}

+ (NSValueTransformer *)bannerJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        NSArray *jsonArray = value;
        NSMutableArray *array = [NSMutableArray array];
        [jsonArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            MCArtInfoNewestModel *banner = [MTLJSONAdapter modelOfClass:[MCArtInfoNewestModel class] fromJSONDictionary:obj error:error];
            [array addObject:banner];
//            if (!error) {
//                [array addObject:banner];
//            }
//            else {
//                 NSLog(@"error = %@",*error);
//            }
        }];
        return array;
    }];
}

+ (NSValueTransformer *)listJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(id value, BOOL *success, NSError *__autoreleasing *error) {
        NSArray *jsonArray = value;
        NSMutableArray *array = [NSMutableArray array];
        [jsonArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            MCArtInfoNewestModel *banner = [MTLJSONAdapter modelOfClass:[MCArtInfoNewestModel class] fromJSONDictionary:obj error:error];
            [array addObject:banner];
//            if (!error) {
//                [array addObject:banner];
//            }
//            else {
//                NSLog(@"error = %@",*error);
//            }
        }];
        return array;
    }];
}

@end
