//
//  MCArtInfoTopicModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/27.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCArtInfoTopicModel.h"

@implementation MCArtInfoTopicModel

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"id" : @"id",
             @"name" : @"name",
             @"pic" : @"pic"
             };
}


@end
