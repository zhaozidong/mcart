//
//  MCArtInfoTopicModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/1/27.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface MCArtInfoTopicModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, strong) NSString *id;

@property (nonatomic, strong) NSString *name;

@property (nonatomic, strong) NSString *pic;

@end
