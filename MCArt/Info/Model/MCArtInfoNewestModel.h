//
//  MCArtInfoNewestModel.h
//  MCArt
//
//  Created by Derek.zhao on 15/12/30.
//  Copyright © 2015年 zzd. All rights reserved.
//

#import <Mantle/Mantle.h>
#import "MCBaseModel.h"

typedef NS_ENUM(NSInteger,MCArtInfoNewestType) {
    MCArtInfoNewestTypeLocal = 1,
    MCArtInfoNewestTypeInternal,
    MCArtInfoNewestTypeForeign,
    MCArtInfoNewestTypeTopic
};


typedef NS_ENUM(NSInteger,MCArtInfoShowType) {
    MCArtInfoShowTypePic = 1,
    MCArtInfoShowTypeText,
    MCArtInfoShowTypeVideo
};


@interface MCArtInfoNewestImageModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, copy) NSString *src;

@property (nonatomic, copy) NSString *descr;

@end



@interface MCArtInfoNewestVideoModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, copy) NSString *src;

@property (nonatomic, copy) NSString *type;

@end




@interface MCArtInfoNewestModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, strong) NSString *id;

@property (nonatomic, strong) NSString *title;

@property (nonatomic, strong) NSString *time;

// 1为本地、2为国内、3为国际、4为专题
@property (nonatomic, assign) NSInteger type;

//展示类型  1文本资讯 2图集 3视频
@property (nonatomic, assign) NSInteger class_type;

//文本资讯内容，图集介绍，视频介绍,相当于副标题
@property (nonatomic, strong) NSString *content;

//封面图片
@property (nonatomic, strong) NSString *pic_path;

//图集时有值
@property (nonatomic, strong) NSArray *imgs_list;

//视频地址，视频时有值
@property (nonatomic, strong) MCArtInfoNewestVideoModel *video;

//分享用的url
@property (nonatomic, strong) NSString *share_url;

//分享用的url
@property (nonatomic, strong) NSString *share_content;


@end


@interface MCArtInfoModel : MCBaseModel


@property (nonatomic, copy) NSArray *banner;

@property (nonatomic, copy) NSArray *list;


@end
