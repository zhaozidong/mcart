//
//  MCArtInfoTopicViewModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/29.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCArtInfoTopicViewModel.h"
#import "MCNetworkManager.h"
#import "MCArtInfoTopicModel.h"

#define INFO_PAGE_SIZE @"20"


@implementation MCArtInfoTopicViewModel{
    __block NSMutableArray *_arrTopic;
    __block NSInteger pageIndex;
}


SINGLETON_FOR_CLASS(MCArtInfoTopicViewModel)

- (instancetype)init{
    self = [super init];
    if (self) {
        _arrTopic = [NSMutableArray array];
    }
    return self;
}

- (void)getTopic:(successBlock)successBlock failure:(failureBlock)failureBlock{
    pageIndex = 1;
    NSDictionary *dict = @{@"page" : @"1", @"pageSize" : INFO_PAGE_SIZE};
    [_arrTopic removeAllObjects];
    [[MCNetworkManager sharedInstance] postWithURL:@"appNews/getZT" parameter:dict  success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            NSError *err=nil;
            NSArray *array=[MTLJSONAdapter modelsOfClass:[MCArtInfoTopicModel class] fromJSONArray:[resquestData objectForKey:@"list"] error:&err];
            if (!err) {
                pageIndex ++;
                _arrTopic = [array mutableCopy];
                successBlock(_arrTopic);
            }else{
                [MCMessageUtilities showErrorMessage:err.description];
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}

- (void)getNextTopic:(successBlock)successBlock failure:(failureBlock)failureBlock{
    NSString *page = [NSString stringWithFormat:@"%ld",(long)pageIndex];
    NSDictionary *dict = @{@"page" : page, @"pageSize" : INFO_PAGE_SIZE};
    [[MCNetworkManager sharedInstance] postWithURL:@"appNews/getZT" parameter:dict  success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            NSError *err=nil;
            NSArray *array=[MTLJSONAdapter modelsOfClass:[MCArtInfoTopicModel class] fromJSONArray:[resquestData objectForKey:@"list"] error:&err];
            if (!err) {
                pageIndex ++;
                [_arrTopic addObjectsFromArray:array];
                successBlock(_arrTopic);
            }else{
                [MCMessageUtilities showErrorMessage:err.description];
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}




@end
