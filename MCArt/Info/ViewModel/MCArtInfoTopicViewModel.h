//
//  MCArtInfoTopicViewModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/1/29.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Foundation/Foundation.h>




@interface MCArtInfoTopicViewModel : NSObject

+ (instancetype)sharedInstance;


- (void)getTopic:(successBlock)successBlock failure:(failureBlock)failureBlock;


- (void)getNextTopic:(successBlock)successBlock failure:(failureBlock)failureBlock;



@end
