//
//  MCArtInfoViewModel.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/27.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCArtInfoViewModel.h"
#import "MCNetworkManager.h"
#import "MCArtInfoTopicModel.h"


@implementation MCArtInfoViewModel{
    __block NSInteger pageIndex;
    __block NSMutableArray *_banner;
    __block NSMutableArray *_list;
    MCArtInfoType _type;

}

SINGLETON_FOR_CLASS(MCArtInfoViewModel)

- (instancetype)init{
    self = [super init];
    if (self) {
        _list = [NSMutableArray array];
        _banner = [NSMutableArray array];
    }
    return self;
}

- (NSDictionary *)getRequestPara{
    NSString *index = [NSString stringWithFormat:@"%ld" , (long)pageIndex];
    NSDictionary *dict;
    switch (_type) {
        case MCArtInfoTypeAll: {
            dict = @{@"type" : @"0", @"page" :index, @"pageSize" : MC_PAGE_SIZE};
            break;
        }
        case MCArtInfoTypeLocal: {
            dict = @{@"type" : @"1", @"page" :index, @"pageSize" : MC_PAGE_SIZE};
            break;
        }
        case MCArtInfoTypeInternal: {
            dict = @{@"type" : @"2", @"page" :index, @"pageSize" : MC_PAGE_SIZE};
            break;
        }
        case MCArtInfoTypeForeign: {
            dict = @{@"type" : @"3", @"page" :index, @"pageSize" : MC_PAGE_SIZE};
            break;
        }
        case MCArtInfoTypeTopic: {
            dict = @{@"type" : @"4", @"page" :index, @"pageSize" : MC_PAGE_SIZE};
            break;
        }
        default:{
            dict = @{@"type" : [NSString stringWithFormat:@"%ld",(long)_type], @"page" :index, @"pageSize" : MC_PAGE_SIZE};
            break;
        }
            
    }
    return dict;
}

- (void)getInfoByType:(MCArtInfoType)type success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    _type = type;
    pageIndex = 1;
    [_list removeAllObjects];
    [_banner removeAllObjects];
    [[MCNetworkManager sharedInstance] postWithURL:@"appNews/getYszx" parameter:[self getRequestPara] success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            NSError *err=nil;
            MCArtInfoModel *info = [MTLJSONAdapter modelOfClass:[MCArtInfoModel class] fromJSONDictionary:resquestData error:&err];
            if (!err) {
                pageIndex ++ ;
                _list = [info.list mutableCopy];
                successBlock(info);
            }else{
                [MCMessageUtilities showErrorMessage:err.description];
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}


- (void)getNextPageSuccess:(successBlock)successBlock failure:(failureBlock)failureBlock{
    [[MCNetworkManager sharedInstance] postWithURL:@"appNews/getYszx" parameter:[self getRequestPara] success:^(id resquestData) {
        if ([[resquestData objectForKey:@"response"] boolValue]) {
            NSError *err=nil;
            MCArtInfoModel *info = [MTLJSONAdapter modelOfClass:[MCArtInfoModel class] fromJSONDictionary:resquestData error:&err];
            if (!err) {
                pageIndex ++;
                [_list addObjectsFromArray:info.list];
                successBlock(_list);
            }else{
                [MCMessageUtilities showErrorMessage:err.description];
            }
        }else{
            [MCMessageUtilities showErrorMessage:[resquestData objectForKey:@"info"]];
        }
    } failure:^(NSError *error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}

@end
