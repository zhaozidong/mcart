//
//  MCArtInfoViewModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/1/27.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MCArtInfoNewestModel.h"

typedef NS_ENUM(NSInteger,MCArtInfoType) {
    MCArtInfoTypeAll,
    MCArtInfoTypeLocal,
    MCArtInfoTypeInternal,
    MCArtInfoTypeForeign,
    MCArtInfoTypeTopic
};


@interface MCArtInfoViewModel : NSObject

+ (instancetype)sharedInstance;

- (void)getInfoByType:(MCArtInfoType)type success:(successBlock)successBlock failure:(failureBlock)failureBlock;


- (void)getNextPageSuccess:(successBlock)successBlock failure:(failureBlock)failureBlock;




@end
