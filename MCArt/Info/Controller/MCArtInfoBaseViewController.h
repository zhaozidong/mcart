//
//  MCArtInfoBaseViewController.h
//  MCArt
//
//  Created by Derek.zhao on 16/1/26.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseTableViewController.h"
#import "MCArtInfoViewModel.h"


@interface MCArtInfoBaseViewController : MCBaseTableViewController

@property (nonatomic, assign)NSInteger type;

@end
