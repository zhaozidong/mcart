//
//  MCArtInfoBaseViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/26.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCArtInfoBaseViewController.h"
#import "MCArtInfoNewestBannerCell.h"
#import "MCNetworkManager.h"
#import "MCArtInfoNewestModel.h"
#import "MCArtInfoNewestRecordCell.h"
#import "MCArtInfoDetailViewController.h"

static NSString *const bannerIdentifier=@"bannerCell";
static NSString *const recordIdentifier=@"recordCell";

@interface MCArtInfoBaseViewController ()<MCArtInfoNewestBannerCellDelegate>{
    __block NSMutableArray *_list;
    __block NSMutableArray *_banner;
}

@end

@implementation MCArtInfoBaseViewController

- (instancetype)init{
    self = [super initWithRefreshType: MCTableViewRefreshTypeBottom | MCTableViewRefreshTypeTop];
    if (self) {
        _list = [NSMutableArray array];
        _banner = [NSMutableArray array];
    }
    return self;
}

- (instancetype)initWithRefreshType:(MCTableViewRefreshType)type {
    self = [super initWithRefreshType:type];
    if (self) {
        _list = [NSMutableArray array];
        _banner = [NSMutableArray array];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self.tableView registerNib:[UINib nibWithNibName:@"MCArtInfoNewestBannerCell" bundle:nil] forCellReuseIdentifier:bannerIdentifier];
    [self.tableView registerClass:[MCArtInfoNewestRecordCell class] forCellReuseIdentifier:recordIdentifier];
    [self getData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.backgroundColor=kBlackColor;
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    
    if (_banner.count > 0) {
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)getData{
    __weak typeof(self) weakSelf = self;
    [MCMessageUtilities showProgressMessage:@""];
    [[MCArtInfoViewModel sharedInstance] getInfoByType:self.type success:^(id resquestData) {
        [MCMessageUtilities dismissHUD];
        MCArtInfoModel *info = resquestData;
        _list = [info.list mutableCopy];
        _banner = [info.banner mutableCopy];
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error) {
        [MCMessageUtilities dismissHUD];
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_list.count > 0) {
        if (_banner.count > 0) {
            return _list.count + 1;
        }else{
            return _list.count;
        }
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_banner.count > 0) {
        if (indexPath.row == 0 && (self.type == MCArtInfoTypeAll || self.type == MCArtInfoTypeLocal || self.type == MCArtInfoTypeInternal || self.type == MCArtInfoTypeForeign)) {
            MCArtInfoNewestBannerCell *bannerCell=[tableView dequeueReusableCellWithIdentifier:bannerIdentifier];
            bannerCell.delegate = self;
            [bannerCell updateWithInfo:_banner];
            return bannerCell;
        }else{
            MCArtInfoNewestModel *info=[_list objectAtIndex:indexPath.row-1];
            MCArtInfoNewestRecordCell *recordCell=[tableView dequeueReusableCellWithIdentifier:recordIdentifier];
            [recordCell updateWithInfo:info];
            return recordCell;
        }
    }else {
        MCArtInfoNewestModel *info=[_list objectAtIndex:indexPath.row];
        MCArtInfoNewestRecordCell *recordCell=[tableView dequeueReusableCellWithIdentifier:recordIdentifier];
        [recordCell updateWithInfo:info];
        return recordCell;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_banner.count > 0) {
        if (indexPath.row == 0 && (self.type == MCArtInfoTypeAll || self.type == MCArtInfoTypeLocal || self.type == MCArtInfoTypeInternal || self.type == MCArtInfoTypeForeign)) {
            return [MCArtInfoNewestBannerCell cellHeight];
        }else{
            return [MCArtInfoNewestRecordCell cellHeight];
        }
    }else {
        return [MCArtInfoNewestRecordCell cellHeight];
    }
    return 44.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (_banner.count > 0) {
        if (indexPath.row > 0) {
            MCArtInfoNewestModel *info=[_list objectAtIndex:indexPath.row-1];
            if (info && info.share_url) {
                MCArtInfoDetailViewController  *detail=[[MCArtInfoDetailViewController alloc] initWithNibName:@"MCArtInfoDetailViewController" bundle:nil];
                detail.url=info.share_url;
                detail.info = info;
                [self.navigationController pushViewController:detail animated:YES];
            }
        }
    }else{
        if (indexPath.row > 0) {
            MCArtInfoNewestModel *info=[_list objectAtIndex:indexPath.row];
            if (info && info.share_url) {
                MCArtInfoDetailViewController  *detail=[[MCArtInfoDetailViewController alloc] initWithNibName:@"MCArtInfoDetailViewController" bundle:nil];
                detail.url=info.share_url;
                detail.info = info;
                [self.navigationController pushViewController:detail animated:YES];
            }
        }
    }
}

- (void)beginRefreshHeader:(MJRefreshHeader *)header{
    [header endRefreshing];
    [self.tableView reloadData];
    [self getData];
}

- (void)beginRefreshFooter:(MJRefreshFooter *)footer{
    [footer endRefreshing];
    __weak typeof(self) weakSelf = self;
    [MCMessageUtilities showProgressMessage:@""];
    [[MCArtInfoViewModel sharedInstance] getNextPageSuccess:^(id resquestData) {
        [MCMessageUtilities dismissHUD];
        _list = [((NSMutableArray *)resquestData) mutableCopy];
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error) {
        [MCMessageUtilities dismissHUD];
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];
    }];
}


#pragma mark MCArtInfoNewestBannerCellDelegate
- (void)clickedImageAtIndex:(NSInteger)index{
    MCArtInfoNewestModel *info=[_banner objectAtIndex:index];
    if (info && info.share_url) {
        MCArtInfoDetailViewController  *detail=[[MCArtInfoDetailViewController alloc] initWithNibName:@"MCArtInfoDetailViewController" bundle:nil];
        detail.url=info.share_url;
        detail.info = info;
        [self.navigationController pushViewController:detail animated:YES];
    }
}

@end
