//
//  MCArtInfoTopicViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/5.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCArtInfoTopicViewController.h"
#import "MCArtInfoTopicCell.h"
#import "MCArtInfoDetailViewController.h"
#import "MCArtInfoTopicModel.h"
#import "MCArtInfoTopicDetailViewController.h"
#import "MCArtInfoTopicViewModel.h"

static NSString *const topicIdentifier=@"topicCell";

@interface MCArtInfoTopicViewController (){
    __block NSMutableArray *_arrDataSource;
}

@end

@implementation MCArtInfoTopicViewController
- (instancetype)init{
    self = [super initWithRefreshType:MCTableViewRefreshTypeTop | MCTableViewRefreshTypeBottom];
    if (self) {
        _arrDataSource = [NSMutableArray array];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.separatorStyle=UITableViewCellSeparatorStyleNone;
    [self.tableView registerClass:[MCArtInfoTopicCell class] forCellReuseIdentifier:topicIdentifier];
    [self getData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.backgroundColor=kBlackColor;
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (void)getData{
    __weak typeof(self) weakSelf = self;
    [[MCArtInfoTopicViewModel sharedInstance] getTopic:^(id resquestData) {
        _arrDataSource = [((NSMutableArray *)resquestData) mutableCopy];
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error) {
        
    }];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [_arrDataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MCArtInfoTopicModel *info = [_arrDataSource objectAtIndex:indexPath.row];
    MCArtInfoTopicCell *topicCell=[tableView dequeueReusableCellWithIdentifier:topicIdentifier];
    [topicCell updateWithInfo:info];
    return topicCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return kMainFrameWidth*0.476;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(nonnull NSIndexPath *)indexPath{
    [self.tableView deselectRowAtIndexPath:indexPath animated:NO];
    MCArtInfoTopicModel *info = [_arrDataSource objectAtIndex:indexPath.row];
    MCArtInfoTopicDetailViewController *detail = [[MCArtInfoTopicDetailViewController alloc] initWithRefreshType:MCTableViewRefreshTypeTop | MCTableViewRefreshTypeBottom infoType:[info.id integerValue]];
    detail.type = [info.id integerValue];
    [self.navigationController pushViewController:detail animated:YES];
}

- (void)beginRefreshHeader:(MJRefreshHeader *)header{
    [header endRefreshing];
    [self.tableView reloadData];
    [self getData];
}

- (void)beginRefreshFooter:(MJRefreshFooter *)footer{
    [footer endRefreshing];
    __weak typeof(self) weakSelf = self;
//    [[MCArtInfoViewModel sharedInstance] getNextPageSuccess:^(id resquestData) {
//        _arrDataSource = [((NSMutableArray *)resquestData) mutableCopy];
//        [weakSelf.tableView reloadData];
//    } failure:^(NSError *error) {
//        
//    }];
    
    
    [[MCArtInfoTopicViewModel sharedInstance] getNextTopic:^(id resquestData) {
        _arrDataSource = [((NSMutableArray *)resquestData) mutableCopy];
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error) {
        
    }];
}


@end
