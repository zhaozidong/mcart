//
//  MCArtInfoViewController.m
//  MCArt
//
//  Created by Derek on 15/12/22.
//  Copyright © 2015年 zzd. All rights reserved.
//

#import "MCArtInfoViewController.h"
#import "MCArtInfoLocalViewController.h"
#import "MCArtInfoInternalViewController.h"
#import "MCArtInfoForeignViewController.h"
#import "MCArtInfoTopicViewController.h"
#import "MCArtInfoAllViewController.h"

@interface MCArtInfoViewController ()

@end

@implementation MCArtInfoViewController

- (instancetype)init{
    NSArray *arrControls=@[[MCArtInfoAllViewController class],[MCArtInfoLocalViewController class],[MCArtInfoInternalViewController class],[MCArtInfoForeignViewController class],[MCArtInfoTopicViewController class]];
    NSArray *arrTitles=@[@"最新",@"本地",@"国内",@"国际",@"专题"];
    self=[super initWithViewControllerClasses:arrControls andTheirTitles:arrTitles];
    if (self) {
//        self.navStyle=MCNavStyleBlack;
    }
    return self;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title=@"艺术资讯";
//    self.navStyle=MCNavStyleBlack;
    
    
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    
//    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
//    self.navigationController.navigationBar.backgroundColor=kBlackColor;
//    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    
    
}


- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
    self.navigationController.navigationBar.backgroundColor=kBlackColor;
    self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/







@end
