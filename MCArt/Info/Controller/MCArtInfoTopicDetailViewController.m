//
//  MCArtInfoTopicDetailViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/27.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCArtInfoTopicDetailViewController.h"
#import "MCArtInfoNewestRecordCell.h"
#import "MCArtInfoDetailViewController.h"



static NSString *const recordIdentifier=@"recordCell";

@interface MCArtInfoTopicDetailViewController (){
    NSInteger _infoType;
    __block NSMutableArray *_list;
}

@end

@implementation MCArtInfoTopicDetailViewController
- (instancetype)initWithRefreshType:(MCTableViewRefreshType)refreshType infoType:(NSInteger)type{
    _infoType = type;
    self = [super initWithRefreshType:refreshType];
    if (self) {
        self.type = type;
    }
    return self;
    
}


- (void)viewDidLoad {
    self.type = _infoType;
//    NSLog(@"MCArtInfoTopicDetailViewController.type = %ld",(long)self.type);
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self setTitle:@"专题详情" withStyle:MCNavStyleBlack];
    
    _list = [NSMutableArray array];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)getData{
    __weak typeof(self) weakSelf = self;
    [MCMessageUtilities showProgressMessage:@""];
    [[MCArtInfoViewModel sharedInstance] getInfoByType:self.type success:^(id resquestData) {
        [MCMessageUtilities dismissHUD];
        MCArtInfoModel *info = resquestData;
        _list = [info.list mutableCopy];
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error) {
        [MCMessageUtilities dismissHUD];
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];;
    }];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_list.count > 0) {
        return _list.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    MCArtInfoNewestModel *info=[_list objectAtIndex:indexPath.row];
    MCArtInfoNewestRecordCell *recordCell=[tableView dequeueReusableCellWithIdentifier:recordIdentifier];
    [recordCell updateWithInfo:info];
    return recordCell;

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [MCArtInfoNewestRecordCell cellHeight];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    MCArtInfoNewestModel *info=[_list objectAtIndex:indexPath.row];
    if (info && info.share_url) {
        MCArtInfoDetailViewController  *detail=[[MCArtInfoDetailViewController alloc] initWithNibName:@"MCArtInfoDetailViewController" bundle:nil];
        detail.url=info.share_url;
        detail.info = info;
        [self.navigationController pushViewController:detail animated:YES];
    }
}

- (void)beginRefreshHeader:(MJRefreshHeader *)header{
    [header endRefreshing];
    [self.tableView reloadData];
    [self getData];
}

- (void)beginRefreshFooter:(MJRefreshFooter *)footer{
    [footer endRefreshing];
    __weak typeof(self) weakSelf = self;
    [MCMessageUtilities showProgressMessage:@""];
    [[MCArtInfoViewModel sharedInstance] getNextPageSuccess:^(id resquestData) {
        [MCMessageUtilities dismissHUD];
        _list = [((NSMutableArray *)resquestData) mutableCopy];
        [weakSelf.tableView reloadData];
    } failure:^(NSError *error) {
        [MCMessageUtilities dismissHUD];
        [MCMessageUtilities showErrorMessage:@"网络连接失败"];
    }];
}




@end
