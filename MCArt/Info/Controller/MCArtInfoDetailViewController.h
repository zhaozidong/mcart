//
//  MCArtInfoDetailViewController.h
//  MCArt
//
//  Created by Derek.zhao on 16/1/7.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseViewController.h"
#import "MCArtInfoNewestModel.h"


@interface MCArtInfoDetailViewController : MCBaseViewController

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (nonatomic, strong) NSString *url;

@property (nonatomic, strong) MCArtInfoNewestModel *info;

@end
