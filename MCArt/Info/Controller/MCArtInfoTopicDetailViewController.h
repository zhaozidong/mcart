//
//  MCArtInfoTopicDetailViewController.h
//  MCArt
//
//  Created by Derek.zhao on 16/1/27.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCArtInfoBaseViewController.h"

@interface MCArtInfoTopicDetailViewController : MCArtInfoBaseViewController

- (instancetype)initWithRefreshType:(MCTableViewRefreshType)refreshType infoType:(NSInteger)type;


@end
