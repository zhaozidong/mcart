//
//  MCArtInfoDetailViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/7.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCArtInfoDetailViewController.h"
#import "MCShareView.h"
#import "MCShareViewModel.h"


@interface MCArtInfoDetailViewController ()<MCShareViewDelegate>

@property (nonatomic, retain) UIButton *shareButton;


@end

@implementation MCArtInfoDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [self setTitle:@"资讯详情" withStyle:MCNavStyleWhite];
    
    
    UIBarButtonItem *right = [[UIBarButtonItem alloc] initWithCustomView:self.shareButton];
    self.navigationItem.rightBarButtonItem = right;
    
    
    
    if (self.url) {
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
    }else{
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.youku.com"]]];
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (UIButton *)shareButton{
    if (!_shareButton) {
        _shareButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 45, 45)];
        [_shareButton setImage:[UIImage imageNamed:@"titleShare_white"] forState:UIControlStateNormal];
        [_shareButton addTarget:self action:@selector(btnDidClickedShare:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _shareButton;
}

- (void)btnDidClickedShare:(id)sender{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    MCShareView *share = [[MCShareView alloc] initWithFrame:self.view.bounds];
//    share.delegate = self;
    [share updateWithTitle:self.info.title image:self.info.pic_path url:self.info.share_url content:self.info.share_content];
    [window addSubview:share];
}


- (void)webViewDidStartLoad:(UIWebView *)webView{
    
//    NSLog(@"start");
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    
//    NSLog(@"finish");
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(nullable NSError *)error{
    
//    NSLog(@"fail desc=%@, code=%ld",error.description,error.code);
    
}

//- (void)clickedShare:(MCShareType)type{
//    switch (type) {
//        case MCShareTypeWeiXin: {
//            [MCShareViewModel shareWithType:SSDKPlatformSubTypeWechatSession content:self.info.share_content image:kImagePath(self.info.pic_path) url:self.info.share_url title:self.info.title];
//            break;
//        }
//        case MCShareTypeWieXinFriends: {
//            [MCShareViewModel shareWithType:SSDKPlatformSubTypeWechatTimeline content:self.info.share_content image:kImagePath(self.info.pic_path) url:self.info.share_url title:self.info.title];
//            break;
//        }
//        case MCShareTypeQQ: {
//            [MCShareViewModel shareWithType:SSDKPlatformSubTypeQQFriend content:self.info.share_content image:kImagePath(self.info.pic_path) url:self.info.share_url title:self.info.title];
//            break;
//        }
//        case MCShareTypeQZone: {
//            [MCShareViewModel shareWithType:SSDKPlatformSubTypeQZone content:self.info.share_content image:kImagePath(self.info.pic_path) url:self.info.share_url title:self.info.title];
//            break;
//        }
//        case MCShareTypeWeibo: {
//            [MCShareViewModel shareWithType:SSDKPlatformTypeSinaWeibo content:self.info.share_content image:kImagePath(self.info.pic_path) url:self.info.share_url title:self.info.title];
//            break;
//        }
//    }
//}

@end
