//
//  MLKMenuPopover.h
//  MLKMenuPopover
//
//  Created by NagaMalleswar on 20/11/14.
//  Copyright (c) 2014 NagaMalleswar. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MLKMenuPopover;

@protocol MLKMenuPopoverDelegate

- (void)menuPopover:(MLKMenuPopover *)menuPopover didSelectMenuItemAtIndex:(NSInteger)selectedIndex;

@end

@interface MLKMenuPopover : UIView <UITableViewDataSource,UITableViewDelegate>

@property(nonatomic,weak) id<MLKMenuPopoverDelegate> menuPopoverDelegate;

@property(nonatomic, assign) BOOL isVisible; //

- (id)initWithFrame:(CGRect)frame menuItems:(NSArray *)aMenuItems imgItems:(NSArray *)arrImages;
- (void)showInView:(UIView *)view;
- (void)dismissMenuPopover;
- (void)layoutUIForInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation;

+ (CGFloat )heightWithArray:(NSArray *)array;

@end


