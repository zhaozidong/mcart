//
//  MCBaseModel.h
//  MCArt
//
//  Created by Derek.zhao on 16/4/16.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Mantle/Mantle.h>

@interface MCBaseModel : MTLModel<MTLJSONSerializing>

@property (nonatomic, assign) NSInteger response;

@property (nonatomic, copy) NSString *info;

@property (nonatomic, assign) long long timestamp;


@end
