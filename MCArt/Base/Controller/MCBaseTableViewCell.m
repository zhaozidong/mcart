//
//  MCBaseTableViewCell.m
//  MCArt
//
//  Created by Derek.zhao on 15/12/28.
//  Copyright © 2015年 zzd. All rights reserved.
//

#import "MCBaseTableViewCell.h"

@implementation MCBaseTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self=[super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle=UITableViewCellSelectionStyleNone;
        [self setUpSubViews];
    }
    return self;
}


- (void)setUpSubViews{
    
    
    
    
    
}


@end
