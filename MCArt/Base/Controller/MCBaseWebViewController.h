//
//  MCBaseWebViewController.h
//  MCArt
//
//  Created by Derek.zhao on 16/1/31.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseViewController.h"

@interface MCBaseWebViewController : MCBaseViewController

@property (nonatomic, strong) NSString *url;

- (void)updateWithURL:(NSString *)url;


@end
