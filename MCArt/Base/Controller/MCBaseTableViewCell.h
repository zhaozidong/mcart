//
//  MCBaseTableViewCell.h
//  MCArt
//
//  Created by Derek.zhao on 15/12/28.
//  Copyright © 2015年 zzd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Masonry.h>
#import <UIImageView+WebCache.h>

@interface MCBaseTableViewCell : UITableViewCell


- (void)setUpSubViews;


@end
