//
//  MCBaseViewController.m
//  MCArt
//
//  Created by Derek on 15/12/18.
//  Copyright © 2015年 zzd. All rights reserved.
//

#import "MCBaseViewController.h"
#import "MCLoginViewController.h"


@interface MCBaseViewController (){
    NSString *_title;
}

@end

@implementation MCBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)]) {
//        self.edgesForExtendedLayout=UIRectEdgeNone;
//    }
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.navStyle == MCNavStyleBlack) {
        self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
        self.navigationController.navigationBar.backgroundColor=kBlackColor;
        self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
        UIBarButtonItem *back=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"nav_back_white"] style:UIBarButtonItemStylePlain target:self action:@selector(back:)];
        self.navigationItem.leftBarButtonItem=back;
    }else if (self.navStyle == MCNavStyleWhite){
        self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
        self.navigationController.navigationBar.backgroundColor=kWhiteColor;
        self.navigationController.navigationBar.tintColor=[UIColor grayColor];
        UIBarButtonItem *back=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"nav_back_gray"] style:UIBarButtonItemStylePlain target:self action:@selector(back:)];
        self.navigationItem.leftBarButtonItem=back;
    }
}


//- (void)viewDidAppear:(BOOL)animated{
//    [super viewDidAppear:animated];
//}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//- (UIStatusBarStyle)preferredStatusBarStyle
//{
//    return UIStatusBarStyleLightContent;
//}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (void)back:(id)sender{
    if (self.presentingViewController) {
        [self dismissViewControllerAnimated:YES completion:nil];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (NSString *)title{
    return _title;
}


- (void)setTitle:(NSString *)title{
    _title=title;
    UILabel *label=[[UILabel alloc] init];
    label.font=kMCFont(20);
    label.textColor=kWhiteColor;
    label.text=title;
    [label sizeToFit];
    self.navigationItem.titleView=label;
}


- (void)setTitle:(NSString *)title withColor:(UIColor *)color{
    _title=title;
    UILabel *label=[[UILabel alloc] init];
    label.font=kMCFont(20);
    label.textColor=color;
    label.text=title;
    [label sizeToFit];
    self.navigationItem.titleView=label;
}

- (void)setTitle:(NSString *)title withStyle:(MCNavStyle)style{
    _title=title;
    self.navStyle = style;
    if (style == MCNavStyleBlack) {
        [self setTitle:title withColor:kWhiteColor];
    }else if (style == MCNavStyleWhite){
        [self setTitle:title withColor:kBlackColor];
    }
}

- (void)setTitle:(NSString *)title titleColor:(UIColor *)color style:(MCNavStyle)style{
    _title=title;
    self.navStyle = style;
    if (style == MCNavStyleBlack) {
        [self setTitle:title withColor:kWhiteColor];
    }else if (style == MCNavStyleWhite){
        [self setTitle:title withColor:kBlackColor];
    }
}


- (void)showLoginViewController{
    MCLoginViewController *login = [[MCLoginViewController alloc] initWithNibName:NSStringFromClass([MCLoginViewController class]) bundle:nil];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:login];
    [self presentViewController:nav animated:YES completion:nil];
}

@end
