//
//  MCBaseWebViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/31.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseWebViewController.h"

@interface MCBaseWebViewController ()<UIWebViewDelegate>

@property(nonatomic, strong)UIWebView *webView;

@end



@implementation MCBaseWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view addSubview:self.webView];
    
    if (self.url) {
        [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
    }
}


- (UIWebView *)webView{
    if (!_webView) {
        _webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
        _webView.delegate =self;
    }
    return _webView;
}

- (void)updateWithURL:(NSString *)url{
    self.url = url;
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
    self.webView.frame = self.view.bounds;
}


#pragma mark UIWebViewDelegate

//- (void)webViewDidStartLoad:(UIWebView *)webView{
//    
//    //    NSLog(@"start");
//}
//- (void)webViewDidFinishLoad:(UIWebView *)webView{
//    
//    //    NSLog(@"finish");
//}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(nullable NSError *)error{
    
    NSLog(@"fail desc=%@, code=%ld",error.description,error.code);
    
}

@end
