//
//  MCBaseViewController.h
//  MCArt
//
//  Created by Derek on 15/12/18.
//  Copyright © 2015年 zzd. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, MCNavStyle) {
    MCNavStyleWhite,
    MCNavStyleBlack,
    MCNavStyleTransprent
};

@interface MCBaseViewController : UIViewController

@property (nonatomic, assign) MCNavStyle navStyle;


- (void)setTitle:(NSString *)title withColor:(UIColor *)color;

- (void)setTitle:(NSString *)title withStyle:(MCNavStyle)style;

- (void)showLoginViewController;

- (void)back:(id)sender;


@end
