//
//  MCBaseTableViewController.h
//  MCArt
//
//  Created by Derek.zhao on 16/1/5.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCBaseViewController.h"
#import <MJRefresh.h>



static NSString *const baseCell=@"baseCell";


typedef NS_OPTIONS(NSInteger, MCTableViewRefreshType) {
    MCTableViewRefreshTypeNone = 0,
    MCTableViewRefreshTypeTop = 1 << 0,
    MCTableViewRefreshTypeBottom = 1 << 1
};


@interface MCBaseTableViewController : UITableViewController

@property (nonatomic, assign) MCNavStyle navStyle;

- (void)setTitle:(NSString *)title withStyle:(MCNavStyle)style;


- (instancetype)initWithRefreshType:(MCTableViewRefreshType)type;

- (instancetype)initWithStyle:(UITableViewStyle)style refreshType:(MCTableViewRefreshType)type;

- (void)beginRefreshHeader:(MJRefreshHeader *)header;

- (void)beginRefreshFooter:(MJRefreshFooter *)footer;

- (void)showLoginViewController;

@end
