//
//  MCBaseTableViewController.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/5.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCBaseTableViewController.h"
#import "MCLoginViewController.h"

@interface MCBaseTableViewController (){
    NSString *_title;
}

@end

@implementation MCBaseTableViewController

- (instancetype)initWithRefreshType:(MCTableViewRefreshType)type{
    self = [super init];
    if (self) {
        if (type & MCTableViewRefreshTypeTop){
            self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(beginRefreshHeader:)];
        }
        if (type & MCTableViewRefreshTypeBottom) {
            self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(beginRefreshFooter:)];
        }
    }
    return self;
}

- (instancetype)initWithStyle:(UITableViewStyle)style refreshType:(MCTableViewRefreshType)type{
    self = [super initWithStyle:style];
    if (self) {
        if (type & MCTableViewRefreshTypeTop){
            self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(beginRefreshHeader:)];
        }
        if (type & MCTableViewRefreshTypeBottom) {
            self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(beginRefreshFooter:)];
        }
    }
    return self;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
    self.tableView.tableHeaderView=[[UIView alloc] init];
    self.tableView.tableFooterView=[[UIView alloc] init];
    
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:baseCell];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.navStyle == MCNavStyleBlack) {
        self.navigationController.navigationBar.barStyle = UIBarStyleBlack;
        self.navigationController.navigationBar.backgroundColor=kBlackColor;
        self.navigationController.navigationBar.tintColor=[UIColor whiteColor];
        UIBarButtonItem *back=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"nav_back_white"] style:UIBarButtonItemStylePlain target:self action:@selector(back:)];
        self.navigationItem.leftBarButtonItem=back;
    }else if (self.navStyle == MCNavStyleWhite){
        self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
        self.navigationController.navigationBar.backgroundColor=kWhiteColor;
        self.navigationController.navigationBar.tintColor=[UIColor grayColor];
        UIBarButtonItem *back=[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"nav_back_gray"] style:UIBarButtonItemStylePlain target:self action:@selector(back:)];
        self.navigationItem.leftBarButtonItem=back;
    }
}



- (void)back:(id)sender{
    [self.navigationController popViewControllerAnimated:YES];
}


- (NSString *)title{
    return _title;
}

- (void)setTitle:(NSString *)title{
    _title=title;
    UILabel *label=[[UILabel alloc] init];
    label.font=[UIFont fontWithName:@"ShiShangZhongHeiJianTi" size:20];
    label.textColor=kWhiteColor;
    label.text=title;
    [label sizeToFit];
    self.navigationItem.titleView=label;
}

- (void)setTitle:(NSString *)title withColor:(UIColor *)color{
    _title=title;
    UILabel *label=[[UILabel alloc] init];
    label.font=[UIFont fontWithName:@"ShiShangZhongHeiJianTi" size:20];
    label.textColor=color;
    label.text=title;
    [label sizeToFit];
    self.navigationItem.titleView=label;
}

- (void)setTitle:(NSString *)title withStyle:(MCNavStyle)style{
    _title=title;
    self.navStyle = style;
    if (style == MCNavStyleBlack) {
        [self setTitle:title withColor:kWhiteColor];
    }else if (style == MCNavStyleWhite){
        [self setTitle:title withColor:kBlackColor];
    }
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:baseCell forIndexPath:indexPath];
    cell.textLabel.text=self.title;
    return cell;
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark MJRefresh

- (void)beginRefreshHeader:(MJRefreshHeader *)header{
    
    
    
    
    
}

- (void)beginRefreshFooter:(MJRefreshFooter *)footer{
    
    
    
}

- (void)showLoginViewController{
    MCLoginViewController *login = [[MCLoginViewController alloc] initWithNibName:NSStringFromClass([MCLoginViewController class]) bundle:nil];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:login];
    [self presentViewController:nav animated:YES completion:nil];
}

@end
