//
//  MCUserDefaultUtils.h
//  MCArt
//
//  Created by Derek.zhao on 16/2/16.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <Foundation/Foundation.h>


#define kUserDefaultShowAds @"showAds"







@interface MCUserDefaultUtils : NSObject

+(void)saveValue:(id) value forKey:(NSString *)key;

+(id)valueWithKey:(NSString *)key;

+(BOOL)boolValueWithKey:(NSString *)key;

+(void)saveBoolValue:(BOOL)value withKey:(NSString *)key;

+(void)removeValueForKey:(NSString *)key;



@end
