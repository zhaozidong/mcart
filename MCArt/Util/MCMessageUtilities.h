//
//  MCMessageUtilities.h
//  MCArt
//
//  Created by Derek.zhao on 16/1/27.
//  Copyright © 2016年 zzd. All rights reserved.
//







@interface MCMessageUtilities : NSObject

/********************* SVProgressHUD **********************/
//弹出操作错误信息提示框
+ (void)showErrorMessage:(NSString *)message;
//弹出操作成功信息提示框
+ (void)showSuccessMessage:(NSString *)message;
//弹出加载提示框
+ (void)showProgressMessage:(NSString *) message;
//取消弹出框
+ (void)dismissHUD;

+ (void)showTotast:(NSString *)message;

@end
