//
//  NSString+Utilities.h
//  Iwu
//
//  Created by chris on 4/29/14.
//  Copyright (c) 2014 superjia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Utilities)
- (BOOL) contains:(NSString*)string;
- (NSString*) add:(NSString*)string;
- (NSDictionary*) firstAndLastName;
- (BOOL) isValidEmail;
- (BOOL) isCheckValidMobile;
- (BOOL) containsOnlyLetters;
- (BOOL) containsOnlyNumbers;
- (BOOL) containsOnlyNumbersAndLetters;
- (NSString*) safeSubstringToIndex:(NSUInteger)index;
- (NSString*) stringByRemovingPrefix:(NSString*)prefix;
- (NSString*) stringByRemovingPrefixes:(NSArray*)prefixes;
- (BOOL) hasPrefixes:(NSArray*)prefixes;
- (BOOL) isEqualToOneOf:(NSArray*)strings;
- (NSString*) md5;
- (NSString *)sha256;
- (NSString*) telephoneWithReformat;
- (NSString*) telephoneWithDashFormat;
- (NSString*) trim;
- (NSArray*) splitBy:(NSString*) splitString;
- (NSArray*) rangesOfString:(NSString*) aString;
+ (NSString*) safeStr:(NSString*)str;
+ (BOOL) isNullOrEmpty:(NSString*)str;
+ (BOOL) equals:(NSString *)str1 to:(NSString *)str2;

- (NSString *)maskPhoneNumberByHeadLength:(int)headLength tailLength:(int)tailLength;

- (NSMutableAttributedString *)subString:(NSString *)subString attrs:(NSDictionary *)attrs;

@end
