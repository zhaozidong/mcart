//
//  MCNetworkManager.h
//  MCArt
//
//  Created by Derek.zhao on 15/12/30.
//  Copyright © 2015年 zzd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking.h>

@interface MCNetworkManager : NSObject

+ (instancetype)sharedInstance;

- (void)getWithURL:(NSString *)url parameter:(NSDictionary *)parameter success:(successBlock)successBlock failure:(failureBlock)failureBlock;

- (void)postWithURL:(NSString *)url parameter:(NSDictionary *)parameter success:(successBlock)successBlock failure:(failureBlock)failureBlock;

- (void)putWithURL:(NSString *)url parameter:(NSDictionary *)parameter success:(successBlock)successBlock failure:(failureBlock)failureBlock;

- (void)deleteWithURL:(NSString *)url parameter:(NSDictionary *)parameter success:(successBlock)successBlock failure:(failureBlock)failureBlock;

- (void)uploadWithURL:(NSString *)url
                 data:(id)data
       uploadProgress:(void (^)(NSProgress *uploadProgress)) uploadProgress
              success:(successBlock)successBlock
              failure:(failureBlock)failureBlock;

@end
