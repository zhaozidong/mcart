//
//  IHDeviceUtilities.h
//  iHouse
//
//  Created by chris on 6/19/14.
//  Copyright (c) 2014 superjia. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IHDeviceUtilities : NSObject
{
    NSString *_deviceType;
    NSString *_systemVersion;
    NSString *_deviceUid;
    NSString *_cellularProviderName;
    NSString *_imei;
    NSString *_idfa;
    NSString *_mac;
}
@property (nonatomic, readonly, getter = getDeviceType) NSString *deviceType;
@property (nonatomic, readonly, getter = getSystemVersion) NSString *systemVersion;
@property (nonatomic, readonly, getter = getSystemMajorVersion) NSInteger systemMajorVersion;
@property (nonatomic, readonly, getter = getDeviceUid) NSString *deviceUid;
@property (nonatomic, readonly, getter = getCellularProviderName) NSString *cellularProviderName;
@property (nonatomic, readonly, getter = getIMEI) NSString *imei;
@property (nonatomic, readonly, getter = getIdfa) NSString *idfa;
@property (nonatomic, readonly, getter = getMacAddress) NSString *mac;
+ (IHDeviceUtilities*)sharedInstance;

- (NSString *)deviceIPAdress;

@end
