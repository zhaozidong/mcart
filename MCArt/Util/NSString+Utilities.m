//
//  NSString+Utilities.m
//  Iwu
//
//  Created by chris on 4/29/14.
//  Copyright (c) 2014 superjia. All rights reserved.
//

#import "NSString+Utilities.h"
#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonCryptor.h>
#import "NSArray+Utilities.h"



@implementation NSString (Utilities)
- (BOOL)contains:(NSString*)string {
    return [self rangeOfString:string].location != NSNotFound;
}

#define MaxEmailLength 254
- (BOOL)isValidEmail
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    BOOL validEmail = [emailTest evaluateWithObject:self];
    if(validEmail && self.length <= MaxEmailLength)
        return YES;
    return NO;
}

- (NSArray*) splitBy:(NSString *)splitString{
    return [self componentsSeparatedByString: splitString];
//    return [self componentsSeparatedByCharactersInSet:
//            [NSCharacterSet characterSetWithCharactersInString:splitString]
//            ];
}

- (BOOL) isCheckValidMobile{
    return [self isValidWildMobile] ||
            [self isValidMobile2] ||
            [self isValidCMMobile] ||
            [self isValidCUMobile] ||
            [self isValidCTMobile];
}

- (BOOL) isValidWildMobile{
    NSString * MOBILE = @"^\\d{11,15}$";
    NSPredicate *regexTestMobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    return [regexTestMobile evaluateWithObject:self];
}

- (BOOL) isValidMobile2{
    /**
     * 手机号码
     * 移动：134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     * 联通：130,131,132,152,155,156,185,186
     * 电信：133,1349,153,180,189
     */
    NSString * MOBILE = @"^1(3[0-9]|5[0-35-9]|8[025-9])\\d{8}$";
    NSPredicate *regexTestMobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    return [regexTestMobile evaluateWithObject:self];
}

- (BOOL) isValidCMMobile{
    /**
     10         * 中国移动：China Mobile
     11         * 134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     12         */
    NSString * CM = @"^1(34[0-8]|(3[5-9]|5[017-9]|8[278])\\d)\\d{7}$";
    NSPredicate *regexTestCM = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    return [regexTestCM evaluateWithObject:self];
}

- (BOOL) isValidCUMobile{
    /**
     15         * 中国联通：China Unicom
     16         * 130,131,132,152,155,156,185,186
     17         */
    NSString * CU = @"^1(3[0-2]|5[256]|8[56])\\d{8}$";
    NSPredicate *regexTestCU = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    return [regexTestCU evaluateWithObject:self];
}

- (BOOL) isValidCTMobile{
    /**
     20         * 中国电信：China Telecom
     21         * 133,1349,153,180,189
     22         */
    NSString * CT = @"^1((33|53|8[09])[0-9]|349)\\d{7}$";
    NSPredicate *regexTestCT = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    return [regexTestCT evaluateWithObject:self];
}

- (BOOL) isValidPHSMobile{
    /**
     25         * 大陆地区固话及小灵通
     26         * 区号：010,020,021,022,023,024,025,027,028,029
     27         * 号码：七位或八位
     28         */
    NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    NSPredicate *regexTestPHS = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", PHS];
    return [regexTestPHS evaluateWithObject:self];
}

- (NSString*)add:(NSString*)string
{
    if(!string || string.length == 0)
        return self;
    return [self stringByAppendingString:string];
}

- (NSDictionary*)firstAndLastName
{
    NSMutableDictionary *dic = [NSMutableDictionary new];
    NSArray *comps = [self componentsSeparatedByString:@" "];
    if(comps.count > 0) dic[@"firstName"] = comps[0];
    if(comps.count > 1) dic[@"lastName"] = comps[1];
    return dic;
}

- (BOOL)containsOnlyLetters
{
    NSCharacterSet *blockedCharacters = [[NSCharacterSet letterCharacterSet] invertedSet];
    return ([self rangeOfCharacterFromSet:blockedCharacters].location == NSNotFound);
}

- (BOOL)containsOnlyNumbers
{
    NSCharacterSet *numbers = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789"] invertedSet];
    return ([self rangeOfCharacterFromSet:numbers].location == NSNotFound);
}

- (BOOL)containsOnlyNumbersAndLetters
{
    NSCharacterSet *blockedCharacters = [[NSCharacterSet alphanumericCharacterSet] invertedSet];
    return ([self rangeOfCharacterFromSet:blockedCharacters].location == NSNotFound);
}

- (NSString*)safeSubstringToIndex:(NSUInteger)index
{
    if(index >= self.length)
        index = self.length - 1;
    return [self substringToIndex:index];
}

- (NSString*)stringByRemovingPrefix:(NSString*)prefix
{
    NSRange range = [self rangeOfString:prefix];
    if(range.location == 0) {
        return [self stringByReplacingCharactersInRange:range withString:@""];
    }
    return self;
}

- (NSString*)stringByRemovingPrefixes:(NSArray*)prefixes
{
    for(NSString *prefix in prefixes) {
        NSRange range = [self rangeOfString:prefix];
        if(range.location == 0) {
            return [self stringByReplacingCharactersInRange:range withString:@""];
        }
    }
    return self;
}

- (BOOL)hasPrefixes:(NSArray*)prefixes
{
    for(NSString *prefix in prefixes) {
        if([self hasPrefix:prefix])
            return YES;
    }
    return NO;
}

- (BOOL)isEqualToOneOf:(NSArray*)strings
{
    for(NSString *string in strings) {
        if([self isEqualToString:string]) {
            return YES;
        }
    }
    return NO;
}

- (NSString *) md5
{
    const char *cStr = [self UTF8String];
    unsigned char result[16];
    CC_MD5( cStr, (unsigned int)strlen(cStr), result ); // This is the md5 call
    return [NSString stringWithFormat:
            @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}


- (NSString *)sha256 {
    
    NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];
    uint8_t digest[CC_SHA256_DIGEST_LENGTH];
    CC_SHA256(data.bytes, (CC_LONG)data.length, digest);
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH * 2];
    
    for (int i = 0; i < CC_SHA256_DIGEST_LENGTH; i++) {
        [output appendFormat:@"%02x", digest[i]];
    }
    
    return output;
}


- (NSString*)telephoneWithReformat
{
    if ([self contains:@"-"])
    {
        return [self stringByReplacingOccurrencesOfString:@"-" withString:@""];
    }
    
    if ([self contains:@" "])
    {
        return [self stringByReplacingOccurrencesOfString:@" " withString:@""];
    }
    
    if ([self contains:@"("])
    {
        return [self stringByReplacingOccurrencesOfString:@"(" withString:@""];
    }
    
    if ([self contains:@")"])
    {
        return [self stringByReplacingOccurrencesOfString:@")" withString:@""];
    }
    
    return self;
}

- (NSString*) telephoneWithDashFormat {
    if (!self) {
        return self;
    }
    if ([self length]==11) {
        NSMutableArray *array = [NSMutableArray array];
        for (NSInteger i = 0, n = [self length]; i < n; ++i) {
            NSString *ch = [self substringWithRange:NSMakeRange(i, 1)];
            [array addObject:ch];
        }
        [array insertObject:@"-" atIndex:7];
        [array insertObject:@"-" atIndex:3];
        return [array join:@""];
    }
    return self;
}

- (NSString*) trim{
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
}

+(NSString*) safeStr:(NSString*)str{
    
    return str?str:@"";
}

+(BOOL) isNullOrEmpty:(NSString*)str{
    return  !str || str==nil || (NSString*)[NSNull null]==str || [str isEqualToString:@""];
}

+ (BOOL) equals:(NSString *)str1 to:(NSString *)str2{
    if([NSString isNullOrEmpty:str1]){
        return [NSString isNullOrEmpty:str2];
    }
    if([NSString isNullOrEmpty:str2]){
        return [NSString isNullOrEmpty:str1];
    }
    return [str1 isEqualToString:str2];
}

- (NSString *)maskPhoneNumberByHeadLength:(int)headLength tailLength:(int)tailLength {
    int totalDisplayCount = headLength + tailLength;
    if (self.length < totalDisplayCount) {
        return self;
    }
    
    NSMutableString *maskString = [NSMutableString stringWithFormat:@""];
    for (int i = 0 ; i < self.length - totalDisplayCount ; i++) {
        [maskString appendString:@"*"];
    }
    
    NSMutableAttributedString *attributStr = [[NSMutableAttributedString alloc] initWithString:self];
    [attributStr replaceCharactersInRange:NSMakeRange(3, [self length] - totalDisplayCount) withString:maskString];

    return [attributStr string];
}

- (NSArray*) rangesOfString:(NSString*) aString {
    NSMutableArray *retArray = [[NSMutableArray alloc] init];
    
    NSUInteger location = 0;
    while (location < self.length) {
        NSRange range = [self rangeOfString:aString options:0 range:NSMakeRange(location, self.length - location)];
        if (range.location == NSNotFound) return retArray;
        
        [retArray addObject:[NSValue valueWithRange:range]];
        location = range.location + 1;
    }
    
    return retArray;
}

- (NSMutableAttributedString *)subString:(NSString *)subString attrs:(NSDictionary *)attrs
{
    NSMutableAttributedString *atStr = [[NSMutableAttributedString alloc] initWithString:self];
    if (subString) {
        NSRange range = [self rangeOfString:subString];
        if (range.location != NSNotFound) {
            [atStr addAttributes:attrs range:range];
        }
    }
    return atStr;
}

@end
