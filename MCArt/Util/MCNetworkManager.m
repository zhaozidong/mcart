//
//  MCNetworkManager.m
//  MCArt
//
//  Created by Derek.zhao on 15/12/30.
//  Copyright © 2015年 zzd. All rights reserved.
//

#import "MCNetworkManager.h"
#import "IHDeviceUtilities.h"


static MCNetworkManager *instance;

@implementation MCNetworkManager{
    AFHTTPSessionManager *_manager;
}

+ (instancetype)sharedInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance=[self new];
    });
    return instance;
}

- (instancetype)init{
    self=[super init];
    if (self) {
        _manager=[[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:kAppHost]];
//        _manager.requestSerializer=[AFJSONRequestSerializer serializer];
        _manager.responseSerializer=[AFJSONResponseSerializer serializer];
        
//        [_manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//        [_manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//        
//        [_manager.requestSerializer setValue:@"appKey" forKey:@""];
//        [_manager.requestSerializer setValue:@"appTime" forKey:@""];
//        [_manager.requestSerializer setValue:@"appVersion" forKey:@""];
//        [_manager.requestSerializer setValue:@"appOS" forKey:@"iOS"];
//        [_manager.requestSerializer setValue:@"appIMEI" forKey:[IHDeviceUtilities sharedInstance].deviceUid];
//        [_manager.requestSerializer setValue:@"appMoedl" forKey:[IHDeviceUtilities sharedInstance].deviceType];
        
    }
    return self;
    
}

- (void)getWithURL:(NSString *)url parameter:(NSDictionary *)parameter success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    [_manager GET:url parameters:parameter progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (successBlock) {
            successBlock(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}

- (void)postWithURL:(NSString *)url parameter:(NSDictionary *)parameter success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    [_manager POST:url parameters:parameter progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (successBlock) {
            successBlock(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}

- (void)putWithURL:(NSString *)url parameter:(NSDictionary *)parameter success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    [_manager PUT:url parameters:parameter success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (successBlock) {
            successBlock(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}

- (void)deleteWithURL:(NSString *)url parameter:(NSDictionary *)parameter success:(successBlock)successBlock failure:(failureBlock)failureBlock{
    [_manager DELETE:url parameters:parameter success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (successBlock) {
            successBlock(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}


- (void)uploadWithURL:(NSString *)url
                 data:(id)data
       uploadProgress:(uploadBlock)uploadBlock
              success:(successBlock)successBlock
              failure:(failureBlock)failureBlock{
    [_manager POST:url parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        [formData appendPartWithFileData:data
                                    name:@"file"
                                fileName:@"picture.jpg"
                                mimeType:@"image/jpeg"];
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        if (uploadBlock) {
            uploadBlock(uploadProgress);
        }
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (successBlock) {
            successBlock(responseObject);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}


@end
