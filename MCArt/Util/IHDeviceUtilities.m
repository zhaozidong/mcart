//
//  IHDeviceUtilities.m
//  iHouse
//
//  Created by chris on 6/19/14.
//  Copyright (c) 2014 superjia. All rights reserved.
//

#import "IHDeviceUtilities.h"
#import <sys/utsname.h>
#import  <CoreTelephony/CTCarrier.h>
#import  <CoreTelephony/CTTelephonyNetworkInfo.h>
#import <AdSupport/ASIdentifierManager.h>
#import <UIKit/UIDevice.h>
#import "IPAddress.h"
#import "NSString+Utilities.h"


#define uuid @"uuid"
@implementation IHDeviceUtilities
static IHDeviceUtilities * _sharedInstance = nil;

+ (IHDeviceUtilities*) sharedInstance
{
    @synchronized([IHDeviceUtilities class])
	{
        if(!_sharedInstance)
            _sharedInstance = [[self alloc]init];
        return _sharedInstance;
    }
    return nil;
}

+ (id)alloc
{
    @synchronized([IHDeviceUtilities class])
	{
		NSAssert(_sharedInstance == nil, @"Attempted to allocate a second instance of a singleton.");
		_sharedInstance = [super alloc];
		return _sharedInstance;
	}
	return nil;
}
- (NSString*) getDeviceUid{
//    _deviceUid = [[NSUserDefaults standardUserDefaults]objectForKey:uuid];
    if(!_deviceUid){
        _deviceUid = [[[UIDevice currentDevice] identifierForVendor] UUIDString];
//        [[NSUserDefaults standardUserDefaults]setObject:_deviceUid forKey:uuid];
    }
    return _deviceUid;
}


- (NSString*) getDeviceType{
    if(!_deviceType){
        struct utsname systemInfo;
        uname(&systemInfo);
        NSString *code = [NSString stringWithCString:systemInfo.machine
                                            encoding:NSUTF8StringEncoding];
        
        if ([code rangeOfString:@"iPod"].location != NSNotFound){
            _deviceType = @"iPod touch";
        }
        else if ([code rangeOfString:@"iPad"].location != NSNotFound){
            _deviceType = @"iPad";
            if([code rangeOfString:@"iPad1,1"].location != NSNotFound ||
               [code rangeOfString:@"iPad2,1"].location != NSNotFound ||
               [code rangeOfString:@"iPad2,2"].location != NSNotFound ||
               [code rangeOfString:@"iPad2,3"].location != NSNotFound ||
               [code rangeOfString:@"iPad2,4"].location != NSNotFound){
                _deviceType = @"iPad1";
            }
            else if([code rangeOfString:@"iPad2,5"].location != NSNotFound ||
                    [code rangeOfString:@"iPad2,6"].location != NSNotFound ||
                    [code rangeOfString:@"iPad2,7"].location != NSNotFound){
                _deviceType = @"iPad Mini";
            }
            else if([code rangeOfString:@"iPad3,1"].location != NSNotFound ||
                    [code rangeOfString:@"iPad3,2"].location != NSNotFound ||
                    [code rangeOfString:@"iPad3,3"].location != NSNotFound){
                _deviceType = @"iPad3";
            }
            else if([code rangeOfString:@"iPad3,4"].location != NSNotFound ||
                    [code rangeOfString:@"iPad3,5"].location != NSNotFound ||
                    [code rangeOfString:@"iPad3,6"].location != NSNotFound){
                _deviceType = @"iPad4";
            }
            else if([code rangeOfString:@"iPad4,1"].location != NSNotFound ||
                    [code rangeOfString:@"iPad4,2"].location != NSNotFound){
                _deviceType = @"iPad Air";
            }
            else if([code rangeOfString:@"iPad4,4"].location != NSNotFound ||
                    [code rangeOfString:@"iPad4,5"].location != NSNotFound){
                _deviceType = @"iPad Mini Retia";
            }
        }
        else if ([code rangeOfString:@"iPhone"].location != NSNotFound){
            _deviceType = @"iPhone";
            if([code rangeOfString:@"iPhone3,1"].location != NSNotFound ||
               [code rangeOfString:@"iPhone3,3"].location != NSNotFound){
                _deviceType = @"iPhone4";
            }
            else if([code rangeOfString:@"iPhone4,1"].location != NSNotFound){
                _deviceType = @"iPhone4s";
            }
            else if([code rangeOfString:@"iPhone5,1"].location != NSNotFound ||
                    [code rangeOfString:@"iPhone5,2"].location != NSNotFound){
                _deviceType = @"iPhone5";
            }
            else if([code rangeOfString:@"iPhone5,3"].location != NSNotFound ||
                    [code rangeOfString:@"iPhone5,4"].location != NSNotFound){
                _deviceType = @"iPhone5C";
            }
            else if([code rangeOfString:@"iPhone6,1"].location != NSNotFound ||
                    [code rangeOfString:@"iPhone6,2"].location != NSNotFound){
                _deviceType = @"iPhone5S";
            }
            else if([code rangeOfString:@"iPhone7,2"].location != NSNotFound){
                _deviceType = @"iPhone6";
            }
            else if([code rangeOfString:@"iPhone7,1"].location != NSNotFound){
                _deviceType = @"iPhone6 Plus";
            }
        }
        else if([code rangeOfString:@"i386"].location != NSNotFound){
            return @"Simulator i386";
        }
        else if([code rangeOfString:@"x86_64"].location != NSNotFound){
            return @"Simulator x86_64";
        }
    }
    return _deviceType;
}

- (NSString*) getIMEI{
    if(!_imei){
//        extern CFStringRef kLockdownIMEIKey;
//        void* connection = lockdown_connect();
//        _imei = [(NSString*)lockdown_copy_value(connection, NULL, kLockdownIMEIKey) autorelease];
//        lockdown_disconnect(connection);
        _imei = @"";
    }
    return _imei;
}

- (NSString*) getIdfa {
    if (!_idfa) {
        _idfa = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
    }
    return _idfa;
}

- (NSString*) getMacAddress{
    if (!_mac) {
        _mac = [self deviceIPAdress];
    }
    return _mac;
}

- (NSString *)deviceIPAdress {
    InitAddresses();
    GetIPAddresses();
    GetHWAddresses();
    
    int i;
    NSString *deviceIP;
    for (i=0; i<MAXADDRS; ++i)
    {
        static unsigned long localHost = 0x7F000001;		// 127.0.0.1
        unsigned long theAddr;
        
        theAddr = ip_addrs[i];
        
        if (theAddr == 0) {
            if (deviceIP) {
                break;
            } else {
                continue;
            }
        }
        if (theAddr == localHost) continue;
        
//        NSLog(@"%s %s %s\n", if_names[i], hw_addrs[i], ip_names[i]);
        deviceIP = [NSString stringWithFormat:@"%s", ip_names[i]];//warning:此处频繁调用可能出现错误，慎用
    }
    
    return deviceIP;
    
    //this will get you the right IP from your device in format like 198.111.222.444. If you use the for loop above you will se that ip_names array will also contain localhost IP 127.0.0.1 that's why I don't use it. Eventualy this was code from mac that's why it uses arrays for ip_names as macs can have multiple IPs
    //return [NSString stringWithFormat:@"%s", ip_names[1]];
}

- (NSString*) getSystemVersion{
    if(!_systemVersion){
        _systemVersion = [[ UIDevice currentDevice ] systemVersion];
    }
    return _systemVersion;
}

- (NSInteger) getSystemMajorVersion{
    return [[self getSystemVersion] doubleValue];
}



-(NSString*)getCellularProviderName
{
    if (!_cellularProviderName) {
        CTTelephonyNetworkInfo *netInfo = [[CTTelephonyNetworkInfo alloc]init];
        
        CTCarrier*carrier = [netInfo subscriberCellularProvider];
        //NSLog(@"carrier:%@",carrier);
        //NSString * imsi=@"";
        if (carrier!=NULL) {
//            NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
//            [dic setObject:[carrier carrierName] forKey:@"Carriername"];
//            [dic setObject:[carrier mobileCountryCode] forKey:@"MobileCountryCode"];
//            [dic setObject:[carrier mobileNetworkCode]forKey:@"MobileNetworkCode"];
//            [dic setObject:[carrier isoCountryCode] forKey:@"ISOCountryCode"];
//            [dic setObject:[carrier allowsVOIP]?@"YES":@"NO" forKey:@"AllowsVOIP"];
            //imsi=[JSONDecoder NSDictionaryJSONString:dic];
            _cellularProviderName = [carrier carrierName];
        }
    }
    return [NSString isNullOrEmpty:_cellularProviderName]? @"" : _cellularProviderName;
//    return _cellularProviderName;//cellularProviderName;
    
}

@end
