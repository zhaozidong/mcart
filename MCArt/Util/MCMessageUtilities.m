//
//  MCMessageUtilities.m
//  MCArt
//
//  Created by Derek.zhao on 16/1/27.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCMessageUtilities.h"
#import <SVProgressHUD.h>
#import "PishumToast.h"

@implementation MCMessageUtilities
+ (void)load{
    [SVProgressHUD setBackgroundColor:kRGBA(0, 0, 0, 0.54)];
    [SVProgressHUD setForegroundColor:kWhiteColor];
}

+ (void)showSuccessMessage:(NSString *)message
{
    [SVProgressHUD setOffsetFromCenter:UIOffsetMake(0, -44)];
    [SVProgressHUD showSuccessWithStatus:message];
}

+ (void)showErrorMessage:(NSString *)message
{
    [SVProgressHUD setOffsetFromCenter:UIOffsetMake(0, -44)];
    [SVProgressHUD showErrorWithStatus:message];
}

+ (void)showProgressMessage:(NSString *) message
{
    [SVProgressHUD setOffsetFromCenter:UIOffsetMake(0, -44)];
    [SVProgressHUD showWithStatus:message maskType:SVProgressHUDMaskTypeClear];
}

+ (void)dismissHUD
{
    [SVProgressHUD dismiss];
}

+ (void)showTotast:(NSString *)message{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
//    NSInteger length = message.length;
//    if (length <= 17) {
//        
//    }else if (length > 17) {
//        
//    }
    
    [PishumToast showToastWithMessage:message Length:TOAST_MIDDLE ParentView:window];

}

@end
