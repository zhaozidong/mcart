//
//  MCArrowButton.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/6.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCArrowButton.h"

@implementation MCArrowButton{
    UIImageView *_arrow;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (instancetype)init{
    self = [super init];
    if (self) {
        [self setUpSubeViews];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUpSubeViews];
    }
    return self;
}


- (void)setStatus:(MCArrowButtonStatus)status{
    if (_status == status) {
        return;
    }
    _status = status;
    
    if (status == MCArrowButtonStatusUp) {
        [UIView animateWithDuration:0.2 animations:^{
            _arrow.transform= CGAffineTransformMakeRotation(M_PI);
        }];
    }else{
        [UIView animateWithDuration:0.2 animations:^{
            _arrow.transform = CGAffineTransformIdentity;
        }];
    }
}

- (MCArrowButtonStatus)changeToOtherStatus{
    if (self.status == MCArrowButtonStatusUp) {
        self.status = MCArrowButtonStatusDown;
    }else{
        self.status = MCArrowButtonStatusUp;
    }
    return self.status;
}

- (void)setUpSubeViews{
    if (!_arrow) {
        _arrow = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 13, 13)];
        _arrow.image = [UIImage imageNamed:@"arrowdown"];
    }
    [self addSubview:_arrow];
    
    _status = MCArrowButtonStatusDown;
}

- (void)layoutSubviews{
    [super layoutSubviews];
    _arrow.frame = CGRectMake(CGRectGetMaxX(self.titleLabel.frame)+2, CGRectGetMinY(self.titleLabel.frame)+2, 13, 13);
    
}


@end
