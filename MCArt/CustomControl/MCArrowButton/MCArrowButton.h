//
//  MCArrowButton.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/6.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, MCArrowButtonStatus) {
    MCArrowButtonStatusUp = 1,
    MCArrowButtonStatusDown
};



@interface MCArrowButton : UIButton

@property (nonatomic, assign) MCArrowButtonStatus status;

- (MCArrowButtonStatus)changeToOtherStatus;

@end
