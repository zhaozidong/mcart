//
//  MCStyleButton.m
//  MCArt
//
//  Created by Derek.zhao on 16/2/3.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCStyleButton.h"

@implementation MCStyleButton


- (instancetype)init{
    self = [super init];
    if (self) {
        [self setSelfStyle];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setSelfStyle];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setSelfStyle];
    }
    return self;
}

- (void)setSelfStyle{
    self.layer.cornerRadius = 4.f;
    [self setBackgroundColor:[UIColor redColor]];
    [self.titleLabel setFont:kMCFont(18.f)];
}




@end
