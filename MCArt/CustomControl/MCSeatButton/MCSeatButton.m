//
//  MCSeatButton.m
//  MCArt
//
//  Created by Derek.zhao on 16/3/7.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import "MCSeatButton.h"

@implementation MCSeatButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (instancetype)init{
    self = [super init];
    if (self) {
        [self setUp];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self setUp];
    }
    return self;
}

- (void)setUp{
    [self setImage:[UIImage imageNamed:@"chair_big_gray"] forState:UIControlStateNormal];
    [self setImage:[UIImage imageNamed:@"chair_big_red"] forState:UIControlStateDisabled];
    [self setImage:[UIImage imageNamed:@"chair_big_blue"] forState:UIControlStateSelected];
}

- (void)setStatus:(MCSeatStatus)status{
    _status = status;
    self.hidden = NO;
    switch (status) {
        case MCSeatStatusNormal: {
            self.enabled = YES;
            self.selected = NO;
            break;
        }
        case MCSeatStatusHidden: {
            self.hidden = YES;
            break;
        }
        case MCSeatStatusLocked: {
            self.enabled = NO;
            break;
        }
        case MCSeatStatusSelected: {
            self.selected = YES;
            break;
        }
    }
}











@end
