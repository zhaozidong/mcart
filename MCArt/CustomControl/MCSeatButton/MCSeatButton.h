//
//  MCSeatButton.h
//  MCArt
//
//  Created by Derek.zhao on 16/3/7.
//  Copyright © 2016年 zzd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MCTicketAreaModel.h"
//typedef NS_ENUM(NSInteger,MCSeatStatus) {
//    MCSeatStatusNormal,
//    MCSeatStatusLocked,
//    MCSeatStatusSelected
//};

@interface MCSeatButton : UIButton

@property (nonatomic, assign) MCSeatStatus status;

@property (nonatomic, copy) MCTicketSeatDetailModel *seat;


@end
